@extends("base_dashboard")

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>        
    <li><a class="spf-link" href="{{ url('dashboard_it_officer') }}">Dashboard</a></li>
    <li><span>Pembayaran</span></li>
  </ul>
@endsection

@section('content')
  <ul class="uk-tab">
    <!-- <li class="uk-active">
      <a href="?filter=all" class="spf-link">
        <span uk-icon="icon:list;ratio:.7"></span> Semua
      </a>
    </li> -->
    <!-- <li class="uk-active">
      <a href="?filter=refund" class="spf-link">
        <span uk-icon="icon:refresh;ratio:.7"></span> Perlu Dikembalikan
      </a>
    </li> -->

<!--     
      <div class="uk-position-right uk-visible@s">
        <button class="uk-button uk-button-warning" uk-toggle="target: #modal-import-refund-bill">
          <span uk-icon="upload"></span> Import
        </button>
        <button class="uk-button uk-button-primary" uk-toggle="target: #modal-export-refund-bill">
          <span uk-icon="download"></span> Export
        </button>
      </div>
      <div class="uk-margin uk-hidden@s">
        <button class="uk-button uk-button-warning" uk-toggle="target: #modal-import-refund-bill">
          <span uk-icon="upload"></span> Import
        </button>
        <button class="uk-button uk-button-primary" uk-toggle="target: #modal-export-refund-bill">
          <span uk-icon="download"></span> Export
        </button>
      </div> -->

      <!-- <div id="modal-import-refund-bill" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
          <form class="uk-form" method="post" action="." enctype="multipart/form-data">
          @csrf
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h3 class="uk-margin-remove">Import Pengembalian Dana</h3>
            <ul class="uk-list uk-alert uk-alert-warning">
              <li><span uk-icon="check"></span> Pastikan pihak PlazaDana sudah mengembalikan sisa dana ini ke rekening investor.</li>
              <li><span uk-icon="check"></span> Mohon gunakan format (.csv) table persis seperti contoh berikut:</li>
            </ul>
            <div class="uk-overflow-auto">
              <table class="uk-table uk-table-striped">
                <tbody>
                  <tr>
                    <td>id</td>
                    <td>user</td>
                    <td>refund</td>
                    <td>virtual_account_number</td>
                    <td>status</td>
                  </tr>
                  <tr>
                    <td>7</td>
                    <td>mamunzuberi</td>
                    <td>0</td>
                    <td>0101000002</td>
                    <td>top_up</td>
                  </tr>
                  <tr>
                    <td>14</td>
                    <td>unggal_bagus</td>
                    <td>15000</td>
                    <td>0101000001</td>
                    <td>investation</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <ul class="uk-list uk-list-bullet">
              <li><code>id</code> merupakan id dari pembayaran</li>
              <li><code>user</code> merupakan username dari pembayar</li>
              <li><code>refund</code> merupakan <b>sisa dana setelah dikembalikan</b></li>
              <li><code>virtual_account_number</code> merupakan nomor V.A pasif milik investor</li>
              <li><code>status</code> merupakan status pembayaran, 'investation' atau 'top_up'</li>
            </ul>
            <div class="uk-margin uk-text-right">
              <div uk-form-custom="target: true">
                <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
                <input type="file" accept=".csv" name="file_csv" required>
                <input class="uk-input uk-form-width-medium" type="text" placeholder="{% trans 'Pilih file .csv' %}">
              </div>
            </div>
            <div class="uk-text-right">
              <div class="uk-button uk-button-default uk-modal-close">
                <span uk-icon="close"></span> Batal
              </div>
              <button type="submit" name="import-mode" class="uk-button uk-button-primary">
                <span uk-icon="check"></span> Import
              </button>
            </div>
          </form>
        </div>
      </div> -->

      <!-- <div id="modal-export-refund-bill" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
          <form class="uk-form" method="post" action=".">
            @csrf
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h3 class="uk-margin-remove">Export Pengembalian Dana</h3>
            <div class="uk-alert uk-alert-warning">
              <span uk-icon="info"></span> Konten table (.csv) yang akan di export persis seperti contoh berikut:
            </div>
            <div class="uk-overflow-auto">
              <table class="uk-table uk-table-striped">
                <tbody>
                  <tr>
                    <td>id</td>
                    <td>user</td>
                    <td>refund</td>
                    <td>virtual_account_number</td>
                    <td>status</td>
                  </tr>
                  <tr>
                    <td>7</td>
                    <td>mamunzuberi</td>
                    <td>3000</td>
                    <td>0101000002</td>
                    <td>top_up</td>
                  </tr>
                  <tr>
                    <td>14</td>
                    <td>unggal_bagus</td>
                    <td>15000</td>
                    <td>0101000001</td>
                    <td>investation</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <ul class="uk-list uk-list-bullet">
              <li><code>id</code> merupakan id dari pembayaran</li>
              <li><code>user</code> merupakan username dari pembayar</li>
              <li><code>refund</code> merupakan <b>sisa dana yang harus dikembalikan</b></li>
              <li><code>virtual_account_number</code> merupakan nomor V.A pasif milik investor</li>
              <li><code>status</code> merupakan status pembayaran, 'investation' atau 'top_up'</li>
            </ul>
            <div class="uk-text-right">
              <div class="uk-button uk-button-default uk-modal-close">
                <span uk-icon="close"></span> Batal
              </div>
              <button type="submit" name="export-mode" class="uk-button uk-button-primary">
                <span uk-icon="check"></span> Export
              </button>
            </div>
          </form>
        </div>
      </div> -->
  </ul>

  <div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-striped uk-table-hover uk-table-responsive">
      <thead>
        <tr>
          <!-- <th>P.ID</th> -->
          <!-- <th>V.A</th> -->
          <th>User</th>
          <th>Proposal</th>
          <th>Teaser</th>
          <th>Simpanan Pokok</th>
          <th>Simpanan Wajib</th>
          <th>Simpanan Invest</th>
          <th>Simpanan SUKS</th>
          <th>Biaya Admin</th>
          <th>Bill</th>
          <th>Keterangan</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @if ($bills)
        @foreach($bills as $bill)
          <tr>
            <td> {{ $bill->id }}</td>
            <td> {{ $bill->user->name }}</td>
            <td> {{ $bill->teaser->title }}</td>
            <td> {{ number_format($bill->invest->simpanan_pokok) }}</td>
            <td> {{ number_format($bill->invest->simpanan_wajib) }}</td>
            <td> {{ number_format($bill->invest->invest) }}</td>
            <td> {{ number_format($bill->invest->SUKS) }}</td> 
            <td> {{ '2.000' }}</td> 
            <td> {{ number_format($bill->BILL) }}</td>
            <td> {{ $bill->DESCRIPTION }}</td>
            <td> {{$bill->STATUS }}</td>
          </tr>
        @endforeach
        @else
          <tr>
            <td colspan="6">
              <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Pembayaran tidak ada!
                </div>
              </div>
            </td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
  {{$bills->links('vendor.pagination.ui-kit')}}

  @endsection
  
  
@section('js')
  <script>
    // $(document).ready(function() {
    //   // refund mode
    //   $(document).on('click', '.refund-action', function(){
    //     var bill_id = $(this).data('bill-id');
    //     var confirmed = confirm('Pastikan pihak PlazaDana sudah mengembalikan sisa dana ini ke rekening investor.');
    //     if(confirmed) {
    //       $.ajax({
    //         url: '',
    //         type: 'POST',
    //         data: {
    //           'refund': true,
    //           'bill-id': bill_id,
    //           'csrfmiddlewaretoken': 'csrf'
    //         },
    //         success: function(response) {
    //           location.reload();
    //         }
    //       });
    //     }
    //   });
    // });
  </script>
@endsection
