
@extends("base_dashboard")

@section('breadcrumb')
  <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">
      <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><span>Dashboard IT<span></li>
      </ul>
    </div>
  </nav>
@endsection

@section('content')
<div class="uk-child-width-1-4@m uk-grid-small uk-grid-match uk-grid" uk-grid="">
    <div class="uk-first-column">
      <div class="uk-card uk-card-hover uk-card-default uk-card-body">
        <h4 class="uk-margin-remove-bottom uk-text-bold">{{ $teasers->count() }}</h4>
        <p class="uk-margin-remove">Proposal disetujui</p>
        <div class="uk-position-top-right uk-padding-small uk-text-primary">
          <span uk-icon="icon:copy;ratio:1" class="uk-margin-small-left uk-icon"></span>
        </div>
      </div>
    </div>
    <div>
      <div class="uk-card uk-card-hover uk-card-default uk-card-body">
        <h4 class="uk-margin-remove-bottom uk-text-bold">{{ $invests->count() }}</h4>
        <p class="uk-margin-remove">Investasi disetujui</p>
        <div class="uk-position-top-right uk-padding-small uk-text-primary">
          <span uk-icon="icon:paint-bucket;ratio:1" class="uk-margin-small-left uk-icon"></span>
        </div>
      </div>
    </div>
    <div>
      <div class="uk-card uk-card-hover uk-card-primary uk-card-body">
        {{-- <h5 class="uk-margin-remove-bottom uk-text-bold"> total_proposals_money|as_rupiah ,-</h5> --}}
        <h5 class="uk-margin-remove-bottom uk-text-bold">----,-</h5>
        <p class="uk-margin-remove">Penggalangan Dana</p>
        <div class="uk-position-top-right uk-padding-small">
          <span uk-icon class="uk-margin-small-left uk-icon uk-text-bold">$</span>
        </div>
      </div>
    </div>
    <div>
      <div class="uk-card uk-card-hover uk-card-secondary uk-card-body">
        {{-- <h5 class="uk-margin-remove-bottom uk-text-bold"> total_investations_money|as_rupiah,-</h5> --}}
        <h5 class="uk-margin-remove-bottom uk-text-bold">------,-</h5>
        <p class="uk-margin-remove">Total Investasi</p>
        <div class="uk-position-top-right uk-padding-small">
          <span uk-icon class="uk-margin-small-left uk-icon uk-text-bold">$</span>
        </div>
      </div>
    </div>
  </div>
  <hr class="uk-divider-icon">
  <div class="chart-main">
    <canvas id="chart-this-year" height="100"></canvas>
  </div>
@endsection

@section('js')
    
<script src="{{asset('js/chart.min.js')}}"></script>

<script>
    window.chartColors = {
      red: 'rgb(255, 99, 132)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };
    var timeFormat = 'MM/DD/YYYY HH:mm';
    var color = Chart.helpers.color;
    var toFloat = function(numb) {
      return numb.replace(',', '.');
    }

    // global chart this year
    var globalConfig = {
      type: 'bar',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Nov', 'Des'],
        datasets: [{
          type: 'bar',
          label: 'Proposal disetujui',
          backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
          borderColor: window.chartColors.green,
          data: [
            @foreach ($filtered_by_month['teasers'] as $month => $value)
              toFloat('{{ $value }}'),
            @endforeach

          ],
        }, {
          type: 'line',
          label: 'Investasi disetujui',
          backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
          borderColor: window.chartColors.blue,
          fill: false,
          data: [
            @foreach ($filtered_by_month['invests'] as $month => $value)
              toFloat('{{ $value }}'),
            @endforeach
          ],
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Rekaman bulanan dalamam tahun {{date("Y")}}'
        },
        legend: {
          position: 'right',
        },
      }
    };

    window.onload = function() {
      var ctx1 = document.getElementById('chart-this-year').getContext('2d');
      window.myLine = new Chart(ctx1, globalConfig);
    };
  </script>
@endsection
