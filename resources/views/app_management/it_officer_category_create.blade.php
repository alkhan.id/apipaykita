@extends("base_dashboard")

@section('breadcrumb')
  <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">
      <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><a class="spf-link" href="">Dashboard</a></li>
        <li><span>Kategori Penggalangan Dana</span></li>
      </ul>
    </div>
  </nav>
@endsection

@section('content')
  <form class="form-category" method="post" action="{{url('category')}}">
  <form id="teaser-form" class="uk-form-horizontal" enctype="multipart/form-data" method="post" action="{{url('category')}}">
  @csrf
    <div class="uk-margin uk-overflow-auto-disabled">

    <fieldset class="uk-margin-bottom uk-fieldset">
      <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
        <div class="uk-margin">
          <label class="uk-flex uk-flex-left uk-form-label" for="form-name">
            Title <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <input type="text" name="title" id="title" placeholder="Judul" class="uk-width-1-1 uk-input" required />
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-left uk-form-label" for="form-address">
            Deskripsi <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <textarea name="description" id="id_description" placeholder="Deskripsi" class="uk-width-1-1 uk-textarea" required></textarea>
          </div>
        </div>
        <input type="submit" class="uk-button uk-button-warning uk-cookies-button">
      </div>  
    </fieldset>
    </div>
  </form>

@endsection
