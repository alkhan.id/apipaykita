@extends('app_plaza/plaza_report_disqus')

@section('breadcrumb')
    
<ul class="uk-breadcrumb">
  <li><a class="spf-link" href="/">{% trans "Beranda" %}</a></li>
  <li><a class="spf-link" href="{% url 'user_dashboard' %}">{% trans "Dashboard" %}</a></li>
  <li><a class="spf-link" href="{% url 'dashboard_report_list' %}">{% trans "Laporan Keuangan" %}</a></li>
  <li><span>{% blocktrans with teaser=report.teaser.title time=report.created|date:"M-Y" %}Laporan Keuangan "{{ teaser }}" pada {{ time }}{% endblocktrans %}</span></li>
</ul>

@endsection
