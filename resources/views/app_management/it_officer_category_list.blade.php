@extends("base_dashboard")

@section('breadcrumb')
  <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">
      <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
        <li><span>Kategori Penggalangan Dana</span></li>
      </ul>
    </div>
    
          <div class="uk-navbar-right">
            <form class="uk-nav-nav" action="." method="get">
              <input name="q" class="uk-input uk-form-width-medium" type="text" placeholder="" required>
              <button type="submit" class="uk-button uk-button-primary">
                <span uk-icon="icon: search"></span>
              </button>
            </form>
          </div>
  </nav>
@endsection

@section('content')
<a href="{{ route('category.create')}}">
    <button class="uk-button uk-button-primary uk-cookies-button">Tambah</button>
  </a>
  <form class="form-category" method="post" action=".">
    
    
    <div class="uk-margin uk-overflow-auto-disabled">
      <table class="uk-table uk-table-small uk-table-striped uk-table-hover uk-table-responsive">
        <thead>
          <tr>
            <th>Judul</th>
            <th>Deskripsi</th>
            <th colspan="2">Tindakan</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categories as $category)
          <tr>
            <td>{{ $category->title }}</td>
            <td><span>{{ $category->description }}</span></td>
            <td>
              <a uk-icon="file-edit" href="{{url('/category/'.$category->id.'/edit')}}">
                {{-- <button class="uk-button uk-button-warning uk-cookies-button" type="submit">Edit</button> --}}
              </a>     
            </td>
            <td>
              <form action="{{ route('category.destroy', $category->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button uk-icon="icon: trash"></button>
                  {{-- <button class="uk-button uk-button-danger uk-cookies-button" type="submit">Delete</button> --}}
                </form>
              </td>
              
            </tr>
            @endforeach
            
            @if(count($categories) == 0)  
            <tr>
              <td colspan="5">
                <div class="uk-card uk-card-default uk-margin">
                  <div class="uk-card-body">
                    <span uk-icon="info"></span> &nbsp; Kategori tidak ada!
                  </div>
                </div>
              </td>
            </tr>
            @endif
          </tbody>
        </table>
        {{$categories->links('vendor.pagination.ui-kit')}}


  </div>


@endsection
