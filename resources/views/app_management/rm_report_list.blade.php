@extends('base_dashboard')
@section('breadcrumb')
    <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
        <div class="uk-navbar-left">
            <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
                <li><a class="spf-link" href="/">Beranda</a></li>
                <li><a class="spf-link" href="/dashboard_relation_manager">Dashboard</a></li>
                <li><span>Laporan Keuangan</span></li>
            </ul>
        </div>
        <div class="uk-navbar-right">
            <form class="uk-nav-nav" action="." method="get">
                <input name="q" class="uk-input uk-form-width-medium" type="text"
                       placeholder="Cari" required>
                <button type="submit" class="uk-button uk-button-primary">
                    <span uk-icon="icon: search"></span>
                </button>
            </form>
        </div>
    </nav>
@endsection

@section('content')

    <ul class="uk-tab uk-visible@s">
        <li @if (request()->get('status') != 'pending' && request()->get('status') != 'approved' && request()->get('status') != 'rejected') class="uk-active" @endif>
            <a href="?status=all" class="spf-link">
                <span uk-icon="icon:list;ratio:.7"></span> Semua
            </a>
        </li>
        <li @if (request()->get('status') == 'pending') class="uk-active" @endif>
            <a href="?status=pending" class="spf-link">
                <span uk-icon="icon:clock;ratio:.7"></span> Menunggu
            </a>
        </li>
        <li @if (request()->get('status') == 'approved') class="uk-active" @endif>
            <a href="?status=approved" class="spf-link">
                <span uk-icon="icon:check;ratio:.7"></span> Diterima
            </a>
        </li>
        <li @if (request()->get('status') == 'rejected') class="uk-active" @endif>
            <a href="?status=rejected" class="spf-link">
                <span uk-icon="icon:close;ratio:.7"></span> Ditolak
            </a>
        </li>
    </ul>
    <div class="uk-margin uk-hidden@s">
        <select name="status" class="uk-width-1-1 uk-select" onchange="window.location.href=this.value">
            <option value="?status=all">Semua</option>
            <option value="?status=pending" {% if request.GET.status==
            'pending' %}selected{% endif %}>Menunggu</option>
            <option value="?status=approved" {% if request.GET.status==
            'approved' %}selected{% endif %}>Diterima</option>
            <option value="?status=rejected" {% if request.GET.status==
            'rejected' %}selected{% endif %}>Ditolak</option>
        </select>
    </div>

    <div class="uk-margin uk-overflow-auto-disabled">
        <table class="uk-table uk-table-justify uk-table-divider uk-table-responsive">
            <thead>
            <tr>
                <th>Nomor</th>
                <th>Proyek</th>
                <th>Koperasi</th>
                <th>Dibuat</th>
                <th>Status</th>
                <th>Tindakan</th>
            </tr>
            </thead>
            <tbody>
              
              @if ($reports)

                @foreach ($reports as $report)
                  <tr>
                      <td>{{ $report->teaser->number }}</td>
                      <td><a class="spf-link" title="{{ $report->teaser->title }}"
                              href="{{url('teaser_detail/'.$report->teaser->id)}}">{{ $report->teaser->title}}</a></td>
                      <td>{{ $report->teaser->cooperative_name }}</td>
                      <td>{{ $report->created }}</td>
                      <td>
                          @if ($report->status == 'approved')
                              <span class="uk-badge uk-badge-success">
                          @elseif( $report->status == 'rejected' )
                              <span class="uk-badge uk-badge-danger">
                          @else
                              <span class="uk-badge uk-badge-warning">
                          @endif
                            {{ $report->get_status_display }}</span>
                      </td>
                      <td>
                          {{-- <a href="/app_plaza:report_detail/slug"
                              uk-tooltip="title:Laporan Keuangan; pos: bottom"
                              class="uk-icon-link uk-margin-small-right spf-link" uk-icon="settings"
                              style="transform: rotate(90deg);"></a> --}}
                          {{-- <a href="/dashboard_report_disqus/report-pk"
                              uk-tooltip="title: Diskusikan; pos: bottom"
                              class="uk-icon-link uk-margin-small-right spf-link" uk-icon="comments"></a> --}}
                          <a href="{{url('report/'.$report->teaser->id)}}"
                              uk-tooltip="title: Ubah; pos: bottom"
                              class="uk-icon-link spf-link" uk-icon="file-edit"></a>
                      </td>
                  </tr>
                @endforeach
              @else
                <tr>
                    <td colspan="6">
                        <div class="uk-card uk-card-default uk-margin">
                            <div class="uk-card-body">
                                <span uk-icon="info"></span> &nbsp; {% trans "Laporan tidak ada!" %}
                            </div>
                        </div>
                    </td>
                </tr>
              @endif
            </tbody>
        </table>
    </div>
@endsection

@section('js')
    <script>$('li.list-report-me').addClass('uk-active')</script>
@endsection

        