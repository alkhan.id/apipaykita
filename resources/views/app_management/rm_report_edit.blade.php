
@extends('app_plaza/plaza_report_submit')
@section('breadcrumb')
<ul class="uk-breadcrumb">
  <li><a class="spf-link" href="/">Beranda</a></li>
  <li><a class="spf-link" href="/user_dashboard">Dashboard</a></li>
  <li><a class="spf-link" href="/dashboard_report_list">Laporan Keuangan</a></li>
  <li><span>Laporan Keuangan "{{ $teaser->title }}" pada {{ $time }}</span></li>
</ul>
@overwrite
