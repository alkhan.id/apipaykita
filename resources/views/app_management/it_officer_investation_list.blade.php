@extends('base_dashboard')
@section('breadcrumb')
    <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
        <div class="uk-navbar-left">
            <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
                <li><a class="spf-link" href="/">Beranda</a></li>
                <li><a class="spf-link" href="#">Dashboard</a></li>
                <li><span>Investasi</span></li>
            </ul>
        </div>
        <div class="uk-navbar-right">
            <form class="uk-nav-nav" action="." method="get">
                <input name="q" value="---" class="uk-input uk-form-width-medium" type="text" placeholder="Cari ..."
                       required>
                {{-- <input name="status" type="hidden" value="status"> --}}
                <button type="submit" class="uk-button uk-button-primary">
                    <span uk-icon="icon: search"></span>
                </button>
            </form>
        </div>
    </nav>
@endsection

@section('content')

    <ul class="uk-tab uk-visible@s">
        <li @if (request()->get('status') != 'pending' && request()->get('status') != 'approved' && request()->get('status') != 'rejected' && request()->get('status') != 'finished')
            class="uk-active" @endif>
            <a href="?status=all" class="spf-link">
                <span uk-icon="icon:list;ratio:.7"></span> Semua
            </a>
        </li>
        <li @if (request()->get('status') == 'pending') class="uk-active" @endif>
            <a href="?status=pending" class="spf-link">
                <span uk-icon="icon:clock;ratio:.7"></span> Menunggu
            </a>
        </li>
        <li @if (request()->get('status') == 'approved') class="uk-active" @endif>
            <a href="?status=approved" class="spf-link">
                <span uk-icon="icon:check;ratio:.7"></span> Diterima
            </a>
        </li>
        <li @if (request()->get('status') == 'rejected') class="uk-active" @endif>
            <a href="?status=rejected" class="spf-link">
                <span uk-icon="icon:close;ratio:.7"></span> Ditolak
            </a>
        </li>
    </ul>

    <div class="uk-margin uk-hidden">
        <select name="status" class="uk-width-1-1 uk-select" onchange="window.location.href=this.value">
            <option value="?status=all">Semua</option>
            <option value="?status=pending" {% if request.GET.status=='pending' %}selected{% endif %}>Menunggu
            </option>
            <option value="?status=approved" {% if request.GET.status=='approved' %}selected{% endif %}>Diterima
            </option>
            <option value="?status=rejected" {% if request.GET.status=='rejected' %}selected{% endif %}>Ditolak
            </option>
        </select>
    </div>

    <div class="uk-margin uk-overflow-auto-disabled">
        <table class="uk-table uk-table-striped uk-table-hover uk-table-responsive">
            <thead>
            <tr>
                <th>I.ID</th>
                <th>Investor</th>
                <th>Peluang Investasi</th>
                <th>Jumlah Investasi</th>
                <th>SUKS</th>
                <th>SUKUK</th>
                <th>Tagihan</th>
                <th>Tanggal Investasi</th>
                <th>Status</th>
                {{-- <th>Tindakan</th> --}}
            </tr>
            </thead>
            <tbody>
            @foreach($investations as $investation)

                <tr>
                    <td>{{ $investation->id }}</td>
                    <td>{{ $investation->user->name}}</td>
                    <td><a class="spf-link" title="{{ $investation->teaser->title ?? ''}}"
                           href="">{{ $investation->teaser->title ?? ''}}</a></td>
                    <td>{{ number_format($investation->invest) }},-</td>
                    <td>{{ number_format($investation->SUKS) }},-</td>
                    <td>{{ number_format($investation->invest) }},-</td>
                    <td>{{ number_format($investation->invest + $investation->simpanan_pokok + $investation->simpanan_wajib + $investation->SUKS + $investation->SUKUK + 2000) }}
                        ,-
                    </td>
                    <td>{{ $investation->created_at }}</td>
                    <td>
                        @if ($investation->status == 0)
                            <span uk-tooltip="title: Menunggu Pembayaran; pos: bottom"
                                  class="uk-icon-button uk-background-warning uk-light" uk-icon="clock"></span>
                        @elseif ($investation->status == 1)
                            <span uk-tooltip="title: Telah Lunas; pos: bottom"
                                  class="uk-icon-button uk-background-success uk-light" uk-icon="check"></span>
                        @else
                            <span uk-tooltip="title: Ditolak; pos: bottom"
                                  class="uk-icon-button uk-background-danger uk-light" uk-icon="close"></span>
                        @endif
                    </td>
                    {{-- <td>
                        <a href=""
                        uk-tooltip="title: Diskusikan Proposal; pos: bottom"
                        class="uk-icon-link uk-margin-small-right spf-link" uk-icon="comments"></a>
                        <a uk-toggle="target: #modal-change-status-investation-{{ $investation->pk }}"
                           uk-tooltip="title: Ubah Status; pos: bottom"
                           class="uk-icon-link spf-link" uk-icon="file-edit"></a>
                    </td> --}}
                </tr>

{{--                <div id="modal-change-status-investation-{{ $investation->pk }}" uk-modal>--}}
{{--                    <div class="uk-modal-dialog uk-modal-body">--}}
{{--                        <form class="uk-form" method="POST" action="{{url('investation/'.$investation->id)}}">--}}
{{--                            @csrf--}}
{{--                            @method('PATCH')--}}
{{--                            <button class="uk-modal-close-default" type="button" uk-close></button>--}}
{{--                            <h3 class="uk-margin-remove">Ubah Status Investasi</h3>--}}
{{--                            <table class="uk-table uk-table-small uk-table-striped">--}}
{{--                                <tbody>--}}
{{--                                <tr>--}}
{{--                                    <th>ID &amp; Username</th>--}}
{{--                                    <td>{{ $investation->user->name }} &bull; {{ $investation->user->name ?? '' }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Nama Lengkap</th>--}}
{{--                                    <td>{{ $investation->user->fullName }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Nama Ibu</th>--}}
{{--                                    <td>{{ $investation->user->motherName }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Nomor KTP</th>--}}
{{--                                    <td>{{ $investation->user->ktp }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Nomor NPWP</th>--}}
{{--                                    <td>{{ $investation->user->npwp }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Nomor Telepon</th>--}}
{{--                                    <td>{{ $investation->user->hp }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Peluang Investasi</th>--}}
{{--                                    <td>--}}
{{--                                        <a href="{{ url('teaser_detail') }}" class="spf-link">--}}
{{--                                            {{ $investation->teaser->title }}--}}
{{--                                        </a>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Jumlah Investasi</th>--}}
{{--                                    <td><strong>{{ number_format($investation->invest)}},-</strong></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>SUKS</th>--}}
{{--                                    <td><strong>{{ number_format($investation->SUKS)}},-</strong></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>SUKUK</th>--}}
{{--                                    <td><strong>{{ number_format($investation->SUKUK)}},-</strong></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Yang Harus Dibayar</th>--}}
{{--                                    <td>--}}
{{--                                        <strong>{{ number_format($investation->invest + $investation->simpanan_pokok + $investation->simpanan_wajib + $investation->SUKS + $investation->SUKUK + 2000) }}--}}
{{--                                            ,-</strong></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Status</th>--}}
{{--                                    <td>--}}
{{--                                        @if ($investation->status == 'pending')--}}
{{--                                            <span class="uk-label uk-label-warning"></span>--}}
{{--                                        @elseif ($investation->status == 'approved')--}}
{{--                                            <span class="uk-label uk-label-success"></span>--}}
{{--                                        @else--}}
{{--                                            <span class="uk-label uk-label-danger"> </span>--}}
{{--                                        @endif--}}
{{--                                        {{ $investation->get_status_display }}--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th>Ubah Status</th>--}}
{{--                                    <td>--}}
{{--                                        <input type="hidden" name="investation_id" value="{{ $investation }}">--}}
{{--                                        <select name="investation_status" class="uk-select" required>--}}
{{--                                            <option value selected>--------------</option>--}}
{{--                                            <option value=0>Menunggu</option>--}}
{{--                                            <option value=1>Diterima</option>--}}
{{--                                            <option value=2>Ditolak</option>--}}
{{--                                        </select>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                            <div class="uk-text-right">--}}
{{--                                <div class="uk-button uk-button-default uk-modal-close">--}}
{{--                                    <span uk-icon="close"></span> Batal--}}
{{--                                </div>--}}
{{--                                <button type="submit" class="uk-button uk-button-primary" href="">--}}
{{--                                    <span uk-icon="check"></span> Simpan--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}

            @endforeach
            @if(count($investations) == 0)
                <tr>
                    <td colspan="7">
                        <div class="uk-card uk-card-default uk-margin">
                            <div class="uk-card-body">
                                <span uk-icon="info"></span> &nbsp; Investasi tidak ada!
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{$investations->links('vendor.pagination.ui-kit')}}

    </div>

@endsection