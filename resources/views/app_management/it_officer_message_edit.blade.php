@extends("base_dashboard")

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
    <li><a class="spf-link" href="{{url('app_plaza:message')}}">Pesan</a></li>
    <li><span></span></li>
  </ul>
@endsection

@section('content')

<form id="teaser-form" class="uk-form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('message.update', $message->id) }}">
@csrf
@method('PATCH')     
<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Pesan</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
        nama <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        {{$message->name}}
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="id_category">
        Email <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        {{$message->email}}
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Pesan <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        {!! $message->pesan !!}
        
      </div>
    </div>
    
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="id_description">
        Deskripsi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        
<div class="django-ckeditor-widget" data-field-id="id_description" style="display: inline-block;">
<textarea cols="40" id="editor1" name="description" rows="10" cols="50">
  {{$message->response}}
</textarea>
</div>
<button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: check"></span> Kirimkan Pesan
        </button>
        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>

</form>


    <!-- end spfjs -->
  </body>

@endsection

