@extends('base_dashboard')

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>        
    <li>Dashboard</li>
  </ul>
@endsection

@section('content')

  <div class="chart-main">
    <canvas id="chart-this-year" height="100"></canvas>
  </div>
    
@endsection


@section('js')
    
<script src="{% static 'js/chart.min.js' %}"></script>

<script src="{{asset('js/chart.min.js')}}"></script>

<script>
    window.chartColors = {
      red: 'rgb(255, 99, 132)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };
    var timeFormat = 'MM/DD/YYYY HH:mm';
    var color = Chart.helpers.color;
    var toFloat = function(numb) {
      return numb.replace(',', '.');
    }

    // global chart this year
    var globalConfig = {
      type: 'bar',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Nov', 'Des'],
        datasets: [{
          type: 'bar',
          label: 'Proposal disetujui',
          backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
          borderColor: window.chartColors.green,
          data: [
            @foreach ($filtered_by_month['teasers'] as $month => $value)
              toFloat('{{ $value }}'),
            @endforeach

          ],
        }, {
          type: 'line',
          label: 'Investasi disetujui',
          backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
          borderColor: window.chartColors.blue,
          fill: false,
          data: [
            @foreach ($filtered_by_month['invests'] as $month => $value)
              toFloat('{{ $value }}'),
            @endforeach
          ],
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Rekaman bulanan dalamam tahun {{date("Y")}}'
        },
        legend: {
          position: 'right',
        },
      }
    };

    window.onload = function() {
      var ctx1 = document.getElementById('chart-this-year').getContext('2d');
      window.myLine = new Chart(ctx1, globalConfig);
    };
  </script>
@endsection
