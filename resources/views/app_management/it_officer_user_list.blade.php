@extends('base_dashboard')

@section('breadcrumb')
    
<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
  <div class="uk-navbar-left">
    <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
        <li><span>Pengguna</span></li>
      </ul>
    </div>
    <div class="uk-navbar-right">
      <form class="uk-nav-nav" action="." method="get">
        <input name="q" class="uk-input uk-form-width-medium" type="text" placeholder="" required>
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: search"></span>
        </button>
      </form>
    </div>
  </nav>
@endsection


@section('content')
  <div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-small uk-table-striped uk-table-hover uk-table-responsive">
      <thead>
        <tr>
          <th>Username</th>
          <th>Nama Lengkap</th>
          <th>Email</th>
          <th>Telepon</th>
          <th>Tanggal Gabung</th>
        </tr>
      </thead>
      <tbody>
        @if ($users)
            
          @foreach ($users as $user)  
          <tr>
              <td>{{ $user->name }}</td>
              <td> {{ $user->fullName }}</td>
              <td> {{ $user->email }}</td>
              <td> {{ $user->hp }} </td>
              <td>{{ $user->created_at }}</span></td>
            </tr>        
          @endforeach    
        @else

          <tr>
            <td colspan="7">
              <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Pengguna tidak ada!
                </div>
              </div>
            </td>
          </tr>
        @endif
          
        </tbody>
    </table>
  </div>
  {{$users->links('vendor.pagination.ui-kit')}}

@endsection
