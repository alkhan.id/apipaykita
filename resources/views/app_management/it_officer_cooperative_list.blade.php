@extends("base_dashboard")

@section('breadcrumb')
  <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">
      <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><a class="spf-link" href="{{ url('dashboard_it_officer') }}">Dashboard</a></li>
        <li><span>Koperasi</span></li>
      </ul>
    </div>
    <div class="uk-navbar-right">
      <form class="uk-nav-nav" action="." method="get">
        <input name="q" value="---" class="uk-input uk-form-width-medium" type="text" placeholder="Cari ..." required>
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: search"></span>
        </button>
      </form>
    </div>
  </nav>
@endsection

@section('content')
  <div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-small uk-table-striped uk-table-hover uk-table-responsive">
      <thead>
        <tr>
          <th>Pengguna</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>Dibuat</th>
        </tr>
      </thead>
      <tbody>
        @if ($cooperatives)
            
        @foreach ($cooperatives as $cooperative)
        
        <tr>
          <td>{{ $cooperative->user->name }} @if ($cooperative->user->fullName) &bull; {{ $cooperative->user->fullName }}@endif</td>
          <td>{{ $cooperative->name }}</td>
          <td>{{ $cooperative->address }}</td>
          <td>{{ $cooperative->created_at }}</td>
        </tr>
        @endforeach
        @else
            
        <tr>
          <td colspan="4">
            <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Koperasi tidak ada!
                </div>
              </div>
            </td>
          </tr>
          @endif
          
        </tbody>
      </table>
      {{$cooperatives->links('vendor.pagination.ui-kit')}}

    </div>

  
@endsection
