@extends('base_dashboard')
@section('title')Proposal Penggalangan Dana
@endsection

@section('breadcrumb')
  <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">
      <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
        <li><span>Proposal Penggalangan Dana</span></li>
      </ul>
    </div>
    <div class="uk-navbar-right">
      <form class="uk-nav-nav" action="." method="get">
        <input name="q" class="uk-input uk-form-width-medium" type="text" placeholder="Cari ..." required>
        <input name="status" type="hidden" value="">
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: search"></span>
        </button>
      </form>
    </div>
  </nav>
@endsection

@section('content')
  {{-- <ul class="uk-tab uk-visible@s">
    <li @if($status == 'all') class="uk-active" @endif>
      <a href="{{url('dashboard_proposal')}}" class="spf-link">
        <span uk-icon="icon:list;ratio:.7"></span> Semua
      </a>
    </li>
    <li @if($status == '0') class="uk-active" @endif>
      <a href="{{url('Proposal/pending')}}" class="spf-link">
        <span uk-icon="icon:clock;ratio:.7"></span> Menunggu
      </a>
    </li>
    <li @if($status == '1') class="uk-active" @endif>
      <a href="{{url('Proposal/approved')}}" class="spf-link">
        <span uk-icon="icon:check;ratio:.7"></span> Diterima
      </a>
    </li>
    <li @if($status == '2') class="uk-active" @endif>
      <a href="{{url('Proposal/rejected')}}" class="spf-link">
        <span uk-icon="icon:close;ratio:.7"></span> Ditolak
      </a>
    </li>
    <li @if($status == '3') class="uk-active" @endif>
      <a href="{{url('Proposal/finished')}}" class="spf-link">
        <span uk-icon="icon:info;ratio:.7"></span> Selesai
      </a>
    </li>
  </ul> --}}
  <ul class="uk-tab uk-visible@s">
      <li @if (request()->get('status') != 'pending' && request()->get('status') != 'approved' && request()->get('status') != 'rejected' && request()->get('status') != 'finished')    
          class="uk-active" @endif>
        <a href="?status=all" class="spf-link">
          <span uk-icon="icon:list;ratio:.7"></span> Semua
        </a>
      </li>
      <li @if (request()->get('status') == 'pending') class="uk-active" @endif>
        <a href="?status=pending" class="spf-link">
          <span uk-icon="icon:clock;ratio:.7"></span> Menunggu
        </a>
      </li>
      <li @if (request()->get('status') == 'approved') class="uk-active" @endif>
        <a href="?status=approved" class="spf-link">
          <span uk-icon="icon:check;ratio:.7"></span> Diterima
        </a>
      </li>
      <li @if (request()->get('status') == 'rejected') class="uk-active" @endif>
        <a href="?status=rejected" class="spf-link">
          <span uk-icon="icon:close;ratio:.7"></span> Ditolak
        </a>
      </li>
      <li @if(request()->get('status') == 'finished') class="uk-active" @endif>
          <a href="?status=finished" class="spf-link">
            <span uk-icon="icon:info;ratio:.7"></span> Selesai
          </a>
        </li>
    </ul>
  <div class="uk-margin uk-hidden@s">
    <select name="status" class="uk-width-1-1 uk-select" onchange="window.location.href=this.value">
      <option value="?status=all">Semua</option>
      <option value="?status=pending" {% if request.GET.status == 'pending' %}selected{% endif %}>Menunggu</option>
      <option value="?status=approved" {% if request.GET.status == 'approved' %}selected{% endif %}>Diterima</option>
      <option value="?status=rejected" {% if request.GET.status == 'rejected' %}selected{% endif %}>Ditolak</option>
      <option value="?status=finished" {% if request.GET.status == 'finished' %}selected{% endif %}>Selesai</option>
    </select>
  </div>

  <div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-striped uk-table-hover uk-table-responsive">
      <thead>
        <tr>
          <th>ID & Nomor</th>
          <th>Proposal</th>
          <th>Kadaluarsa</th>
          <th>Tanggal Kadaluarsa</th>
          <th>Status</th>
          <th>Tindakan</th>
        </tr>
      </thead>
      <tbody>
        @foreach($teasers as $teaser)
          <tr>
            <td>{{$teaser->id . ' :: ' . $teaser->cooperative->id}}</td>
            <td><a class="spf-link" title="{{ $teaser->title }}" href="{{ url('dashboard_proposal_disqus')}}">{{ $teaser->title}}</a></td>
            <td><label class="uk-label">< {{ Carbon\Carbon::parse($teaser->expiration_date)->diffForHumans() }}</label> &nbsp; </td>
            <td>{{ $teaser->expiration_date }}</td>
            <td>
              @if($teaser->status == 0)
                <span uk-tooltip="title: Menunggu; pos: bottom"
                  class="uk-icon-button uk-background-warning uk-light" uk-icon="clock"></span>
              @elseif($teaser->status == 1)
                <span uk-tooltip="title: Diterima; pos: bottom"
                  class="uk-icon-button uk-background-success uk-light" uk-icon="check"></span>
              @elseif($teaser->status == 2)
                <span uk-tooltip="title: Ditolak; pos: bottom"
                  class="uk-icon-button uk-background-danger uk-light" uk-icon="close"></span>
              @else
                <span uk-tooltip="title: Selesai; pos: bottom"
                  class="uk-icon-button uk-background-secondary uk-light" uk-icon="info"></span>
              @endif
            </td>
            <td>
              <!-- <a href="{% url 'app_plaza:report_detail' slug=teaser.slug %}"
                uk-tooltip="title: {% trans 'Laporan Keuangan' %}; pos: bottom"
                class="uk-icon-link uk-margin-small-right spf-link" uk-icon="settings" style="transform: rotate(90deg);"></a>
              <a href="{% url 'dashboard_proposal_disqus')}}"
                uk-tooltip="title: {% trans 'Diskusikan' %}; pos: bottom"
                class="uk-icon-link uk-margin-small-right spf-link" uk-icon="comments"></a> -->
              <a href="{{url('dashboard_proposal/'.$teaser->id.'/edit')}}"
                uk-tooltip="Ubah"
                class="uk-icon-link spf-link" uk-icon="file-edit"></a>
            </td>
          </tr>
        @endforeach
        @if(count($teasers) == 0 )
          <tr>
            <td colspan="5">
              <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Proposal tidak ada!
                </div>
              </div>
            </td>
          </tr>
        @endif
      </tbody>
    </table>
    {{$teasers->links('vendor.pagination.ui-kit')}}

  </div>

@endsection
