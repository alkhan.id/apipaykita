@extends('base')
@section('title')
  Peluang Investasi kategori 
@endsection

@section('seo')
  <meta name="author" content="Plazadana">
  <meta name="description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
  <meta name="keywords" content="Plazadana, Retail equity, crowdfunding, syariah, koperasi, koperasi syariah">
  <meta property="og:locale" content="id_ID">
  <meta property="og:type" content="website">
  <meta property="og:title" content="">
  <meta property="og:description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
  <meta property="og:url" content="https://plazadana.com">
  <meta property="og:image" content="https://plazadana.com/{{asset('images/logos/png/logo-name-medium.png')}}">
  <meta property="og:site_name" content="plazadana.com">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
  <meta name="twitter:image:src" content="https://plazadana.com/{{asset('images/logos/png/logo-name-medium.png')}}">
  <meta name="twitter:title" content="Peluang Investasi kategori">
  <meta name="twitter:domain" content="plazadana.com">
  <meta name="twitter:creator" content="@plazadana.com">
@endsection

@section('content')
  <div class="search-menu">
    <div class="uk-text-center">
      <p>Tentukan peluang Investasi yang sesuai dengan keinginan Anda.</p>
    </div>
    <form class="uk-margin uk-text-center uk-flex uk-flex-center uk-flex-middle" method="post" action="{{url('teaser_list/searching')}}">
    @csrf
      <div class="uk-width-1-3 uk-inline">
        <input class="uk-input" type="text" name="q" value=""
          placeholder="Cari peluang investasi..." required>
      </div>
      <div class="uk-inline">
        <select class="uk-select" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
          <option value="{{url('teaser_list')}}">Semua Kategori</option>
          @foreach($categories as $category)
            <option value="{{url('teaser_list/'.$category->id)}}" {{ $id == $category->id ? 'selected' : ''}} > {{$category->title}}</option>
          @endforeach
        </select>
      </div>
      <div class="uk-inline">
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon:search"></span>
        </button>
      </div>
    </form>
  </div>
  <hr class="uk-divider-icon">
  <div class="uk-grid uk-child-width-expand@s" uk-grid>
    @foreach($teasers as $teaser)
      <div class="uk-width-1-3@m content">
        <div class="uk-card uk-card-hover uk-card-default">
          <div class="uk-card-media-top">
            <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">
              <img class="teaser-card" src="{{asset('file/'.$teaser->featured_image)}}" alt="{{$teaser->title}}">
            </a>
          </div>
          <div class="uk-card-body uk-padding-20">
            <a href="{{url('teaser_list/'.$teaser->category->id)}}" class="uk-card-badge uk-label spf-link">{{$teaser->category->title}}</a>
            <h3 class="uk-card-title uk-margin-remove text-limit">
              <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">{{$teaser->title}}</a>
            </h3>
            <p></p>

            <?php
                                $bills = App\Models\Bill::where('teaser_id', $teaser->id)->get();
                                // dd($bills);
                                $kosong = 0;
                                $terkumpul = 0;
                                foreach($bills as $bill){
                                  $terkumpul = $kosong + $bill->BILL;
                                }
                                $persen = $terkumpul / $teaser->amount * 100;
                              ?>
            <progress class="uk-progress" value="{{$persen}}" max="100"></progress>

            <table class="uk-table u uk-table-small uk-table-justify">
              <tbody>
                <tr>
                  <td>Nomor &amp; ID</td>
                  <td><strong>{{$teaser->cooperative->id}} :: {{$teaser->id}}</strong></td>
                </tr>
                <tr>
                  <td>Target</td>
                  <td><strong>Rp {{number_format($teaser->amount, 0, '', '.')}},-</strong></td>
                </tr>
                <tr>
                  <td>Terkumpul</td>
                  <td>
                    @if ($persen)
                    <span class="uk-label">{{$persen}}%</span>
                    @else
                    <strong>-</strong>
                    @endif
                  </td>
                </tr>
                <tr>
                  <td>Proyeksi Imbal hasil pertahun </td>
                  <td><strong>{{ $teaser->percentage_of_return }}%</strong></td>
                </tr>
                <tr>
                  <td>Pembayaran SHU</td>
                  <td><strong>
                  @if($teaser->payment_schedule == "monthly")1 bulan (bulanan)
                              @elseif($teaser->payment_schedule == "quarterly")3 bulan (triwulanan)
                              @elseif($teaser->payment_schedule == "semesters")6 bulan (semesteran)
                              @elseif($teaser->payment_schedule == "yearly")12 bulan (tahunan)
                              @endif
                  </strong></td>
                </tr>
                <tr>
                  <td>Kadaluarsa</td>
                  <td><strong>{{ $teaser->expiration_date }}</strong></td>
                </tr>
                <tr>
                  <td>Sisa hari</td>
                  <td><strong>{{\Carbon\Carbon::parse($teaser->expiration_date)->diffForHumans()}}</strong></td>
                </tr>
                <tr>
                            <td></td>
                            <td>        
                            <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">
                            <button class="uk-button uk-button-primary">
                                <!-- <i uk-icon="sign-in"></i>  -->
                                Investasi
                              </button>
                            </a>  
                            </td>
                          </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    @endforeach  
    @if(count($teasers) == 0)
      <div class="uk-width-1-1@m content">
        <div class="uk-card uk-card-default">
          <div class="uk-card-body">
            <i uk-icon="info"></i> &nbsp;
              <span>Mohon maaf belum ada peluang Investasi yang tersedia</span>
          </div>
        </div>
      </div>
    @endif

  </div><!-- end /.uk-child-width-expand@s -->
@endsection
