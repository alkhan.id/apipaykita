@extends('base_dashboard')

@section('breadcrum')
<ul class="uk-breadcrumb uk-breadcrumb-dashboard">
  <li><a class="spf-link" href="/">Beranda</a></li>
  {{-- <li><a class="spf-link" href="url 'user_dashboard'"> Dashboard</a></li>
  <li><a class="spf-link" href="url 'app_plaza:proposal_list'"> Proposalku</a></li> --}}
  <li><a class="spf-link" href="#"> Dashboard</a></li>
  <li><a class="spf-link" href="#"> Proposalku</a></li>
  <li><span>{{ $teaser->title }}</span></li>
</ul>
@endsection

@section('content')

<div class="uk-card uk-card-default">
  <div class="uk-card-header">
    <div class="uk-grid-small uk-flex-middle" uk-grid>
      <h3 class="uk-card-title uk-margin-remove-bottom">
        {{ $teaser->title }} :: {{ $teaser->cooperative->name }}
      </h3>
    </div>
    <p class="uk-article-meta uk-margin-remove">{{ $teaser->cooperative->address }}</p>
    <progress class="uk-progress" value="{{ $teaser->percentage_of_collected_funds }}" max="100"></progress>
    <div class="uk-position-top-right uk-padding-small">
      @if ($teaser->status == 0)          
      <a class="uk-icon-button uk-button-warning" uk-icon="clock" uk-tooltip="title: {% trans 'Menunggu Konfirmasi' %}"></a>
      @elseif ($teaser->status == 1)
      <a class="uk-icon-button uk-button-success" uk-icon="check" uk-tooltip="title: {% trans 'Diterima' %}"></a>
      @elseif ($teaser->status == 2)
      <a class="uk-icon-button uk-button-danger" uk-icon="close" uk-tooltip="title: {% trans 'Ditolak' %}"></a>
      @elseif ($teaser->status == 3)
      <a class="uk-icon-button uk-button-secondary" uk-icon="info" uk-tooltip="title: {% trans 'Selesai' %}"></a>
      @endif

      {% if request.user.profile.status == 'it_officer' or request.user.is_superuser %}
      @if ()
          
      @else
          
      @endif
      <a href="{% url 'dashboard_proposal_edit' slug=teaser.slug %}"
      class="uk-icon-button uk-button-warning spf-link" uk-icon="file-edit"
      uk-tooltip="title: {% trans 'Ubah' %}"></a>
      {% else %}
      {% if teaser.status == 'pending' or teaser.status == 'rejected' %}
      <a href="{% url 'app_plaza:proposal_edit' slug=teaser.slug %}"
      class="uk-icon-button uk-button-warning spf-link" uk-icon="file-edit"
      uk-tooltip="title: {% trans 'Ubah' %}"></a>
      {% endif %}
      {% endif %}
      
      <a href="{% url 'app_plaza:teaser_detail' slug=teaser.slug %}"
      class="uk-icon-button uk-button-primary spf-link" uk-icon="link"
      uk-tooltip="title: {% trans 'Lihat' %}"></a>
    </div>
  </div>
  <div class="uk-card-body">
    <div class="uk-child-width-expand@s" uk-grid>
      <div class="content-table-left">
        <table class="uk-table uk-table-divider uk-table-justify">
          <tbody>
            <tr>
              <td>{% trans "Investasi yang dibutuhkan" %}</td>
              <td><strong>{{ teaser.amount|as_rupiah }},-</strong></td>
            </tr>
            <tr>
              <td>{% trans "Dana yang terkumpul" %}</td>
              <td>
                <strong>{{ teaser.collected_funds|as_rupiah }},-</strong>
                <span class="uk-label">{{ teaser.percentage_of_collected_funds }}%</span>
              </td>
            </tr>
            <tr>
              <td>{% trans "Proyeksi Imbal hasil pertahun " %}</td>
              <td><strong>{{ teaser.percentage_of_return }}%</strong></td>
            </tr>
            <tr>
              <td>{% trans "Pembayaran imbal hasil setiap" %}</td>
              <td><strong>{{ teaser.get_payment_schedule_display }}</strong></td>
            </tr>
            <tr>
              <td>{% trans "Tanggal kadaluarsa" %}</td>
              <td><strong>{{ teaser.expiration_date }}</strong></td>
            </tr>
            <tr>
              <td>{% trans "Sisa hari" %}</td>
              <td><strong>{{ teaser.days_remaining }}</strong></td>
            </tr>
          </tbody>
        </table>
      </div><!-- end /.content-table-left -->
      <div class="content-table-right">
        <table class="uk-table uk-table-divider uk-table-justify">
          <tbody>
            <tr>
              <td>{% trans "Nomor &amp; ID" %}</td>
              <td><strong>{{ teaser.number }} :: {{ teaser.id }}</strong></td>
            </tr>
            <tr>
              <td>{% trans "Kategori" %}</td>
              <td><strong><a class="spf-link" href="{% url 'app_plaza:teaser_category_list' slug=teaser.category.slug %}">{{ teaser.category }}</a></strong></td>
            </tr>
            <tr>
              <td>{% trans "Dibuat pada" %}</td>
              <td><strong>{{ teaser.created }}</strong></td>
            </tr>
            <tr>
              <td>{% trans "Dimodifikasi pada" %}</td>
              <td><strong>{{ teaser.modified }}</strong></td>
            </tr>
            {% if teaser.attachment %}
            <tr>
              <td>{% trans "Lampiran" %}</td>
              <td>
                <div class="uk-form-custom">
                  <i class="uk-icon uk-icon-download uk-visible@s" uk-icon="icon: download"></i>
                  <a class="uk-button uk-button-default" href="{{ teaser.attachment.url }}" target="_blank">{% trans "Unduh Lampiran" %}</a>
                </div>
              </td>
            </tr>
            {% endif %}
          </tbody>
        </table>
      </div><!-- end /.content-table-right -->
    </div>
    
    <ul uk-accordion>
      <li>
        <a class="uk-accordion-title uk-text-primary">{% trans "Lihat Selengkapnya" %} <span uk-icon="chevron-down"></span></a>
        <div class="uk-accordion-content">
          <div class="read-more-tables">
            <h3 class="uk-card-title uk-heading-divider">{% trans "Data Koperasi" %}</h3>
            <table class="uk-table uk-table-divider uk-table-justify">
              <tbody>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Nama Koperasi" %}</td>
                  <td><strong>{{ teaser.cooperative_name }}</strong></td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Alamat Koperasi" %}</td>
                  <td><strong>{{ teaser.cooperative_address }}</strong></td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Proposal Unit Usaha" %}</td>
                  <td>
                    {% if teaser.cooperative_proposal_business_unit %}
                    <div class="uk-form-custom">
                      <i class="uk-icon uk-icon-download uk-visible@s" uk-icon="icon: download"></i>
                      <a class="uk-button uk-button-default" href="{{ teaser.cooperative_proposal_business_unit.url }}" target="_blank">{% trans "Unduh File" %}</a>
                    </div>
                    {% else %}-
                    {% endif %}
                  </td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Akte Pendirian Usaha" %}</td>
                  <td>
                    {% if teaser.cooperative_deed_of_incorporation %}
                    <div class="uk-form-custom">
                      <i class="uk-icon uk-icon-download uk-visible@s" uk-icon="icon: download"></i>
                      <a class="uk-button uk-button-default" href="{{ teaser.cooperative_deed_of_incorporation.url }}" target="_blank">{% trans "Unduh File" %}</a>
                    </div>
                    {% else %}-
                    {% endif %}
                  </td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "SIUP" %}</td>
                  <td>
                    {% if teaser.cooperative_business_license %}
                    <div class="uk-form-custom">
                      <i class="uk-icon uk-icon-download uk-visible@s" uk-icon="icon: download"></i>
                      <a class="uk-button uk-button-default" href="{{ teaser.cooperative_business_license.url }}" target="_blank">{% trans "Unduh File" %}</a>
                    </div>
                    {% else %}-
                    {% endif %}
                  </td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Izin Domisili" %}</td>
                  <td>
                    {% if teaser.cooperative_domicile_permit %}
                    <div class="uk-form-custom">
                      <i class="uk-icon uk-icon-download uk-visible@s" uk-icon="icon: download"></i>
                      <a class="uk-button uk-button-default" href="{{ teaser.cooperative_domicile_permit.url }}" target="_blank">{% trans "Unduh File" %}</a>
                    </div>
                    {% else %}-
                    {% endif %}
                  </td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Lainnya" %}</td>
                  <td>{{ teaser.cooperative_others|safe }}</td>
                </tr>
              </tbody>
            </table>
            <hr class="uk-divider-icon">
            <h3 class="uk-heading-divider">{% trans "Data Penaggung Jawab" %}</h3>
            <table class="uk-table uk-table-divider uk-table-justify">
              <tbody>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Nama Lengkap" %}</td>
                  <td><strong>{{ teaser.person_full_name }}</strong></td>
                </tr>
                {{-- <tr>
                  <td class="uk-width-1-4@m">{% trans "Nama Ibu" %}</td>
                  <td><strong>{{ teaser.person_mother_name }}</strong></td>
                </tr> --}}
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Nomor KTP" %}</td>
                  <td><strong>{{ teaser.person_ktp_number }}</strong></td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Nomor NPWP" %}</td>
                  <td><strong>{{ teaser.person_npwp_number }}</strong></td>
                </tr>
                <tr>
                  <td class="uk-width-1-4@m">{% trans "Nomor Telepon" %}</td>
                  <td><strong>{{ teaser.person_phone_number }}</strong></td>
                </tr>
              </tbody>
            </table>
          </div>
          
          <div class="uk-description">
            <h3 class="uk-heading-divider">{% trans "Deskripsi Proposal" %}</h3>
            {{ teaser.description|safe }}
          </div>
        </div><!-- end /.uk-accordion-content -->
      </li>
    </ul>
  </div>
</div>

<hr class="uk-divider-icon">

<div class="uk-card uk-card-default">
  <div class="uk-card-header">
    <div class="uk-grid-small uk-flex-middle" uk-grid>
      <h3 class="uk-card-title uk-margin-remove-bottom">
        {% if request.user.profile.status == 'it_officer' or request.user.is_superuser %}
        <span uk-icon="comments"></span> {% trans "Diskusikan dengan Koperasi/Pelapak" %}
        {% else %}
        <span uk-icon="comments"></span> {% trans "Diskusikan dengan IT Officer" %}
        {% endif %}
      </h3>
    </div>
  </div>
  <div class="uk-card-body uk-card-responsive" style="border-top:none">
      <ul class="uk-comment-list">
        {% for comment in comments %}
        <li class="comment-item {% if forloop.counter > 1 %}uk-margin-top{% endif %}" id="comment-{{ comment.pk }}">
            <article class="uk-comment uk-comment-primary uk-visible-toggle uk-comment-responsive-transparent" {% if comment.sender.profile.status == 'it_officer' %}style="background-color:#fff7e0"{% endif %}>
              <header class="uk-comment-header uk-position-relative">
                <div class="uk-grid-medium uk-flex-middle" uk-grid>
                  <div class="uk-width-auto">
                    <img class="uk-comment-avatar" src="{{ comment.sender.email|gravatar:'50' }}">
                  </div>
                  <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove">
                      <a class="uk-link-reset" href="#comment-{{ comment.pk }}">
                          {{ comment.sender }} <i><small>{% trans "kepada" %}</small> {{ comment.receiver }}</i>
                        </a>
                      </h4>
                      <p class="uk-comment-meta uk-margin-remove-top">
                        <a class="uk-link-reset" href="#comment-{{ comment.pk }}">{{ comment.created }}</a>
                      </p>
                    </div>
                  </div>
                  <div class="uk-position-top-right uk-position-small uk-hidden-hover">
                    <a class="uk-link-muted" href="#comment-form" uk-scroll>{% trans "Balas" %}</a>
                  </div>
                </header>
                <div class="uk-comment-body">
                  {{ comment.comment|safe }}
                </div>
              </article>
            </li>
            {% empty %}
            <li>
              <article class="uk-comment uk-visible-toggle">
                <div class="uk-comment-body uk-padding-responsive">
                  <span uk-icon="info"></span> {% trans "Belum ada sesuatu yang didiskusikan!" %}
                </div>
              </article>
            </li>
            {% endfor %}
            
            <li class="uk-margin-top" id="comment-form">
              <article class="uk-comment uk-comment-primary uk-visible-toggle uk-comment-responsive-transparent">
                <header class="uk-comment-header uk-margin-remove">
                  <h4 class="uk-comment-title">
                    <span uk-icon="comments"></span> {% trans "Komentar Anda" %}
                  </h4>
                </header>
            <div class="uk-comment-body">
              <form class="uk-form" method="post" action=".">{% csrf_token %}
                {% if form.receiver %}{# hidden for admin officer #}
                <div class="uk-margin">
                  <label class="uk-form-label" for="id_receiver">
                    {{ form.receiver.label }} <i class="uk-text-danger">*</i>
                  </label>
                  <div class="uk-form-controls">
                    {{ form.receiver }}
                    {% if form.receiver.errors %}
                    {% if form.receiver.errors|length > 1 %}
                    <ol class="uk-text-small uk-padding-small uk-margin-remove uk-text-left">
                      {% for error in form.receiver.errors %}
                      <li class="uk-text-warning">{{ error }}</li>
                      {% endfor %}
                    </ol>
                    {% else %}
                    <div class="uk-text-small uk-text-left uk-text-warning">
                      {{ form.receiver.errors|first }}
                    </div>
                    {% endif %}
                    {% endif %}
                  </div>
                </div>
                {% endif %}
                
                <div class="uk-margin">
                  <div class="uk-form-controls">
                    {{ form.comment }}
                    {% if form.comment.errors %}
                    {% if form.comment.errors|length > 1 %}
                    <ol class="uk-text-small uk-padding-small uk-margin-remove uk-text-left">
                      {% for error in form.comment.errors %}
                      <li class="uk-text-warning">{{ error }}</li>
                      {% endfor %}
                    </ol>
                    {% else %}
                    <div class="uk-text-small uk-text-left uk-text-warning">
                      {{ form.comment.errors|first }}
                    </div>
                    {% endif %}
                    {% endif %}
                  </div>
                </div>
                <div class="uk-margin">
                  <div class="uk-form-controls">
                    <button type="submit" class="uk-button uk-button-primary">
                      <span uk-icon="icon: check"></span> {% trans "Kirimkan" %}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </article>
        </li>
      </ul>
    </div>
  </div>
  {% endblock %}
  
  {% block js %}
  <script>$('li.list-teaser-me').addClass('uk-active')</script>
  <script src="{% static 'ckeditor/ckeditor-init.js' %}" data-ckeditor-basepath="{% static 'ckeditor/ckeditor/' %}"></script>
  <script src="{% static 'ckeditor/ckeditor/ckeditor.js' %}"></script>
  <script>
    $(document).ready(function() {
      setTimeout(function(){
        var mq = window.matchMedia('(min-width: 970px)');
        if (mq.matches) {
          $('#cke_id_comment').attr({'style':'width:865px'});
        }
      }, 1000);
    });
  </script>
@endsection
