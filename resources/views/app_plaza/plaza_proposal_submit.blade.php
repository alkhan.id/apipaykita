@extends("base_dashboard")

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
    <li><a class="spf-link" href="{{url('app_plaza:proposal_list')}}">Proposalku</a></li>
    <li><span></span></li>
  </ul>
@endsection

@section('content')
<div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Data Proposal</a></li>
      <li><a>Data Koperasi</a></li>
      <li><a>Data Penanggung Jawab</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item penggalangan-dana">

<form id="teaser-form" class="uk-form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('proposal') }}">
@csrf

<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Proposal</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
        Judul <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" name="title" class="uk-width-1-1 uk-input" placeholder="Proyek Surindo Syariah" id="id_title" required />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="id_category">
        Kategori <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <select name="category" class="uk-width-1-2@m uk-select" id="id_category" required>
        <option value="" selected>---------</option>
        @foreach($categorys as $category)
          <option value="{{$category->id}}">{{$category->title}}</option>
        @endforeach
        </select>
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Investasi yang dibutuhkan <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="number" name="amount" class="uk-width-1-2@m uk-input" placeholder="7520000" min="100000" id="id_amount" required />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Proyeksi Imbal hasil pertahun  <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="number" name="percentage_of_return" class="uk-width-1-2@m uk-input" id="id_percentage_of_return" required placeholder="10" min="1" max="99" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Pembayaran imbal hasil setiap <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <select name="payment_schedule" class="uk-width-1-2@m uk-select" id="id_payment_schedule">
          <option value="monthly" selected>1 bulan (bulanan)</option>
          <option value="quarterly">3 bulan (triwulanan)</option>
          <option value="semesters">6 bulan (semesteran)</option>
          <option value="yearly">12 bulan (tahunan)</option>
        </select>        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-featured_image">Gambar Unggulan</label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" accept="image/*" name="featured_image" >
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih gambar">
        </div>
        
        <span class="uk-text-muted uk-text-small">
          <br />Gambar ini akan ditampilkan secara publik.
        </span>
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">Lampiran</label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="attachment" >
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
        <span class="uk-text-muted uk-text-small">
          <br />Lampiran ini akan ditampilkan secara publik.
        </span>
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">Anggota Baru</label>
      <div class="uk-form-controls">
      <input type="checkbox" name="add_member" checked value="1">
        <span class="uk-text-muted uk-text-small">
          <br /> Membuka investor menjadi anggota baru.
        </span>
      </div>
    </div>

    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">Investasi</label>
      <div class="uk-form-controls">
      <input type="checkbox" name="invest" checked value="1">
        <span class="uk-text-muted uk-text-small">
          <br /> Membuka kesempatan investor berinvestasi.
        </span>
      </div>
    </div>

    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">SUKUK</label>
      <div class="uk-form-controls">
      <input type="checkbox" name="SUKUK" checked value="1">
        <span class="uk-text-muted uk-text-small">
          <br /> Menjual Surat Utang.
        </span>
      </div>
    </div>

    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">SUKS</label>
      <div class="uk-form-controls">
      <input type="checkbox" name="SUKS" checked value="1">
        <span class="uk-text-muted uk-text-small">
          <br /> Menjual Surat Utang.
        </span>
      </div>
    </div>
    
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="id_description">
        Deskripsi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        
<div class="django-ckeditor-widget" data-field-id="id_description" style="display: inline-block;">
<textarea cols="40" id="editor1" name="description" rows="10" cols="50"></textarea>
</div>

        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>

      </li>
      <li>


<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Koperasi</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_name">
        Nama Koperasi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input disabled type="text" name="cooperative_name" class="uk-width-1-1 uk-input" placeholder="Megan Koperasi" id="id_cooperative_name" value="{{$cooperative->name}}" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_address">
        Alamat Koperasi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <textarea disabled name="cooperative_address" class="uk-width-1-1 uk-textarea" placeholder="Jl. Gatot Subroto, Ngaliyan, Kota Semarang, Jawa Tengah" value="{{$cooperative->address}}" id="id_cooperative_address">
{{$cooperative->address}}
</textarea>
        
      </div>
    </div>
    <!-- <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_proposal_business_unit">
        Proposal Unit Usaha
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_proposal_business_unit" >
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_deed_of_incorporation">
        Akte Pendirian Usaha <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_deed_of_incorporation" required>
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_business_license">
        SIUP <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_business_license" required>
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_domicile_permit">
        Izin Domisili <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_domicile_permit" required>
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div> -->
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="form-cooperative_others">Lainnya</label>
      <div class="uk-form-controls">
        
      <div class="django-ckeditor-widget" data-field-id="id_cooperative_others" style="display: inline-block;">
      <textarea class="uk-width-1-1 uk-textarea" id="editor1" name="cooperative_others">
        {{$cooperative->other}}
      </textarea>
      </div>

        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>

      </li>
      <li>


<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Penanggung Jawab</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="profile-form-full_name">
        Nama Lengkap <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_full_name" value="{{$user->name}}" class="uk-width-1-1 uk-input" placeholder="Abul Qasim" id="id_person_full_name" />
        
      </div>
    </div>
    {{-- <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="profile_form-mother_name">
        Nama Ibu <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_mother_name" value="" class="uk-width-1-1 uk-input" placeholder="Ummu Khadijah" id="id_person_mother_name" />
        
      </div>
    </div> --}}
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="profile_form-ktp_number">
        Nomor KTP <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text"disabled name="person_ktp_number" value="" class="uk-width-1-1 uk-input" placeholder="160317100692xxxx" id="id_person_ktp_number" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="profile_form-npwp_number">
        Nomor NPWP <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_npwp_number" value="" class="uk-width-1-1 uk-input" placeholder="31.364.002.4-490.xxx" id="id_person_npwp_number" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-left uk-form-label" for="profile_form-phone_number">
        Nomor Telepon <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_phone_number" value="" class="uk-width-1-1 uk-input" placeholder="0821-3701-xxxx" id="id_person_phone_number" />
        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>

</li>
</ul>

<fieldset class="uk-margin-bottom uk-fieldset">
  <div class="uk-padding uk-panel">
  <div class="uk-margin">
            <label class="uk-flex uk-flex-left uk-form-label">
              Tanggal kadaluarsa <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input name="expiration_date" type="text" value="2019-07-02 11:21:38"
                class="uk-width-1-2@m uk-input has-datetimepicker input-timepicker">
            </div>
          </div>

    <div class="uk-margin">
      <div class="uk-form-controls">
        <ul class="uk-list">
          <li><i uk-icon="check" class="uk-text-primary"></i> Saya menyatakan bahwa informasi ini benar dan akurat.</li>
        </ul>
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: check"></span> Kirimkan Proposal
        </button>
      </div>
    </div>
  </div>
</fieldset>
</form>


    <!-- end spfjs -->
  </body>

@endsection

