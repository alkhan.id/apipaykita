Halo dari plazadana.com!<br/>
<br/>
Berikut ini detail informasi Investasi Anda di PlazaDana.com<br/>
<br/>
<pre>
Nama Proyek         : {{ investation.teaser }}<br/>
Nomor Proyek        : {{ investation.teaser.number }}<br/>
Status Investasi    : SUDAH DIBAYAR<br/>
Jumlah Investasi    : {{ investation.amount|as_rupiah }},-<br/>
Iuran Premi Tahunan : {{ investation.cost_volunteer|as_rupiah }},-<br/>
Biaya Adm Bank      : {{ PLAZADANA_BANK_FEE|as_rupiah }},-<br/>
Total Pembayaran    : <b>{{ investation.total_information_investation_payment|as_rupiah }},-</b><br/>


</pre>
<br/>
<blockquote style="font-weight: bold;background: #fff3e8;padding: 10px;margin: 0">
Anda dapat melihat <a href="{{ report_url }}">Laporan keuangan dari proyek ini disini</a>.
</blockquote>
<br/>
Terimakasih dari plazadana.com!<br/>
plazadana.com
