@extends("base_dashboard")

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
    <li><a class="spf-link" href="{{url('app_plaza:proposal_list')}}">Proposalku</a></li>
    <li><span></span></li>
  </ul>
@endsection

@section('content')
<form id="teaser-form" class="uk-form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('proposal/'.$teaser->id) }}">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Proposal</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
        Judul <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" value="{{$teaser->title}}" name="title" class="uk-width-1-1 uk-input" placeholder="Proyek Surindo Syariah" id="id_title" required />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="id_category">
        Kategori <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <select name="category" class="uk-width-1-2@m uk-select" id="id_category" required>
        <option value="">---------</option>
        @foreach($categorys as $category)
          <option value="{{$category->id}}" <?php if($category->id == $teaser->category_id){ echo 'selected'; } ?> >{{$category->title}}</option>
        @endforeach
        </select>
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Investasi yang dibutuhkan <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="number"  value="{{$teaser->amount}}" name="amount" class="uk-width-1-2@m uk-input" placeholder="7520000" min="100000" id="id_amount" required />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Proyeksi Imbal hasil pertahun  <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="number"  value="{{$teaser->percentage_of_return}}" name="percentage_of_return" class="uk-width-1-2@m uk-input" id="id_percentage_of_return" required placeholder="10" min="1" max="99" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-amount">
        Pembayaran imbal hasil setiap <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <select name="payment_schedule" class="uk-width-1-2@m uk-select" id="id_payment_schedule">
          <option value="monthly" <?php if($teaser->payment_schedule == 'monthly'){ echo 'selected'; } ?>>1 bulan (bulanan)</option>
          <option value="quarterly" <?php if($teaser->payment_schedule == 'quarterly'){ echo 'selected'; } ?>>3 bulan (triwulanan)</option>
          <option value="semesters" <?php if($teaser->payment_schedule == 'semesters'){ echo 'selected'; } ?>>6 bulan (semesteran)</option>
          <option value="yearly" <?php if($teaser->payment_schedule == 'yearly'){ echo 'selected'; } ?>>12 bulan (tahunan)</option>
        </select>        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-featured_image">Gambar Unggulan</label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" accept="image/*"  value="{{$teaser->featured_image}}" name="featured_image" >
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih gambar">
        </div>
        
        <span class="uk-text-muted uk-text-small">
          <br />Gambar ini akan ditampilkan secara publik.
        </span>
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">Lampiran</label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file"  value="{{$teaser->attachment}}" name="attachment" >
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
        <span class="uk-text-muted uk-text-small">
          <br />Lampiran ini akan ditampilkan secara publik.
        </span>
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="id_description">
        Deskripsi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        
<div class="django-ckeditor-widget" data-field-id="id_description" style="display: inline-block;">
<textarea cols="40" id="id_description" name="description" rows="10" required data-processed="0" data-external-plugin-resources="[]" data-id="id_description" data-type="ckeditortype">
    {{$teaser->description}}
</textarea>
</div>

        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>
<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Koperasi</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_name">
        Nama Koperasi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input disabled type="text" name="cooperative_name" class="uk-width-1-1 uk-input" placeholder="Megan Koperasi" id="id_cooperative_name" value="{{$cooperative->name}}" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_address">
        Alamat Koperasi <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <textarea disabled name="cooperative_address" class="uk-width-1-1 uk-textarea" placeholder="Jl. Gatot Subroto, Ngaliyan, Kota Semarang, Jawa Tengah" value="{{$cooperative->address}}" id="id_cooperative_address">
{{$cooperative->address}}
</textarea>
        
      </div>
    </div>
    <!-- <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_proposal_business_unit">
        Proposal Unit Usaha
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_proposal_business_unit" >
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_deed_of_incorporation">
        Akte Pendirian Usaha <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_deed_of_incorporation" required>
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_business_license">
        SIUP <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_business_license" required>
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_domicile_permit">
        Izin Domisili <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <div uk-form-custom="target: true">
          <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
          <input type="file" name="cooperative_domicile_permit" required>
          <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
        </div>
        
      </div>
    </div> -->
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="form-cooperative_others">Lainnya</label>
      <div class="uk-form-controls">
        
      <div class="django-ckeditor-widget" data-field-id="id_cooperative_others" style="display: inline-block;">
      <textarea disabled class="uk-width-1-1 uk-textarea" id="id_cooperative_others" name="cooperative_others" data-processed="0" data-config="{&quot;filebrowserUploadUrl&quot;: &quot;/ckeditor/upload/&quot;, &quot;skin&quot;: &quot;moono-lisa&quot;, &quot;filebrowserWindowHeight&quot;: 725, &quot;extraPlugins&quot;: &quot;youtube&quot;, &quot;toolbar&quot;: &quot;Custom&quot;, &quot;filebrowserWindowWidth&quot;: 940, &quot;width&quot;: &quot;auto&quot;, &quot;height&quot;: &quot;200px&quot;, &quot;filebrowserBrowseUrl&quot;: &quot;/ckeditor/browse/&quot;, &quot;toolbar_Full&quot;: [[&quot;Styles&quot;, &quot;Format&quot;, &quot;Bold&quot;, &quot;Italic&quot;, &quot;Underline&quot;, &quot;Strike&quot;, &quot;SpellChecker&quot;, &quot;Undo&quot;, &quot;Redo&quot;], [&quot;Link&quot;, &quot;Unlink&quot;, &quot;Anchor&quot;], [&quot;Image&quot;, &quot;Flash&quot;, &quot;Table&quot;, &quot;HorizontalRule&quot;], [&quot;TextColor&quot;, &quot;BGColor&quot;], [&quot;Smiley&quot;, &quot;SpecialChar&quot;], [&quot;Source&quot;]], &quot;toolbar_Custom&quot;: [[&quot;Format&quot;, &quot;Blockquote&quot;, &quot;Bold&quot;, &quot;Italic&quot;, &quot;Underline&quot;], [&quot;NumberedList&quot;, &quot;BulletedList&quot;, &quot;Link&quot;, &quot;Image&quot;, &quot;Youtube&quot;], [&quot;RemoveFormat&quot;, &quot;Maximize&quot;, &quot;Source&quot;]], &quot;toolbar_Basic&quot;: [[&quot;Source&quot;, &quot;-&quot;, &quot;Bold&quot;, &quot;Italic&quot;]], &quot;language&quot;: &quot;id&quot;}" data-external-plugin-resources="[]" data-id="id_cooperative_others" data-type="ckeditortype">
        {{$cooperative->other}}
      </textarea>
      </div>

        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>
<fieldset class="uk-margin-bottom uk-fieldset">
  <h3 class="uk-heading-line"><span>Data Penanggung Jawab</span></h3>
  <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="profile-form-full_name">
        Nama Lengkap <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_full_name" value="{{$user->name}}" class="uk-width-1-1 uk-input" placeholder="Abul Qasim" id="id_person_full_name" />
        
      </div>
    </div>
    {{-- <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="profile_form-mother_name">
        Nama Ibu <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_mother_name" value="" class="uk-width-1-1 uk-input" placeholder="Ummu Khadijah" id="id_person_mother_name" />
        
      </div>
    </div> --}}
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="profile_form-ktp_number">
        Nomor KTP <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text"disabled name="person_ktp_number" value="" class="uk-width-1-1 uk-input" placeholder="160317100692xxxx" id="id_person_ktp_number" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="profile_form-npwp_number">
        Nomor NPWP <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_npwp_number" value="" class="uk-width-1-1 uk-input" placeholder="31.364.002.4-490.xxx" id="id_person_npwp_number" />
        
      </div>
    </div>
    <div class="uk-margin">
      <label class="uk-flex uk-flex-right uk-form-label" for="profile_form-phone_number">
        Nomor Telepon <i class="uk-text-danger">*</i>
      </label>
      <div class="uk-form-controls">
        <input type="text" disabled name="person_phone_number" value="" class="uk-width-1-1 uk-input" placeholder="0821-3701-xxxx" id="id_person_phone_number" />
        
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <i>Tanda * wajib diisi.</i>
      </div>
    </div>
  </div>
</fieldset>
<fieldset class="uk-margin-bottom uk-fieldset">
  <div class="uk-padding uk-panel">
  <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label">
              Tanggal kadaluarsa <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input name="expiration_date" type="text" value="2019-07-02 11:21:38"
                class="uk-width-1-2@m uk-input has-datetimepicker input-timepicker">
            </div>
          </div>

    <div class="uk-margin">
      <div class="uk-form-controls">
        <ul class="uk-list">
          <li><i uk-icon="check" class="uk-text-primary"></i> Saya menyatakan bahwa informasi ini benar dan akurat.</li>
        </ul>
      </div>
    </div>
    <div class="uk-margin">
      <div class="uk-form-controls">
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: check"></span> Kirimkan Proposal
        </button>
      </div>
    </div>
  </div>
</fieldset>
</form>

<script>
$(document).ready(function(){
    $('input.timepicker').timepicker({});
});
</script>
@endsection

