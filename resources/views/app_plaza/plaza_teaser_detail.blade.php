@extends('base')
@section('css')<style>.uk-base-section{padding-top: 0}</style>@endsection

@section('seo')
  <!-- <meta name="author" content=" $teaser->author }}"> -->
  <!-- <meta name="description" content=" $teaser->description|safe|striptags|truncatewords:"20" }}"> -->
  <meta name="keywords" content="Plazadana, Retail equity, crowdfunding, syariah, koperasi, koperasi syariah">
  <meta property="og:locale" content="id_ID">
  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$teaser->title ?? ''}} - Plazadana">
  <!-- <meta property="og:description" content="{ $teaser->description|safe|striptags|truncatewords:"20" }}"> -->
  <!-- <meta property="og:url" content="https://plazadana.com{ request.get_full_path }}"> -->
  <!-- <meta property="og:image" content="https://plazadana.com if $teaser->featured_image %}{ $teaser->featured_image.url }}{% else %}{% static 'images/default-featured-image.png' %}{% endif %}"> -->
  <meta property="og:site_name" content="plazadana.com">
  <meta name="twitter:card" content="summary">
  <!-- <meta name="twitter:description" content="{ $teaser->description|safe|striptags|truncatewords:"20" }}"> -->
  <!-- <meta name="twitter:image:src" content="https://plazadana.com if $teaser->featured_image %}{ $teaser->featured_image.url }}{% else %}{% static 'images/default-featured-image.png' %}{% endif %}"> -->
  <meta name="twitter:title" content="{{ $teaser->title ?? ''}} - Plazadana">
  <meta name="twitter:domain" content="plazadana.com">
  <meta name="twitter:creator" content="@plazadana.com">
@endsection

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({% if $teaser->featured_image %}{{ $teaser->featured_image }}{% else %}{% static 'images/default-featured-image.png' %}{% endif %});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #f3ca62);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">{{$teaser->title ?? ''}}</h1>
      <!-- <p class="uk-text-white short-description-teaser-detail">
      </p> -->
    </div>
    <div class="uk-position-bottom-right uk-padding-remove">
      @if($teaser->user_id == Auth::user()->id)

      @else
        @if($teaser->status == 'finished')
          <div class="uk-button uk-button-secondary uk-button-large">
            <i uk-icon="check"></i> Sudah Selesai
          </div>
        @else
        <button class="uk-button uk-button-primary" uk-toggle="target: #modal-wakalah">
                <i uk-icon="sign-in"></i> Investasi 
              </button>
        @endif
      @endif
    </div>
  </div>
  <hr class="uk-divider-icon">
  <div class="uk-grid-divider uk-proposal-specifications" uk-grid>
    <div class="uk-width-expand@m contents">
      <div class="uk-card uk-card-default">
        <div class="uk-card-body">
          {!! $teaser->description !!}
        </div>
      </div>
    </div>
    <div class="uk-width-1-2@m">
      <div class="uk-card uk-card-default">
        <div class="uk-card-header">
          <div class="uk-grid-small uk-flex-middle" uk-grid>
            <h3 class="uk-card-title uk-margin-remove-bottom">{{ $teaser->cooperative->name }}</h3>
          </div>
          <p class="uk-article-meta uk-margin-remove">{{ $teaser->cooperative->address }}</p>          
          <?php
            $bills = App\Models\Bill::where('teaser_id', $teaser->id)->get();
            // dd($bills);
            $kosong = 0;
            $terkumpul = 0;
            foreach($bills as $bill){
              $terkumpul = $kosong + $bill->BILL;
            }
            $persen = $terkumpul / $teaser->amount * 100;
          ?>
          <progress class="uk-progress" value="{{ $persen }}" max="100"></progress>
        </div>
        <div class="uk-card-body">
          <table class="uk-table uk-table-divider uk-table-justify">
            <tbody>
              <tr>
                <td>Nomor &amp; ID</td>
                <td><strong>{{$teaser->cooperative->id }} :: {{$teaser->id }}</strong></td>
              </tr>
              <tr>
                <td>Kategori</td>
                <td><strong><a class="spf-link" href="{{url('teaser_category_list/'.$teaser->category_id)}}">{{$teaser->category->title ?? ''}}</a></strong></td>
              </tr>
              <tr>
                <td>Investasi yang dibutuhkan</td>
                <td><strong>Rp {{number_format($teaser->amount, 0, '', '.')}},-</strong></td>
              </tr>
              <tr>
                <td>Dana yang terkumpul</td>
                <td>
                  <strong>Rp {{number_format($terkumpul)}},-</strong>
                  <span class="uk-label">{{$persen}}%</span>
                </td>
              </tr>
              <tr>
                <td>Proyeksi Imbal hasil pertahun </td>
                <td><strong>{{ $teaser->percentage_of_return }}%</strong></td>
              </tr>
              <tr>
                <td>Pembayaran imbal hasil setiap</td>
                <td><strong>
                @if($teaser->payment_schedule == "monthly")1 bulan (bulanan)
                @elseif($teaser->payment_schedule == "quarterly")3 bulan (triwulanan)
                @elseif($teaser->payment_schedule == "semesters")6 bulan (semesteran)
                @elseif($teaser->payment_schedule == "yearly")12 bulan (tahunan)
                @endif      
                </strong></td>
              </tr>
              <tr>
                <td>Tanggal kadaluarsa</td>
                <td><strong>{{ $teaser->expiration_date }}</strong></td>
              </tr>
              <tr>
                <td>Sisa hari</td>
                <td><strong>{{\Carbon\Carbon::parse($teaser->expiration_date)->diffForHumans()}}</strong></td>
              </tr>
            
              @if($teaser->attachment)
                <tr>
                  <td>Lampiran</td>
                  <td>
                    <div class="uk-form-custom">
                      <i class="uk-icon uk-icon-download uk-visible@s" uk-icon="icon: download"></i>
                      <a class="uk-button uk-button-default" href="{{asset('file/'.$teaser->attachment) }}" target="_blank">Unduh Lampiran</a>
                    </div>
                  </td>
                </tr>
              @endif
              <tr>
                            <td></td>
                            <td>        
                            <button class="uk-button uk-button-primary" uk-toggle="target: #modal-wakalah">
                <i uk-icon="sign-in"></i> Investasi 
              </button>
                            </td>
                          </tr>
            </tbody>
          </table>
        </div>
          <!-- <div class="uk-card-footer">
              <button class="uk-button uk-button-primary" uk-toggle="target: #modal-wakalah">
                <i uk-icon="sign-in"></i> Investasi Sekarang
              </button>
          </div> -->
      </div>
    </div>
  </div><!-- end /.uk-proposal-specifications -->

  
  <!-- <div id="modal-confirm-invest" class="uk-modal-container" uk-modal>
      <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
      </div>
  </div>
   -->
    <div id="modal-wakalah" class="uk-modal-container"  uk-modal>
      <div class="uk-modal-dialog">
      <button class="uk-modal-close-default" type="button" uk-close></button>
      <div class="uk-modal-header">
            <h3 class="uk-modal-title uk-modal-title-license-agreement">Pernyataan Minat</h3>
          </div>
          <div class="uk-modal-body" uk-overflow-auto>
            <div class="uk-content-license-agreement">
            <form class="uk-form-horizontal uk-form-invest" method="post" action="{{url('invest')}}">
            @csrf  
            @if($invest == null)
            <p>Dengan ini saya menyatakan berminat untuk:</p>
              <div class="uk-alert uk-margin-remove-top">
              <label><input type="checkbox" class="uk-checkbox" id="SimpananCheck" />    
              Menjadi anggota Koperasi <b> {{$teaser->cooperative->name}} </b> dan bersedia untuk Membayar :</label></div>
              <div class="uk-margin">
                <label class="uk-flex uk-form-label">
                 Simpanan Pokok 
                </label>
                <div class="uk-form-controls">
                  <input type="text" name="simpanan_pokok" value="" id="simpanan_pokok" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                </div>
              </div>
              <div class="uk-margin">
                <label class="uk-flex uk-form-label">
                 Simpanan Wajib
                </label>
                <div class="uk-form-controls">
                  <input type="text" name="simpanan_wajib" value="" id="simpanan_wajib" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                </div>
                Secara periodic atau dibayarkan sekali dalam 1 (satu) tahun
              </div>
              <div class="uk-margin">
              <label class="uk-flex uk-form-label">
                 Simpanan Investasi
                </label>
                <div class="uk-form-controls">
                  <input type="text" name="invest" value="" id="Invest" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                </div>
              </div>
              <p>Mentaati Anggaran Dasar/Anggaran Rumah Tangga/ketentuan yang berlaku serta menanda tangani Akad 
                <a onclick="myMusyarakah()" class="btn btn-primary" >
                Akad Musyarakah<i class="uk-text-danger">*</i>
                </a>
              </p>
            </div><!-- end /.license-agreement -->


<!-- Akad Musyarakah -->

<div id="myDIVMusyarakah">
<p align="justify"><strong>AKAD MUSYARAKAH</strong></p>
<p align="justify"><strong>Antara Koperasi dengan Anggota</strong></p>
<!-- <p align="justify">No :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p> -->
<p align="justify">Pada hari ini , {{ date('d M Y') }}, yang bertandatangan di bawah ini:</p>

<p> 1. Nama : {{$teaser->user->name}}.</p>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bertindak dalam jabatannya selaku Ketua Pengurus KOPERASI {{$teaser->cooperative->name}} Terpilih dan diangkat melalui RAT dan merupakan pemegang Kuasa ANGGOTA dari dan karenanya, bertindak untuk dan atas nama serta mewakili seluruh ANGGOTA yang tergabung dalam KOPERASI {{$teaser->cooperative->name}}.. berkedudukan di, Jalan {{$teaser->cooperative->address}}.. yang telah mendapatkan pengesahan Menteri Kehakiman  beserta perubahan-perubahannya yang terakhir  yang dibuat di hadapan Notaris di Jakarta. Sebagai &ldquo;syarik&rdquo; Selanjutnya disebut &rdquo;<strong>KOPERASI</strong>&rdquo;.</p>
<p> 2. Nama : {{Auth::user()->fullname}} alias {{ Auth::user()->name}}</p>
<p align="justify">Tempat/tanggal lahir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->tempatLahir}}, {{ Auth::user()->tglLahir}}</p>
<p align="justify">No.KTP/ Paspor&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->ktp}}.</p>
<p align="justify">NPWP&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ Auth::user()->npwp}}</p>
<p align="justify">Bertindak untuk dan atas nama diri sendiri Sebagai &ldquo;syarik&rdquo;, Selanjutnya disebut &ldquo;CALON ANGGOTA&rdquo;</p>
<p align="justify">KOPERASI dan CALON ANGGOTA, selanjutnya bersama-sama disebut &ldquo;<strong>Para Pihak</strong>&rdquo;,&nbsp; terlebih dahulu menerangkan bahwa:</p>
<ol>
<li>CALON ANGGOTA akan bergabung menjadi ANGGOTA KOPERASI {{$teaser->cooperative->name}}. dan dengan ini bersedia untuk membayar iuran simpanan pokok/wajib sebagai <em>&ldquo;ra&rsquo;sul maal&rdquo;</em> untuk penyertaan <em>&ldquo;syirkah&rdquo;</em>, dan bersedia untuk mematuhi Anggaran Dasar/Rumah Tangga/ketentuan yang berlaku</li>
<li>KOPERASI {{$teaser->cooperative->name}} telah menyatakan persetujuannya untuk menerima sebagai ANGGOTA KOPERASI</li>
</ol>
<p align="justify">Selanjutnya, Para Pihak sepakat untuk membuat dan menandatangani Akad Musyarakah (selanjutnya disebut &rdquo;<strong>Akad</strong>&rdquo;) ini untuk dipatuhi dan dilaksanakan oleh Para Pihak dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut:</p>
<p align="justify"><strong><em>BISMILLAHIRRAHMAANIRRAHIM</em></strong></p>
<p align="justify">&ldquo;Dengan nama ALLAH yang Maha Pengasih dan Penyayang&rdquo;</p>
<p align="justify"><em>&ldquo;Hai orang-orang yang beriman, janganlah kamu mengkhianati Allah dan Rasul dan juga janganlah kamu mengkhianati amanah-amanah yang dipercayakan kepada kamu, sedang kamu mengetahui&rdquo;</em></p>
<p align="justify">(QS. Al-Anfaal: 27).</p>
<h4>Pasal 1</h4>
<p align="justify"><strong>DEFINISI</strong></p>
<p align="justify">Dalam Akad ini, yang dimaksud dengan:</p>
<ol>
<li><em>Akad syirkah</em> adalah akad kerja sama antara dua pihak atau lebih untuk suatu usaha tertentu di mana setiap pihak memberikan kontribusi dana/modal usaha <em>(ra's al-mal)</em> dengan ketentuan bahwa keuntungan dibagi secara proporsional, sedangkan kerugian juga ditanggung oleh para pihak secara proporsional. Syirkah ini merupakan salah satu bentuk Syirkah amwal dan dikenal dengan nama <em>syirkah inan</em></li>
<li><em>Syarik </em>adalah KOPERASI DAN ANGGOTA, para mitra atau para pihak yang melakukan akad syirkah, baik berupa orang <em>(syakhshiyah thabi'iyah/natuurlijke persoon)</em> maupun yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum (syakhshiyah i'tibariah/syakhshiyah hukmiyah/rechts per son)<em>. </em></li>
<li><em>Ra's al-mal </em>adalah modal usaha berupa harta kekayaan; yang disatukan, yang berasal dari para<em> syarik</em> dimana dalam hal ini berbentuk simpanan pokok/simpanan wajib/simpanan investasi/simpanan lainnya.</li>
<li><em>Bagi Hasil</em> adalah pembagian hasil usaha yang dihitung dari pendapatan dikurangi biaya-biaya dalam satu bulan takwim atau periode tertentu yang disepakati oleh Para Pihak.</li>
<li><em>Nisbah bagi hasil</em> &ndash; dapat juga disingkat <em>nisbah</em> - adalah perbandingan yang dinyatakan dengan angka seperti persentase untuk membagi hasil usaha, baik nisbah-proporsional maupun nisbah-kesepakatan <em>(dalam akad ini menggunakan nisbah proporsional)</em>.</li>
<li>Nisbah-proporsional adalah nisbah atas dasar porsi <em>ra's al-mal</em> para pihak <em>(syarik)</em> dalam <em>syirkah</em> yang dijadikan dasar untuk membagi keuntungan dan kerugian</li>
<li>Kerugian usaha musyarakah adalah hasil usaha, di mana jumlah modal usaha <em>(ra's al-ma[)</em> yang diinvestasikan mengalami penurunan atau jumlah modal dan biaya biaya melebihi jumlah pendapatan.</li>
<li>Hari kerja adalah setiap hari, kecuali Sabtu, Minggu dan hari libur resmi lainnya yang ditetapkan oleh pemerintah Republik Indonesia, dimana BANK-BANK buka di seluruh Indonesia untuk melaksanakan kegiatan usaha dan menjalankan transaksi kliring.</li>
</ol>
<h4>Pasal 2</h4>
<h6>POKOK AKAD DAN JANGKA WAKTU</h6>
<ol>
<li>KOPERASI dengan ini mengikatkan diri untuk bermitra (bermusyarakah) dengan ANGGOTA untuk menjalankan unit usaha secara syariah dengan sebaik-baiknya sesuai dengan Anggaran Dasar/Rumah Tangga serta ketentuan-ketentuan yang berlaku.</li>
<li>ANGGOTA dengan ini mengikatkan diri untuk bermitra (bermusyarakah) dengan KOPERASI serta bersedia untuk membayar iuran simpanan pokok/wajib/investasi sebagai penyertaan &ldquo;Ra&rsquo;sul Maal&rdquo; untuk musyarakah, dan bersedia untuk mematuhi Anggaran Dasar/Rumah Tangga/ketentuan yang berlaku</li>
<li>Besarnya setoran dana ra&rsquo;sul mal sebagaimana disebutkan dalam Pasal 1 Ayat 3 masing-masing:</li>
<li>Simpanan Pokok dibayarkan sekali pada saat pendaftaran,</li>
<li>Simpanan wajib dibayarkan secara periodik atau bisa dibayarkan secara tahunan sesuai kemampuan.</li>
<li>Simpanan investasi dibayarkan sekali atau bisa ditambah</li>
</ol>
<p align="justify">(Besaran simpanan-simpanan tersebut akan diisi nominalnya melalui form pernyataan minat yang tertera dalam pernyataan minat investasi di platform plazadana dan merupakan satu kesatuan yang tidak terpisahkan dengan akad ini)</p>
<ol start="4">
<li>Jangka waktu akad musyarakah adalah sesuai dengan Anggaran Dasar/Anggaran Rumah Tangga</li>
</ol>
<h4>Pasal 3</h4>
<p align="justify"><strong>HAK &amp; KEWAJIBAN PARA PIHAK DALAM </strong><strong>PENGELOLAAN KEGIATAN USAHA</strong></p>
<ol>
<li>ANGGOTA dan KOPERASI selaku Mitra secara bersama-sama bertanggung jawab penuh terhadap jalannya operasional usaha dan tidak ada satu pihak yang dapat mengendalikan atau berwenang penuh mengendalikan sendiri aktivitas usaha dan setiap pihak melaksanakan kerja sebagai wakil serta masing-masing pihak memberi wewenang kepada pihak lain dalam Akad ini untuk melakukan aktivitas Musyarakah. Para Pihak selaku Mitra secara bersama-sama berhak untuk membuat atau mengambil berbagai keputusan keuangan dan operasi, serta tidak diizinkan mencairkan/menginvestasikan dana untuk kepentingannya sendiri, kecuali terhadap hal-hal yang telah ditetapkan dalam Akad ini yang tidak memerlukan persetujuan bersama di antara Para Pihak.</li>
<li>ANGGOTA dengan ini memberikan kuasa tanpa hak substitusi kepada KOPERASI untuk mengelola usaha yang disepakati bersama, dengan syarat dan ketentuan sebagaimana tercantum dalam Akad ini dan Anggaran Dasar/Rumah Tangga/ketentuan yang berlaku. Selanjutnya, setelah pemberian kuasa ini, ANGGOTA berkedudukan sebagai mitra pasif dan KOPERASI berkedudukan sebagai mitra pengelola (mitra aktif).</li>
<li>ANGGOTA dan KOPERASI selaku Mitra secara bersama-sama mengakui kepemilikan asset, baik yang diserahkan dalam kerjasama atau terhadap asset yang dibeli untuk kegiatan usaha, guna menghasilkan keuntungan bagi usaha yang dijalankan</li>
<li>ANGGOTA dan KOPERASI selaku Mitra secara bersama-sama berhak untuk mengambil bagiannya atas hasil usaha sesuai dengan besarnya Nisbah Bagi Hasil yang telah disepakati Para Pihak dalam Akad ini.</li>
<li>ANGGOTA dan KOPERASI selaku Mitra menanggung kerugian secara proporsional menurut porsi Modal masing-masing, kecuali terhadap hal-hal yang dilakukan oleh KOPERASI menyimpang dari ketentuan dan kebijakan (AD/ART) yang telah ditetapkan atau disepakati seperti penyelewengan, spekulasi, monopoli, <em>gharar</em>, salah-urus, Cidera Janji dan pelanggaran yang dilakukan KOPERASI dengan sengaja atau tidak disengaja maka menjadi tanggung jawab dan Kewajiban KOPERASI.</li>
<li>Selama Periode Musyarakah, KOPERASI harus mengelola kegiatan usaha untuk kepentingan Musyarakah dan Para Pihak sesuai dengan Akad ini. Tanpa mengurangi hal yang telah disebutkan di atas, KOPERASI wajib:
<ol>
<li>melakukan semua kegiatan yang diperlukan untuk menjalankan kegiatan usaha dalam kebiasaan usaha yang wajar;</li>
<li>menjaga semua aset, properti dan fasilitas lainnya yang diperlukan atau diinginkan untuk kegiatan usaha;</li>
<li>membangun dan mengembangkan kegiatan usaha;</li>
<li>membayar pembayaran pajak lainnya (jika ada) yang dikenakan, dipungut atau diklaim sehubungan dengan Musyarakah atau kegiatan usaha oleh otoritas perpajakan yang terkait dan mengumpulkan semua pajak terkait pada waktu yang tepat;</li>
<li>memperoleh semua kewenangan dan ijin yang diperlukan sehubungan dengan Modal Musyarakah, kegiatan usaha dan transaksi yang dimaksudkan oleh Akad ini;</li>
<li>menjaga agar Rekening KOPERASI mencukupi dan akurat sehubungan dengan kegiatan usaha dan Modal Musyarakah.</li>
</ol>
</li>
<li>KOPERASI sebagai Mitra aktif, wajib memiliki sistem standar akuntansi pembukuan dan wajib membuat catatan administrasi pembukuan yang terpisah dari kegiatan usaha lain yang dimiliki KOPERASI (JIKA ADA).</li>
<li>KOPERASI wajib melakukan publikasi laporan kinerja keuangan secara periodik (bulanan/triwulanan/semesteran/tahunan) melalui platform digital plazadana atau sesuai kesepakatan</li>
</ol>
<h4>Pasal 4</h4>
<h4>STANDAR PERLAKUAN</h4>
<p align="justify">Pihak KOPERASI sebagai Pengelola KOPERASI, wajib melakukan kegiatan usaha sesuai dengan:</p>
<ol>
<li>Ketentuan Syariah.</li>
<li>seluruh hukum dan Ketentuan Peraturan Perundang-undangan yang berlaku;</li>
</ol>
<h4>Pasal 5</h4>
<h6>BAGI HASIL</h6>
<ol>
<li>ANGGOTA dan KOPERASI sepakat, dan dengan ini mengikatkan diri satu terhadap yang lain, bahwa Nisbah Bagi Hasil untuk masing-masing pihak adalah proporsional sesuai dengan kontribusi didasarkan pada prinsip <em>profit sharing</em>.</li>
<li>ANGGOTA dan KOPERASI juga sepakat, dan dengan ini saling mengikatkan diri satu terhadap yang lain, bahwa pelaksanaan perhitungan Bagi Hasil akan dilakukan pada setiap periode ......... dan bagi hasil tersebut (jika untung) wajib dibayar paling lambat setiap tanggal .................. setiap ......... bulannya.</li>
<li>ANGGOTA berjanji dan dengan ini mengikatkan diri untuk menanggung kerugian finansial yang timbul dalam pelaksanaan Akad ini sebanding dengan porsi kontribusi Modal, kecuali apabila kerugian tersebut terjadi karena ketidakjujuran dan/atau Cidera Janji KOPERASI khususnya sebagaimana dimaksud Pasal 6 dan Pasal 7 Akad ini, dan/atau pelanggaran yang dilakukan KOPERASI atas syarat-syarat sebagaimana dimaksud Pasal 8 Akad ini.</li>
</ol>
<h4>Pasal 6</h4>
<p align="justify"><strong>CIDERA JANJI/</strong><strong>KELALAIAN/PELANGGARAN</strong></p>
<p align="justify">Menyimpang dari ketentuan dalam Pasal 2, 3, 4 dan 5 Akad ini, ANGGOTA berhak untuk meminta kembali kepada KOPERASI atau siapa pun juga yang memperoleh hak darinya, atas seluruh atau sebagian jumlah Kewajiban KOPERASI kepada ANGGOTA berdasarkan Akad ini, untuk dibayar dengan seketika dan sekaligus, tanpa diperlukan adanya surat pemberitahuan, surat teguran, atau surat lainnya, apabila terjadi salah satu hal atau lebih peristiwa tersebut di bawah ini :</p>
<ol>
<li>Pihak yang bertindak untuk dan atas nama serta mewakili KOPERASI dalam Akad ini menjadi pemboros, pemabuk, atau dihukum penjara atau kurungan;</li>
<li>KOPERASI tidak memenuhi dan atau melanggar salah satu ketentuan atau lebih ketentuan-ketentuan yang tercantum dalam Akad ini ;</li>
<li>KOPERASI atau pihak ketiga telah memohon kepailitan terhadap KOPERASI;</li>
<li>KOPERASI memberikan keterangan, baik lisan atau tertulis, yang tidak benar dalam arti materiil tentang keadaan kekayaannya, penghasilan, dan segala keterangan atau dokumen yang ditunjukkan kepada ANGGOTA sehubungan Kewajiban KOPERASI kepada ANGGOTA atau jika KOPERASI menyerahkan tanda bukti penerimaan uang dan atau surat pemindahbukuan yang ditandatangani oleh pihak&ndash;pihak yang tidak berwenang untuk menandatanganinya sehingga tanda bukti penerimaan atau surat pemindahbukuan tersebut tidak sah.</li>
<li>KOPERASI, sebelum atau sesudah Akad ini ditandatangani, juga mempunyai utang kepada pihak ketiga dan hal yang demikian tidak diberitahukan kepada ANGGKOTA baik sebelum ini diberikan atau sebelum pendanaan lain tersebut diperoleh.</li>
<li>KOPERASI dibubarkan/bubar, melakukan atau terlibat dalam suatu perbuatan/peristiwa yang menurut pertimbangan ANGGOTA dapat membahayakan kelangsungan Musyarakah, ditangkap pihak yang berwajib atau dijatuhi hukuman penjara.</li>
<li>KOPERASI melakukan penyimpangan/ kelalaian terhadap hal-hal yang disepakati dalam Akad yang mengakibatkan kerugian ANGGOTA sesuai dengan ketentuan yang berlaku.</li>
<li>Cross Default</li>
</ol>
<ul>
<li>KOPERASI dan/atau salah satu pengurus lalai melaksanakan sesuatu kewajiban atau melakukan pelanggaran terhadap sesuatu ketentuan dalam akad lain dan/atau perjanjian jaminan lain yang dibuat dengan BANK.</li>
<li>Bilamana KOPERASI dan/atau Pengurus lalai melaksanakan sesuatu kewajiban atau melakukan pelanggaran terhadap sesuatu ketentuan dalam sesuatu akad/perjanjian lain baik dengan ANGGOTA maupun dengan orang/pihak/bank lain termasuk yang mengenai atau berhubungan dengan pinjaman uang/pemberian fasilitas pembiayaan dimana KOPERASI dan/atau salah seorang Pengurus adalah sebagai pihak yang menerima pinjaman atau sebagai penjamin dan kelalaian atau pelanggaran mana memberikan hak kepada ANGGOTA maupun pihak yang memberikan pinjaman atau fasilitas pembiayaan untuk menuntut pembayaran kembali atas apa yang terutang atau wajib dibayar oleh KOPERASI dan/atau salah seorang penjamin dalam perjanjian tersebut secara sekaligus selama akad ini belum berakhir.</li>
</ul>
<h4>Pasal 7</h4>
<h4>PERNYATAAN DAN JAMINAN PIHAK KEDUA</h4>
<ol>
<li><strong>Kewenangan </strong></li>
<li>Pihak yang mewakili KOPERASI berhak, cakap, dan berwenang sepenuhnya untuk menandatangani Akad ini dan semua surat dokumen yang menjadi kelengkapannya serta berhak pula untuk menjalankan usaha tersebut dalam Akad ini.</li>
<li>KOPERASI tidak sedang dalam keadaan menderita kerugian yang mempengaruhi jalannya usahanya secara materil atau mempengaruhi kemampuannya dalam melaksanakan kewajibannya, dan pada saat ini tidak berada dalam keadaan</li>
<li><strong>Tindakan </strong><strong>Hukum KOPERASI</strong></li>
<li>KOPERASI telah mengambil semua tindakan yang diperlukan sesuai ketentuan yang berlaku KOPERASI yang memberi wewenang untuk pelaksanaan Akad ini dan dokumen lain yang disyaratkan, dan pihak-pihak yang menandatangani dokumen-dokumen tersebut, telah diberi wewenang untuk berbuat demikian atas nama KOPERASI.</li>
</ol>
<ol>
<li>KOPERASI memiliki ijin-ijin dari pihak-pihak yang terkait yang mengharuskan KOPERASI memperoleh ijin-ijin tersebut untuk membuat dan menandatangani Akad ini,</li>
<li>Diadakannya Akad ini dan/atau akad tambahan (Addendum) Akad ini tidak akan bertentangan dengan suatu akad/perjanjian yang telah ada&nbsp;&nbsp; atau&nbsp;&nbsp; yang&nbsp;&nbsp; akan &nbsp;&nbsp;diadakan oleh KOPERASI dengan pihak ketiga lainnya.
<ul>
<li><strong>Perikatan </strong><strong>Akad ini </strong>
<ol>
<li>Akad ini, Perjanjian dan dokumen lain yang disyaratkan bila dilaksanakan dan diserahkan merupakan suatu kewajiban hukum bagi KOPERASI dan karenanya dapat dieksekusi sesuai dengan ketentuan-ketentuan yang tercantum di dalamnya.</li>
<li>Akad ini, Perjanjian dan dokumen-dokumen lain yang disyaratkan, pada saat ditandatangani tidak melanggar Undang-Undang, Peraturan, Ketetapan atau Keputusan dari Negara Republik Indonesia dan juga tidak bertentangan dengan atau mengakibatkan pelanggaran terhadap setiap perjanjian yang mengikat KOPERASI.</li>
</ol>
</li>
</ul>
</li>
</ol>
<ol>
<li>T<strong>idak Terjadi/Mengalami Peristiwa Cidera Janji</strong></li>
</ol>
<ol>
<li>KOPERASI tidak mengalami hal atau peristiwa yang merupakan suatu peristiwa Cidera Janji, kelalaian/ pelanggaran sebagaimana dimaksud dalam Akad ini maupun merupakan peristiwa kelalaian/ pelanggaran terhadap perjanjian lain yang dibuat KOPERASI dengan pihak lain.</li>
<li>KOPERASI tidak terlibat perkara pidana maupun perdata, tuntutan pajak atau sengketa yang sedang berlangsung atau menurut pengetahuan KOPERASI akan menjadi ancaman dikemudian hari atau yang dapat berakibat negatif terhadap KOPERASI atau harta kekayaannya, yang nantinya mempengaruhi keadaan keuangan atau usahanya atau dapat mengganggu kemampuannya untuk melakukan kewajibannya berdasarkan Akad ini.</li>
<li><strong>Data-Data Keuangan</strong></li>
</ol>
<p align="justify">Semua buku-buku keuangan KOPERASI, keterangan-keterangan antara lain tetapi tidak terbatas pada Laporan Keuangan yang dipublikasikan oleh KOPERASI dan data lain yang telah dan/ atau dikemudian hari akan dipublikasikan oleh KOPERASI kepada ANGGOTA adalah lengkap dan benar, dan buku-buku itu disiapkan dan dipelihara sesuai dengan prinsip-prinsip akuntansi yang berlaku di Negara Republik Indonesia yang diterapkan secara terus menerus dan menunjukkan secara benar keadaan keuangan dan hasil usaha KOPERASI pada tanggal buku-buku tersebut dibuat/ disiapkan, dan sejak tanggal dibuat/ disiapkan tersebut tidak terjadi perubahan keadaan keuangan KOPERASI yang mempengaruhi jalannya usaha KOPERASI secara materiil atau mempengaruhi kemampuan KOPERASI dalam melaksanakan kewajibannya kepada pihak lain berdasarkan Akad ini dan/ atau perjanjian-perjanjian lainnya yang dibuat antara Pihak lain dan KOPERASI.</p>
<ul>
<li>Selama berlangsungnya Akad ini, KOPERASI akan menjaga semua perizinan, lisensi, persetujuan dan sertifikat yang wajib dimiliki untuk melaksanakan usahanya.</li>
</ul>
<h4>pasal 8</h4>
<p align="justify"><strong>KEWAJIBAN DAN PEMBATASAN TERHADAP TINDAKAN KOPERASI</strong></p>
<ol>
<li>KOPERASI dengan ini berjanji dan mengikatkan diri selama jangka waktu Akad ini, maka KOPERASI wajib melakukan hal-hal sebagai berikut:
<ol>
<li>KOPERASI wajib menyampaikan publikasi laporan kinerja keuangan dalam bentuk dokumen-dokumen, surat-surat atau dalam bentuk lainnya mengenai keadaan keuangan KOPERASI melalui platform difital plazadana dan/atau dalam bentuk yang diminta ANGGOTA.</li>
<li>KOPERASI wajib memelihara pembukuan, administrasi dan catatan-catatan yang cukup mengenai usaha yang dijalankan/diusahakan oleh KOPERASI dan/atau pengurus sesuai dengan dan menurut prinsip-prinsip dan praktek-praktek akuntansi yang umum diterima di Republik Indonesia dan yang diterapkan secara terus menerus.</li>
<li>KOPERASI wajib mengizinkan perwakilan ANGGOTA memasuki kantor-kantor, gedung-gedung, pabrik-pabrik, atau lokasi usaha KOPERASI dan/atau pengurus guna melakukan pemeriksaan atas kekayaan dan usaha KOPERASI dan memeriksa/mengaudit pembukuan, catatan-catatan dan administrasi KOPERASI dan membuat salinan-salinan atau foto copy atau catatan-catatan dari padanya.</li>
<li>KOPERASI wajib menjaga: kekayaannya yang penting untuk kegiatan usahanya; kelangsungan eksistensi KOPERASI secara hukum; dan eksistensi semua hak, izin dan hal-hal lain, yang perlu untuk melaksanakan usahanya secara sah, tertib dan efisien.</li>
<li>KOPERASI wajib membayar semua pajak dan beban-beban lainnya berdasarkan ketentuan yang berlaku.</li>
<li>KOPERASI wajib mengasuransikan dan memelihara atau menyuruh mengasuransikan atau memelihara/mempertahankan asuransi atas barang-barang (baik berupa barang-barang bergerak maupun barang-barang tidak bergerak) yang sekarang telah dan/atau dikemudian hari akan</li>
</ol>
</li>
<li>KOPERASI wajib mempublikasikan rencana kegiatan usahanya untuk jangka waktu 1 (satu) tahun dan setiap rencana penggunaan Modal Musyarakah.</li>
<li>KOPERASI berjanji dan dengan ini mengikatkan diri, bahwa selama masa berlangsungnya Akad ini, kecuali setelah mendapatkan persetujuan tertulis dari PENGAWAS/RAT atau sejenisnya, KOPERASI tidak akan melakukan salah satu, sebahagian atau seluruh perbuatan-perbuatan sebagai berikut:</li>
</ol>
<ol>
<li>menyewakan, menjaminkan, mengalihkan atau menyerahkan, baik sebagian atau seluruhusaha, hasil usaha dan/atau Agunan kepada pihak lain;</li>
</ol>
<ol>
<li>Menerima sesuatu pembiayaan uang atau fasilitas keuangan, fasilitas leasing berupa apapun juga atau untuk mengikat diri sebagai penjamin/avalis untuk menjamin utang orang/pihak lain (kecuali utang dagang yang dibuat dalam rangka menjalankan usaha sehari-hari).</li>
<li>Menjual, menyewakan, mentransfer, memindahkan hak dan/atau kepentingan, menghapuskan sebagian besar atau seluruh harta kekayaan KOPERASI atau menjaminkan/mengagunkan barang-barang bergerak maupun barang-barang tidak bergerak milik KOPERASI dan/atau penjamin dengan cara bagaimanapun juga dan kepada orang/pihak siapapun juga (kecuali menjual dalam rangka menjalankan sifat usaha yang normal).
<ol>
<li>Melakukan investasi lainnya atau menjalankan kegiatan usaha yang tidak mempunyai hubungan dengan usaha yang sedang dijalankan atau melakukan perubahan usaha yang dapat mempengaruhi berlangsungnya usaha KOPERASI.</li>
<li>Mengajukan permohonan untuk dinyatakan pailit oleh Pengadilan Niaga atau mengajukan permohonan penundaan pembayaran utang (surseance van betalling).</li>
<li>Melakukan perubahan susunan pengurus.</li>
<li>Melakukan penggabungan usaha (merger) dengan badan usaha lain, peleburan usaha (konsolidasi) bersama badan usaha lain dan pengambilalihan (akuisisi) saham-saham dalam badan usaha lain.</li>
<li>Mengubah struktur permodalan KOPERASI, kecuali untuk peningkatan modal yang berasal dari laba yang ditahan (<em>retained earnings</em>)</li>
</ol>
</li>
<li>KOPERASI wajib memberitahukan secara tertulis kepada ANGGOTA, jika terjadi kejadian berikut ini:
<ol>
<li>Setiap tuntutan perkara perdata terhadap KOPERASI yang nilainya minimal 1/3 (satu per tiga) dari nilai Modal KOPERASI;</li>
<li>Sesuatu perkara atau tuntutan hukum yang terjadi antara KOPERASI dengan suatu badan/instansi pemerintah; dan/atau</li>
<li>Suatu kejadian yang dengan lewatnya waktu atau karena pemberitahuan atau kedua-duanya akan menjadi kejadian kelalaian ke pihak lain,</li>
</ol>
</li>
</ol>
<h4>pasal 9</h4>
<h4>HUKUM YANG BERLAKU</h4>
<p align="justify">Pelaksanaan Akad ini tunduk kepada peraturan perundang-undangan yang berlaku di Indonesia dan ketentuan syariah, termasuk namun tidak terbatas pada Fatwa Dewan Syariah Nasional Majelis Ulama Indonesia.</p>
<h4>Pasal 10</h4>
<h4>PENYELESAIAN PERSELISIHAN DAN DOMISILI HUKUM</h4>
<ol>
<li>Apabila di kemudian hari terjadi perbedaan pendapat atau penafsiran atas hal-hal yang tercantum di dalam Akad ini atau terjadi perselisihan atau sengketa dalam pelaksanaan Akad ini, Para Pihak sepakat untuk menyelesaikannya secara musyawarah untuk mufakat.</li>
<li>Dalam hal musyawarah untuk mufakat sebagaimana dimaksud ayat 1 Pasal ini tidak tercapai, maka Para Pihak bersepakat, dan dengan ini berjanji serta mengikatkan diri satu terhadap yang lain, untuk menyelesaikannya melalui Pengadilan Agama</li>
</ol>
<h4>Pasal 11</h4>
<p align="justify"><strong>KOMUNIKASI DAN PEMBERITAHUAN</strong></p>
<ol>
<li>Semua surat menyurat atau pemberitahuan-pemberitahuan yang harus dikirim oleh masing-masing pihak kepada pihak lain dalam Akad ini mengenai atau sehubungan dengan Akad ini, dilakukan dengan pos &ldquo;tercatat&rdquo; atau melalui perusahaan ekspedisi (kurir) ke alamat-alamat yang tersebut di bawah ini :</li>
<p align="justify"><strong>KOPERASI</strong></p>
<p align="justify">&nbsp;Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{$teaser->user->name}}</p>
<p align="justify">Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{$teaser->user->email}}</p>
<p align="justify">Telp./Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{$teaser->user->hp}}</p>
<p align="justify"><strong>ANGGOTA</strong></p>
<p align="justify">Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->name}}</p>
<p align="justify">Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->email}}</p>
<p align="justify">Telp./Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->hp}}</p>
<li>Pemberitahuan dari salah satu pihak kepada pihak lainnya dianggap diterima:</li>
<p>a. Jika dikirim melalui kurir (ekspedisi) pada tanggal penerimaan dan/atau;</p>
<p>b. Jika dikirim melalui pos tercatat 7 (tujuh) hari setelah tanggal pengirimannya.</p>
<li>PKOPERASI dapat mengganti alamatnya dengan memberitahukan secara tertulis kepada ANGGOTA. Perubahan alamat tersebut dianggap diterima oleh Pihak Kedua sesuai dengan ketentuan ayat 2 Pasal ini.</li>
<li>Dalam hal terjadi perubahan alamat KOPERASI, pemberitahuan perubahan alamat KOPERASI melalui media massa (cetak) berskala nasional atau lokal merupakan pemberitahuan resmi kepada ANGGOTA.</li>
</ol>
<h4>Pasal 12</h4>
<p align="justify"><strong>KETENTUAN PENUTUP</strong></p>
<ol>
<li>Sebelum Akad ini ditandatangani, Para Pihak mengakui dengan sebenarnya bahwa Para Pihak telah membaca dengan cermat atau dibacakan kepadanya seluruh isi Akad ini berikut semua surat dan/atau dokumen yang menjadi lampiran Akad ini, sehingga Para Pihak memahami sepenuhnya segala yang akan menjadi akibat hukum setelah Para Pihak menandatangani Akad ini.</li>
<li>Akad ini&nbsp; mengikat&nbsp; Para&nbsp; Pihak&nbsp; yang sah,&nbsp; para&nbsp; pengganti&nbsp; atau pihak-pihak&nbsp; yang menerima hak dari&nbsp; masing-masing Para Pihak.</li>
<li>Akad ini memuat (jika tidak ditentukan lain di dalam Akad ini), dan karenanya menggantikan semua pengertian dan kesepakatan yang telah dicapai oleh Para Pihak sebelum ditandatanganinya Akad ini, baik tertulis maupun lisan, mengenai hal yang sama.&nbsp;</li>
<li>Jika salah satu atau sebagian ketentuan-ketentuan dalam Akad ini menjadi batal atau tidak berlaku, maka tidak mengakibatkan seluruh Akad ini menjadi batal atau tidak berlaku seluruhnya.</li>
<li>Para Pihak mengakui bahwa judul pada setiap pasal dalam Akad ini dipakai hanya untuk memudahkan pembaca Akad ini, karenanya judul tersebut&nbsp; tidak memberikan penafsiran apapun atas isi Akad ini.</li>
<li>Apabila ada hal-hal yang belum diatur dalam Akad ini, maka KOPERASI dan CALON ANGGOTA akan mengaturnya bersama secara musyawarah untuk mufakat dalam suatu akad tambahan (Addendum) yang ditandatangani oleh Para Pihak.</li>
<li>Tiap akad tambahan (Addendum) dari Akad ini merupakan satu kesatuan yang tidak terpisahkan dari Akad ini.</li>
</ol>
<p align="justify">Demikian, Akad ini dibuat dan ditandatangani oleh KOPERASI dan CALON ANGGOTA, bermeterai cukup dalam dua rangkap, yang masing-masing disimpan oleh KOPERASI dan CALON ANGGOTA, dan masing-masing berlaku sebagai aslinya.</p>
<p align="justify">KOPERASI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CALON ANGGOTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menyetujui,</p>
<p align="justify">&hellip;&hellip;&hellip;..&hellip;&hellip;&hellip;&hellip;..&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &hellip;.&hellip;&hellip;&hellip;.&hellip;&hellip;&hellip;&hellip;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p align="justify"><strong>Saksi-saksi,</strong></p>
<p align="justify"><strong>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</strong><strong>&hellip;.</strong></p>


</div>



            @else
            <p>Anda telah terdaftar di koperasi {{$teaser->cooperative->name}} dengan membayar : <br>
                Simpanan pokok : {{number_format($invest->simpanan_pokok)}} dan Simpanan wajib : {{number_format($invest->simpanan_wajib)}} Sebelumnya </p>
            <p>Dengan ini saya menyatakan berminat untuk </p>
            
              <div class="uk-alert uk-margin-remove-top">
              <label><input type="checkbox" class="uk-checkbox" id="SimpananCheck" />    
              Menambah investasi di <b> {{$teaser->cooperative->name}} </b>  :</label></div>

                  <input type="text" name="simpanan_pokok" hidden value="{{$invest->simpanan_pokok}}" id="simpanan_pokok" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                  <input type="text" name="simpanan_wajib" hidden value="{{$invest->simpanan_wajib}}" id="simpanan_wajib" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
              <div class="uk-margin">
              <label class="uk-flex uk-form-label">
                 Simpanan Investasi
                </label>

                <div class="uk-form-controls">
                  <input type="text" name="invest" value="" id="Invest" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                </div>
                              </div>
            </div><!-- end /.license-agreement -->


            @endif
            <div class="uk-form-horizontal uk-form-invest">
              
              <div class="uk-margin">
                <label class="uk-flex uk-form-label">
                <input type="text" name="teaser_id" value="{{$teaser->id}}" id="" required class="uk-width-1-1 uk-input" placeholder="" hidden />
                <input type="text" name="cooperative_id" value="{{$teaser->cooperative_id}}" id="" required class="uk-width-1-1 uk-input" placeholder="" hidden />
                <input type="text" name="user_id" value="{{$user->id}}" id="" required class="uk-width-1-1 uk-input" placeholder="" hidden />
              </div>
              <!-- <p>Menanda tangani Akad <a href="" uk-toggle="target: #modal-wakalah">wakalah<i class="uk-text-danger">*</i></a>  dan Akad <a href=""> Mudharabah<i class="uk-text-danger">*</i></a></p> -->
              <!-- <div class="text-italic">
                <i>Untuk mempelajari lebih lanjut klik <a href="http://www.asyki.com" target="_blank">http://www.asyki.com</a></i>
              </div> -->
              </div><!-- end /.uk-form-invest -->
            <div id="form-SUKS" class="uk-form-horizontal uk-form-invest">
              <div class="uk-alert uk-margin-remove-top">
              <label><input type="checkbox" class="uk-checkbox" id="SUKSCheck" />  
              Membeli instrument Sertifikat penyertaan investasi yang diktawarkan oleh Koperasi <b> {{$teaser->cooperative->name}} :</label></div>  
              <div class="uk-margin">
                <label class="uk-flex uk-form-label">
                <input type="text" name="teaser_id" value="{{$teaser->id}}" id="" required class="uk-width-1-1 uk-input" placeholder="" hidden />
                <input type="text" name="cooperative_id" value="{{$teaser->cooperative_id}}" id="" required class="uk-width-1-1 uk-input" placeholder="" hidden />
                <input type="text" name="user_id" value="{{$user->id}}" id="" required class="uk-width-1-1 uk-input" placeholder="" hidden />
                  Surat Utang Koperasi Syariah/SUKS 
                  <!-- <i class="uk-text-danger">*</i> -->
                </label>
                <div class="uk-form-controls">
                  <input type="text" name="SUKS" value="" id="SUKS" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                </div>
              </div>
              <div class="uk-margin">
                <label class="uk-flex uk-form-label">
                  SUKUK 
                  <!-- <i class="uk-text-danger">*</i> -->
                </label>
                <div class="uk-form-controls">
                  <input type="text" name="SUKUK" value="" id="SUKUK" disabled class="uk-width-1-1 uk-input" placeholder="1.000.000" />
                </div>
              </div>
              <div class="text-italic">
                <i>Detail Akad akan dikirimkan by Email</i>
              </div>

Dengan ini saya menyetujui 
<a onclick="myFunction()" class="btn btn-primary" >
    Akad Wakalah<i class="uk-text-danger">*</i>
  </a>
  <br>
  <div id="myDIV">
<p><strong>Antara Investor dengan Plazadana</strong></p>
<!-- <p>No : </p> -->
<p>Pada hari ini {{ date('d M Y') }}, yang bertandatangan di bawah ini :</p>
<p>1. Nama : Ma'mun Zuberi.</p>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; bertindak dalam&nbsp; jabatannya selaku Direktur, bertindak untuk dan atas nama serta mewakili PT Plazadana Mitra Investama. berkedudukan di Bekasi Selatan, Komplek Ruko Bekasi Mas No.10 Blok A, Jalan Jenderal Ahmad Yani berdasarkan Akta Pendirian Nomor 58 yang dibuat di hadapan Notaris Irene Kusumawadhani SH, di Bekasi dan telah mendapatkan pengesahan Menteri Kehakiman Nomor AHU-0011077.AH.01.01. Tahun 2019 Tanggal 28 Febuari 2019 di Jakarta, sebagai <em>Wakil</em>. Selanjutnya disebut &rdquo;<strong>Pihak Pertama</strong>&rdquo;.</p>
<p>2. Nama : {{Auth::user()->fullName}} alias {{Auth::user()->name}}</p>
<p>Tempat/tanggal lahir  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;: {{Auth::user()->tempatLahir}}, {{Auth::user()->tglLahir}} </p>
<p>No.KTP/ Paspor&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->ktp}}</p>
<p>NPWP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{Auth::user()->npwp}}</p>
<p align="justify">Bertindak untuk diri sendiri yang selanjutnya disebut &ldquo;Pihak Kedua&rdquo;</p>
<p align="justify">Pihak Pertama dan Pihak Kedua, selanjutnya bersama-sama disebut &ldquo;<strong>Para Pihak</strong>&rdquo;,&nbsp; terlebih dahulu menerangkan bahwa:</p>
<ol>
<li><p align="justify"><strong>Pihak Kedua</strong> berminat untuk berinvestasi / membeli SUKS di Koperasi {{$teaser->cooperative->name}} berdasarkan proposal peluang investasi yang dtawarkan melalui <a href="http://www.plazadana.com">plazadana.com</a> selanjutnya disebut &ldquo;investasi&rdquo;</p></li>
</ol>
<ol start="2">
<li><p align="justify">Sehubungan dengan Investasi tersebut, dengan ini Pihak Kedua/Investor/Pemberi Kuasa memberikan kuasa kepada Pihak Pertama/Penerima Kuasa khusus untuk dan atas nama Pihak Kedua/Pemberi Kuasa melakukan hal-hal sebagai berikut:</p></li>
</ol>
<ul>
<li><p align="justify">Membuat dan melangsungkan akad/perjanjian dengan Pihak Koperasi sebagai pengelola usaha <em>(mudharib)</em> untuk kepentingan Pihak Kedua sebagai investor <em>(shahibul maal)</em> berdasarkan Akad <em>mudharabah</em>,</p></li>
<li><p align="justify">Membuat perubahan atas akad/perjanjian yang sudah ditandatangani oleh Wakil dan pihak Koperasi tersebut sepanjang perubahan tersebut sesuai dengan praktek industri yang berlaku umum dan wajar; dan</p></li>
<li><p align="justify">Mewakili segala kepentingan Pihak Kedua <em>(Muwakkil)</em> dalam rangka pelaksanaan akad/perjanjian <em>mudharabah</em> dengan pihak Koperasi sebagai pengelola usaha <em>(mudharib)</em>, termasuk namun tidak terbatas untuk melakukan penagihan, menerima dan menyalurkan seluruh imbal hasil usaha dari pihak Koperasi sebagai <em>mudharib</em> kepada Pihak Kedua sebagai <em>shahibul maal</em> (investor), dan lain-lain dalam rangka mewakili kepentingan Pihak Kedua</p></li>
</ul>
<p>Surat Kuasa ini diberikan tanpa hak substitusi.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PEMBERI KUASA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PENERIMA KUASA</strong></p>
<p>&nbsp;</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INVESTOR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PT PLAZADANA MITRA INVESTAMA</p>
<p><strong>&nbsp;</strong></p>



</div>

          <label><input type="checkbox" class="uk-checkbox" id="SetujuCheck" />  
              Tanda Tangan</label></div>  
          <div class="uk-modal-footer uk-text-right">
            <!-- <div class="uk-button uk-button-primary uk-button-agree-to-invest">
              <i uk-icon="check"></i> Selanjutnya
            </div> -->
            <!-- <button class="uk-button uk-button-primary" uk-toggle="target: #modal-confirm-invest">
                <i uk-icon="sign-in"></i> Investasi Sekarang
              </button> -->
              
              <button type="submit" id="setuju" disabled 
              class="uk-button uk-button-primary uk-button-submit-invest">
              <i uk-icon="check"></i> Kirim
            </button>
          </div>
          </div>
        </form>
        
      </div>
    </div>

 
@endsection

@section('js')
  <script>
    $('.uk-button-agree-to-invest').click(function(){
      // $('.uk-modal-title-license-agreement').htm/l('Form Investasi');
      // $('.uk-content-license-agreement').hide();
      // $('.uk-button-submit-invest').show();
      // $('.uk-form-invest').show();
      // $(this).hide();
    });

    
document.getElementById('SUKSCheck').onchange = function() {
    document.getElementById('SUKS').disabled = !this.checked;
    document.getElementById('SUKUK').disabled = !this.checked;
};

document.getElementById('SetujuCheck').onchange = function() {
    document.getElementById('setuju').disabled = !this.checked;
};


document.getElementById('SimpananCheck').onchange = function() {
    document.getElementById('simpanan_pokok').disabled = !this.checked;
    document.getElementById('simpanan_wajib').disabled = !this.checked;
    document.getElementById('Invest').disabled = !this.checked;
};
var x = document.getElementById("myDIV");
x.style.display = "none";
function myFunction() {
  // var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}


var y = document.getElementById("myDIVMusyarakah");
y.style.display = "none";
function myMusyarakah() {
  // var y = document.getElementById("myDIV");
  if (y.style.display === "none") {
    y.style.display = "block";
  } else {
    y.style.display = "none";
  }
}
// function required(inputtx) 
//    {
//      if (inputtx.value.length == 0)
//       { 
//          alert("message");  	
//          return false; 
//       }  	
//       return true; 
//     } 
  </script>
@endsection
