@extends("base_dashboard")

@section("breadcrumb")
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
    <li><a class="spf-link" href="{{url('report_list')}}">Laporanku</a></li>
    <li><span></span></li>
  </ul>
@endsection

@section('content')
  <form id="teaser-form" class="uk-form-horizontal" enctype="multipart/form-data" method="post" action="{{url('report/'.$report->id)}}">
  {{ csrf_field() }}
{{ method_field('PATCH') }}

    <div class="uk-alert-warning" uk-alert>
      <a class="uk-alert-close" uk-close></a>
      <ul class="uk-list">
        <li><i uk-icon="check" class="uk-icon"></i> Tanda * wajib diisi.</li>
        <li><i uk-icon="check" class="uk-icon"></i> Isikan angka 0 (nol) apabila tidak ada biaya.</li>
        <li><i uk-icon="check" class="uk-icon"></i> Laporan yang akan dikirimkan untuk tanggal d M Y.</li>
        <li><i uk-icon="check" class="uk-icon"></i> Lakukan diskusi apabila butuh pergantian tanggal/bulan (setelah membuat laporan).</li>
      </ul>
    </div>
    <input type="text" name="proposal_id" value="{{$report->teaser->id}}" required id="id_actual" class="uk-input" />
    <div class="uk-child-width-expand@s" uk-grid>
      <div class="left-content">
        <div class="uk-background-muted uk-placeholder">
          <h5 class="uk-text-bold">Laporan Umum</h5>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Realisasi <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="actual" value="{{$report->actual}}"  required id="id_actual" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Target <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="target" value="{{$report->target}}"  required id="id_target" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Pendapatan <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="revenue" value="{{$report->revenue}}"  required id="id_revenue" class="uk-input" />
              
            </div>
          </div>
        </div>
        <div class="uk-background-muted uk-placeholder">
          <h5 class="uk-text-bold">A. Biaya Sales/penjualan (biaya untuk memperoleh pesanan)</h5>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Gaji Wiraniaga <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_sales_person" value="{{$report->costs_sales_person}}"  required id="id_costs_sales_person" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Komisi Penjualan <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_sales_commission" value="{{$report->costs_sales_commission}}"  required id="id_costs_sales_commission" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Promosi dan Periklanan <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_promotion_and_advertising" value="{{$report->costs_promotion_and_advertising}}"  required id="id_costs_promotion_and_advertising" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Pergudangan <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_warehouse" value="{{$report->costs_warehouse}}"  required id="id_costs_warehouse" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Penagihan <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_billing_charge" value="{{$report->costs_billing_charge}}"  required id="id_costs_billing_charge" class="uk-input" />
              
            </div>
          </div>
        </div>
        <div class="uk-background-muted uk-placeholder">
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-attachment">Lampiran Opsional</label>
            <div class="uk-form-controls">
              <div uk-form-custom="target: true">
                <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
                <input type="file" name="attachment" value="{{$report->attachment}}" >
                <input class="uk-input" type="text" placeholder="Pilih file" style="width:60%">
              </div>
              
            </div>
          </div>
        </div>
      </div><!-- end /.left-content -->
      <div class="right-content">
        <div class="uk-background-muted uk-placeholder">
          <h5 class="uk-text-bold">B. Biaya Umum &amp; Administrasi</h5>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Telepon/Listrik <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_phone_and_electricity" value="{{$report->costs_phone_and_electricity}}"  required id="id_costs_phone_and_electricity" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Transportasi <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_transport" value="{{$report->costs_transport}}"  required id="id_costs_transport" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Gaji Pegawai <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_employee_salary" value="{{$report->costs_employee_salary}}"  required id="id_costs_employee_salary" class="uk-input" />
              
            </div>
          </div>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Peralatan Kantor <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_office_equipment" value="{{$report->costs_office_equipment}}"  required id="id_costs_office_equipment" class="uk-input" />
              
            </div>
          </div>
        </div>
        <div class="uk-background-muted uk-placeholder">
          <h5 class="uk-text-bold">C. Biaya bahan baku/raw materials</h5>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Bahan Baku <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_raw_material" value="{{$report->costs_raw_material}}"  required id="id_costs_raw_material" class="uk-input" />
              
            </div>
          </div>
        </div>
        <div class="uk-background-muted uk-placeholder">
          <h5 class="uk-text-bold">D. Biaya IT</h5>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Teknologi Informasi (IT) <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_it" value="{{$report->costs_it}}"  required id="id_costs_it" class="uk-input" />
              
            </div>
          </div>
        </div>
        <div class="uk-background-muted uk-placeholder">
          <h5 class="uk-text-bold">E. Biaya lain-lain</h5>
          <div class="uk-margin">
            <label class="uk-flex uk-flex-right uk-form-label" for="form-title">
              Biaya Lain-lain <i class="uk-text-danger">*</i>
            </label>
            <div class="uk-form-controls">
              <input type="number" name="costs_other" value="{{$report->costs_other}}"  required id="id_costs_other" class="uk-input" />
              
            </div>
          </div>
        </div>
      </div>
    </div><!-- end uk-grid -->

    <hr class="uk-divider-icon">

    <div class="uk-child-width-expand@s" uk-grid>
      <div class="left-content">
        <div class="uk-margin">
          <label>Penjelasan Opsional</label>
          <div class="uk-form-editor">
            
<div class="django-ckeditor-widget" data-field-id="id_description" style="display: inline-block;">
    <textarea cols="40" id="id_description" name="description" rows="10" data-processed="0" data-type="ckeditortype"> {{$report->description}}</textarea>
</div>

            
          </div>
        </div>
      </div>
      <div class="right-content">
        <div class="uk-margin uk-margin-top">
          <div class="uk-form-controls">
            
            <label class="uk-text-small">
              <input class="uk-checkbox" type="checkbox" required>
              &nbsp; Laporan ini sudah benar ?
            </label>
          </div>
        </div>
        <div class="uk-margin">
          <div class="uk-form-controls">
            <button type="submit" class="uk-button uk-button-primary uk-width-1-1@m">
              <span uk-icon="icon: check"></span> Kirimkan Laporan
            </button>
          </div>
        </div>
      </div>
    </div>
  </form>

            </div>
          </div>
        </div>
      </div>
@endsection