@extends('base_dashboard')
@section('title')
  Koperasiku
@endsection

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
    <li><span>Koperasiku</span></li>
  </ul>
@endsection

@section('content')
@if (isset($warning))
      <div class="uk-alert-warning" uk-alert>
          <a class="uk-alert-close" uk-close></a>
          <p>{{$warning}}.</p>
      </div>
      @endif
<div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Koperasi yang dikelola</a></li>
      <li><a>Koperasi yang diikuti</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item penggalangan-dana">



<form id="teaser-form" class="uk-form-horizontal" enctype="multipart/form-data"
    method="post" action="{{url('cooperative')}}">@csrf

    <fieldset class="uk-margin-bottom uk-fieldset">
      <div class="uk-background-muted uk-padding uk-panel" style="border: 1px dashed #ddd">
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-name">
            Nama Koperasi <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <input type="text" name="name" id="name" placeholder="Megan Koperasi" class="uk-width-1-1 uk-input" required />
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-address">
            Alamat Koperasi <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <textarea name="address" id="id_address" placeholder="Jl. Gatot Subroto, Ngaliyan, Kota Semarang, Jawa Tengah" class="uk-width-1-1 uk-textarea" required>
</textarea>
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-proposal_business_unit">
            Proposal Unit Usaha
          </label>
          <div class="uk-form-controls">
            <div uk-form-custom="target: true">
              <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
              <input type="file" name="proposal_business_unit" >
              <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
            </div>
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-deed_of_incorporation">
            Akte Pendirian Usaha <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <div uk-form-custom="target: true">
              <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
              <input type="file" name="deed_of_incorporation" required>
              <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
            </div>
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-business_license">
            AD / ART <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <div uk-form-custom="target: true">
              <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
              <input type="file" name="business_license" required>
              <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
            </div>
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-domicile_permit">
            NIB <i class="uk-text-danger">*</i>
          </label>
          <div class="uk-form-controls">
            <div uk-form-custom="target: true">
              <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
              <input type="file" name="domicile_permit" required>
              <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih file dokumen">
            </div>
            
          </div>
        </div>
        <div class="uk-margin">
          <label class="uk-flex uk-flex-right uk-form-label" for="form-others">Lainnya</label>
          <div class="uk-form-controls">    
          <div class="django-ckeditor-widget" data-field-id="id_form-others" style="display: inline-block;">
              <textarea class="uk-width-1-1 uk-textarea" id="id_form-others" name="others" data-processed="0" data-config="{&quot;skin&quot;: &quot;moono-lisa&quot;, &quot;filebrowserUploadUrl&quot;: &quot;/ckeditor/upload/&quot;, &quot;extraPlugins&quot;: &quot;youtube&quot;, &quot;toolbar_Custom&quot;: [[&quot;Format&quot;, &quot;Blockquote&quot;, &quot;Bold&quot;, &quot;Italic&quot;, &quot;Underline&quot;], [&quot;NumberedList&quot;, &quot;BulletedList&quot;, &quot;Link&quot;, &quot;Image&quot;, &quot;Youtube&quot;], [&quot;RemoveFormat&quot;, &quot;Maximize&quot;, &quot;Source&quot;]], &quot;toolbar_Basic&quot;: [[&quot;Source&quot;, &quot;-&quot;, &quot;Bold&quot;, &quot;Italic&quot;]], &quot;filebrowserBrowseUrl&quot;: &quot;/ckeditor/browse/&quot;, &quot;filebrowserWindowHeight&quot;: 725, &quot;language&quot;: &quot;id&quot;, &quot;toolbar&quot;: &quot;Custom&quot;, &quot;height&quot;: &quot;200px&quot;, &quot;filebrowserWindowWidth&quot;: 940, &quot;width&quot;: &quot;auto&quot;, &quot;toolbar_Full&quot;: [[&quot;Styles&quot;, &quot;Format&quot;, &quot;Bold&quot;, &quot;Italic&quot;, &quot;Underline&quot;, &quot;Strike&quot;, &quot;SpellChecker&quot;, &quot;Undo&quot;, &quot;Redo&quot;], [&quot;Link&quot;, &quot;Unlink&quot;, &quot;Anchor&quot;], [&quot;Image&quot;, &quot;Flash&quot;, &quot;Table&quot;, &quot;HorizontalRule&quot;], [&quot;TextColor&quot;, &quot;BGColor&quot;], [&quot;Smiley&quot;, &quot;SpecialChar&quot;], [&quot;Source&quot;]]}" data-external-plugin-resources="[]" data-id="id_form-others" data-type="ckeditortype"></textarea>
          </div>
          </div>
        </div>
        <div class="uk-margin">
          <div class="uk-form-controls">
            <i>Tanda * wajib diisi.</i>
          </div>
        </div>
        <div class="uk-margin">
          <div class="uk-form-controls">
            <button type="submit" class="uk-button uk-button-primary">
              <span uk-icon="icon: check"></span> Simpan Koperasi
            </button>
          </div>
        </div>
      </div>
    </fieldset>
  </form>

  </li>
<li>

<div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-justify uk-table-divider uk-table-responsive">
      <thead>
        <tr>
          <th>Koperasi</th>
          <th>Proposal</th>
          <th>Tanggal Bergabung</th>
          <!-- <th>Status</th> -->
          <th>Simpanan Pokok</th>
          <th>Simpanan Wajib</th>
        </tr>
      </thead>
      <tbody>
        @foreach($invests as $invest)
          <tr>
            <td>{{$invest->teaser->cooperative->name}}</td>
            <td>{{$invest->teaser->title}}</td>
            <td><label class="uk-label">
              {{\Carbon\Carbon::parse($invest->created_at)->diffForHumans()}}</label> &nbsp; {{$invest->created_at->format('d, M Y H:i')}}
            </label></td>
            <td>Rp {{number_format($invest->simpanan_pokok)}}   </td>
            <td>Rp {{number_format($invest->simpanan_wajib)}}</td>
          </tr>
        @endforeach

        @if(count($invests) == 0)
          <tr>
            <td colspan="5">
              <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Anda belum terdaftar dikoperasi lainya
                </div>
              </div>
            </td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>




</li>
</ul>
</div>


@endsection

@section('js')
  <script src="{{asset('ckeditor/ckeditor-init.js')}}" data-ckeditor-basepath="{{asset('ckeditor/ckeditor/')}}"></script>
  <script src="{{asset('ckeditor/ckeditor/ckeditor.js')}}"></script>
  <script>
    $(document).ready(function() {
      setTimeout(function(){
        var mq = window.matchMedia('(min-width: 970px)');
        if (mq.matches) {
          $('#cke_id_form-others').attr({'style':'width:700px'});
        }
      }, 1000);
    });
  </script>
@endsection
