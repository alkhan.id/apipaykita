@extends("base_dashboard")
@section('title')Pembayaran untuk 
@endsection

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}>Dashboard</a></li>
    <li><a class="spf-link" href="{{url('investation_list')}}>Investasiku</a></li>
    <li><span>Pembayaran untuk </span></li>
  </ul>
@endsection

{% block content %}
  <div class="uk-card uk-card-default uk-card-hover uk-margin">
    <div class="uk-card-body">
      <h3 class="uk-card-title">
        <a href="{{url('app_plaza:teaser_detail')}} class="spf-link">{{$investation->teaser->title}}</a>
      </h3>
      <p>{{$investation->teaser->description}}</p>

      <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
        <div class="left-content">
          <table class="uk-table uk-table-small">
            <tbody>
              <tr>
                <th>Status Investasi</th>
                <td>
                  @if('investation.status' == 'approved')
                    <span class="uk-label uk-label-success">Lunas</span>
                  @elseif('investation.status' == 'rejected')
                    <span class="uk-label uk-label-danger">Pembayaran Ditolak</span>
                  @else
                    <span class="uk-label uk-label-warning">Belum Dibayar</span>
                  @endif
                </td>
              </tr>
              <tr>
                <th>Jumlah Investasi</th>
                <td>{{ $investation->amount }},-</td>
              </tr>
              <tr>
                <th>Iuran Premi Mu'awanah</th>
                <td>{{ $investation->cost_volunteer}},-</td>
              </tr>
              <tr>
                <th>Biaya Adm Bank</th>
                <td>,-</td>
              </tr>
              <tr>
                <th>Total yg harus ditransfer</th>
                <td><b>,-</b></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="right-content">
          <table class="uk-table uk-table-small">
            <tbody>
              <tr>
                <th>Koperasi</th>
                <td>{{$investation->teaser->cooperative }}</td>
              </tr>
              <tr>
                <th>Nomor &amp; ID</th>
                <td><strong>{{$investation->teaser->number }} :: {{ $investation->teaser->id }}</strong></td>
              </tr>
              <tr>
                <th>Proyeksi Imbal hasil pertahun </th>
                <td><strong>{{ $investation->teaser->percentage_of_return }}%</strong></td>
              </tr>
              <tr>
                <th>Pembayaran setiap</th>
                <td><strong>{{ $investation->teaser->get_payment_schedule_display }}</strong></td>
              </tr>
              <tr>
                <th>Tanggal Investasi</th>
                <td>{{ $investation->created }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div><!-- end /.ui-grid-divider -->
    </div>

    <div class="uk-card-footer">
      <form class="uk-grid-small" uk-grid method="post" action=".">@csrf


        <h4 class="uk-text-bold">Cara Pembayaran</h4>
        <div class="uk-width-1-1@s uk-alert">
          <ul class="uk-list uk-list-divider">
            <li><span class="uk-badge">1</span> Pastikan anda mendownload & menginstall aplikasi Paykita di HP anda. Jika belum, silahkan <a href="{{ url_e_transaksi }}">download disini.</a></li>
            <li><span class="uk-badge">2</span> Pastikan anda mentransfer dana dahulu ke no virtual account 77772 + nomor simpanan plazadana yg ada di Paykita.</li>

             <!-- if request.user|total_investations_money > 0 %}
              <li><span class="uk-badge">3</span> Nominal yang harus anda transfer adalah <b>{{ total_payment }},-</b> (jumlah investasi + biaya adm bank).</li>
             else %}
              <li><span class="uk-badge">3</span> Nominal yang harus anda transfer adalah <b>{{ total_payment }},-</b> (jumlah investasi + biaya premi + biaya adm bank).</li>
             endif %} -->

            <li><span class="uk-badge">4</span> Masukkan Nomor Rekening Simpanan Plazadana pada Paykita anda kedalam form dibawah ini.</li>
          </ul>
        </div>
        <div class="uk-width-1-5@s">
          <input class="uk-input" type="text" value="7459" disabled>
        </div>
        <div class="uk-width-1-6@s">
          <input class="uk-input" type="text" value="22" disabled>
        </div>
        <div class="uk-width-1-3@s">
          {{ }}
        </div>
        <div class="uk-width-1-4@s">
          <button type="submit" class="uk-button uk-button-warning">
            <span uk-icon="check"></span> Cek Pembayaran
          </button>
        </div>
      </form>
      <ul uk-accordion>
        <li>
          <a class="uk-accordion-title uk-text-small" href="#">Klik disini jika transfer selain dari Bank Muamalat</a>
          <div class="uk-accordion-content">
            <p><span class="uk-label">147</span> + <span class="uk-label">77772</span> + <span class="uk-label">22</span> + <span class="uk-label">0002010xxxx</span></p>
          </div>
        </li>
      </ul>
    </div>
  </div>
@endsection

@section('js')
  <script>
    // active dashboard menu
    $('.list-invest-me').addClass('uk-active');
  </script>
@endsection
