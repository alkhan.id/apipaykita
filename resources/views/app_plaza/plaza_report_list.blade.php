@extends("base_dashboard")


@section('content')

<?php
$teaser = App\Models\Teaser::where('id',$id)->first();
?>
@if ($teaser)
    
@if(Auth::user()->id == $teaser->user->id)
<a href="{{url('/report/'.$id.'/create')}}">
  <button class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Buat Laporan</button>
</a>
@endif
@endif
  <div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-justify uk-table-divider uk-table-responsive">
      <thead>
        <tr>
          <th>Proposal</th>
          <th>Tanggal</th>
          <th>Status</th>
          <th>Tindakan</th>
        </tr>
      </thead>
      <tbody>
        @foreach($reports as $report)
          <tr>
            <td>{{$report->teaser->title}}</td>
            <td><label class="uk-label">
              {{\Carbon\Carbon::parse($report->created_at)->diffForHumans()}}</label> &nbsp; {{$report->created_at->format('d, M Y H:i')}}
            </label></td>
            <td>
              @if($report->status == 'approved')
                <span class="uk-badge uk-badge-success">
              @elseif($report->status == 'rejected')
                <span class="uk-badge uk-badge-danger">
              @else
                <span class="uk-badge uk-badge-warning">
              @endif
              {{ $report->status }}</span>
            </td>
            <td>
              <!-- <a href="{% url 'app_plaza:report_detail' slug=report.teaser.slug %}"
                uk-tooltip="title: {% trans 'Laporan Keuangan' %}; pos: bottom"
                class="uk-icon-link uk-margin-small-right spf-link" uk-icon="settings" style="transform: rotate(90deg);"></a>
              <a href="{% url 'app_plaza:report_disqus' pk=report.pk %}"
                uk-tooltip="title: {% trans 'Diskusikan' %}; pos: bottom"
                class="uk-icon-link uk-margin-small-right spf-link" uk-icon="comments"></a> -->

              @if($report->status != 'approved')
                <a href="{{url('report/'.$report->id.'/edit')}}"
                  uk-tooltip="Ubah"
                  class="uk-icon-link spf-link" uk-icon="file-edit"></a>
              @endif
            </td>
          </tr>
        @endforeach

        @if(count($reports) == 0)
          <tr>
            <td colspan="5">
              <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Laporan tidak ada! 
                  @if ($teaser)
                      
                  @if(Auth::user()->id == $teaser->user->id)
                  <a href="{{url('/report/'.$id.'/create')}}">Buat Laporan Baru</a>
                  @endif
                  @endif
                </div>
              </div>
            </td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>


@endsection
