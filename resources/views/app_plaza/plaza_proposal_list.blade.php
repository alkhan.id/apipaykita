@extends("base_dashboard")

@section('breadcrumb')
  <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">
      <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
        <li><a class="spf-link" href="/">Beranda</a></li>
        <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
        <li><span>Proposal Penggalangan Dana</span></li>
      </ul>
    </div>
    <div class="uk-navbar-right">
      <form class="uk-nav-nav" action="." method="get">@csrf
        <input name="q" class="uk-input uk-form-width-medium" type="text" placeholder="Cari ..." required>
        <button type="submit" class="uk-button uk-button-primary">
          <span uk-icon="icon: search"></span>
        </button>
      </form>
    </div>
  </nav>
@endsection

@section('content')
  <div class="uk-margin uk-overflow-auto-disabled">
    <table class="uk-table uk-table-justify uk-table-divider uk-table-responsive">
      <thead>
        <tr>
          <th>Judul Proposal</th>
          <th>Koperasi</th>
          <th>Kadaluarsa</th>
          <th>Status</th>
          <th>Tindakan</th>
        </tr>
      </thead>
      <tbody>
        @if(isset($proposals))

        @foreach($proposals as $teaser)
          <tr>
            <td><a class="spf-link" title="{{ $teaser->title }}" href="{{url('proposal/'.$teaser->id)}}">{{$teaser->title}}</a></td>
            <td>{{$teaser->cooperative->name}}</td>
            <td><label class="uk-label">{{\Carbon\Carbon::parse($teaser->expiration_date)->diffForHumans()}}</label> &nbsp; {{$teaser->expiration_date}}</td>
            <td>
              @if($teaser->status == 'approved')
                <span class="uk-badge uk-badge-success">
              @elseif($teaser->status == 'rejected')
                <span class="uk-badge uk-badge-danger">
              @elseif($teaser->status == 'finished')
                <span class="uk-badge uk-badge-secondary">
              @else
                <span class="uk-badge uk-badge-warning">
              @endif
              {{$teaser->status}}</span>
            </td>
            <td>
              <!-- <a href="url 'app_plaza:report_detail' slug=teaser->slug %}"
                uk-tooltip="title:Laporan Keuangan; pos: bottom"
                class="uk-icon-link uk-margin-small-right spf-link" uk-icon="settings" style="transform: rotate(90deg);"></a>
              <a href="url 'app_plaza:proposal_disqus' slug=teaser->slug %}"
                uk-tooltip="title:Diskusikan; pos: bottom"
                class="uk-icon-link uk-margin-small-right spf-link" uk-icon="comments"></a> -->

              @if($teaser->status != 'approved' && $teaser->status != 'finished')
              <a href="{{url('proposal/'.$teaser->id.'/edit')}}"
                uk-tooltip="title:Ubah; pos: bottom"
                class="uk-icon-link spf-link" uk-icon="file-edit"></a>
              @else
              <a href="{{url('report/'.$teaser->id)}}"
                uk-tooltip="title:Laporan; pos: bottom"
                class="uk-icon-link spf-link" uk-icon="file-edit"> Laporan </a>
              @endif
            </td>
          </tr>
        @endforeach
        @endif

          
        @if(count($proposals) == 0)
          <tr>
            <td colspan="5">
              <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-body">
                  <span uk-icon="info"></span> &nbsp; Proposal tidak ada!
                </div>
              </div>
            </td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>

@endsection
