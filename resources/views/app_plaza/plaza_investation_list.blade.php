@extends('base_dashboard')
@section('title')
  Investasiku
@endsection

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-position-relative uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="/">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
    <li><span>Investasiku</span></li>
    <div class="uk-first-column">
      <a href="{{url('teaser_list')}}" class="uk-position-right uk-text-link spf-link">
        <span uk-icon="forward"></span> Lihat Peluang Investasi
      </a>
    </div>
  </ul>
@endsection

@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Investasi</a></li>
      <li><a>Invoice</a></li>
      <!-- <li><a>Data Penanggung Jawab</a></li> -->
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li>

  @foreach($investations as $investation)
    <div class="uk-card uk-card-default uk-card-hover uk-margin">
      <div class="uk-card-body">
        <h3 class="uk-card-title">
          <a href="{{url('app_plaza:teaser_detail')}}" class="spf-link">{{$investation->teaser->title}}</a>
        </h3>
        <p></p>

        <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
          <div class="left-content">
            <table class="uk-table uk-table-small uk-table-responsive">
              <tbody>
                <tr>
                  <th>Status Investasi</th>
                  <td>
                    @if($investation->status == 1)
                      <span class="uk-label uk-label-success">Aktif</span>
                    @else
                      <span class="uk-label uk-label-info">Menunggu Pembayaran</span>
                    @endif
                  </td>
                </tr>
                @if($investation->invest != null )
                <tr>
                  <th><strong>Investasi</strong></th>
                  <td><strong>Rp {{number_format($investation->invest)}},-</strong></td>
                </tr>
                @endif
                @if($investation->SUKUK != null )
                <tr>
                  <th><strong>SUKUK</strong></th>
                  <td><strong>Rp {{number_format($investation->SUKUK)}},-</strong></td>
                </tr>
                @endif
                @if($investation->SUKS != null )
                <tr>
                  <th><strong>SUKS</strong></th>
                  <td><strong>Rp {{number_format($investation->SUKS)}},-</strong></td>
                </tr>
                @endif
                <tr>
                  <th>Proyeksi Imbal hasil pertahun </th>
                  <td><strong>{{ $investation->teaser->percentage_of_return }}%</strong></td>
                </tr>
                <tr>
                  <th>Pembayaran SHU</th>
                  <td><strong>
                  @if($investation->teaser->payment_schedule == "monthly")1 bulan (bulanan)
                              @elseif($investation->teaser->payment_schedule == "quarterly")3 bulan (triwulanan)
                              @elseif($investation->teaser->payment_schedule == "semesters")6 bulan (semesteran)
                              @elseif($investation->teaser->payment_schedule == "yearly")12 bulan (tahunan)
                              @endif
                  
                  </strong></td>
                <tr>
                  <th>Tanggal Pembuatan</th>
                  <td><strong>{{$investation->created_at->format('d, M Y H:i') }}</strong></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="right-content">
            <table class="uk-table uk-table-small uk-table-responsive">
              <tbody>
                <tr>
                  <th>Nomor &amp; ID</th>
                  <td><strong>{{$investation->teaser->cooperative->id }} :: {{$investation->teaser->id }}</strong></td>
                </tr>
                <tr>
                <tr>
                  <th>Koperasi</th>
                  <td><strong>{{$investation->teaser->cooperative->name }}</strong></td>
                </tr>
                @if($investation->simpanan_pokok != null )
                <tr>
                  <th><strong>Simpanan Pokok</strong></th>
                  <td><strong>Rp {{number_format($investation->simpanan_pokok)}},-</strong></td>
                </tr>
                @endif
                @if($investation->simpanan_wajib != null )
                <tr>
                  <th><strong> Simpanan Wajib</strong></th>
                  <td><strong>Rp {{number_format($investation->simpanan_wajib)}},-</strong></td>
                </tr>
                @endif
                </tr>
              </tbody>
            </table>
          </div>
        </div><!-- end /.ui-grid-divider -->

      </div>
      <div class="uk-card-footer">
        
        @if($investation->status == 1)
          <a href="{{url('report/'.$investation->id)}}"
          class="uk-button uk-button-default uk-button-responsive remove-bill">Lihat Laporan Keuangan</a>
        @else
          <form method="POST" action="{{ url('investation_delete', [$investation->id]) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <!-- <button onclick="return confirm('Apakah anda yakin?')" type="submit" class="uk-button uk-button-info uk-button-responsive spf-link"><span uk-icon="close"></span>Batalkan</button> -->
            <a href="{{url('user_dashboard')}}" class="uk-button uk-button-success uk-button-responsive spf-link">Dashboard</a>
          </form>
        @endif
      </div>
    </div>
  @endforeach
  {{$investations->links("pagination::bootstrap-4")}}
  
  @if(count($investations) == 0)
    <div class="uk-card uk-card-default uk-margin">
      <div class="uk-card-body">
        <span uk-icon="info"></span> &nbsp; Anda belum memiliki investasi apapun!
      </div>
    </div>
  @endif


</li>
<li>

@foreach($bills as $bill)
    <div class="uk-card uk-card-default uk-card-hover uk-margin">
      <div class="uk-card-body">
        <h3 class="uk-card-title">
          <a href="{{url('app_plaza:teaser_detail')}}" class="spf-link">{{$bill->DESCRIPTION}}</a>
        </h3>
        <p></p>

        <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
          <div class="right-content">
            <table class="uk-table uk-table-small uk-table-responsive">
              <tbody>
                <tr>
                  <th> <strong>Jumlah Tagihan</strong> </th>
                  <td> <strong>Rp {{number_format($bill->BILL)}},-</strong> </td>
                </tr>
                <tr>
                  <th> <strong> Tanggal </strong></th>
                  <td> <strong>{{$bill->created_at->format('d, M Y H:i') }}</strong> </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- <div class="right-content"> -->
            <!-- <table class="uk-table uk-table-small uk-table-responsive">
              <tbody>
                <tr>
                  <th>Nomor &amp; ID</th>
                  <td><strong>{{$bill->teaser->cooperative->id }} :: {{$bill->teaser->id }}</strong></td>
                </tr>
                <tr>
                <tr>
                  <th>Koperasi</th>
                  <td><strong>{{$bill->teaser->cooperative->name }}</strong></td>
                </tr>
                <tr>
                  <th>Proyeksi Imbal hasil pertahun </th>
                  <td><strong>{{ $bill->teaser->percentage_of_return }}%</strong></td>
                </tr>
                <tr>
                  <th>Pembayaran SHU</th>
                  <td><strong>
                  @if($bill->teaser->payment_schedule == "monthly")1 bulan (bulanan)
                              @elseif($bill->teaser->payment_schedule == "quarterly")3 bulan (triwulanan)
                              @elseif($bill->teaser->payment_schedule == "semesters")6 bulan (semesteran)
                              @elseif($bill->teaser->payment_schedule == "yearly")12 bulan (tahunan)
                              @endif
                  
                  </strong></td>
                </tr>
              </tbody>
            </table> -->
          <!-- </div> -->
        </div><!-- end /.ui-grid-divider -->




      </div>
      <div class="uk-card-footer">
        
      <ul uk-accordion>
    <li >
        <a class="uk-accordion-title" href="#">
        <img src="{{asset('images/muamalat.png')}}" width="25%" alt="">
        </a>
        <div class="uk-accordion-content">
          <ul uk-accordion>
            <li >
                <a class="uk-accordion-title" href="#">
                <img src="{{asset('images/atm muamalat.png')}}" width="15%" alt="">
                </a>
                <div class="uk-accordion-content">
                      <p>1. masukkan kartu ATM dan PIN</p>  
                      <p>2. pilih menu Transaksi Lain</p>  
                      <p>3. pilih menu Pembayaran, kemudian pilih menu Virtual Account</p>  
                      <p>4. masukkan 16 digit nomor Virtual Account Anda <input type="text" value="{{Auth::user()->va}}" id="myInput"> <a> <span uk-icon="icon: copy" title="Copy VA" onclick="myFunction()"></span></a></p> 
                      <p>5. pilih bayar jika informasi pembayaran telah sesuai</p> 
                      <p>6. masukkan nominal jumlah pembayaran tanpa biaya transaksi Virtual Account (jika diminta)</p>  
                      <p>7. pilih Benar kemudian pilih Bayar</p> 
                      <p>8. simpan struk ATM sebagai bukti pembayaran.</p> 
                </div>
            </li>
            <li >
                <a class="uk-accordion-title" href="#">
                <img src="{{asset('images/mobile banking muamalat.png')}}" width="15%" alt="">
                </a>
                <div class="uk-accordion-content">
                      <p>1. masukkan kartu ATM dan PIN</p>  
                      <p>2. pilih menu Transaksi Lain</p>  
                      <p>3. pilih menu Pembayaran, kemudian pilih menu Virtual Account</p>  
                      <p>4. masukkan 16 digit nomor Virtual Account Anda <input type="text" value="{{Auth::user()->va}}" id="myInput"> <a> <span uk-icon="icon: copy" title="Copy VA" onclick="myFunction()"></span></a></p> 
                      <p>5. pilih bayar jika informasi pembayaran telah sesuai</p> 
                      <p>6. masukkan nominal jumlah pembayaran tanpa biaya transaksi Virtual Account (jika diminta)</p>  
                      <p>7. pilih Benar kemudian pilih Bayar</p> 
                      <p>8. simpan struk ATM sebagai bukti pembayaran.</p> 
                </div>
            </li>
            <li >
                <a class="uk-accordion-title" href="#">
                <img src="{{asset('images/internet banking muamalat.png')}}" width="15%" alt="">
                </a>
                <div class="uk-accordion-content">
                      <p>1. masukkan kartu ATM dan PIN</p>  
                      <p>2. pilih menu Transaksi Lain</p>  
                      <p>3. pilih menu Pembayaran, kemudian pilih menu Virtual Account</p>  
                      <p>4. masukkan 16 digit nomor Virtual Account Anda <input type="text" value="{{Auth::user()->va}}" id="myInput"> <a> <span uk-icon="icon: copy" title="Copy VA" onclick="myFunction()"></span></a></p> 
                      <p>5. pilih bayar jika informasi pembayaran telah sesuai</p> 
                      <p>6. masukkan nominal jumlah pembayaran tanpa biaya transaksi Virtual Account (jika diminta)</p>  
                      <p>7. pilih Benar kemudian pilih Bayar</p> 
                      <p>8. simpan struk ATM sebagai bukti pembayaran.</p> 
                </div>
            </li>
            <li >
                <a class="uk-accordion-title" href="#">
                <img src="{{asset('images/counter muamalat.png')}}" width="15%" alt="">
                </a>
                <div class="uk-accordion-content">
                      <p>1. masukkan kartu ATM dan PIN</p>  
                      <p>2. pilih menu Transaksi Lain</p>  
                      <p>3. pilih menu Pembayaran, kemudian pilih menu Virtual Account</p>  
                      <p>4. masukkan 16 digit nomor Virtual Account Anda <input type="text" value="{{Auth::user()->va}}" id="myInput"> <a> <span uk-icon="icon: copy" title="Copy VA" onclick="myFunction()"></span></a></p> 
                      <p>5. pilih bayar jika informasi pembayaran telah sesuai</p> 
                      <p>6. masukkan nominal jumlah pembayaran tanpa biaya transaksi Virtual Account (jika diminta)</p>  
                      <p>7. pilih Benar kemudian pilih Bayar</p> 
                      <p>8. simpan struk ATM sebagai bukti pembayaran.</p> 
                </div>
            </li>
<script>
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>


        </div>
    </li>
    <li>
        <a class="uk-accordion-title" href="#">Selain Bank Muamalat</a>
        <div class="uk-accordion-content">
        <p>1. pilih menu Transfer Antar Bank Online</p> 
         <p>2. isi kode Bank Muamalat 147 diikuti 16 digit nomor Virtual Account – {{Auth::user()->va}}</p> 
         <p>3. masukkan jumlah transaksi sesuai tipe payment; <br><br> (a) Full.jumlah transaksi yang diinput harus sama persis dengan informasi tagihan di layar ATM. <br> (b) Partial dan open: jumlah transaksi yang diinput adalah nominal pembayaran + biaya transaksi Virtual Account,</p> 
         <p>4. pastikan informasi pembayaran telah sesuai</p> 
        </div>
    </li>
    <li>
        <a class="uk-accordion-title" href="#">
        <img src="{{asset('images/paykita.png')}}" width="25%" alt="">
        </a>
        <div class="uk-accordion-content">
            <p>Saldo anda adalah <span class="uk-badge">{{number_format(Auth::user()->saldo)}}</span>
            
            @if(Auth::user()->saldo != 0)
            <a href="{{url('paykita/'.$bill->id)}}"> <button class="uk-button uk-align-right uk-button-primary uk-button-small">Bayar</button></a>
            @else
            <button class="uk-button uk-align-right uk-button-primary uk-button-small demo" type="button" onclick="UIkit.notification({message: 'Anda belum memiliki saldo di Paykita <br> Silahkan isi saldo', status: 'warning'})">Bayar</button>
            @endif
          </p>
            
            <ul uk-accordion>
              <li >
                  <a class="uk-accordion-title" href="#">
                  <img src="{{asset('images/topup paykita.png')}}" width="20%" alt="">
                  </a>
                  <div class="uk-accordion-content">
                        <p>1. masukkan kartu ATM dan PIN</p>  
                        <p>2. pilih menu Transaksi Lain</p>  
                        <p>3. pilih menu Pembayaran, kemudian pilih menu Virtual Account</p>  
                        <p>4. masukkan 16 digit nomor Virtual Account Anda <input type="text" value="{{Auth::user()->va}}" id="myInput"> <a> <span uk-icon="icon: copy" title="Copy VA" onclick="myFunction()"></span></a></p> 
                        <p>5. pilih bayar jika informasi pembayaran telah sesuai</p> 
                        <p>6. masukkan nominal jumlah pembayaran tanpa biaya transaksi Virtual Account (jika diminta)</p>  
                        <p>7. pilih Benar kemudian pilih Bayar</p> 
                        <p>8. simpan struk ATM sebagai bukti pembayaran.</p> 
                  </div>
              </li>
            </ul>
        </div>
    </li>
</ul>


      </div>
    </div>
  @endforeach
  {{$bills->links("pagination::bootstrap-4")}}
  
  @if(count($bills) == 0)
    <div class="uk-card uk-card-default uk-margin">
      <div class="uk-card-body">
        <span uk-icon="info"></span> &nbsp; Anda belum memiliki investasi apapun!
      </div>
    </div>
  @endif

</li>

@endsection


