
@if(isset($session))
@if(($session != true))
  <div id="target" class="uk-cookies-message uk-position-bottom-center uk-position-fixed" style="z-index:99999">
    <div class="uk-padding-small uk-background-primary uk-light">
      <span uk-icon="info"></span>Situs ini menggunakan cookie. Dengan melanjutkan menelusuri situs, Anda menyetujui penggunaan cookie kami
      &nbsp;
      <button class="uk-button uk-button-warning uk-cookies-button">
        <span uk-icon="check"></span>Ya, Silahkan
      </button>
    </div>
    <script>
      $('.uk-cookies-button').click(function(){
        document.getElementById("target").style.display = "none";

        $.ajax({
          type: 'GET',
          url: '{{url("set_cookies_page")}}',
          success: function(response) {
            console.log(response);
            $('.uk-cookies-message').hide();
          }
        })
      });
    </script>
  </div>
@endif
@endif