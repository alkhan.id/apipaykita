
<a class="icon-notif">
  <i uk-icon="icon: bell"></i>
  {% if request.user|get_comments|length > 0 %}
    <span class="uk-badge uk-badge-notification">{{ request.user|get_comments|length }}</span>
  {% endif %}
</a>
<div class="uk-navbar-dropdown uk-width-1-4 uk-padding-small">
  <ul class="uk-nav uk-navbar-dropdown-nav">
    {% for comment in request.user|get_comments %}
      {% if forloop.counter <= 5 %}
        {% if comment.content_type.model == 'teaser' %}
          <li class="item-notif">
            {% if request.user.profile.status == 'it_officer' or request.user.is_superuser %}
              <a class="comment-link" data-read-url="{% url 'app_plaza:comment_read' pk=comment.pk %}" data-href="{% url 'app_management:dashboard_proposal_disqus' slug=comment.content_object.slug %}#comment-{{ comment.id }}">
            {% else %}
              <a class="comment-link" data-read-url="{% url 'app_plaza:comment_read' pk=comment.pk %}" data-href="{% url 'app_plaza:proposal_disqus' slug=comment.content_object.slug %}#comment-{{ comment.id }}">
            {% endif %}
              <div class="uk-grid-collapse uk-child-width-expand@s" uk-grid>
                <div class="uk-width-1-4@m">
                  <img class="uk-comment-avatar" src="{{ comment.sender.email|gravatar:'45' }}">
                </div>
                <div class="uk-width-expand@m">
                  {{ comment.comment|safe|striptags|truncatechars:"50" }}
                </div>
              </div>
            </a>
          </li>
        {% elif comment.content_type.model == 'report' %}
          <li class="item-notif">
            {% if request.user.profile.status == 'relationship_manager' or request.user.is_staff %}
              <a class="comment-link" data-read-url="{% url 'app_plaza:comment_read' pk=comment.pk %}" data-href="{% url 'app_management:dashboard_report_disqus' pk=comment.content_object.pk %}#comment-{{ comment.id }}">
            {% else %}
              <a class="comment-link" data-read-url="{% url 'app_plaza:comment_read' pk=comment.pk %}" data-href="{% url 'app_plaza:report_disqus' pk=comment.content_object.pk %}#comment-{{ comment.id }}">
            {% endif %}
              <div class="uk-grid-collapse uk-child-width-expand@s" uk-grid>
                <div class="uk-width-1-4@m">
                  <img class="uk-comment-avatar" src="{{ comment.sender.email|gravatar:'45' }}">
                </div>
                <div class="uk-width-expand@m">
                  {{ comment.comment|safe|striptags|truncatechars:"50" }}
                </div>
              </div>
            </a>
          </li>
        {% endif %}
      {% endif %}{# forloop.counter <= 5 #}
    {% empty %}
      <li class="item-notif">
        <div class="uk-padding-small uk-margin-remove">
          <span uk-icon="info"></span> {% trans "Tidak ada notifikasi apapun!" %}
        </div>
      </li>
    {% endfor %}
  </ul>
</div>

<script>
  $('.comment-link').click(function(){
    var readUrl = $(this).data('read-url');
    var linkTarget = $(this).data('href');
    $.ajax({
      type: 'GET',
      url: readUrl,
      success: function(){
        location.href = linkTarget;
      }
    });
  });
</script>
