<div class="uk-section-secondary uk-section" uk-height-viewport="expand: true" style="box-sizing: border-box;">
  <div class="uk-container">
    <div class="uk-grid-large uk-grid-margin-large uk-grid" uk-grid>
      <div class="uk-width-expand@l uk-width-1-2@m uk-first-column">
        <div class="uk-margin">
          <a href="/" class="spf-link">
            <img uk-svg src="{{asset('images/logos/svg/logo-full-small.svg')}}" class="el-image" hidden="true" alt="PlazaDana.com">
          </a>
          <p class="uk-text-small uk-text-muted">PT. Plazadana Mitra Investama<br />Copyright &copy; <?php echo date('Y'); ?>; all right reserved.</p>
        </div>
      </div>
      <div class="uk-width-expand@l uk-width-1-2@m">
        <h5 class="uk-text-bold">PlazaDana</h5>
        <ul class="uk-list">
          <li><a class="uk-text-muted spf-link" href="{{url('investor_page')}}">Peluang Investasi (Investor)</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('fundraising_page')}}">Penggalangan Dana</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('about_us_page')}}">Tentang Kami</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('our_team_page')}}">Tim Kami</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('career')}}">Karir</a></li>
        </ul>
      </div>
      <div class="uk-width-expand@l uk-width-1-2@m">
        <h5 class="uk-text-bold">Bantuan</h5>
        <ul class="uk-list">
          <li><a class="uk-text-muted spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('tos_page')}}">Ketentuan Penggunaan</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('risk_warning_page')}}">Peringatan Risiko</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('information_page')}}">Informasi</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('contact_us_page')}}">Kontak</a></li>
          <li><a class="uk-text-muted spf-link" href="{{url('faq_page')}}">FAQs</a></li>
        </ul>
      </div>
      <div class="uk-width-expand@l uk-width-1-2@m uk-inline">
        <h5 class="uk-text-bold">Sosial Media</h5>
        <div class="uk-position-top-right">
          <a uk-totop="" uk-scroll="" class="uk-totop uk-icon"></a>
        </div>
        <div class="uk-social-buttons">
          <a class="uk-icon-button uk-box-shadow-large uk-icon" href="https://facebook.com/plaza.dana.5" target="_blank" uk-icon="icon: facebook" uk-tooltip="pos: bottom; title: Facebook" aria-expanded="false" title=""></a>
          <a class="uk-icon-button uk-box-shadow-large twitter uk-icon" href="https://twitter.com/plazadana" target="_blank" uk-icon="icon: twitter" uk-tooltip="pos: bottom; title: Twitter" aria-expanded="false" title=""></a>
          <a class="uk-icon-button uk-box-shadow-large linkedin uk-icon" href="https://linkedin.com/in/plazadana" target="_blank" uk-icon="icon: linkedin" uk-tooltip="pos: bottom; title: LinkedIn" aria-expanded="false" title=""></a>
          <a class="uk-icon-button uk-box-shadow-large whatsapp uk-icon" href="https://api.whatsapp.com/send?phone=50671124852" target="_blank" uk-icon="icon: whatsapp" uk-tooltip="pos: bottom; title: Whatsapp" aria-expanded="false" title=""></a>
        </div>
        <hr/>
        <p class="uk-text-muted">Jl. Ahmad Yani, <br/>Ruko Bekasi Mas, Blok A, No 10, <br/>Bekasi</p>
      </div>
    </div>
  </div>
</div>
