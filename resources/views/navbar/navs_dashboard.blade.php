<div uk-sticky="media: 960" class="tm-navbar-container uk-sticky uk-sticky-fixed">
  <div class="uk-container uk-container-expand">
    <nav class="uk-navbar" uk-navbar>
      <div class="uk-navbar-left">
        <a href="/" class="uk-navbar-item uk-logo spf-link">
          <img uk-svg src="{{asset('images/logos/svg/logo-full-small.svg')}}" class="uk-margin-small-right" hidden="true">
        </a>
      </div>
      <div class="uk-navbar-right">
        <ul class="uk-navbar-nav uk-visible@m">
          <li><a href="/" class="spf-link">Beranda</a></li>
          <li class="{{ Request::is('investor_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('investor_page')}}">Investor</a>
          </li>
          <li class="{{ Request::is('fundraising_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('fundraising_page')}}">Penggalangan Dana</a>
          </li>
          <li class="{{ Request::is('information_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('information_page')}}">Informasi</a>
          </li>
          <li class="{{ Request::is('our_team_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('our_team_page')}}">Tim Kami</a>
          </li>

          @auth
            <li class="menu-notification">
              <!-- include "includes/notifications.html -->
            </li>
            <li class="menu-features">
              <a><i uk-icon="icon: more-vertical"></i></a>
              <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                <div class="uk-navbar-dropdown-grid uk-child-width-1-2" uk-grid>
                  <div>
                    <!-- it_officer or superuser -->
                    @if(Auth::User()->role == 1 || Auth::User()->role == 2 )
                      <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li class="uk-nav-header">IT Officer</li>
                        <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_proposal')}}">
                             <span>Proposal</span>
                            
                           </a>
                        </li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_investation_list')}}">
                             <span>Investasi</span>
                            
                          </a>
                        </li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_payment_list')}}">
                             <span>Pembayaran</span>
                             
                          </a>
                        </li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_bill_list')}}">
                             <span>Bill</span>                 
                          </a>
                        </li>
                        <li><a class="spf-link" href="{{url('dashboard_cooperative_list')}}">Koperasi</a></li>
                        <li><a class="spf-link" href="{{url('dashboard_user_list')}}">Pengguna</a></li>
                        <li><a class="spf-link" href="{{url('category')}}">Kategori</a></li>
                      </ul>
                      <!-- relationship_manager' or request.user.is_staff  -->  
                    @elseif(Auth::User()->role == 3 || Auth::User()->role == 4)
                      <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li class="uk-nav-header">Relationship Manager</li>
                        <li><a class="spf-link" href="{{ url('dashboard_relationship_manager')}}">Dashboard</a></li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_report_list')}}">
                             <span>Laporan</span>
                            
                          </a>
                        </li>
                      </ul>
                    @else
                      <ul class="uk-nav uk-navbar-dropdown-nav">
                                                <!-- <li class="uk-nav-header">Layanan</li> -->
                          <!-- <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
                          <li><a class="spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li> -->

                        <li class="uk-nav-header">Investor</li>
                        <li><a class="spf-link" href="{{url('teaser_list')}}">Peluang Investasi</a></li>
                        <li>
                          <a class="spf-link uk-position-relative" href="{{url('investation_list')}}">
                            <span>Investasiku</span>
                           
                          </a>
                        </li>
                        <li class="uk-nav-header">Koperasi</li>
                        <li><a class="spf-link" href="{{url('proposal/create')}}">Kirim Proposal</a></li>
                        <li>
                          <a class="spf-link uk-position-relative" href="{{url('proposal')}}">
                            <span>Proposalku</span>
                            
                          </a>
                        </li>
                        <!-- <li>
                          <a class="spf-link uk-position-relative" href="{{url('report_list')}}">
                            <span>Laporanku</span>
                            
                          </a>
                        </li> -->
                        <li><a class="spf-link" href="{{url('cooperative')}}">Koperasiku</a></li>
                        <!-- <li><a class="spf-link" href="{{url('app_user:user_banks_list')}}">Akun Bank</a></li> -->
                      </ul>
                      <!-- </ul> -->
                      <ul class="uk-nav uk-navbar-dropdown-nav">
           <li class="uk-nav-header">User</li>
            <li><a class="spf-link" href="{{url('profile')}}">Profil</a></li>
          </ul>
                    @endif
                  </div>
                  <div>
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                      <!-- user -->
                        @if(Auth::User()->role == 5)

                        <!-- <div class="uk-width-medium-1-4"> -->
                    <div class="uk-panel uk-panel-box uk-text-center">
                        <img class="uk-border-circle" width="120" height="120" src="{{asset('file/'.Auth::user()->photo)}}" alt="">
                        <!-- <h2 class=>{{Auth::user()->name}}</h2> -->
                    </div>
                    <!-- <p class="uk-text-small uk-margin-top-remove uk-text-muted">
                        {{Auth::user()->va}}<br>
                        Saldo : {{Auth::user()->saldo}}
                    </p> -->
          <!-- </div> -->


                        @endif
                        <center>
                      <li class="uk-nav-header">Akun Saya</li>
                      <li>{{Auth::user()->name}}</li>
                      <li>{{Auth::user()->va}}</li>
                      <li>{{Auth::user()->email}}</li>
                      <li>Saldo : Rp {{Auth::user()->saldo}}</li>
                      <li class="uk-nav-divider"></li>
                      </center>
                      <li>


                      <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>


                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
          @endauth
        </ul>

        @guest
          <div class="uk-navbar-item uk-visible@m">
            <a href="{{url('account_login')}}" class="uk-button uk-button-default tm-button-default uk-icon">
              Masuk / Daftar <canvas uk-icon="icon: sign-in" width="20" height="20" class="uk-icon" hidden="true"></canvas>
            </a>
          </div>
        @endguest

        <a uk-navbar-toggle-icon="" href="#offcanvas" uk-toggle="" class="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon">
          <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
            <rect y="9" width="20" height="2"></rect>
            <rect y="3" width="20" height="2"></rect>
            <rect y="15" width="20" height="2"></rect>
          </svg>
        </a>
      </div>
    </nav>
  </div>

  <div id="offcanvas" uk-offcanvas="mode: push; overlay: true" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
      <div class="uk-panel">
        <ul class="uk-nav uk-nav-default tm-nav">
          <li class="uk-nav-header">Umum</li>
          <li><a href="/" class="spf-link">Beranda</a></li>
          <li><a class="spf-link" href="{{url('investor_page')}}">Investor</a></li>
          <li><a class="spf-link" href="{{url('fundraising_page')}}">Penggalangan Dana</ali>
          <li><a class="spf-link" href="{{url('information_page')}}">Informasi</a></li>
          <li><a class="spf-link" href="{{url('our_team_page')}}">Tim Kami</a></li>
          <li><a class="spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li>
        </ul>

        @auth
          <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">IT Officer</li>
              <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_proposal')}}">
                   <span>Proposal</span>
                  
                 </a>
              </li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_investation_list')}}">
                   <span>Investasi</span>
                   
                </a>
              </li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_payment_list')}}">
                   <span>Pembayaran</span>
                  
                </a>
              </li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_bill_list')}}">
                   <span>Bill</span>
                  
                </a>
              </li>
              <li><a class="spf-link" href="{{url('dashboard_cooperative_list')}}">Koperasi</a></li>
              <li><a class="spf-link" href="{{url('dashboard_user_list')}}">Pengguna</a></li>
              <li><a class="spf-link" href="{{url('category')}}">Kategori</a></li>
            </ul>
            @elseif(Auth::User()->role == 3 || Auth::User()->role == 4)
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">Relationship Manager</li>
              <li><a class="spf-link" href="{{ url('dashboard_relationship_manager')}}">Dashboard</a></li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_report_list')}}">
                   <span>Laporan</span>
                </a>
              </li>
            </ul>
          @else
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">Layanan</li>
              <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
              <li><a class="spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li>
              <li class="uk-nav-header">Investor</li>
              <li><a class="spf-link" href="{{url('teaser_list')}}">Peluang Investasi</a></li>
              <li>
                <a class="spf-link uk-position-relative" href="{{url('investation_list')}}">
                  <span>Investasiku</span>
                  {% if request.user|get_total_pending_investations > 0 %}
                    <span class="uk-badge uk-badge-notification-navbar uk-position-right">
                      {{ request.user|get_total_pending_investations }}
                    </span>
                  {% endif %}
                </a>
              </li>
              <li class="uk-nav-header">Koperasi</li>
              <li><a class="spf-link" href="{{url('proposal/create')}}">Kirim Proposal</a></li>
              <li>
                <a class="spf-link uk-position-relative" href="{{url('porposal')}}">
                  <span>Proposalku</span>
                </a>
              </li>
              <!-- <li>
                <a class="spf-link uk-position-relative" href="{{url('report_list')}}">
                  <span>Laporanku</span>
                  {% if request.user|get_total_pending_reports > 0 %}
                    <span class="uk-badge uk-badge-notification-navbar uk-position-right">
                      {{ request.user|get_total_pending_reports }}
                    </span>
                  {% endif %}
                </a>
              </li> -->
              <li><a class="spf-link" href="{{url('cooperative')}}">Koperasiku</a></li>
              <!-- <li><a class="spf-link" href="{{url('app_user:user_banks_list')}}">Akun Bank</a></li> -->
            </ul>
            <!-- </ul> -->
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('profile')}}"><span uk-icon="icon:user;ratio:.8" class="uk-margin-small-right"></span>Profil</a></li>
            <li><a class="spf-link" href="{{url('history')}}"><span uk-icon="icon:user;ratio:.8" class="uk-margin-small-right"></span>History</a></li>
          </ul>
          {% endif %}

          <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('user_profile_edit')}}">Profil</a></li>
            <li><a class="spf-link" href="{{url('account_email')}}">Email</a></li>
            <li><a class="spf-link" href="{{url('account_change_password')}}">Password</a></li>
            <li class="uk-nav-divider"></li>
            <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

            </li>
          </ul>
        @endauth
        @guest
          <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('account_login')}}">Masuk</a></li>
            <li><a class="spf-link" href="{{url('account_signup')}}">Daftar</a></li>
          </ul>
        @endguest
      </div>
    </div>
  </div>


  @auth
    <div class="tm-sidebar-left uk-visible@m uk-background-cover uk-sidebar-dashboard">

    @if(Auth::User()->role == 1 || Auth::User()->role == 2 )
        <h3><a class="spf-link uk-remove-underline" href="{{url('dashboard_it_officer')}}">Dashboard IT</a></h3>
        <ul class="uk-nav uk-nav-default tm-nav">
          <li class="list-teaser-me {{ Request::is('dashboard_proposal') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('dashboard_proposal')}}">
              <span uk-icon="icon:copy;ratio:.8" class="uk-margin-small-right"></span> Proposal
            </a>
          </li>
          <li class="list-invest-me {{ Request::is('dashboard_investation_list') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('dashboard_investation_list')}}">
              <span uk-icon="icon:paint-bucket;ratio:.8" class="uk-margin-small-right"></span> Investasi
            </a>
          </li>
          <li class="{{ Request::is('dashboard_payment_list') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('dashboard_payment_list')}}">
              <span class="uk-margin-small-right" style="padding:0 4px">$</span> Pembayaran
            </a>
          </li>
          <li class="{{ Request::is('dashboard_bill_list') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('dashboard_bill_list')}}">
              <span class="uk-margin-small-right" style="padding:0 4px">$</span> Bill
            </a>
          </li>
          <li class="list-cooperative-edit {{ Request::is('dashboard_cooperative_list') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('dashboard_cooperative_list')}}">
              <span uk-icon="icon:location;ratio:.8" class="uk-margin-small-right"></span> Koperasi
            </a>
          </li>
          <li class="{{ Request::is('dashboard_user_list') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('dashboard_user_list')}}">
              <span uk-icon="icon:users;ratio:.8" class="uk-margin-small-right"></span> Pengguna
            </a>
          </li>
          <li class="{{ Request::is('category') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('category')}}">
              <span uk-icon="icon:tag;ratio:.8" class="uk-margin-small-right"></span> Kategori
            </a>
          </li>
          <li class="{{ Request::is('message') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('message')}}">
              <span uk-icon="icon:mail;ratio:.8" class="uk-margin-small-right"></span> Pesan
            </a>
          </li>
        </ul>
        @elseif(Auth::User()->role == 3 || Auth::User()->role == 4)
        <h3><a class="spf-link uk-remove-underline" href="{{ url('dashboard_relationship_manager')}}">Dashboard RM</a></h3>
        <ul class="uk-nav uk-nav-default tm-nav">
          <li class="list-report-me">
            <a class="spf-link uk-position-relative" href="{{url('dashboard_report_list')}}">
              <span uk-icon="icon:calendar;ratio:.8" class="uk-margin-small-right"></span> Laporan
            </a>
          </li>
        </ul>
      @else
        <!-- <h3><a class="spf-link uk-remove-underline" href="{{url('user_dashboard')}}">Dashboard</a></h3> -->
        
        <ul class="uk-nav uk-nav-default tm-nav">
        <li class="uk-nav-header">Layanan</li>
          <li class="list-invest-me {{ Request::is('user_dashboard') ? 'uk-active' : '' }}">
          <a class="spf-link uk-position-relative" href="{{url('user_dashboard')}}">
          <span uk-icon="icon:home;ratio:.8" class="uk-margin-small-right"></span>Dashboard</a>
          </li>
          <li class="list-invest-me {{ Request::is('help_page') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('help_page')}}">
              <span uk-icon="icon:info;ratio:.8" class="uk-margin-small-right"></span> Bantuan Pengguna
            </a>
          </li>
          <li class="uk-nav-header">Investor</li>
          <li class="list-invest-me {{ Request::is('teaser_list') ? 'uk-active' : '' }}">
          <a class="spf-link uk-position-relative" href="{{url('teaser_list')}}">
          <span uk-icon="icon:paint-bucket;ratio:.8" class="uk-margin-small-right"></span>Peluang Investasi</a>
          </li>
          <li class="list-invest-me {{ Request::is('investation_list') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('investation_list')}}">
              <span uk-icon="icon:paint-bucket;ratio:.8" class="uk-margin-small-right"></span> Investasiku
            </a>
          </li>
          <li class="uk-nav-header">Koperasi</li>
          <li class="list-teaser-submit {{ Request::is('proposal/create') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('proposal/create')}}">
              <span uk-icon="icon:forward;ratio:.8" class="uk-margin-small-right"></span> Kirim Proposal
            </a>
          </li>
          <li class="list-teaser-me {{ Request::is('proposal') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('proposal')}}">
              <span uk-icon="icon:copy;ratio:.8" class="uk-margin-small-right"></span> Proposalku
            </a>
          </li>
          <!-- <li class="list-report-me {{ Request::is('report_list') ? 'uk-active' : '' }}">
            <a class="spf-link uk-position-relative" href="{{url('report_list')}}">
              <span uk-icon="icon:calendar;ratio:.8" class="uk-margin-small-right"></span> Laporanku
            </a>
          </li> -->
          <li class="list-cooperative-edit {{ Request::is('cooperative_edit') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('cooperative')}}">
              <span uk-icon="icon:location;ratio:.8" class="uk-margin-small-right"></span> Koperasiku
            </a>
          </li>
          <!-- <li class="list-banks-list {{ Request::is('user_banks_list') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('app_user:user_banks_list')}}">
              <span uk-icon="icon:credit-card;ratio:.8" class="uk-margin-small-right"></span> Akun Bank
            </a>
          </li> -->
        </ul>
        <!-- </ul> -->
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('profile')}}"><span uk-icon="icon:user;ratio:.8" class="uk-margin-small-right"></span>Profil</a></li>
            <li><a class="spf-link" href="{{url('history')}}"><span uk-icon="icon:file;ratio:.8" class="uk-margin-small-right"></span>History</a></li>
          </ul>
      @endif

      <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
        <!-- <li class="uk-nav-header">Akun saya</li>
        <li class="{{ Request::is('user_profile_edit') ? 'uk-active' : '' }}">
          <a class="spf-link" href="{{url('user_profile_edit')}}">
            <span uk-icon="icon:user;ratio:.8" class="uk-margin-small-right"></span> Profil
          </a>
        </li>
        <li class="{{ Request::is('account_email') ? 'uk-active' : '' }}">
          <a class="spf-link" href="{{url('account_email')}}">
            <span uk-icon="icon:mail;ratio:.8" class="uk-margin-small-right"></span> E-Mail
          </a>
        </li>
        <li class="{{ Request::is('account_change_password') ? 'uk-active' : '' }}">
          <a class="spf-link" href="{{url('account_change_password')}}">
            <span uk-icon="icon:lock;ratio:.8" class="uk-margin-small-right"></span> Password</a>
        </li> -->
        <li class="{{ Request::is('account_logout') ? 'uk-active' : '' }}">

        <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>


        </li>
      </ul>
<!-- 
      <div class="uk-position-small uk-position-bottom-left">
        <a href="{{url('help_page')}}" class="uk-icon-button" uk-icon="info"
          uk-tooltip="title: Bantuan Pengguna; pos: right"></a>
      </div> -->
    </div>
  @endauth
</div>
