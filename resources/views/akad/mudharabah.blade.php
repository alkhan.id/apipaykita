<p align="justify"><strong>AKAD MUDHARABAH</strong></p>

<p align="justify"><strong>Antara PT Plazadana Mitra Investama dengan Koperasi</strong></p>
<!-- <p align="justify">No :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p> -->
<p align="justify">Pada hari ini, {{ date('d M Y') }}, yang bertandatangan di bawah&nbsp;&nbsp; ini:</p>
<p>1. Nama : Mamun Zuberi.</p>
<p align="justify">bertindak dalam&nbsp; jabatannya selaku Direktur PT Plazadana Mitra Investama, bertindak untuk dan atas nama serta mewakili PIHAK PERTAMA. berkedudukan di Bekasi Selatan, Komplek Ruko Bekasi Mas No.10 Blok A, Jalan Jenderal Ahmad Yani berdasarkan Akta Pendirian Nomor. 58 yang telah mendapatkan pengesahan Menteri Kehakiman Nomor AHU 0011077.AH.01.01 Tahun 2019 Notaris Irene Kusumawardhani SH di Bekasi. beserta perubahan-perubahannya yang terakhir, sebagai &ldquo;<em>Wakil Shohibul mal/</em>INVESTOR&rdquo;. Selanjutnya disebut &rdquo;<strong>PIHAK PERTAMA</strong>&rdquo;.</p>
<p>2. Nama : {{$teaser->user->name}}.</p>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bertindak dalam jabatannya selaku Ketua Pengurus PIHAK KEDUA terpilih dan merupakan pemegang Kuasa ANGGOTA dari dan karenanya, bertindak untuk dan atas nama serta mewakili seluruh ANGGOTA yang tergabung dalam PIHAK KEDUA berkedudukan di {{$teaser->cooperative->address}} sebagai <em>&ldquo;Mudharib&rdquo;</em> Selanjutnya disebut &rdquo;<strong>PIHAK KEDUA</strong>&rdquo;.</p>
<p align="justify">&nbsp;</p>
<p align="justify">PIHAK PERTAMA dan PIHAK KEDUA, selanjutnya bersama-sama disebut &ldquo;<strong>Para Pihak</strong>&rdquo;, terlebih dahulu menerangkan bahwa:</p>
<ol>
<li>PIHAK KEDUA memiliki peluang investasi dan menawarkannya kepada PIHAK PERTAMA sebagai <em>shahibul maal</em> untuk berpartisipasi dalam membiayai peluang investasi milik PIHAK KEDUA</li>
<li>PIHAK PERTAMA berminat untuk berpartisipasi dalam membiayai peluang investasi milik PIHAK KEDUA yang ditawarkan kepada PIHAK PERTAMA</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify">Selanjutnya, Para Pihak sepakat untuk membuat dan menandatangani <strong>Akad Mudharabah</strong> (selanjutnya disebut &rdquo;<strong>Akad</strong>&rdquo;) ini untuk dipatuhi dan dilaksanakan oleh Para Pihak dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut:</p>
<p align="justify"><strong><em>BISMILLAHIRRAHMAANIRRAHIM</em></strong></p>
<p align="justify">&ldquo;Dengan nama ALLAH yang Maha Pengasih dan Penyayang&rdquo;</p>
<p align="justify"><em>&ldquo;Hai orang-orang yang beriman, janganlah kamu mengkhianati Allah dan Rasul dan juga janganlah kamu mengkhianati amanah-amanah yang dipercayakan kepada kamu, sedang kamu mengetahui&rdquo;</em></p>
<p align="justify">(QS. Al-Anfaal: 27).</p>
<h1>Pasal 1</h1>
<p align="justify"><strong>DEFINISI</strong></p>
<p align="justify">Dalam Akad ini, yang dimaksud dengan:</p>
<ol>
<li><em>Akad mudharabah </em>adalah akad kerja sama suatu usaha arrtara pemilik modal<em> (malik/shahib al-ma[)</em> yang menyediakan seluruh modal dengan pengelola<em> ('amil/mudharib)</em> dan keuntungan usaha dibagi di antara mereka sesuai nisbah yang disepakati dalam akad.</li>
<li><em>Shahib al-mal/malik </em>adalah pihak penyedia dana dalam usaha kerja sama usaha mudharabah<em>, </em>baik berupa orang<em> (Syakhshiyah thabi'iyah/natuurlijke persoon) </em>maupun yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum <em>(Syakhshiyah i'tibariah /syakhshiyah hukrniyah/rechtsperson).</em></li>
<li><em>'Amil/mudharib</em> adalah pihak pengelola dana dalam usaha kerja sama usaha mudharabah, baik berupa orang <em>(syakhshiyah thabi'iyah/natuurlijke persoon)</em> maupun yang disamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum <em>(syakhshiyah i'tibariah/syakhshiyah hukmiyah/ rechtsperson).</em></li>
<li><em>Ra's mal al-mudharabah</em> (modal) adalah sejumlah dana dan atau barang yang terlebih dahulu dinilai dengan uang berdasarkan nilai pasar atau wajar pada saat penyerahan, yang diberikan oleh PIHAK PERTAMA kepada PIHAK KEDUA untuk menjalankan usaha kerjasama sebagaimana penawaran yang diajukan PIHAK KEDUA kepada PIHAK PERTAMA.</li>
<li>Nisbah bagi hasil adalah nisbah atau perbandingan yang dinyatakan dengan angka seperti persentase untuk membagi hasil usaha.</li>
<li>Keuntungan usaha <em>(ar-ribh)</em> mudharabah adalah pendapatan usaha berupa pertambahan dari investasi setelah dikurangi modal, atau modal dan biaya-biaya.</li>
<li>Kerugian usaha (al-khasarah) mudharabah adalah hasil usaha, di mana jumlah modal usaha yang diinvestasikan mengalami penurunan atau jumlah modal dan biaya-biaya melebihi jumlah pendapatan</li>
<li><strong>Agunan </strong>adalah barang bergerak maupun barang tidak bergerak yang diserahkan oleh pemilik Agunan kepada PIHAK PERTAMA guna menjamin pelunasan Kewajiban PIHAK KEDUA.</li>
<li><strong>Bagi Hasil </strong>adalah pembagian hasil usaha yang dihitung dari pendapatan dalam satu bulan takwim atau periode tertentu yang disepakati oleh Para Pihak.</li>
<li><strong>Cidera Janji</strong> adalah peristiwa atau peristiwa-peristiwa sebagaimana dimaksud Pasal 11 Akad ini, yang menyebabkan PIHAK PERTAMA dapat menghentikan seluruh atau sebagian dari isi Akad ini, menagih seketika dan sekaligus jumlah kewajiban PIHAK KEDUA kepada PIHAK PERTAMA sebelum jangka waktu Akad ini berakhir.</li>
<li><strong>Dokumen Agunan </strong>adalah segala macam dan bentuk surat bukti tentang kepemilikan atau hak-hak lainnya atas barang yang dijadikan Agunan bagi terlaksananya Kewajiban PIHAK KEDUA terhadap PIHAK PERTAMA berdasarkan Akad ini.</li>
<li><strong>Harga Pokok Penjualan </strong>adalah seluruh dana atau setara kas yang dikeluarkan untuk memperoleh asset sampai asset tersebut dalam suatu tempat siap untuk dijual/digunakan.</li>
<li><strong>Hari kerja </strong>adalah setiap hari, kecuali Sabtu, Minggu dan hari libur resmi lainnya yang ditetapkan oleh pemerintah Republik Indonesia, dimana PIHAK PERTAMA-PIHAK PERTAMA buka di seluruh Indonesia untuk melaksanakan kegiatan usaha dan menjalankan transaksi kliring.</li>
<li><strong>Jaminan</strong> adalah Agunan dan atau segala sesuatu yang berwujud maupun tidak berwujud untuk mendukung keyakinan PIHAK PERTAMA atas kemampuan dan kesanggupan PIHAK KEDUA untuk melunasi Kewajiban PIHAK KEDUA sesuai Akad.</li>
<li><strong>Kewajiban PIHAK KEDUA</strong> adalah segala sesuatu yang berkaitan dengan pengembalian modal yang menjadi hak PIHAK PERTAMA oleh PIHAK KEDUA, Bagi Hasil yang sudah menjadi hak PIHAK PERTAMA namun belum dibayar PIHAK KEDUA, biaya-biaya yang menjadi beban PIHAK KEDUA dan Kewajiban sebagaimana dimaksud dalam Pasal 7 Akad ini.</li>
<li><strong><em>Revenue Sharing </em></strong>adalah bagi hasil yang dihitung dari pendapatan usaha PIHAK KEDUA.</li>
<li><strong>Nisbah Bagi Hasil</strong> adalah perbandingan pembagian hasil usaha dari kerjasama antara PIHAK PERTAMA dan PIHAK KEDUA yang ditetapkan berdasarkan Akad ini.</li>
<li><strong>Pembiayaan Mudharabah atau Pembiayaan </strong>adalah pembiayaan dalam rangka kerjasama suatu usaha antara dua pihak dimana PIHAK PERTAMA selaku pemilik seluruh dana <em>(<strong>Shahibul mal</strong>) </em>dan PIHAK KEDUA selaku pengelola modal <em>(<strong>Mudharib</strong>)</em> dengan <strong><em>Nisbah</em></strong> bagi hasil yang disepakati dalam Akad ini.</li>
<li><strong>Perjanjian Jaminan </strong>adalah perjanjian pengikatan atas jaminan yang diserahkan oleh PIHAK KEDUA dan/atau pemilik jaminan/ penjamin kepada PIHAK PERTAMA baik yang dibuat dalam akta otentik maupun akta dibawah tangan.</li>
<li><strong>Rekening PIHAK KEDUA </strong>adalah rekening giro/ rekening Koran/rekening virtual atas nama PIHAK KEDUA yang ada pada PIHAK PERTAMA sebagai tempat penampungan dana yang akan digunakan untuk pelaksanaan transaksi keuangan sehubungan dengan pemberian Pembiayaan oleh PIHAK PERTAMA.</li>
<li><strong>Tanda Terima Modal</strong> adalah bukti penerimaan Modal baik berupa uang maupun barang oleh PIHAK KEDUA.</li>
<li>Hari kerja adalah setiap hari, kecuali Sabtu, Minggu dan hari libur resmi lainnya yang ditetapkan oleh pemerintah Republik Indonesia.</li>
</ol>
<h1>Pasal 2</h1>
<h6>POKOK AKAD DAN JANGKA WAKTU</h6>
<p align="justify">&nbsp;</p>
<ol>
<li>PIHAK PERTAMA berjanji dan dengan ini mengikatkan diri untuk menyediakan Pembiayaan Mudharabah kepada PIHAK KEDUA secara sekaligus atau bertahap sesuai dengan permintaan PIHAK KEDUA yang semata-mata akan dipergunakan untuk&nbsp; tujuan usaha sesuai dengan rencana kerja yang disiapkan oleh PIHAK KEDUA yang disetujui PIHAK PERTAMA, yang dilampirkan pada dan karenanya merupakan satu kesatuan yang tak terpisahkan dari Akad ini.</li>
<!-- <li>Jangka waktu Pembiayaan Mudharabah berlangsung selama &hellip;&hellip;. (&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.) bulan, terhitung mulai tanggal .............................. sampai dengan tanggal ................................... dan dengan demikian, PIHAK KEDUA wajib mengembalikan Modal PIHAK PERTAMA pada akhir jangka waktu Akad ini.</li> -->
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 3</strong></p>
<p align="justify">SYARAT REALISASI</p>
<p align="justify">&nbsp;</p>
<ol>
<li>Dengan tetap memperhatikan batasan-batasan dan ketentuan-ketentuan lain di dalam Akad ini maupun yang ditetapkan oleh pihak yang berwenang, PIHAK PERTAMA berjanji dan mengikat diri untuk melaksanakan realisasi pembiayaan, setelah PIHAK KEDUA memenuhi seluruh persyaratan sebagai berikut:</li>
</ol>
<ol>
<li>menyerahkan kepada PIHAK PERTAMA seluruh dokumen yang disyaratkan oleh PIHAK PERTAMA termasuk namun tidak terbatas pada dokumen legalitas PIHAK KEDUA, Tanda Terima Modal, Surat Kuasa Debet, Dokumen Agunan dan atau surat lainnya yang berkaitan dengan Akad ini dan dokumen pengikatan Agunan, yang ditentukan dalam Surat Persetujuan Prinsip Pembiayaan dari PIHAK PERTAMA yang telah ditandatangani oleh PIHAK KEDUA;</li>
<li>menandatangani Akad ini dan akad pengikatan Agunan yang disyaratkan oleh PIHAK PERTAMA;</li>
<li>melunasi biaya-biaya dan hal-hal yang disyaratkan oleh PIHAK PERTAMA sebagaimana tercantum dalam Surat Persetujuan Prinsip Pembiayaan dan yang terkait dengan pembuatan Akad ini;</li>
<li>menyerahkan laporan dan rencana kerja termasuk perhitungan usaha yang dibiayai dengan Pembiayaan Mudharabah berdasarkan Akad ini, secara periodik pada tiap-tiap tanggal ......................dan selanjutnya disepakati Para Pihak.</li>
</ol>
<ol start="2">
<li>PIHAK PERTAMA sewaktu-waktu berhak (atas kebijaksanaan PIHAK PERTAMA sendiri) untuk mengurangi pagu/ plafon Pembiayaan Mudharabah dan atau membatalkan tanpa syarat Pembiayaan Mudharabah dengan semata-mata menurut pertimbangan PIHAK PERTAMA:
<ol>
<li>Bahwa Jaminan yang disediakan PIHAK KEDUA dan/ atau pemilik Jaminan/penjamin nilainya tidak mencukupi lagi dan PIHAK KEDUA atau pemilik Jaminan tidak bersedia menambah jaminan, pertimbangan mana dilakukan dengan mengacu kepada kebijakan PIHAK PERTAMA dan peraturan/ketentuan terutama peraturan/ ketentuan tentang manajemen risiko yang ditetapkan oleh OJK.</li>
<li>Bahwa kondisi/ kualitas Pembiayaan Mudharabah yang diperoleh PIHAK KEDUA dari PIHAK PERTAMA atau pembiayaan lainnya menurun menjadi kurang lancar, diragukan atau macet</li>
</ol>
</li>
<li>[lain-lain disesuaikan dengan Surat Persetujuan Prinsip Pembiayaan]</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>Pasal 4</strong></p>
<h1>STANDAR PERLAKUAN</h1>
<p align="justify">&nbsp;</p>
<p align="justify">PIHAK KEDUA sebagai Pengelola Modal, wajib melakukan kegiatan usaha Mudharabah sesuai dengan:</p>
<ol>
<li>Ketentuan Syariah.</li>
<li>seluruh hukum dan Ketentuan Peraturan Perundang-undangan yang berlaku;</li>
<li>tingkat keahlian dan kepedulian bahwa kegiatan usaha dan Modal Mudharabah akan diperlakukan seperti aset sendiri</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 5</strong></p>
<h6>PEMBAGIAN HASIL USAHA</h6>
<p align="justify">&nbsp;</p>
<ol>
<li>PIHAK PERTAMA dan PIHAK KEDUA sepakat, dan dengan ini mengikatkan diri satu terhadap yang lain, bahwa Nisbah Bagi Hasil untuk masing-masing pihak adalah &hellip;&hellip;&hellip;% (&hellip;&hellip;&hellip;. persen) untuk PIHAK KEDUA dan &hellip;..% (&hellip;&hellip;&hellip; persen) untuk PIHAK PERTAMA didasarkan pada prinsip <em>revenue sharing</em>.</li>
<li>PIHAK PERTAMA dan PIHAK KEDUA juga sepakat, dan dengan ini saling mengikatkan diri satu terhadap yang lain, bahwa pelaksanaan perhitungan Bagi Hasil akan dilakukan pada setiap periode ...........dan bagi hasil tersebut wajib dibayar paling lambat setiap tanggal .......................setiap ....... bulannya.</li>
<li>PIHAK PERTAMA berjanji dan dengan ini mengikatkan diri untuk menanggung kerugian finansial yang timbul dalam pelaksanaan Akad ini, kecuali apabila kerugian tersebut terjadi karena ketidakjujuran dan/atau Cidera Janji PIHAK KEDUA khususnya sebagaimana dimaksud Pasal 11 dan Pasal 13 Akad ini, dan/atau pelanggaran yang dilakukan PIHAK KEDUA atas syarat-syarat sebagaimana dimaksud Pasal 14 Akad ini.</li>
<li>PIHAK PERTAMA akan mengakui terjadinya kerugian sebagaimana dimaksud ayat 3 Pasal ini, apabila PIHAK PERTAMA telah menerima dan menilai kembali segala perhitungan yang dibuat dan disampaikan oleh PIHAK KEDUA kepada PIHAK PERTAMA, dan PIHAK PERTAMA telah menyerahkan hasil penilaiannya tersebut secara tertulis kepada PIHAK KEDUA.</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 6</strong></p>
<p align="justify"><strong>TATA CARA PEMBAYARAN KEWAJIBAN</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<ol>
<li>PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk mengembalikan kepada PIHAK PERTAMA, seluruh jumlah pembiayaan pokok atau Modal (1) paling lambat pada tanggal berakhirnya Akad ini (2) sesuai dengan jadwal yang disepakati dalam lampiran Akad ini dan menjadi satu kesatuan yang tidak dapat dipisahkan dari Akad ini.</li>
<li>Dalam hal PIHAK KEDUA membayar kembali atau melunasi pembiayaan yang diberikan oleh PIHAK PERTAMA lebih awal dari waktu yang diperjanjikan, maka tidak berarti pembayaran tersebut akan menghapuskan atau mengurangi bagian Bagi Hasil yang menjadi hak PIHAK PERTAMA.</li>
<li>Setiap pembayaran atas Kewajiban PIHAK KEDUA, wajib dilakukan PIHAK KEDUA pada Hari Kerja PIHAK PERTAMA dan jam buka kas di kantor PIHAK PERTAMA atau tempat lain yang ditunjuk oleh PIHAK PERTAMA dan dibayarkan melalui rekening yang dibuka oleh dan atas nama PIHAK KEDUA pada PIHAK PERTAMA, sehingga dalam hal pembayaran diterima oleh PIHAK PERTAMA setelah jam kerja PIHAK PERTAMA, maka pembayaran tersebut akan dibukukan pada keesokan harinya dan apabila hari tersebut bukan Hari Kerja PIHAK PERTAMA, pembukuan akan dilakukan pada Hari Kerja PIHAK PERTAMA yang pertama setelah pembayaran diterima.</li>
<li>Semua pembayaran atau pembayaran kembali atas Kewajiban PIHAK KEDUA, biaya-biaya dan lain-lain jumlah uang yang terutang oleh PIHAK KEDUA kepada PIHAK PERTAMA sehubungan dengan fasilitas Pembiayaan Mudharabah, wajib dilakukan oleh PIHAK KEDUA dalam mata uang yang sama dengan mata uang Kewajiban PIHAK KEDUA apabila tidak ada pemberitahuan tertulis dari PIHAK PERTAMA untuk membayar dalam mata uang yang lain,&nbsp; di kantor PIHAK PERTAMA dimana Rekening PIHAK KEDUA dibuka atau di kantor atau tempat lainnya yang akan diberitahukan oleh PIHAK PERTAMA kepada PIHAK KEDUA secara tertulis.</li>
<li>Dalam hal tanggal jatuh tempo atau saat pembayaran Kewajiban jatuh tidak pada Hari Kerja PIHAK PERTAMA, maka PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk melakukan pembayaran kepada PIHAK PERTAMA pada 1 (satu) Hari Kerja sebelumnya.</li>
<li>Pembayaran kembali oleh PIHAK KEDUA atas pelunasan Pembiayaan Mudharabah kepada PIHAK PERTAMA, dilakukan dengan cara mendebet Rekening PIHAK KEDUA pada PIHAK PERTAMA. Sehubungan dengan hal tersebut, maka PIHAK KEDUA dengan ini sekarang untuk nanti pada waktunya, memberi kuasa kepada PIHAK PERTAMA untuk mendebet Rekening PIHAK KEDUA pada PIHAK PERTAMA atas kewajiban pembayaran PIHAK KEDUA pada PIHAK PERTAMA, dan PIHAK PERTAMA dengan ini menerima kuasa tersebut. Kuasa tersebut diberikan oleh PIHAK KEDUA kepada PIHAK PERTAMA dengan hak subtitusi dan tidak dapat berakhir karena sebab-sebab apapun termasuk namun tidak terbatas pada sebab-sebab yang ditentukan dalam Pasal 1813, 1814 dan 1816 Kitab Undang-Undang Hukum Perdata untuk mendebet rekening PIHAK KEDUA dari waktu ke waktu guna pembayaran seluruh kewajiban yang timbul sehubungan dengan Kewajiban PIHAK KEDUA, termasuk tetapi tidak terbatas pada pembayaran Bagi Hasil yang menjadi hak PIHAK PERTAMA, pembayaran kembali Modal PIHAK PERTAMA, biaya-biaya, denda dan ganti rugi.</li>
<li>Semua pembayaran atau pembayaran kembali atas Kewajiban PIHAK KEDUA, biaya-biaya dan lain-lain jumlah uang yang terutang oleh PIHAK KEDUA kepada PIHAK PERTAMA sehubungan dengan Pembiayaan Mudharabah adalah bebas dan tanpa pengurangan atau pemotongan untuk pajak-pajak, biaya-biaya, pungutan-pungutan atau beban-beban apapun juga yang dikenakan oleh instansi perpajakan yang berwenang.</li>
<li>PIHAK KEDUA tidak diperbolehkan membayar kewajibannya kepada PIHAK PERTAMA dengan jalan menjumpakan atau memperhitungkan (kompensasi) dengan tagihan, tuntutan/klaim PIHAK KEDUA kepada PIHAK PERTAMA bila ada, dan PIHAK KEDUA juga tidak diperbolehkan menuntut suatu pembayaran lain (<em>counter claim</em>) kepada PIHAK PERTAMA. Untuk hal tersebut, PIHAK KEDUA dengan ini melepaskan seluruh haknya sebagaimana dimaksud dalam pasal 1425 sampai dengan pasal 1429 Kitab Undang-Undang Hukum Perdata.</li>
<li>Pembukuan PIHAK PERTAMA merupakan bukti sah dan mengikat terhadap PIHAK KEDUA mengenai transaksi PIHAK KEDUA dengan PIHAK PERTAMA, termasuk namun tidak terbatas pada jumlah Kewajiban pokok, denda dan biaya-biaya lain-lain yang mungkin timbul karena fasilitas Pembiayaan Mudharabah yang diberikan oleh PIHAK PERTAMA kepada PIHAK KEDUA dan wajib dibayar oleh PIHAK KEDUA kepada PIHAK PERTAMA<strong>, </strong>demikian tanpa mengurangi hak PIHAK KEDUA untuk setelah membayar seluruh Kewajiban meminta pembayaran kembali dari PIHAK PERTAMA atas jumlah yang ternyata kelebihan dibayar (jika ada) oleh PIHAK KEDUA kepada PIHAK PERTAMA<strong>. </strong>Untuk kelebihan pembayaran tersebut PIHAK KEDUA tidak berhak meminta ganti rugi apapun dari PIHAK PERTAMA.</li>
</ol>
<p align="justify">Pasal 7</p>
<p align="justify">BIAYA, POTONGAN DAN PAJAK</p>
<p align="justify">&nbsp;</p>
<ol>
<li>PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk menanggung dan membayar biaya-biaya berupa antara lain:</li>
<li>Biaya Administrasi harus dibayar pada saat akad ditandatangani; dan</li>
<li>Biaya-biaya lain yang timbul berkenaan dengan pelaksanaan Akad termasuk tetapi tidak terbatas pada biaya Notaris/PPAT, premi asuransi, dan biaya pengikatan Jaminan;</li>
<li>Dalam hal PIHAK KEDUA Cidera Janji sehingga PIHAK PERTAMA perlu menggunakan jasa advokat untuk menagihnya, maka PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk membayar seluruh biaya jasa advokat, jasa penagihan dan jasa-jasa lainnya sepanjang hal itu dapat dibuktikan secara sah menurut hukum.</li>
<li>Setiap pembayaran/ pelunasan Kewajiban PIHAK KEDUA sehubungan dengan Akad ini dan/ atau perjanjian lain yang terkait dengan Akad ini, dilakukan oleh PIHAK KEDUA kepada PIHAK PERTAMA tanpa potongan, pungutan, bea, pajak dan/ atau biaya-biaya lainnya, kecuali jika potongan, pungutan, bea, pajak dan/ atau biaya-biaya lainnya tersebut diharuskan berdasarkan peraturan perundang-undangan yang berlaku.</li>
<li>PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk membayar melalui PIHAK PERTAMA, setiap potongan yang diharuskan oleh peraturan perundang-undangan yang berlaku.</li>
<li>Segala pajak yang timbul sehubungan dengan Akad ini merupakan tanggungan dan wajib dibayar oleh PIHAK KEDUA, kecuali Pajak Penghasilan PIHAK PERTAMA.</li>
<li>PIHAK PERTAMA dengan ini diberi kuasa dan kewenangan oleh PIHAK KEDUA untuk mengambil pengembalian/penggantian atas jumlah-jumlah biaya yang merupakan Kewajiban PIHAK KEDUA berdasarkan Akad ini dengan mendebet rekening-rekening PIHAK KEDUA yang ada pada PIHAK PERTAMA, atau setidak-tidaknya membukukan biaya-biaya dimaksud menjadi biaya-biaya yang harus dibayar oleh PIHAK KEDUA jika PIHAK PERTAMA telah membayarkan terlebih dahulu biaya-biaya tersebut.</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 8</strong></p>
<p align="justify"><strong>PENGAKUAN UTANG DAN PEMBUKTIAN UTANG</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<ol>
<li>PIHAK KEDUA dengan ini mengaku berutang pada PIHAK PERTAMA atas Kewajiban PIHAK KEDUA yang belum dilunasi kepada PIHAK PERTAMA akibat dari Cidera Janji dan, kelalaian PIHAK KEDUA untuk memenuhi Kewajibannya sebagaimana diatur dalam Akad ini. Oleh karenanya PIHAK KEDUA dengan ini sekarang untuk nanti pada waktunya mengaku benar-benar dan secara sah telah berutang kepada PIHAK PERTAMA disebabkan karena Kewajiban PIHAK KEDUA yang timbul berdasarkan Akad ini, uang yang diterima sebagai utang oleh PIHAK KEDUA dari PIHAK PERTAMA berdasarkan Akad ini, demikian berikut dengan Bagi Hasil yang menjadi hak PIHAK PERTAMA yang wajib dibayar oleh PIHAK KEDUA kepada PIHAK PERTAMA berdasarkan Akad ini.</li>
<li>PIHAK KEDUA menyetujui bahwa jumlah Kewajiban PIHAK KEDUA yang terutang oleh PIHAK KEDUA kepada PIHAK PERTAMA pada waktu-waktu tertentu akan terbukti dari :
<ol>
<li>Rekening PIHAK KEDUA yang dipegang dan dipelihara oleh PIHAK PERTAMA;</li>
<li>buku-buku, catatan-catatan dan administrasi yang dipegang dan dipelihara oleh PIHAK PERTAMA mengenai atau sehubungan dengan pemberian Pembiayaan Mudharabah kepada PIHAK KEDUA; dan/atau</li>
<li>surat-surat dan dokumen-dokumen lain yang dikeluarkan oleh PIHAK PERTAMA.</li>
</ol>
</li>
<li>Jika PIHAK KEDUA di dalam waktu 5 (lima) Hari Kerja setelah menerima salinan/kutipan Rekening PIHAK KEDUA, tidak mengajukan keberatannya secara tertulis kepada PIHAK PERTAMA yang disertai dengan bukti-bukti yang cukup, maka PIHAK KEDUA dianggap menyetujui atas segala apa yang tertulis dalam Rekening PIHAK KEDUA tersebut, dengan catatan bahwa bilamana terjadi kekeliruan pada PIHAK PERTAMA, maka PIHAK PERTAMA setiap waktu dapat dan dengan ini diberi kuasa untuk mengadakan pembetulan-pembetulan pada Rekening PIHAK KEDUA.</li>
<li>Apabila karena kesalahan PIHAK PERTAMA yang dapat dibuktikan oleh PIHAK KEDUA menyebabkan jumlah yang diterima oleh PIHAK PERTAMA melebihi jumlah yang terutang oleh PIHAK KEDUA kepada PIHAK PERTAMA, maka PIHAK PERTAMA wajib mengembalikan kelebihannya kepada PIHAK KEDUA namun PIHAK PERTAMA tidak diwajibkan membayar biaya apapun kepada PIHAK KEDUA atas kelebihan pembayaran tersebut.</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<h6>Pasal 9</h6>
<h3>DENDA (<em>TA&rsquo;ZIR</em>) DAN GANTI RUGI (<em>TA&rsquo;WIDH</em>)</h3>
<ol>
<li>Denda
<ol>
<li>Dalam hal PIHAK KEDUA terlambat membayar Kewajiban dari jadual yang telah ditetapkan sebagaimana dimaksud dalam Akad, maka PIHAK PERTAMA membebankan dan PIHAK KEDUA setuju membayar denda (<em>ta&rsquo;zir</em>) atas keterlambatan pembayaran kewajiban sesuai dengan ketentuan yang berlaku pada PIHAK PERTAMA sebagaimana tercantum pada lampiran</li>
<li>Dana dari denda atas keterlambatan yang diterima oleh PIHAK PERTAMA akan diperuntukkan sebagai dana sosial.</li>
<li>Disamping denda (<em>ta&rsquo;zir</em>) sebagaimana disebutkan dalam Ayat 1.a Pasal ini, PIHAK KEDUA wajib mengganti kerugian riil (<em>ta&rsquo;widh</em>)yang diakibatkan atas keterlambatan pembayaran Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA.</li>
<li>Dana dari <em>ta&rsquo;widh</em> yang diterima oleh PIHAK PERTAMA akan diperuntukkan untuk menutupi kerugian riil PIHAK PERTAMA atas fasilitas ini.</li>
</ol>
</li>
<li>PIHAK KEDUA sepakat dan dengan ini mengikatkan diri untuk membayar biaya administrasi sesuai dengan ketentuan PIHAK PERTAMA.</li>
<li>PIHAK KEDUA dengan ini memberi kuasa kepada PIHAK PERTAMA dengan hak subtitusi untuk mendebet Rekening PIHAK KEDUA untuk keperluan tersebut pada Pasal ini. Apabila saldo Rekening PIHAK KEDUA pada PIHAK PERTAMA tidak mencukupi untuk memenuhi kewajiban-kewajibannya tersebut, maka PIHAK KEDUA wajib segera melakukan penyetoran guna mencukupi Rekening PIHAK KEDUA;</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 10</strong></p>
<p align="justify"><strong>PEMBERIAN JAMINAN DAN AGUNAN</strong></p>
<p align="justify">&nbsp;</p>
<ol>
<li>Untuk menjamin ketaatan PIHAK KEDUA terhadap segala ketentuan dalam Akad ini dan untuk melunasi segala Kewajban PIHAK KEDUA pada PIHAK PERTAMA yang sudah jatuh tempo dan harus dilunasi maka PIHAK KEDUA dan/atau Penjaminmemberikan Jaminan/ Agunan kepada PIHAK PERTAMA berupa:</li>
</ol>
<p align="justify">-&nbsp; Pemberian Hak Tanggungan atau Surat Kuasa MembePIHAK PERTAMAan Hak Tanggungan (SKMHT) / Akta Pembebanan Hak Tanggungan (APHT) atas : [&nbsp;&nbsp;&nbsp;&nbsp; ] yang akan dibebani dengan hak tanggungan:[&nbsp; ]meliputi juga bangunan dan turutan-turutannya yang berdiri di atas bidang tanah tersebut di atas, yang didirikan berdasarkan surat Ijin Mendirikan Bangunan (IMB) yang dikeluarkan oleh pihak yang berwenang berikut pula dengan segala sesuatu yang sekarang ada, tertanam, berdiri termasuk tetapi tidak terbatas pada bangunan, hasil karya, tanaman dan segala sesuatu yang kelak dikemudian hari akan ada, tertanam, berdiri dan atau diperoleh di atas maupun di bawah permukaan bidang tanah hak itu, yang dianggap sebagai kesatuan dan bagian yang tidak terpisahkan dari bidang tanah tersebut.</p>
<ul>
<li>Pemberian Jaminan Fidusia kepada PIHAK PERTAMA atas barang-barang bergerak milik PIHAK KEDUA dan/atau pemilik jaminan termasuk piutang/tagihan atas nama, berupa :[&nbsp;&nbsp;&nbsp;&nbsp; ] dengan Nilai Fidusia :[&nbsp;&nbsp;&nbsp;&nbsp; ]</li>
<li>Pemberian Gadai atas barang-barang bergerak milik PIHAK KEDUA dan/atau pemilik jaminan berupa:[ ]</li>
<li>Pemberian penanggungan/ jaminan pribadi (<em>PersonalGuarantee</em>) maupun penanggungan/ jaminan perusahaan (<em>Corporate Guarantee</em>) dari:[ ]</li>
</ul>
<p align="justify">Serta jaminan-jaminan lain yang mungkin disyaratkan oleh PIHAK PERTAMA dikemudian hari apabila diperlukan oleh PIHAK PERTAMA dalam bentuk dan pengikatan yang akan ditetapkan oleh PIHAK PERTAMA.</p>
<ol start="2">
<li>Apabila menurut pendapat PIHAK PERTAMA nilai dari Agunan tidak lagi cukup untuk menjamin Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA, maka atas permintaan pertama dari PIHAK PERTAMA, PIHAK KEDUA wajib menambah Agunan lainnya yang disetujui PIHAK PERTAMA.</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>Pasal 11</strong></p>
<h4>CIDERA JANJI/KELALAIAN/PELANGGARAN</h4>
<p align="justify">&nbsp;</p>
<p align="justify">Menyimpang dari ketentuan dalam Pasal 2, 5 dan Pasal 6 Akad ini, PIHAK PERTAMA berhak untuk meminta kembali kepada PIHAK KEDUA atau siapa pun juga yang memperoleh hak darinya, atas seluruh atau sebagian jumlah Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA berdasarkan Akad ini, untuk dibayar dengan seketika dan sekaligus, tanpa diperlukan adanya surat pemberitahuan, surat teguran, atau surat lainnya, apabila terjadi salah satu hal atau peristiwa tersebut di bawah ini :</p>
<ol>
<li>PIHAK KEDUA tidak melaksanakan pembayaran/ pelunasan Kewajiban tepat pada waktu yang diperjanjikan sesuai dengan tanggal jatuh tempo atau jadwal pengembalian Modal dan/atau Bagi Hasil yang menjadi hak PIHAK PERTAMA;</li>
<li>Dokumen atau keterangan yang dimasukkan atau disuruh masukkan ke dalam dokumen yang diserahkan PIHAK KEDUA kepada PIHAK PERTAMA sebagaimana dimaksud Pasal 3 Akad ini palsu, tidak sah, atau tidak benar ;</li>
<li>Pihak yang bertindak untuk dan atas nama serta mewakili PIHAK KEDUA dalam Akad ini menjadi pemboros, pemabuk, atau dihukum penjara atau kurungan;</li>
<li>PIHAK KEDUA tidak memenuhi dan atau melanggar salah satu ketentuan atau lebih ketentuan-ketentuan yang tercantum dalam Akad ini;</li>
<li>Apabila berdasarkan peraturan perundang-undangan yang berlaku pada saat Akad ini ditandatangani atau diberlakukan pada kemudian hari, PIHAK KEDUA tidak dapat atau tidak berhak menjadi PIHAK KEDUA;</li>
<li>PIHAK KEDUA atau pihak ketiga telah memohon kepailitan terhadap PIHAK KEDUA;</li>
<li>Apabila karena sesuatu sebab, seluruh atau sebahagian Perjanjian Jaminan dinyatakan batal atau dibatalkan berdasarkan Putusan Pengadilan atau Badan Arbitase atau nilai Agunan berkurang sedemikian rupa sehingga tidak lagi merupakan Agunan yang cukup atas seluruh Kewajiban, satu dan lain menurut pertimbangan dan penetapan PIHAK PERTAMA;</li>
<li>Apabila keadaan keuangan PIHAK KEDUA<strong> / </strong>Penjamin tidak cukup untuk melunasi Kewajibannya kepada PIHAK PERTAMA baik karena kesengajaan atau kelalaian PIHAK KEDUA;</li>
<li>Harta benda PIHAK KEDUA/Penjamin, baik sebagian atau seluruhnya yang diagunkan atau yang tidak diagunkan kepada PIHAK PERTAMA, diletakkan sita jaminan (<em>conservatoir beslag</em>) atau sita eksekusi (<em>executorial beslag</em>) oleh pihak ketiga;</li>
<li>PIHAK KEDUA/Penjamin masuk dalam Daftar Kredit Macet dan atau Daftar Hitam (<em>blacklist</em>) yang dikeluarkan oleh Perbankan Indonesia atau lembaga lain yang terkait .</li>
<li>PIHAK KEDUA/ Penjamin memberikan keterangan, baik lisan atau tertulis, yang tidak benar dalam arti materiil tentang keadaan kekayaannya, penghasilan, barang Agunan dan segala keterangan atau dokumen yang diberikan kepada PIHAK PERTAMA sehubungan Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA atau jika PIHAK KEDUA menyerahkan tanda bukti penerimaan uang dan atau surat pemindahbukuan yang ditandatangani oleh pihak&ndash;pihak yang tidak berwenang untuk menandatanganinya sehingga tanda bukti penerimaan atau surat pemindahbukuan tersebut tidak sah.</li>
<li>PIHAK KEDUA/ Penjamin meminta penundaan pembayaran (<em>surseance van betaling</em>), tidak mampu membayar, memohon agar dirinya dinyatakan pailit atau dinyatakan pailit, ditaruh dibawah perwalian atau pengampuan, atau karena sebab apapun juga tidak berhak lagi mengurus, mengelola atau menguasai harta bendanya atau dilikuidasi (apabila PIHAK KEDUAadalah suatu badan usaha berbadan hukum atau bukan badan hukum).</li>
<li>PIHAK KEDUA, sebelum atau sesudah Akad ini ditandatangani, juga mempunyai utang kepada pihak ketiga dan hal yang demikian tidak diberitahukan kepada PIHAK PERTAMA baik sebelum fasilitas ini diberikan atau sebelum utang lain tersebut diperoleh.</li>
<li>PIHAK KEDUA<strong>/</strong>Penjamin lalai, melanggar atau tidak dapat/tidak memenuhi suatu ketentuan dalam Akad ini, akad pemberian Agunan atau dokumen-dokumen lain sehubungan dengan pemberian fasilitas ini.</li>
<li>PIHAK KEDUA/Penjamin meninggal dunia/dibubarkan/bubar (apabila PIHAK KEDUA adalah suatu badan usaha berbadan hukum atau bukan badan hukum), meninggalkan tempat tinggalnya/pergi ke tempat yang tidak diketahui untuk waktu lebih dari 2 (dua) bulan dan tidak menentu, melakukan atau terlibat dalam suatu perbuatan/peristiwa yang menurut pertimbangan PIHAK PERTAMA dapat membahayakan pemberian pembiayaan, ditangkap pihak yang berwajib atau dijatuhi hukuman penjara.</li>
<li>PIHAK KEDUA melakukan penyimpangan/ kelalaian terhadap hal-hal yang disepakati dalam Akad yang mengakibatkan kerugian PIHAK PERTAMA sesuai dengan ketentuan yang berlaku pada PIHAK PERTAMA.</li>
<li>Cross Default</li>
</ol>
<ul>
<li>PIHAK KEDUA dan/atau salah satu penjamin lalai melaksanakan sesuatu kewajiban atau melakukan pelanggaran terhadap sesuatu ketentuan dalam akad&nbsp; lain&nbsp; dan/ atau perjanjian Jaminan lain yang dibuat dengan PIHAK PERTAMA.</li>
<li>Bila pihak/ PIHAK KEDUA lain yang diberi fasilitas pembiayaan&nbsp; oleh PIHAK PERTAMA dengan jaminan seluruh atau sebagian dari Jaminan sebagaimana disebutkan dalam Akad ini melakukan kelalaian atau pelanggaran yang ditentukan dalam akad pembiayaan&nbsp; yang dibuat pihak/ PIHAK KEDUA&nbsp; lain tersebut dengan PIHAK PERTAMA.</li>
<li>Bilamana PIHAK KEDUA dan/ atau Penjamin lalai melaksanakan sesuatu kewajiban atau melakukan pelanggaran terhadap sesuatu ketentuan dalam sesuatu akad/ perjanjian lain baik dengan PIHAK PERTAMA maupun dengan orang/ pihak/ PIHAK PERTAMA lain termasuk yang mengenai atau berhubungan dengan pinjaman uang/pemberian fasilitas pembiayaan dimana PIHAK KEDUA dan/ atau salah seorang Penjamin adalah sebagai pihak yang menerima pinjaman atau sebagai penjamin dan kelalaian atau pelanggaran mana memberikan hak kepada PIHAK PERTAMA maupun pihak yang memberikan pinjaman atau fasilitas pembiayaan untuk menuntut pembayaran kembali atas apa yang terutang atau wajib dibayar oleh PIHAK KEDUA dan/ atau salah seorang penjamin dalam perjanjian tersebut secara sekaligus sebelum tanggal jatuh tempo pinjamannya.</li>
</ul>
<ol start="18">
<li>Bilamana PIHAK KEDUA dan/atau pemilik jaminan menyewakan, mengalihkan dan/atau menjaminkan kepada pihak lain barang-barang Agunan yang telah diberikan PIHAK KEDUA dan/atau pemilik jaminan kepada PIHAK PERTAMA dengan cara apapun tanpa persetujuan tertulis terlebih dahulu dari PIHAK PERTAMA (kecuali untuk barang jaminan yang berupa <em>inventory</em> dijual dalam rangka menjalankan usaha yang normal).</li>
<li>Terjadi peristiwa apapun yang menurut pendapat PIHAK PERTAMA akan dapat mengakibatkan PIHAK KEDUA/ Penjamin tidak dapat memenuhi Kewajibannya.</li>
<li>[lain-lain disesuaikan dengan Surat Persetujuan Prinsip Pembiayaan].</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>Pasal 12</strong></p>
<h4>AKIBAT CIDERA JANJI</h4>
<p align="justify">&nbsp;</p>
<p align="justify">Apabila terjadi satu atau lebih peristiwa sebagaimana dimaksud dalam Pasal&nbsp; 11 Akad ini, maka dengan mengesampingkan ketentuan dalam Pasal 1266 Kitab Undang-Undang Hukum Perdata, PIHAK PERTAMA berhak untuk:</p>
<ol>
<li>Kewajiban PIHAK PERTAMA untuk merealisasikan Pembiayaan Mudharabah kepada PIHAK KEDUA berdasarkan Akad ini menjadi berakhir.</li>
<li>Menyatakan semua Kewajiban PIHAK KEDUA dan setiap jumlah uang yang pada waktu itu terutang oleh PIHAK KEDUA menjadi jatuh tempo dan dapat ditagih pembayarannya sekaligus oleh PIHAK PERTAMA tanpa peringatan atau teguran berupa apapun dan dari siapapun juga;</li>
<li>PIHAK PERTAMA berhak untuk menjalankan hak-hak dan wewenangnya yang timbul dari atau berdasarkan Akad ini dan Perjanjian Jaminan;</li>
<li>Mengambil langkah-langkah yang dianggap perlu untuk mengamankan PIHAK PERTAMA termasuk namun tidak terbatas pada memasuki pekarangan, tanah dan bangunan, memeriksa Obyek Akad dan atau barang Agunan beserta fasilitasnya yang melekat, memberi peringatan dengan cara memasang papan (plank) atau media lainnya; dan/ atau</li>
<li>Menjual harta benda yang dijaminkan oleh PIHAK KEDUA/Penjamin kepada PIHAK PERTAMA berdasarkan prinsip keadilan, baik dibawah tangan dengan harga pasar yang disepakati PIHAK KEDUA maupun dimuka umum (secara lelang) dan untuk itu PIHAK KEDUA/Penjamin memberi kuasa dengan ketentuan pendapatan bersih dari penjualan pertama-tama dipergunakan untuk pembayaran seluruh Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA dan jika ada sisa, maka sisa tersebut akan dikembalikan kepada PIHAK KEDUA dan/atau Penjamin sebagai pemilik harta benda yang dijaminkan kepada PIHAK PERTAMA, dan sebaliknya, apabila hasil penjualan tersebut tidak cukup untuk melunasi seluruh Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA, maka kekurangan tersebut tetap menjadi Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA dan wajib dibayar PIHAK KEDUA dengan seketika dan sekaligus pada saat ditagih oleh PIHAK PERTAMA.</li>
</ol>
<h6>&nbsp;</h6>
<h6>Pasal 13</h6>
<h4>PERNYATAAN DAN JAMINAN PIHAK KEDUA</h4>
<ol>
<li><strong>Kewenangan </strong></li>
<li>PIHAK KEDUA berhak, cakap, dan berwenang sepenuhnya untuk menandatangani Akad ini dan semua surat dokumen yang menjadi kelengkapannya serta berhak pula untuk menjalankan usaha tersebut dalam Akad ini.</li>
<li>PIHAK KEDUA adalah suatu badan usaha dengan status badan hukum yang didirikan secara sah berdasarkan serta tunduk pada ketentuan perundang-undangan Negara Republik Indonesia. PIHAK KEDUA mempunyai kuasa penuh dan wewenang untuk meminjam dan memenuhi kewajibannya dalam Akad ini dan/ atau dokumen lain, untuk menjalankan usahanya, memiliki harta kekayaan dan aset dan/ atau terdaftar untuk menjalankan usahanya yang dijalankan sekarang, berhak dan/atau terdaftar untuk menjalankan usahanya di domisili hukum manapun</li>
<li>PIHAK KEDUA tidak sedang dalam keadaan menderita kerugian yang mempengaruhi jalannya usahanya secara materil atau mempengaruhi kemampuannya dalam melaksanakan kewajibannya kepada PIHAK PERTAMA, dan pada saat ini tidak berada dalam keadaan sebagaimana dimaksud dalam ketentuan-ketentuan yang mengatur PIHAK KEDUA termasuk namun tidak terbatas pada UU PIHAK KEDUA th 92, PP 33 2008, dan Permenkop 09 2018.</li>
<li>PIHAK KEDUA adalah badan usaha yang mempunyai (i) akta pendirian dan pelaporan kepada instansi terkait; (ii) susunan anggota pengurus dan dewan pengawas; (iii) susunan permodalan dan keanggotaan; sebagaimana tertera dalam Akad ini.</li>
<li><strong>Tindakan </strong><strong>Hukum PIHAK KEDUA</strong></li>
<li>PIHAK KEDUA telah mengambil semua tindakan yang diperlukan sesuai ketentuan yang berlaku PIHAK KEDUA yang memberi wewenang untuk pelaksanaan Akad ini dan dokumen lain yang disyaratkan, dan pihak-pihak yang menandatangani dokumen-dokumen tersebut, telah diberi wewenang untuk berbuat demikian atas nama PIHAK KEDUA.</li>
</ol>
<ol>
<li>PIHAK KEDUA memiliki ijin-ijin dari pihak-pihak yang terkait yang mengharuskan PIHAK KEDUA memperoleh ijin-ijin tersebut untuk membuat dan menandatangani Akad ini, menyerahkan jaminan-jaminan dan dokumen-dokumen lain yang berkaitan dengan Akad ini dan Perjanjian J</li>
<li>Diadakannya Akad ini dan/atau akad tambahan (Addendum) Akad ini tidak akan&nbsp; bertentangan dengan&nbsp; suatu&nbsp; akad/perjanjian&nbsp; yang&nbsp; telah&nbsp;&nbsp; ada&nbsp;&nbsp; atau&nbsp;&nbsp; yang&nbsp;&nbsp; akan&nbsp;&nbsp; diadakan&nbsp; oleh PIHAK KEDUA dengan pihak ketiga lainnya.
<ul>
<li><strong>Perikatan </strong><strong>Akad ini </strong>
<ol>
<li>Akad ini, Perjanjian Jaminan dan dokumen lain yang disyaratkan bila dilaksanakan dan diserahkan merupakan suatu kewajiban hukum bagi PIHAK KEDUA dan karenanya dapat dieksekusi sesuai dengan ketentuan-ketentuan yang tercantum di dalamnya.</li>
<li>Akad ini, Perjanjian Jaminan dan dokumen-dokumen lain yang disyaratkan, pada saat ditandatangani tidak melanggar Undang-Undang, Peraturan, Ketetapan atau Keputusan dari Negara Republik Indonesia dan juga tidak bertentangan dengan atau mengakibatkan pelanggaran terhadap setiap perjanjian yang mengikat PIHAK KEDUA.</li>
<li>Semua permohonan, pendaftaran dan persetujuan yang diperlukan atau diharuskan agar kepastian pelaksanaan, penyerahan, keberhasilan, keabsahan, keefektifan maupun pengeksekusian Akad&nbsp; ini dan dokumen lain yang diperlukan sesuai dengan yang disyaratkan telah dibuat dan diperoleh.</li>
</ol>
</li>
</ul>
</li>
</ol>
<ol>
<li><strong>Tidak Terjadi/Mengalami Peristiwa Cidera Janji</strong></li>
</ol>
<ol>
<li>PIHAK KEDUA tidak mengalami hal atau peristiwa yang merupakan suatu peristiwa Cidera Janji, kelalaian/ pelanggaran sebagaimana dimaksud dalam Akad ini maupun merupakan peristiwa kelalaian/ pelanggaran terhadap perjanjian lain yang dibuat PIHAK KEDUA dengan pihak lain, dan pemberian fasilitas Pembiayaan oleh PIHAK PERTAMA kepada PIHAK KEDUA tidak akan menyebabkan timbulnya suatu peristiwa kelalaian/ pelanggaran menurut perjanjian lain yang dibuat oleh PIHAK KEDUA.</li>
<li>PIHAK KEDUA tidak terlibat perkara pidana maupun perdata, tuntutan pajak atau sengketa yang sedang berlangsung atau menurut pengetahuan PIHAK KEDUA akan menjadi ancaman dikemudian hari atau yang dapat berakibat negatif terhadap PIHAK KEDUA atau harta kekayaannya, yang nantinya mempengaruhi keadaan keuangan atau usahanya atau dapat mengganggu kemampuannya untuk melakukan kewajibannya berdasarkan Akad ini.</li>
<li><strong>Transaksi Dengan Pihak Ketiga </strong></li>
</ol>
<p align="justify">Transaksi atau Perjanjian yang dilakukan oleh PIHAK KEDUA&nbsp; dengan pihak ketiga yang merupakan dasar dari pemberian fasilitas Pembiayaan ini oleh PIHAK PERTAMA kepada PIHAK KEDUA&nbsp; adalah benar adanya, sah dan sesuai dengan peraturan perundang-undangan yang berlaku.</p>
<ol>
<li><strong>Data-Data Keuangan</strong></li>
</ol>
<p align="justify">Semua buku-buku keuangan PIHAK KEDUA, keterangan-keterangan antara lain tetapi tidak terbatas pada Laporan Keuangan yang diserahkan oleh PIHAK KEDUA&nbsp; dan data lain yang telah dan/ atau dikemudian hari akan diberikan oleh PIHAK KEDUA&nbsp; kepada PIHAK PERTAMA adalah lengkap dan benar, dan buku-buku itu disiapkan dan dipelihara sesuai dengan prinsip-prinsip akuntansi yang berlaku di Negara Republik Indonesia yang diterapkan secara terus menerus dan menunjukkan secara benar keadaan keuangan dan hasil usaha PIHAK KEDUA pada tanggal buku-buku tersebut dibuat/ disiapkan, dan sejak tanggal dibuat/ disiapkan tersebut tidak terjadi perubahan keadaan keuangan PIHAK KEDUA yang mempengaruhi jalannya usaha PIHAK KEDUA secara materiil atau mempengaruhi kemampuan PIHAK KEDUA dalam melaksanakan kewajibannya kepada PIHAK PERTAMA berdasarkan Akad ini dan/ atau perjanjian-perjanjian lainnya yang dibuat antara PIHAK PERTAMA dan PIHAK KEDUA .</p>
<ul>
<li>Dalam hal terjadi perselisihan di antara PIHAK KEDUA dengan penjamin dan/ atau pemilik Jaminan, maka PIHAK PERTAMA tetap berhak atas Jaminan yang telah diberikan serta tetap berhak untuk menuntut pemenuhan kewajiban PIHAK KEDUA&nbsp; kepada PIHAK PERTAMA berdasarkan Akad ini, Perjanjian Jaminan, maupun perjanjian-perjanjian lainnya.</li>
<li>Selama berlangsungnya Akad ini, PIHAK KEDUA akan menjaga semua perizinan, lisensi, persetujuan dan sertifikat yang wajib dimiliki untuk melaksanakan usahanya.</li>
</ul>
<ol>
<li>Dalam hal belum dicukupinya Jaminan untuk melunasi Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA, PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk dari waktu ke waktu selama utangnya belum lunas akan menyerahkan kepada PIHAK PERTAMA, jaminan-jaminan tambahan yang dinilai cukup oleh PIHAK PERTAMA.</li>
<li>Sepanjang tidak bertentangan dengan peraturan perundang-undangan yang berlaku, PIHAK KEDUA berjanji dan dengan ini mengikatkan diri mendahulukan untuk membayar dan melunasi Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA dari kewajiban lainnya.</li>
<li>Bahwa semua kuasa yang diberikan PIHAK KEDUA kepada PIHAK PERTAMA untuk mendebet Rekening PIHAK KEDUA guna membayar Denda, biaya-biaya, pelunasan Harga Jual maupun Kewajiban PIHAK KEDUA lain yang wajib dibayar oleh PIHAK KEDUA kepada PIHAK PERTAMA, termasuk tetapi tidak terbatas pada biaya, denda dan ganti rugi, kuasa tersebut diberikan dengan hak subtitusi dan tidak dapat dibatalkan atau dicabut kembali oleh PIHAK KEDUA tanpa adanya persetujuan tertulis dari PIHAK PERTAMA terlebih dahulu dan tidak dapat berakhir karena sebab-sebab apapun termasuk oleh sebab-sebab berakhirnya kuasa yang ditentukan dalam pasal 1813, 1814 dan 1816 Kitab Undang-Undang Hukum Perdata (KUHPer). Apabila dibutuhkan kuasa-kuasa baru untuk pembayaran biaya-biaya yang timbul berdasarkan Perjanjian ini maupun Akad Pembiayaan Mudharabah, maka PIHAK KEDUA dengan ini menyetujui untuk memberikan kuasa-kuasa baru tersebut kepada PIHAK PERTAMA.</li>
</ol>
<ul>
<li>[lain-lain disesuaikan dengan Surat Persetujuan Prinsip Pembiayaan].</li>
</ul>
<p align="justify">&nbsp;</p>
<h6>Pasal 14</h6>
<h4>KEWAJIBAN DAN PEMBATASAN TERHADAP TINDAKAN PIHAK KEDUA</h4>
<p align="justify">&nbsp;</p>
<ol>
<li>PIHAK KEDUA dengan ini berjanji dan mengikatkan diri selama jangka waktu Akad ini dan hingga pembayaran penuh dan lunas atas seluruh Kewajiban PIHAK KEDUA berdasarkan Akad ini, maka PIHAK KEDUA wajib melakukan hal-hal sebagai berikut:
<ol>
<li>PIHAK KEDUA wajib membayar kembali kepada PIHAK PERTAMA seluruh Kewajiban PIHAK KEDUA secara tepat waktu sebagaimana ditentukan di dalam Akad ini.</li>
<li>PIHAK KEDUA wajib menggunakan fasilitas Pembiayaan Mudharabah sesuai dengan tujuan penggunaannya sebagaimana ditetapkan dalam Akad ini.</li>
<li>PIHAK KEDUA wajib mengaktifkan dan melakukan seluruh transaksi operasional PIHAK KEDUA melalui Rekening PIHAK KEDUA&nbsp; di PIHAK PERTAMA.</li>
<li>PIHAK KEDUA wajib memberikan seluruh keterangan baik lisan maupun tertulis dalam bentuk dokumen-dokumen,surat-surat atau dalam bentuk lainnya mengenai keadaan keuangan PIHAK KEDUA dan/atau penjamin pada waktu dan dalam bentuk yang diminta PIHAK PERTAMA.</li>
<li>PIHAK KEDUA wajib memelihara pembukuan, administrasi dan catatan-catatan yang cukup mengenai usaha yang dijalankan/diusahakan oleh PIHAK KEDUA dan/atau penjamin sesuai dengan dan menurut prinsip-prinsip dan praktek-praktek akuntansi yang umum diterima di Republik Indonesia dan yang diterapkan secara terus menerus.</li>
<li>PIHAK KEDUA wajib mengizinkan pegawai-pegawai atau wakil-wakil PIHAK PERTAMA memasuki kantor-kantor, gedung-gedung,pabrik-pabrik, atau lokasi usaha PIHAK KEDUA dan/atau penjamin/pemilik Jaminan guna melakukan pemeriksaan atas kekayaan dan usaha PIHAK KEDUA dan/atau penjamin/pemilik jaminan serta barang-barang Agunan/Jaminan dan memeriksa/mengaudit pembukuan, catatan-catatan dan administrasi PIHAK KEDUA dan/atau penjamin/pemilik Jaminan dan membuat salinan-salinan atau foto copy atau catatan-catatan dari padanya.</li>
<li>PIHAK KEDUA wajib menjaga: kekayaannya yang penting untuk kegiatan usahanya; kelangsungan eksistensi PIHAK KEDUA secara hukum; dan eksistensi semua hak, izin dan hal-hal lain, yang perlu untuk melaksanakan usahanya secara sah, tertib dan efisien.</li>
<li>PIHAK KEDUA wajib membayar semua pajak dan beban-beban lainnya berdasarkan ketentuan yang berlaku.</li>
<li>PIHAK KEDUA wajib mengasuransikan dan memelihara atau menyuruh mengasuransikan atau memelihara/ mempertahankan asuransi atas barang-barang (baik berupa barang-barang bergerak maupun barang-barang tidak bergerak) yang sekarang telah dan/atau dikemudian hari akan dijaminkan/diagunkan oleh PIHAK KEDUA dan/ atau pemilik jaminan kepada PIHAK PERTAMA.</li>
<li>PIHAK KEDUA wajib menyerahkan asli polis-polis asuransi dan lain-lain surat/dokumen mengenai atau yang berhubungan dengan asuransi tersebut kepada dan untuk disimpan oleh PIHAK PERTAMA.</li>
<li>Menyerahkan hasil audit laporan keuangan tahunan selambat-lambatnya 6 bulan setelah tahun buku berakhir dan laporan keuangan semester selambat-lambatnya 3 bulan setelah periode tersebut berakhir.</li>
<li>PIHAK KEDUA <strong>wajib melakukan publikasi laporan kinerja keuangan secara bulanan kepada PIHAK PERTAMA melalui platform digitan Plazadana</strong> atau media lain sesuai dengan kesepakatan paling lambat 10 hari kerja setelah tutup buku tiap bulan.</li>
<li>Melakukan penilaian kembali Agunan oleh Kantor Jasa Penilai Publik (KJPP) yang ditunjuk PIHAK PERTAMA sesuai dengan ketentuan yang berlaku.</li>
<li>[lain-lain disesuaikan dengan Surat Persetujuan Prinsip Pembiayaan].</li>
</ol>
</li>
<li>Pembayaran dari pembeli/ rekanan/ pemilik proyek atau seluruh aktifitas keuangan PIHAK KEDUA berkaitan dengan Akad ini wajib disalurkan melalui Rekening PIHAK KEDUA di PIHAK PERTAMA serta mencantumkan nomor Rekening PIHAK KEDUA di PIHAK PERTAMA pada setiap invoice/ tagihan kepada pembeli/ rekanan.</li>
<li>PIHAK KEDUA wajib menyerahkan rencana kegiatan usahanya untuk jangka waktu 1 (satu) tahun dan setiap rencana penggunaan Fasilitas Mudharabah.</li>
<li>PIHAK KEDUA berjanji dan dengan ini mengikatkan diri, bahwa selama masa berlangsungnya Akad ini, kecuali setelah mendapatkan persetujuan tertulis dari PIHAK PERTAMA, PIHAK KEDUA tidak akan melakukan salah satu, sebahagian atau seluruh perbuatan-perbuatan sebagai berikut:</li>
</ol>
<ol>
<li>menyewakan, menjaminkan, mengalihkan atau menyerahkan, baik sebagian atau seluruh usaha, hasil usaha dan/ atau Agunan kepada pihak lain;</li>
</ol>
<ol>
<li>Menerima sesuatu pembiayaan uang atau fasilitas keuangan, fasilitas leasing berupa apapun juga atau untuk mengikat diri sebagai penjamin/ avalis untuk menjamin utang orang/ pihak lain (kecuali utang dagang yang dibuat dalam rangka menjalankan usaha sehari-hari).</li>
<li>Menjual, menyewakan, mentransfer, memindahkan hak dan/ atau kepentingan, menghapuskan sebagian besar atau seluruh harta kekayaan PIHAK KEDUA dan/ atau penjamin atau menjaminkan/ mengagunkan barang-barang bergerak maupun barang-barang tidak bergerak milik PIHAK KEDUA dan/atau penjamin dengan cara bagaimanapun juga dan kepada orang/ pihak siapapun juga (kecuali menjual dalam rangka menjalankan sifat usaha yang normal).</li>
<li>Melakukan pembayaran atau pembayaran kembali atas semua pembiayaan kepada pihak ketiga siapapun selain pembayaran normal karena sifat usaha PIHAK KEDUA dan/atau penjamin.
<ol>
<li>Melakukan investasi lainnya atau menjalankan kegiatan usaha yang tidak mempunyai hubungan dengan usaha yang sedang dijalankan atau melakukan perubahan usaha yang dapat mempengaruhi pengembalian Pembiayaan PIHAK KEDUA&nbsp; kepada PIHAK PERTAMA.</li>
<li>Mengajukan permohonan untuk dinyatakan pailit oleh Pengadilan Niaga atau mengajukan permohonan penundaan pembayaran utang (<em>surseancevan betalling</em>).</li>
<li>Melakukan perubahan susunan pengurus PIHAK KEDUA dan/atau penjamin.</li>
<li>Melakukan penggabungan usaha (merger) dengan badan usaha lain, peleburan usaha (konsolidasi) bersama badan usaha lain dan pengambilalihan (akuisisi) saham-saham dalam badan usaha lain.</li>
<li>Melakukan pembayaran atau pembayaran kembali kepada pemegang saham PIHAK KEDUA dan/atau penjamin atas pembiayaan -pembiayaan yang telah dan/atau dikemudian hari akan diberikan oleh pemegang saham PIHAK KEDUA&nbsp; dan/ atau penjamin kepada PIHAK KEDUA&nbsp; dan/atau penjamin, baik jumlah pokok, bunga, provisi, atau biaya-biaya lainnya.</li>
<li>Melakukan pembubaran atau likuidasi berdasarkan keputusan Rapat Anggota.</li>
<li>Mengubah struktur permodalan PIHAK KEDUA dan/atau penjamin, kecuali untuk peningkatan modal yang berasal dari laba yang ditahan (<em>retained earnings</em>) atau pengeluaran saham baru atau setoran dari pemegang saham.</li>
</ol>
</li>
<li>Membayar atau menyatakan dapat dibayar sesuatu deviden atau pembagian keuntungan berupa apapun juga atas saham-saham yang dikeluarkan PIHAK KEDUA.</li>
</ol>
<ol>
<li>[lain-lain disesuaikan dengan Surat Persetujuan Prinsip Pembiayaan].</li>
</ol>
<ol start="5">
<li>PIHAK KEDUA dan/ atau penjamin wajib memberitahukan secara tertulis kepada PIHAK PERTAMA, jika terjadi kejadian berikut ini:
<ol>
<li>Setiap tuntutan perkara perdata terhadap PIHAK KEDUA dan/atau penjamin yang nilainya minimal 1/3 (satu per tiga) dari nilai fasilitas Pembiayaan Mudharabah yang diperoleh;</li>
<li>Sesuatu perkara atau tuntutan hukum yang terjadi antara PIHAK KEDUA dan/atau penjamin&nbsp; dengan suatu badan/instansi pemerintah; dan/ atau</li>
<li>Suatu kejadian yang dengan lewatnya waktu atau karena pemberitahuan atau kedua-duanya akan menjadi kejadian kelalaian ke pihak lain,</li>
</ol>
</li>
<li>[lain-lain disesuaikan dengan Surat Persetujuan Prinsip Pembiayaan]</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 15</strong></p>
<p align="justify"><strong>ASURANSI</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<ol>
<li>Selama Kewajiban Mudharabah belum lunas, PIHAK KEDUA wajib menutup asuransi <span style="text-decoration: line-through;">jiwa dan atau asuransi </span>atas barang Agunan atas beban PIHAK KEDUA kepada Perusahaan Asuransi berdasarkan prinsip syariah yang disetujui oleh PIHAK PERTAMA terhadap risiko kerugian yang macam, nilai dan jangka waktunya ditentukan oleh PIHAK PERTAMA.</li>
<li>Dalam polis asuransi wajib dicantumkan klausula yang menyatakan bahwa bilamana terjadi pembayaran ganti rugi dari perusahaan asuransi, maka PIHAK PERTAMA berhak memperhitungkan hasil pembayaran klaim tersebut dengan seluruh Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA (<em>PIHAK PERTAMA&rsquo;s Clause</em>).</li>
<li>PIHAK KEDUA, agen dan karyawan harus mematuhi semua persyaratan dan kondisi dari polis asuransi tersebut, termasuk segera melaporkan kecelakaan atau kerusakan pada PIHAK PERTAMA dan perusahaan asuransi dan akan melakukan semua hal yang diperlukan atau tepat untuk melindungi atau menjaga aset tertanggung sesuai dengan pasal yang sesuai dalam polis asuransi.</li>
<li>PIHAK KEDUA juga harus memberikan semua bantuan kepada perusahaan asuransi dan PIHAK PERTAMA untuk penyelesaian klaim dengan cepat dan harus mengambil semua tindakan yang wajar dan langkah-langkah yang diperlukan dalam hal tersebut.</li>
<li>Premi asuransi Agunan wajib dibayar lunas atau dicadangkan oleh PIHAK KEDUA dibawah penguasaan PIHAK PERTAMA sebelum dilakukan penarikan pembiayaan atau perpanjangan jangka waktu pembiayaan.</li>
<li>Dalam hal penutupan asuransi dilakukan oleh PIHAK PERTAMA, dengan ini PIHAK KEDUA memberikan kuasa kepada PIHAK PERTAMA untuk mengasuransikan barang-barang yang menjadi Agunan serta melakukan&nbsp; tindakan sehubungan dengan Agunan tersebut, dengan ketentuan bahwa biaya yang timbul dari&nbsp; penutupan&nbsp; asuransi sepenuhnya menjadi beban PIHAK KEDUA.</li>
<li>Bila terjadi kerugian atas Agunan yang dipertanggungkan dalam Polis tersebut diatas, maka dengan ini PIHAK KEDUA memberi kuasa kepada PIHAK PERTAMA untuk mengajukan klaim serta menerima hasil klaim tersebut dari perusahaan asuransi untuk kemudian mempergunakan hasil klaim tersebut bagi pelunasan kewajiban/hutang PIHAK KEDUA kepada PIHAK PERTAMA.</li>
<li>Dalam hal hasil uang pertanggungan tidak cukup untuk melunasi Kewajiban PIHAK KEDUA, sisa Kewajiban tersebut tetap menjadi Kewajiban PIHAK KEDUA kepada PIHAK PERTAMA dan wajib dibayar dengan seketika dan sekaligus oleh PIHAK KEDUA pada saat ditagih oleh PIHAK PERTAMA.</li>
<li>Asli kwitansi atau pembayaran resmi premi asuransi dan asli polis asuransi beserta &lsquo;<em>PIHAK PERTAMAr&rsquo;s Clause</em>&rdquo; wajib diserahkan kepada PIHAK PERTAMA.</li>
</ol>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 16</strong></p>
<h4>HUKUM YANG BERLAKU</h4>
<p align="justify">&nbsp;</p>
<p align="justify">Pelaksanaan Akad ini tunduk kepada peraturan perundang-undangan yang berlaku di Indonesia dan ketentuan syariah bagi PIHAK PERTAMA, termasuk namun tidak terbatas pada Undang-undang PIHAK KEDUA, Peraturan &nbsp;Menteri PIHAK KEDUA dan Fatwa Dewan Syariah Nasional Majelis Ulama Indonesia.</p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Pasal 17</strong></p>
<h4>PENYELESAIAN PERSELISIHAN DAN DOMISILI HUKUM</h4>
<p align="justify">&nbsp;</p>
<ol>
<li>Apabila di kemudian hari terjadi perbedaan pendapat atau penafsiran atas hal-hal yang tercantum di dalam Akad ini atau terjadi perselisihan atau sengketa dalam pelaksanaan Akad ini, Para Pihak sepakat untuk menyelesaikannya secara musyawarah untuk mufakat.</li>
<li>Dalam hal musyawarah untuk mufakat sebagaimana dimaksud ayat 1 Pasal ini tidak tercapai, maka Para Pihak bersepakat, dan dengan ini berjanji serta mengikatkan diri satu terhadap yang lain, untuk menyelesaikannya melalui Pengadilan Agama&hellip;&hellip;&hellip;.</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>Pasal 18</strong></p>
<p align="justify"><strong>KOMUNIKASI DAN PEMBERITAHUAN</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<ol>
<li>Semua surat menyurat atau pemberitahuan-pemberitahuan yang harus dikirim oleh masing-masing pihak kepada pihak lain dalam Akad ini mengenai atau sehubungan dengan Akad ini, dilakukan dengan pos &ldquo;tercatat&rdquo; atau melalui perusahaan ekspedisi (kurir) ke alamat-alamat yang tersebut di bawah ini :</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>PIHAK PERTAMA</strong></p>
<p align="justify">&nbsp;Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : PIHAK PERTAMA.</p>
<p align="justify">Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p align="justify">Telp./Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;....&hellip;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>PIHAK KEDUA</strong></p>
<p align="justify">Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p align="justify">Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p align="justify">Telp./Fax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p align="justify">&nbsp;</p>
<h6>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pemberitahuan dari salah satu pihak kepada pihak lainnya dianggap diterima:</h6>
<h6>a.&nbsp;&nbsp;&nbsp;&nbsp; Jika dikirim melalui kurir (ekspedisi) pada tanggal penerimaan dan/atau;</h6>
<h6>b.&nbsp;&nbsp;&nbsp;&nbsp; Jika dikirim melalui pos tercatat 7 (tujuh) hari setelah tanggal pengirimannya.</h6>
<h6>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PIHAK KEDUA dapat mengganti alamatnya dengan memberitahukan secara tertulis kepada PIHAK PERTAMA. Perubahan alamat tersebut dianggap diterima oleh PIHAK PERTAMA sesuai dengan ketentuan ayat 2 Pasal ini.</h6>
<ol start="4">
<li>Dalam hal terjadi perubahan alamat PIHAK PERTAMA, pemberitahuan perubahan alamat PIHAK PERTAMA melalui media massa (cetak) berskala nasional atau lokal merupakan pemberitahuan resmi kepada PIHAK KEDUA.</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify"><strong>Pasal 19</strong></p>
<p align="justify"><strong>KETENTUAN PENUTUP</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<ol>
<li>Sebelum Akad ini ditandatangani oleh PIHAK KEDUA, PIHAK KEDUA mengakui dengan sebenarnya bahwa PIHAK KEDUA telah membaca dengan cermat atau dibacakan kepadanya seluruh isi Akad ini berikut semua surat dan/atau dokumen yang menjadi lampiran Akad ini, sehingga PIHAK KEDUA memahami sepenuhnya segala yang akan menjadi akibat hukum setelah PIHAK KEDUA menandatangani Akad ini.</li>
<li>PIHAK KEDUA menyetujui dan oleh karena itu dengan ini memberi kuasa kepada PIHAK PERTAMA untuk sewaktu-waktu menjual, mengalihkan, menjaminkan atau dengan cara apapun memindahkan piutang/tagihan-tagihan PIHAK PERTAMA kepada PIHAK KEDUA yang timbul berdasarkan Akad ini kepada kantor cabang lain di dalam dan di luar negeri, <em>subsidiary company</em> dari PIHAK PERTAMA, PIHAK PERTAMA Indonesia atau kepada pihak ketiga lainnya dengan siapa PIHAK PERTAMA akan membuat perjanjian subrogasi, cessie, <em>joint financing</em> atau perjanjian kerja sama lain, berikut semua hak, kekuasaan-kekuasaan dan Jaminan/Agunan yang ada pada PIHAK PERTAMA berdasarkan Akad ini atau akta pengikatan Jaminan/Agunan, dengan syarat-syarat dan ketentuan-ketentuan yang dianggap baik oleh PIHAK PERTAMA.</li>
<li>PIHAK KEDUA tidak dapat mengalihkan kewajibannya yang timbul berdasarkan Akad ini kepada pihak lain tanpa persetujuan tertulis dari PIHAK PERTAMA.</li>
<li>Akad ini&nbsp; mengikat&nbsp; Para&nbsp; Pihak&nbsp; yang sah, &nbsp;para&nbsp; pengganti&nbsp; atau pihak-pihak&nbsp; yang menerima hak dari&nbsp; masing-masing Para Pihak.</li>
<li>Akad ini memuat(jika tidak ditentukan lain di dalam Akad ini), dan karenanya menggantikan semua pengertian dan kesepakatan yang telah dicapai oleh Para Pihak sebelum ditandatanganinya Akad ini, baik tertulis maupun lisan, mengenai hal yang sama.&nbsp;</li>
<li>Jika salah satu atau sebagian ketentuan-ketentuan dalam Akad ini menjadi batal atau tidak berlaku, maka tidak mengakibatkan seluruh Akad ini menjadi batal atau tidak berlaku seluruhnya.</li>
<li>Para Pihak mengakui bahwa judul pada setiap pasal dalam Akad ini dipakai hanya untuk memudahkan pembaca Akad ini, karenanya judul tersebut&nbsp; tidak memberikan penafsiran apapun atas isi Akad ini.</li>
<li>Apabila ada hal-hal yang belum diatur dalam Akad ini, maka PIHAK PERTAMA dan PIHAK KEDUA akan mengaturnya bersama secara musyawarah untuk mufakat dalam suatu akad tambahan (Addendum) yang ditandatangani oleh Para Pihak.</li>
<li>Tiap akad tambahan (Addendum) dari Akad ini merupakan satu kesatuan yang tidak terpisahkan dari Akad ini.</li>
<li>Akad ini telah disesuaikan dengan ketentuan peraturan perundang undangan termasuk ketentuan peraturan Otoritas Jasa Keuangan.</li>
<li>PIHAK KEDUA berjanji dan dengan mengikatkan diri, bahwa atas pembiayaan berdasarkan Akad ini, PIHAK KEDUA tidak akan memberikan hadiah atau imbalan dalam bentuk apapun kepada karyawan/ti atau Pejabat serta jajaran Direksi PIHAK PERTAMA.</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify">Demikian, Akad ini dibuat dan ditandatangani di.................. oleh PIHAK PERTAMA dan PIHAK KEDUA, bermeterai cukup dalam dua rangkap, yang masing-masing disimpan oleh PIHAK PERTAMA dan PIHAK KEDUA, dan masing-masing berlaku sebagai aslinya.</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PIHAK PERTAMA&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PIHAK KEDUA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Menyetujui,</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&hellip;&hellip;&hellip;..&hellip;&hellip;&hellip;&hellip;..&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &hellip;.&hellip;&hellip;&hellip;.&hellip;&hellip;&hellip;&hellip;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Saksi-saksi,</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</strong><strong>&hellip;.</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
<p align="justify"><em><u><br /> </u></em></p>
<p align="justify"><em><u>&nbsp;</u></em></p>
<p align="justify">(Lampiran I)</p>
<p align="justify"><strong>SURAT PERMOHONAN REALISASI</strong></p>
<p align="justify"><strong>(DIBUAT DI KOP SURAT PIHAK KEDUA)</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>Kepada&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
<p align="justify"><strong>PIHAK PERTAMA </strong></p>
<p align="justify"><strong>Kantor Cabang &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</strong></p>
<p align="justify"><strong>[&hellip;..alamat&hellip;.]</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify">Bersama ini kami yang bertanda tangan di bawah ini:</p>
<p align="justify">Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<strong>[ 1 ]</strong></p>
<p align="justify">Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<strong>[ 1 ]</strong></p>
<p align="justify">bertindak untuk diri sendiri/dalam kedudukannya selaku &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.<strong>[ 1 ]</strong> dari, dan karenanya berdasarkan .&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.. bertindak untuk dan atas nama..........................................................................<strong>[ 1 ]</strong></p>
<p align="justify">dengan mengacu pada Akad Pembiayaan Mudharabah nomor &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;<strong>[ 1 ]</strong> tanggal &hellip;&hellip;&hellip;&hellip;&hellip;..<strong>[ 1 ]</strong> dibuat dihadapan &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;<strong>[ 1 ]</strong> Notaris di &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..<strong>[ 1 ]</strong>, selanjutnya disebut &ldquo;<strong>Akad</strong>&rdquo;, dengan ini mengajukan realisasi Fasilitas Pembiayaan Mudharabah, dengan rincian&nbsp;&nbsp;&nbsp; <strong>[ 3 ]</strong>:</p>
<p align="justify">&nbsp;</p>
<ol>
<li>Tujuan Fasilitas Pembiayaan Mudharabah : .....................................................</li>
</ol>
<p align="justify">&nbsp;</p>
<ol start="2">
<li>Jangka Waktu : &hellip;&hellip;.. (&hellip;&hellip;&hellip;) sejak realiasi Fasilitas Pembiayaan Mudharabah.</li>
</ol>
<p align="justify">&nbsp;</p>
<ol start="3">
<li>Jumlah Fasilitas Pembiayaan Mudharabah yang dimohonkan Rp&hellip;&hellip;&hellip;&hellip;&hellip;..(&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;)</li>
</ol>
<p align="justify">&nbsp;</p>
<ol start="4">
<li>Nisbah PIHAK PERTAMA : &hellip;..%</li>
</ol>
<p align="justify">&nbsp;</p>
<ol start="5">
<li>Nisbah PIHAK KEDUA : &hellip;.%</li>
</ol>
<p align="justify">&nbsp;</p>
<ol start="6">
<li>Periode Bagi Hasil : &hellip;&hellip;&hellip;&hellip;..(&hellip;&hellip;&hellip;&hellip;&hellip;.) bulan</li>
</ol>
<p align="justify">&nbsp;</p>
<p align="justify">Demikian permohonan ini kami ajukan, atas kesediannya kami ucapkan terima Kasih.</p>
<p align="justify">&nbsp;</p>
<p align="justify">(Tempat), (Tanggal/Bulan/Tahun)</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">&nbsp;</p>
<p align="justify">PIHAK KEDUA<em><u><br /> </u></em><u>Lampiran II</u></p>
<p align="justify"><em><u>&nbsp;</u></em></p>
<p align="justify"><strong>TANDA TERIMA UANG PIHAK KEDUA (MODAL)</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong><br /> </strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><u>Lampiran III</u></p>
<p align="justify"><strong>JADWAL PEMBAYARAN MODAL PIHAK PERTAMA DAN PROYEKSI BAGI HASIL</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>
<p align="justify"><strong>&nbsp;</strong></p>

<h3>PT Plazadana Mitra Investama</h3>
<h4>The only and the first islamic retail equity 
based crowdfunding market place in Indonesia!</h4>
<img src="{{asset('images/logos/origin/PLAZADANA-FA.png')}}"