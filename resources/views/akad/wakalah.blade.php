<p><strong>AKAD WAKALAH</strong></p>
<p><strong>Antara Investor dengan Plazadana</strong></p>
<p>No :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
<p>Pada hari ini &hellip;&hellip;&hellip;&hellip;, tanggal......... bulan ......... tahun ............., yang bertandatangan di bawah&nbsp;&nbsp; ini :</p>
<ol>
<li>Nama : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</li>
</ol>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; bertindak dalam&nbsp; jabatannya selaku Direktur, bertindak untuk dan atas nama serta mewakili PT Plazadana Mitra Investama. berkedudukan di Bekasi Selatan, Komplek Ruko Bekasi Mas No.10 Blok A, Jalan Jenderal Ahmad Yani berdasarkan Akta Pendirian Nomor 58 yang dibuat di hadapan Notaris Irene Kusumawadhani SH, di Bekasi dan telah mendapatkan pengesahan Menteri Kehakiman Nomor AHU-0011077.AH.01.01. Tahun 2019 Tanggal 28 Febuari 2019 di Jakarta, sebagai <em>Wakil</em>. Selanjutnya disebut &rdquo;<strong>Pihak Pertama</strong>&rdquo;.</p>
<ol>
<li>Nama : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</li>
</ol>
<p>Tempat/tanggal lahir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :...................................................................................</p>
<p>No.KTP/ Paspor&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.........&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
<p>NPWP&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :..................................................................................</p>
<p>Bertindak untuk diri sendiri dan untuk tindakan hukum ini telah mendapat persetujuan suami/isterinya yang sah yang bernama &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. Sesuai Akta Nikah Nomor &hellip;&hellip;. yang dikeluarkan oleh Kantor Urusan Agama&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;. /Kantor Catatan Sipil .............................. Sebagai <em>&ldquo;muwakkil/pemberi kuasa&rdquo;</em>, Selanjutnya disebut &ldquo;Pihak Kedua&rdquo;</p>
<p>&nbsp;</p>
<p>Pihak Pertama dan Pihak Kedua, selanjutnya bersama-sama disebut &ldquo;<strong>Para Pihak</strong>&rdquo;,&nbsp; terlebih dahulu menerangkan bahwa:</p>
<p>&nbsp;</p>
<ol>
<li><strong>Pihak Kedua</strong> berminat untuk investasi di Koperasi&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;dengan nilai Rp&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;(&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;Rupiah) berdasarkan proposal peluang investasi Koperasi&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;yang dtawarkan melalui <a href="http://www.plazadana.com">plazadana.com</a> selanjutnya disebut &ldquo;investasi&rdquo;</li>
</ol>
<p>&nbsp;</p>
<ol start="2">
<li>Sehubungan dengan Investasi tersebut, dengan ini Pihak Kedua/Investor/Pemberi Kuasa memberikan kuasa kepada Pihak Pertama/Penerima Kuasa khusus untuk dan atas nama Pihak Kedua/Pemberi Kuasa melakukan hal-hal sebagai berikut:</li>
<li>Membuat dan melangsungkan akad/perjanjian dengan Pihak Koperasi sebagai pengelola usaha <em>(mudharib)</em> untuk kepentingan Pihak Kedua sebagai investor <em>(shahibul maal)</em> berdasarkan Akad <em>mudharabah</em>,</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>Membuat perubahan atas akad/perjanjian yang sudah ditandatangani oleh Wakil dan pihak Koperasi tersebut sepanjang perubahan tersebut sesuai dengan praktek industri yang berlaku umum dan wajar; dan</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>Mewakili segala kepentingan Pihak Kedua <em>(Muwakkil)</em> dalam rangka pelaksanaan akad/perjanjian <em>mudharabah</em> dengan pihak Koperasi sebagai pengelola usaha <em>(mudharib)</em>, termasuk namun tidak terbatas untuk melakukan penagihan, menerima dan menyalurkan seluruh imbal hasil usaha dari pihak Koperasi sebagai <em>mudharib</em> kepada Pihak Kedua sebagai <em>shahibul maal</em> (investor), dan lain-lain dalam rangka mewakili kepentingan Pihak Kedua</li>
</ol>
<p>Surat Kuasa ini diberikan tanpa hak substitusi.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PEMBERI KUASA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PENERIMA KUASA</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INVESTOR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PT PLAZADANA MITRA INVESTAMA</p>
<p><strong>&nbsp;</strong></p>