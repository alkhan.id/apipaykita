<!DOCTYPE html>
<html>
  <head>
    <title>Plazadana</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='' property='fb:admins'>
    <meta content='' property='fb:app_id'>

    <!-- {% block robot %} -->
    @yield('robot')
    <meta name="robots" content="noodp, noydir">
    <!-- {% endblock %} -->

    <link rel="shortcut icon" href="{{asset('icons/favicon.ico')}}">
    <link rel="alternate"  href="/feed/" type="application/rss+xml" title="Latest Feeds">
    <link rel="canonical" href="https://plazadana.com">

    <!-- {% block seo %} -->
    @yield('seo')
    <meta name="author" content="Plazadana">
    <meta name="description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
    <meta name="keywords" content="Plazadana, Retail equity, crowdfunding, syariah, koperasi, koperasi syariah">
    <meta property="og:locale" content="id_ID">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Plazadana">
    <meta property="og:description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
    <meta property="og:url" content="https://plazadana.com">
    <meta property="og:image" content="https://plazadana.com{{asset('images/logos/png/logo-name-medium.png')}}">
    <meta property="og:site_name" content="plazadana.com">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
    <meta name="twitter:image:src" content="https://plazadana.com{{asset('images/logos/png/logo-name-medium.png')}}">
    <meta name="twitter:title" content="Plazadana">
    <meta name="twitter:domain" content="plazadana.com">
    <meta name="twitter:creator" content="@plazadana.com">
    <!-- {% endblock %} -->

    <link rel="stylesheet" href="{{asset('css/theme.min.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/spfjs.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" media="all">
    @yield('css')
    <!-- {% block css %}{% endblock %} -->

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/uikit.min.js')}}"></script>
    <script src="{{asset('js/uikit-icons.min.js')}}"></script>
  </head>
  <body>
    <div class="uk-offcanvas-content">
      @include('navbar.navs')
      @include('navbar.messages')
      <div class="tm-main uk-section uk-section-default uk-base-section">
        <div class="uk-container uk-container-center">
          @yield('content')
        </div>
      </div>

      @include('navbar/chat')
      @include('navbar/cookies')
      @include('navbar/footer')
    </div>

    <!-- begin spfjs -->
    <div id="spfjs-progress-bar"></div>
    <script src="{{asset('js/spfjs.min.js')}}"></script>
    <script src="{{asset('js/spfjs-main.js')}}"></script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script>
  var konten = document.getElementById("editor1");
    CKEDITOR.replace(konten,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154899345-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154899345-1');
</script>
    @yield('js')
    <!-- end spfjs -->
    <!-- {% block js %}{% endblock %} -->
  </body>
</html>
