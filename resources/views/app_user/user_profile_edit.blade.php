@extends("base_dashboard")

@section("title")
  Profil
@endsection

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a class="spf-link" href="{{url('/')}}">Beranda</a></li>
    <li><a class="spf-link" href="{{url('user_dashboard')}}">\Dashboard</a></li>
    <li><span>\Sunting Profil</span></li>
  </ul>
@endsection

@section('content')
      @if (isset($warning))
      <div class="uk-alert-warning" uk-alert>
          <a class="uk-alert-close" uk-close></a>
          <p>{{$warning}}.</p>
      </div>
      @endif
  <div class="uk-child-width-expand@s" uk-grid>
    <div class="content-left">
      <div class="uk-card uk-card-default uk-card-hover uk-card-body">
    
        <form class="uk-form"  enctype="multipart/form-data" method="post" action="{{url('profile/update')}}">
        {{ csrf_field() }}

      <input type="text" name="id" value="{{$user->id}}" hidden>
        <div class="uk-panel">
            <div class="uk-margin">
              <label class="uk-form-label" for="form-full_name">
                Nama Lengkap <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
                <input id="form-full_name" class="uk-width-1-1 uk-input" name="full_name"
                  type="text" placeholder="Arya Gumelar" value="{{$user->fullName}}" required>
                  <input id="form-full_name" class="uk-width-1-1 uk-input" name="id"
                  type="text" placeholder="Arya Gumelar" value="{{$user->id}}" hidden>
                
              </div>
            </div>
            <div class="uk-margin">
              <label class="uk-form-label" for="form-full_name">
                Tempat & Tanggal Lahir <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
                <input id="form-full_name" class="uk-width-1-1 uk-input" name="tempatLahir"
                  type="text" placeholder="Jakarta" value="{{$user->tempatLahir}}" required>
                  <input id="form-full_name" class="uk-width-1-1 uk-input" name="tglLahir"
                  type="date" placeholder="" value="{{$user->tglLahir}}" required>
              </div>
            </div>

            <div class="uk-margin">
            <label class="uk-form-label" for="form-full_name">Foto Profile</label>
            <div class="uk-form-controls">
              <div uk-form-custom="target: true">
                <i class="uk-icon uk-icon-upload" uk-icon="icon: upload"></i>
                <input type="file" accept="image/*" name="photo" >
                <input class="uk-input uk-form-width-medium" type="text" placeholder="Pilih foto">
              </div>
              
              <span class="uk-text-muted uk-text-small">
                <br />Foto Profile Anda.
              </span>
            </div>
          </div>

            {{-- <div class="uk-margin">
              <label class="uk-form-label" for="form-full_name">
                Nama Ibu <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
                <input id="form-mother_name" class="uk-width-1-1 uk-input" name="mother_name"
                  type="text" placeholder="Ummu Khadijah" value="{{$user->motherName}}" required>
                
              </div>
            </div> --}}
            <div class="uk-margin">
              <label class="uk-form-label" for="form-ktp_number">
                Nomor KTP <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
                <input id="form-ktp_number" class="uk-width-1-1 uk-input" name="ktp_number"
                  type="number" placeholder="160317100692xxxx" value="{{$user->ktp}}" required>
                
              </div>
            </div>
            <div class="uk-margin">
              <label class="uk-form-label" for="form-npwp_number">
                Nomor NPWP <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
                <input id="form-npwp_number" class="uk-width-1-1 uk-input" name="npwp_number"
                  type="text" placeholder="31.364.002.4-490.xxx" value="{{$user->npwp}}" required>
                
              </div>
            </div>
            <div class="uk-margin">
              <label class="uk-form-label" for="form-phone_number">
                Nomor Telepon <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
                <input class="uk-width-1-1 uk-input" name="phone_number" type="text"
                  placeholder="0821-3701-xxxx" value="{{$user->hp}}" required>
                
              </div>
            </div>
            <div class="uk-margin">
              <div class="uk-form-controls">
                <p><i>Tanda * wajib diisi.</i></p>
                <button type="submit" class="uk-button uk-button-primary">
                  <span uk-icon="icon: check"></span> Simpan Profil
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="content-right">
      <div class="uk-card uk-card-default uk-card-hover uk-card-body">
      <div class="uk-panel uk-panel-box uk-text-center">
                        <img class="uk-border-circle" width="120" height="120" src="{{asset('file/'.Auth::user()->photo)}}" alt="">
                    </div>
        <table class="uk-table uk-table-small">
          <tbody>
              <th>Username</th>
              <td>{{$user->name}}</td>
            </tr>
            <tr>
              <th>Email</th>
              <td>{{$user->email}}</td>
            </tr>
            <!-- <tr>
              <th>Login Terakhir</th>
              <td><span title="Sep. 15, 2019, 9:39 p.m">0 menit yang lalu</span></td>
            </tr> -->
            <tr>
              <th>Virtual Account</th>
              <td>{{$user->va}}</td>
            </tr>
            <tr>
              <th>Saldo</th>
              <td>{{$user->saldo}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="uk-placeholder uk-margin-top">
        <i uk-icon="info"></i> Mohon sesuaikan data anda dengan teliti. Data profil ini digunakan tiap kali anda akan melakukan Investasi atau membuat Proposal.
      </div>
    </div>
  </div>

@endsection
