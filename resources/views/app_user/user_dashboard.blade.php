@extends('base_dashboard')

@section('breadcrumb')
  <ul class="uk-breadcrumb uk-breadcrumb-dashboard">
    <li><a href="/">Beranda</a></li>
    <li><span>Dashboard</span></li>
  </ul>
@endsection

@section('content')
  <div class="uk-child-width-1-4@m uk-grid-small uk-grid-match uk-grid" uk-grid="">
    <div class="uk-first-column">
      <div class="uk-card uk-card-hover uk-card-default uk-card-body">
        <h4 class="uk-margin-remove-bottom uk-text-bold">{{ $myInvest }}</h4>
        <p class="uk-margin-remove">Investasiku disetujui</p>
        <div class="uk-position-top-right uk-padding-small uk-text-primary">
          <span uk-icon="icon:paint-bucket;ratio:1" class="uk-margin-small-left uk-icon"></span>
        </div>
      </div>
    </div>
    <div>
      <div class="uk-card uk-card-hover uk-card-default uk-card-body">
        <h4 class="uk-margin-remove-bottom uk-text-bold">{{ '0' }}</h4>
        <p class="uk-margin-remove">Proposalku disetujui</p>
        <div class="uk-position-top-right uk-padding-small uk-text-primary">
          <span uk-icon="icon:copy;ratio:1" class="uk-margin-small-left uk-icon">{{$myTeaser}}</span>
        </div>
      </div>
    </div>
    <div>
      <div class="uk-card uk-card-hover uk-card-primary uk-card-body">
        <h5 class="uk-margin-remove-bottom uk-text-bold">Rp {{ number_format($total) }},-</h5>
        <p class="uk-margin-remove">Total Investasiku</p>
        <div class="uk-position-top-right uk-padding-small">
          <span uk-icon class="uk-margin-small-left uk-icon uk-text-bold">{{$myInvest}}</span>
        </div>
      </div>
    </div>
    <div>
      <div class="uk-card uk-card-hover uk-card-secondary uk-card-body">
        <h5 class="uk-margin-remove-bottom uk-text-bold">{{ '0' }},-</h5>
        <p class="uk-margin-remove">Total Galang danaku</p>
        <div class="uk-position-top-right uk-padding-small">
          <span uk-icon class="uk-margin-small-left uk-icon uk-text-bold">$</span>
        </div>
      </div>
    </div>
  </div>

  <h3 class="uk-heading-line uk-heading-bullet">
    <span>Peluang Investasi</span>
  </h3>

  <div class="uk-grid uk-child-width-expand@s" uk-grid>
  @foreach($teasers as $teaser)
      <div class="uk-width-1-3@m content">
        <div class="uk-card uk-card-hover uk-card-default">
          <div class="uk-card-media-top">
            <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">
              <img class="teaser-card" src="{{asset('file/'.$teaser->featured_image)}}" alt="{{$teaser->title}}">
            </a>
          </div>
          <div class="uk-card-body uk-padding-20">
            <a href="{{url('teaser_category_list')}}" class="uk-card-badge uk-label spf-link">{{$teaser->category->title}}</a>
            <h3 class="uk-card-title uk-margin-remove text-limit">
              <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">{{$teaser->title}}</a>
            </h3>
            <p></p>
            <?php
                                $bills = App\Models\Bill::where('teaser_id', $teaser->id)->get();
                                // dd($bills);
                                $kosong = 0;
                                $terkumpul = 0;
                                foreach($bills as $bill){
                                  $terkumpul = $kosong + $bill->BILL;
                                }
                                $persen = $terkumpul / $teaser->amount * 100;
                              ?>
            <progress class="uk-progress" value="{{$teaser->percentage_of_collected_funds}}" max="100"></progress>

            <table class="uk-table u uk-table-small uk-table-justify">
              <tbody>
                <tr>
                  <td>Nomor &amp; ID</td>
                  <td><strong>{{$teaser->cooperative_id}} :: {{$teaser->id}}</strong></td>
                </tr>
                <tr>
                  <td>Target</td>
                  <td><strong>Rp {{number_format($teaser->amount)}},-</strong></td>
                </tr>
                <tr>
                <td>Dana yang terkumpul</td>
                <td>
                  <strong>Rp {{number_format($terkumpul)}},-</strong>
                  <span class="uk-label">{{$persen}}%</span>
                </td>
              </tr>
                <tr>
                  <td>Proyeksi Imbal hasil pertahun </td>
                  <td><strong>{{$teaser->percentage_of_return}}% / Tahun</strong></td>
                </tr>
                <tr>
                  <td>Pembayaran SHU</td>
                  <td><strong>
                  @if($teaser->payment_schedule == "monthly")1 bulan (bulanan)
                              @elseif($teaser->payment_schedule == "quarterly")3 bulan (triwulanan)
                              @elseif($teaser->payment_schedule == "semesters")6 bulan (semesteran)
                              @elseif($teaser->payment_schedule == "yearly")12 bulan (tahunan)
                              @endif

                  </strong></td>
                </tr>
                <tr>
                  <td>Kadaluarsa</td>
                  <td><strong>{{$teaser->expiration_date}}</strong></td>
                </tr>
                <tr>
                  <td>Sisa hari</td>
                  <td><strong>{{\Carbon\Carbon::parse($teaser->expiration_date)->diffForHumans()}}</strong></td>
                </tr>
                <tr>
                            <td></td>
                            <td>        
                            <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">
                            <button class="uk-button uk-button-primary">
                                <!-- <i uk-icon="sign-in"></i>  -->
                                Investasi
                              </button>
                            </a>  
                            </td>
                          </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    @endforeach  

    @if(count($teasers) == 0)
      <div class="uk-width-1-1@m content">
        <div class="uk-card uk-card-default">
          <div class="uk-card-body">
            <i uk-icon="info"></i> &nbsp; <span>Tidak ada peluang Investasi yang tersedia!</span>
          </div>
        </div>
      </div>
    @endif
  </div><!-- end /.uk-child-width-expand@s -->

  @if(count($teasers) == 2)
    <div class="uk-text-center uk-margin">
      <a class="uk-button uk-button-large uk-button-primary spf-link" href="{% url 'teaser_list' %}">
        <span uk-icon="forward" class="uk-icon"></span> Lihat penggalangan dana lainnya...
      </a>
    </div>
  @endif
@endsection
