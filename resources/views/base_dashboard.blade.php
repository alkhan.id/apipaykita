<!DOCTYPE html>
<html>
  <head>
    <title>Plazadana</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='' property='fb:admins'>
    <meta content='' property='fb:app_id'>

    <meta name="robots" content="NONE,NOARCHIVE" />
    

    <link rel="shortcut icon" href="{{asset('icons/favicon.ico')}}">
    <link rel="alternate"  href="/feed/" type="application/rss+xml" title="Latest Feeds">
    <link rel="canonical" href="https://plazadana.com">

    <link rel="stylesheet" href="{{asset('css/theme.min.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/spfjs.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" media="all">
  

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/uikit.min.js')}}"></script>
    <script src="{{asset('js/uikit-icons.min.js')}}"></script>
  </head>
  <body>
    <div class="uk-offcanvas-content">
      @include('navbar/navs_dashboard')

      <div class="tm-main uk-section uk-section-default uk-base-section">
        <div class="uk-container uk-container-center">
          <div uk-grid>
            <div class="uk-width-1-5@m"></div>
            <div class="uk-width-expand@m">
              @yield('breadcrumb')
              <!-- include "includes/messages.html')}} -->
              @yield('content')
            </div>
          </div>
        </div>
      </div>

       <!-- include "includes/chat.html')}} -->
      <!-- include "includes/cookies.html')}} -->
      @include('navbar/footer')}} -->
    </div><!-- end  /.uk-offcanvas-content -->

    <!-- begin spfjs -->
    @yield('js')

    <div id="spfjs-progress-bar"><dt></dt><dd></dd></div>
    <script src="{{asset('js/spfjs.min.js')}}"></script>
    <script src="{{asset('js/spfjs-main.js')}}"></script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script>
  var konten = document.getElementById("editor1");
    CKEDITOR.replace(konten,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154899345-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154899345-1');
</script>

</html>
