@extends('base')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

@section('content')

<h4 class="uk-heading-bullet">Who we are?</h4>
<!-- -->
<p>Plazadana adalah platform digital khusus untuk mempertemukan peluang investasi koperasi dengan anggota/calon anggota Koperasi yang terdaftar di <a href="{{url('/')}}">www.plazadana.com</a>, ber-akad syariah sesuai dengan Fatwa Dewan Syariah Nasional (DSN).</p>
<p>Misi Plazadana adalah mendukung Pemerintah dalam meningkatkan literasi dan inklusi keuangan terutama kelompok masyarakat yang tergabung dalam Koperasi, sehingga tercipta perluasan kesempatan kerja serta pemerataan pendapatan dan terwujudnya Koperasi dalam mendorong pertumbuhan ekonomi dan pengentasan kemiskinan.</p>
<p>Plazadana adalah aggregator ecosystem online yang bertujuan untuk menfasilitasi para pelaku ekonomi <em>(Koperasi, anggota, Investor, Lembaga Keuangan Syariah, Fund Manager, Nadzir Waqaf, Asuransi Syariah, Penjaminan Pembiayaan (Kafil), Rating Agency, Sharia Expert, KAP, Lawyers, dan Lembaga Penunjang sertaa Profesi terkait)</em> dalam berinteraksi secara transaksional sehingga menghasilkan nilai tambah &amp; berkontribusi dalam perluasan kesempatan kerja, pemerataan pendapatan, pertumbuhan ekonomi serta pengentasan kemiskinan.</p>
<p>Untuk mengemban misi mulia tersebut, Plazadana mengundang para milenials berbakat untuk bersinergi dengan Plazadana dan berperan dengan detail sebagai berikut:</p>
<ol>
<li>Software Engineer (Android Apps)</li>
<li>Software Engineer (IOS Apps)</li>
<li>Web Developer</li>
<li>System Engineer</li>
<li>UI Designer</li>
<li>Social Media Specialist</li>
<li>Graphic Designer</li>
<li>Relationship Manager (Financing &amp; Investment)</li>
<li>Accounting &amp; Finance Staff</li>
</ol>

<a data-toggle="collapse" data-target="#demo">Pelajari lebih lanjut&hellip;&hellip;</a>
<br><br>
<div id="demo" class="collapse">
    <h4 class="uk-heading-bullet">Software Engineer, Android Apps</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td>
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Membangun aplikasi mobile dengan platform Android.</p>
    <p>o&nbsp;&nbsp; Bekerjasama dengan perancang UX dan UI membentuk aplikasi dengan tampilan yang menarik dan sesuai dengan perilaku pengguna aplikasi mobile.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Mengembangkan aplikasi Android yang ramah pengguna sesuai dengan kebutuhan bisnis.</p>
    <p>o&nbsp;&nbsp; Melakukan tinjauan kode dan pengujian aplikasi</p>
    <p>o&nbsp;&nbsp; Memastikan aplikasi berjalan baik dan kompatibel dengan berbagai perangkat dan memenuhi desain aplikasi dan antarmuka</p>
    <p>o&nbsp;&nbsp; Mampu bekerja sama dengan tim dari berbagai fungsi</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Lulusan SMK/D3/ S1 di bidang Ilmu Komputer/ Teknik Informatika, atau bidang yang berhubungan dengan pengembangan perangkat lunak.</p>
    <p>o&nbsp;&nbsp; Memiliki keinginan yang kuat untuk belajar, membantu dan berbagi.</p>
    <p>o&nbsp;&nbsp; Rendah hati dan mampu bekerja sama sebagai tim.</p>
    <p>o&nbsp;&nbsp; Memiliki pengetahuan dan pengalaman dalam mengembangkan aplikasi mobile menggunakan native Android maupun React Native.</p>
    <p>o&nbsp;&nbsp; Memahami prinsip pemrograman berorientasi objek dan prinsip desain.</p>
    </td>
    </tr>
    </tbody>
    </table>
    <ul>
    </ul>
    <table width="95%">
    <tbody>
    <tr>
    <td>
    <table width="90%">
    <tbody>
    <tr>
    <td>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    <tr>
    <td>
    </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">Software Engineer, iOS Apps</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td>
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Membangun aplikasi mobile dengan platform iOS.</p>
    <p>o&nbsp;&nbsp; Bekerjasama dengan perancang UX dan UI membentuk aplikasi dengan tampilan yang menarik dan sesuai dengan perilaku pengguna aplikasi mobile.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Mengembangkan aplikasi iOS yang ramah pengguna sesuai dengan kebutuhan bisnis.</p>
    <p>o&nbsp;&nbsp; Melakukan tinjauan kode dan pengujian aplikasi</p>
    <p>o&nbsp;&nbsp; Memastikan aplikasi berjalan baik dan kompatibel dengan berbagai perangkat dan memenuhi desain aplikasi dan antarmuka</p>
    <p>o&nbsp;&nbsp; Mampu bekerja sama dengan tim dari berbagai fungsi</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Lulusan SMK/D3/ S1 di bidang Ilmu Komputer/ Teknik Informatika, atau bidang yang berhubungan dengan pengembangan perangkat lunak.</p>
    <p>o&nbsp;&nbsp; Memiliki keinginan yang kuat untuk belajar, membantu dan berbagi.</p>
    <p>o&nbsp;&nbsp; Rendah hati dan mampu bekerja sama sebagai tim.</p>
    <p>o&nbsp;&nbsp; Memiliki pengetahuan dan pengalaman dalam mengembangkan aplikasi mobile menggunakan native iOS maupun React Native.</p>
    <p>o&nbsp;&nbsp; Memahami prinsip pemrograman berorientasi objek dan prinsip desain.</p>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">Web Developer</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="49">
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Membangun aplikasi web.</p>
    <p>o&nbsp;&nbsp; Bekerjasama dengan perancang UX dan UI membentuk aplikasi dengan tampilan yang menarik dan sesuai dengan perilaku pengguna aplikasi web.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Mengembangkan aplikasi web yang ramah pengguna sesuai dengan kebutuhan bisnis.</p>
    <p>o&nbsp;&nbsp; Melakukan tinjauan kode dan pengujian aplikasi</p>
    <p>o&nbsp;&nbsp; Memastikan aplikasi berjalan baik dan kompatibel dengan berbagai perangkat dan memenuhi desain aplikasi dan antarmuka</p>
    <p>o&nbsp;&nbsp; Mampu bekerja sama dengan tim dari berbagai fungsi.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Lulusan SMK/D3/ S1 di bidang Ilmu Komputer/ Teknik Informatika, atau bidang yang berhubungan dengan pengembangan perangkat lunak.</p>
    <p>o&nbsp;&nbsp; Memiliki keinginan yang kuat untuk belajar, membantu dan berbagi.</p>
    <p>o&nbsp;&nbsp; Rendah hati dan mampu bekerja sama sebagai tim.</p>
    <p>o&nbsp;&nbsp; Memahami prinsip pemrograman berorientasi objek dan prinsip desain.</p>
    <p>o&nbsp;&nbsp; Memiliki pengetahuan dan pengalaman dalam mengembangkan aplikasi web menggunakan bahasa Java/ PHP/ ASP serta terbiasa menggunakan developer framework seperti Laravel/ Code Igniter/ React/ dll.</p>
    <p>o&nbsp;&nbsp; Memahami prinsip dan memiliki keterampilan mengembangkan API, web service/ resource</p>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">System Engineer</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="34">
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>Mengelola environment server aplikasi untuk mendukung aplikasi bisnis dan produktivitas.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Mengelola server aplikasi.</p>
    <p>o&nbsp;&nbsp; Mengelola perangkat dan aplikasi jaringan komputer.</p>
    <p>o&nbsp;&nbsp; Menyiapkan environment untuk konektifitas dengan berbagai pihak.</p>
    <p>o&nbsp;&nbsp; Memastikan linkungan sistem sesuai dengan kebutuhan pengembangan dan produksi.</p>
    <p>o&nbsp;&nbsp; Mampu bekerja sama dengan tim dari berbagai fungsi.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Lulusan SMK/D3/ S1 di bidang Ilmu Komputer/ Teknik Informatika, atau bidang yang berhubungan dengan pengembangan perangkat lunak.</p>
    <p>o&nbsp;&nbsp; Memiliki keinginan yang kuat untuk belajar, membantu dan berbagi.</p>
    <p>o&nbsp;&nbsp; Rendah hati dan mampu bekerja sama sebagai tim.</p>
    <p>o&nbsp;&nbsp; Memiliki pengetahuan dan pengalaman dalam mengelola jaringan dan server di lingkungan UNIX/ Linux/ Windows.</p>
    <p>o&nbsp;&nbsp; Memiliki pengalaman dalam mengelola infrastruktur baik setup maupun perawatan dan monitoring.</p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">UI Designer</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="38">
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Membangun desain UI untuk aplikasi mobile maupun web.</p>
    <p>o&nbsp;&nbsp; Bekerjasama dengan perancang UX dan developer membentuk aplikasi dengan tampilan yang menarik dan sesuai dengan perilaku pengguna aplikasi mobile.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Mengembangkan desain aplikasi web dan mobile yang ramah pengguna sesuai dengan kebutuhan bisnis.</p>
    <p>o&nbsp;&nbsp; Memastikan aplikasi berjalan baik dan kompatibel dengan berbagai perangkat dan memenuhi desain aplikasi dan antarmuka</p>
    <p>o&nbsp;&nbsp; Mampu bekerja sama dengan tim dari berbagai fungsi.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Lulusan SMK/D3/ S1 di bidang Ilmu Komputer/ Teknik Informatika, atau bidang yang berhubungan dengan pengembangan perangkat lunak.</p>
    <p>o&nbsp;&nbsp; Memiliki keinginan yang kuat untuk belajar, membantu dan berbagi.</p>
    <p>o&nbsp;&nbsp; Rendah hati dan mampu bekerja sama sebagai tim.</p>
    <p>o&nbsp;&nbsp; Memahami prinsip desain dan memiliki pengalaman dalam mengembangkan desain UI untuk aplikasi web/ mobile.</p>
    <p>o&nbsp;&nbsp; Memiliki keterampilan menggunakan aplikasi desain dan image/ video processing.</p>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">Social Media Specialist</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="77">&nbsp;</td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>Social media specialist berkontribusi untuk menjalankan fungsi marketing, mencakup aktifitas sales dan brand awareness. Fungsi ini merupakan bagian penting dari sebuah perusahaan berbasis technology internet dengan tantangan bagaimana social media dapat men-disrupt pasar dan efektif dalam menjalankan fungsi marketing.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Menginisisasi konsep dan strategi social media yang efektif untuk perusahaan</p>
    <p>o&nbsp;&nbsp; Membangun image brand melalui social media</p>
    <p>o&nbsp;&nbsp; Menjalankan campaign terkait event, promo, product, dll</p>
    <p>o&nbsp;&nbsp; Membuat social media content yang bermutu dan tepat sasaran</p>
    <p>o&nbsp;&nbsp; Membuat analisa dan laporan pengggunaan social media secara berkala</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; D1/D3/S1 Ilmu komunikasi, Ilmu humaniora</p>
    <p>o&nbsp;&nbsp; Pengalaman di bidang yang sama min. 2 tahun</p>
    <p>o&nbsp;&nbsp; Fresh graduated dengan eksposur social media yang proven</p>
    <p>o&nbsp;&nbsp; Max. 27 tahun</p>
    <p>o&nbsp;&nbsp; Memahami industri e-commerce</p>
    <p>o&nbsp;&nbsp; Dapat bekerjasama dengan team dan self driven</p>
    <p>o&nbsp;&nbsp; Menyukai tantangan baru, kemauan belajar dan meng-ekplorasi yang tinggi</p>
    <p>o&nbsp;&nbsp; Proficient in English, both oral and written</p>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">Graphic Designer</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="55">
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Kami mengundang kandidat yang memiliki minat dan passion untuk bergabung dengan perusahaan start-up yang sedang berkembang di bidang Teknologi Keuangan (Fintech) syariah.</p>
    <p>o&nbsp;&nbsp; Anda akan menjadi tim inti dalam mengembangkan brand perusahaan dan process online marketing. Pekrjaan ini akan berinteraksi secara intense dengan bagian Product, Marketing, dan Sales Team.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Menciptakan sebuah materi komunikasi visual yang modern dan efektif, dalam berbagai format media komunikasi</p>
    <p>o&nbsp;&nbsp; Mampu bekerja dengan team work dan dapat menyelesaikan pekerjaan sesuai target yang disepakati</p>
    <p>o&nbsp;&nbsp; Selalu update dengan perkembangan design visual terkini</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Memiliki pilihan dan selera design yang baik, dibuktikan dengan portfolio yang berkualitas tinggi</p>
    <p>o&nbsp;&nbsp; Mahir dalam mengoperasikan Adobe Photoshop, Adobe Illustrator dan alat desain lain.</p>
    <p>o&nbsp;&nbsp; SMK / Diploma dalam bidang Advertising atau Visual Communication Design</p>
    <p>o&nbsp;&nbsp; Fresh Graduate sangat disambut untuk mendaftar. Memiliki pengalaman kerja dalam posisi yang sama merupakan nilai tambah.</p>
    <p>o&nbsp;&nbsp; Mampu menghasilkan .gif dan grafik gerak sederhana yang berkualitas baik merupakan nilai tambah.</p>
    <p>o&nbsp;&nbsp; Memiliki keterampilan interpersonal dan komunikasi yang kuat.</p>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">Relationship Manager</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="81">
    </td>
    <td>
    <p>Permanen / Full Time<br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Kami mengundang kandidat yang memiliki minat dan passion untuk bergabung dengan perusahaan start-up yang sedang berkembang di bidang Teknologi Keuangan (Fintech).</p>
    <p>o&nbsp;&nbsp; Mencari dan membangun hubungan bisnis dengan potensial nasabah dan menjaga hubungan baik existing nasabah, dengan menyediakan layanan finansial dan solusi untuk memenuhi kebutuhan mereka dan memberikan respon secara aktif untuk kebutuhan mereka. Bertanggung jawab untuk menjalankan KYC-Principles untuk calon nasabah.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Fokus utama untuk menawarkan pembiayaan syariah kepada Koperasi, menyiapkan dan mengajukan proposal pembiayaan disertai dengan analisa kelayakan bisnis/usaha yang memadai dan aman bagi Financiers (pemodal).</p>
    <p>o&nbsp;&nbsp; Melakukan proses pembiayaan mulai dari proposal, pengajuan approval, penandatanganan pembiayaan, dan administrasi pembiayaan seperti pencairan dan perpanjangan.</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Minimum pendidikan: D3 semua jurusan</p>
    <p>o&nbsp;&nbsp; Maksimum umur 35 tahun</p>
    <p>o&nbsp;&nbsp; Memiliki pengalaman minimal 2 tahun di industry perbankan sebagai Account Officer / Relationship Manager Lending / Relationship Officer SME</p>
    <p>o&nbsp;&nbsp; Memiliki pengetahuan yang baik mengenai produk perbankan</p>
    <p>o&nbsp;&nbsp; Memiliki jaringan yang luas</p>
    <p>o&nbsp;&nbsp; Memiliki motivasi tinggi dan berorientasi pada target</p>
    <p>o&nbsp;&nbsp; Mahir menggunakan computer khususnya Microsoft Office</p>
    <p>o&nbsp;&nbsp; Memiliki kemampuan komunikasi dan interpersonal yang baik</p>
    <p><strong>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji dan fasilitas tambahan ke <b> <em>hr@plazadana.com</em> </b></strong></p>
    </td>
    </tr>
    </tbody>
    </table>
    <h4 class="uk-heading-bullet">Accounting Staff</h4>
    <table width="90%">
    <tbody>
    <tr>
    <td colspan="2">
    <p><strong>Sifat</strong></p>
    </td>
    </tr>
    <tr>
    <td width="424">
    <p>&nbsp;Permanen /Full Time</p>
    </td>
    <td>
    <p><br /> </p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Deskripsi Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>Mencatat, memeriksa dan melaporkan semua transaksi yang berkaitan dengan keuangan perusahaan pada jangka waktu tertentu misalnya akhir bulan, triwulan, semester atau akhir tahun, pencatatan yang telah dilakukan dibuat laporan yang disebut dengan Laporan Keuangan</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Tanggung Jawab Pekerjaan</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Melakukan kegiatan bagian Akunting (Accounting) yang mencakup ke bagian finance, payroll dan analis keuangan</p>
    <p>o&nbsp;&nbsp; Membuat jurnal accounting</p>
    <p>o&nbsp;&nbsp; Membuat laporan keuangan/accounting.</p>
    <p>o&nbsp;&nbsp; Menyusun cash flow (keuangan / accounting).</p>
    <p>o&nbsp;&nbsp; Berdomisili di Bekasi</p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p><strong>Kualifikasi</strong></p>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <p>o&nbsp;&nbsp; Pendidikan SMK-D3-S1 Akuntansi ( IPK min : 2,75 &ndash; di utamakan )</p>
    <p>o&nbsp;&nbsp; Pria / Wanita</p>
    <p>o&nbsp;&nbsp; Usia maksimal 35 tahun.</p>
    <p>o&nbsp;&nbsp; Memiliki pengalaman 1 &ndash; 2 tahun ( di utamakan ).</p>
    <p>o&nbsp;&nbsp; Memiliki pemahaman ilmu Akuntansi yang baik.</p>
    <p>o&nbsp;&nbsp; Mampu membuat laporan keuangan.</p>
    <p>o&nbsp; Detail, teliti, dapat mengikuti instruksi dengan akurat, memiliki inisiatif dan mampu memenuhi target pekerjaan.</p>
    <p>o&nbsp; Fresh Graduate Wellcome</p>
    <p>Kirimkan CV beserta &amp; cantumkan ekspektasi gaji ke <b><em>hr@plazadana.com</em></b></p>
    </td>
    </tr>
    </tbody>
    </table>
</div>
@endsection