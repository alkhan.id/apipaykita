<!DOCTYPE html>
<html>
  <head>
    <title>Plazadana</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='' property='fb:admins'>
    <meta content='' property='fb:app_id'>
    <meta name="robots" content="noodp, noydir">

    <link rel="shortcut icon" href="{%  static 'icons/favicon.ico' %}">
    <link rel="alternate"  href="/feed/" type="application/rss+xml" title="Latest Feeds">
    <link rel="canonical" href="https://plazadana.com">

    <meta name="author" content="Plazadana">
    <meta name="description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
    <meta name="keywords" content="Plazadana, Retail equity, crowdfunding, syariah, koperasi, koperasi syariah">
    <meta property="og:locale" content="id_ID">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Plazadana">
    <meta property="og:description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
    <meta property="og:url" content="https://plazadana.com">
    <meta property="og:image" content="https://plazadana.com{{asset('images/logos/png/logo-name-medium.png')}}">
    <meta property="og:site_name" content="plazadana.com">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="Retail equity based crowdfunding platform Syariah pertama di Indonesia.">
    <meta name="twitter:image:src" content="https://plazadana.com{{asset('images/logos/png/logo-name-medium.png')}}">
    <meta name="twitter:title" content="Plazadana">
    <meta name="twitter:domain" content="plazadana.com">
    <meta name="twitter:creator" content="@plazadana.com">

    <link rel="stylesheet" href="{{asset('css/theme.min.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/spfjs.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" media="all">

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/uikit.min.js')}}"></script>
    <script src="{{asset('js/uikit-icons.min.js')}}"></script>
  </head>
  <body>
    <div class="uk-offcanvas-content">
      <div class="uk-section-primary tm-section-texture">
        @include('navbar.messages')
       
       
       
        <div uk-sticky="media: 960" class="tm-navbar-container uk-sticky uk-sticky-fixed">
  <div class="uk-container uk-container-expand">
    <nav class="uk-navbar" uk-navbar>
      <div class="uk-navbar-left">
        <a href="/" class="uk-navbar-item uk-logo spf-link">
          <img uk-svg src="{{asset('images/logos/svg/logo-full-small.svg')}}" class="uk-margin-small-right" hidden="true">
        </a>
      </div>
      <div class="uk-navbar-right">
        <ul class="uk-navbar-nav uk-visible@m">
          <li><a href="/" class="spf-link">Beranda</a></li>
          <li class="{{ Request::is('investor_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('investor_page')}}">Investor</a>
          </li>
          <li class="{{ Request::is('fundraising_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('fundraising_page')}}">Penggalangan Dana</a>
          </li>
          <li class="{{ Request::is('information_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('information_page')}}">Informasi</a>
          </li>
          <li class="{{ Request::is('our_team_page') ? 'uk-active' : '' }}">
            <a class="spf-link" href="{{url('our_team_page')}}">Tim Kami</a>
          </li>

          @auth
            <li class="menu-notification">
              <!-- include("navbar/notifications") -->
            </li>
            <li class="menu-features">
              <a><i uk-icon="icon: more-vertical"></i></a>
              <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                <div class="uk-navbar-dropdown-grid uk-child-width-1-2" uk-grid>
                  <div>
                    <!-- it_officer or superuser -->
                    @if(Auth::User()->role == 1 || Auth::User()->role == 2 )
                      <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li class="uk-nav-header">IT Officer</li>
                        <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_proposal')}}">
                             <span>Proposal</span>
                            
                           </a>
                        </li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_investation_list')}}">
                             <span>Investasi</span>
                            
                          </a>
                        </li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_payment_list')}}">
                             <span>Pembayaran</span>
                             
                          </a>
                        </li>
                        <li><a class="spf-link" href="{{url('dashboard_cooperative_list')}}">Koperasi</a></li>
                        <li><a class="spf-link" href="{{url('dashboard_user_list')}}">Pengguna</a></li>
                        <li><a class="spf-link" href="{{url('category')}}">Kategori</a></li>
                      </ul>
                      <!-- relationship_manager' or request.user.is_staff  -->  
                    @elseif(Auth::User()->role == 3 || Auth::User()->role == 4)
                      <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li class="uk-nav-header">Relationship Manager</li>
                        <li><a class="spf-link" href="{{ url('dashboard_relationship_manager')}}">Dashboard</a></li>
                        <li>
                          <a class="spf-link uk-position-relative"
                             href="{{url('dashboard_report_list')}}">
                             <span>Laporan</span>
                            
                          </a>
                        </li>
                      </ul>
                    @else
                      <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li class="uk-nav-header">Investor</li>
                        <li><a class="spf-link" href="{{url('teaser_list')}}">Peluang Investasi</a></li>
                        <li>
                          <a class="spf-link uk-position-relative" href="{{url('investation_list')}}">
                            <span>Investasiku</span>
                           
                          </a>
                        </li>
                        <li class="uk-nav-header">Koperasi</li>
                        <li><a class="spf-link" href="{{url('proposal/create')}}">Kirim Proposal</a></li>
                        <li>
                          <a class="spf-link uk-position-relative" href="{{url('proposal')}}">
                            <span>Proposalku</span>
                            
                          </a>
                        </li>
                        <!-- <li>
                          <a class="spf-link uk-position-relative" href="{{url('report_list')}}">
                            <span>Laporanku</span>
                            
                          </a>
                        </li> -->
                        <li><a class="spf-link" href="{{url('cooperative')}}">Koperasiku</a></li>
                        <!-- <li><a class="spf-link" href="{{url('app_user:user_banks_list')}}">Akun Bank</a></li> -->
                      <!-- </ul>
                              
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top"> -->
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('profile')}}">Profil</a></li>
          </ul>
                    @endif
                  </div>
                  <div>
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                      <!-- user -->
                        @if(Auth::User()->role == 5)

                          <!-- <li class="uk-nav-header">Layanan</li>
                          <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
                          <li><a class="spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li> -->
                        @endif
                      <!-- <li class="uk-nav-header">Akun Saya</li>
                      <li><a class="spf-link" href="{{url('user_profile_edit')}}">Profil</a></li>
                      <li><a class="spf-link" href="{{url('account_email')}}">E-Mail</a></li>
                      <li><a class="spf-link" href="{{url('account_change_password')}}">Password</a></li>
                      <li class="uk-nav-divider"></li> -->
                      <div class="uk-panel uk-panel-box uk-text-center">
                      <img class="uk-border-circle" width="120" height="120" src="{{asset('file/'.Auth::user()->photo)}}" alt="">
                        
                    </div>
                    
                        <center>
                      <li class="uk-nav-header">Akun Saya</li>
                      </center>
                      <li>{{Auth::user()->name}}</li>
                      <li>{{Auth::user()->va}}</li>
                      <li>{{Auth::user()->email}}</li>
                      <li>Saldo : Rp {{Auth::user()->saldo}}</li>
                      <li class="uk-nav-divider"></li>
                      
                      <li>


                      <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>



                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
          @endauth
        </ul>

        @guest
          <div class="uk-navbar-item uk-visible@m">
            <a href="{{url('login')}}" class="uk-button uk-button-default tm-button-default uk-icon">
              Masuk / Daftar <canvas uk-icon="icon: sign-in" width="20" height="20" class="uk-icon" hidden="true"></canvas>
            </a>
          </div>
        @endguest
        
        <a uk-navbar-toggle-icon="" href="#offcanvas" uk-toggle="" class="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon">
          <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
            <rect y="9" width="20" height="2"></rect>
            <rect y="3" width="20" height="2"></rect>
            <rect y="15" width="20" height="2"></rect>
          </svg>
        </a>
      </div>
    </nav>
  </div>

  <div id="offcanvas" uk-offcanvas="mode: push; overlay: true" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
      <div class="uk-panel">
        <ul class="uk-nav uk-nav-default tm-nav">
          <li class="uk-nav-header">Umum</li>
          <li><a href="/" class="spf-link">Beranda</a></li>
          <li><a class="spf-link" href="{{url('investor_page')}}">Investor</a></li>
          <li><a class="spf-link" href="{{url('fundraising_page')}}">Penggalangan Dana</ali>
          <li><a class="spf-link" href="{{url('information_page')}}">Informasi</a></li>
          <li><a class="spf-link" href="{{url('our_team_page')}}">Tim Kami</a></li>
          <li><a class="spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li>
        </ul>

        @auth
            <!-- 'it_officer' or request.user.is_superuser  -->
          @if(Auth::User()->role == 1 || Auth::User()->role == 2)
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">IT Officer</li>
              <li><a class="spf-link" href="{{url('dashboard_it_officer')}}">Dashboard</a></li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_proposal')}}">
                   <span>Proposal</span>
                  
                 </a>
              </li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_investation_list')}}">
                   <span>Investasi</span>
                   
                </a>
              </li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_payment_list')}}">
                   <span>Pembayaran</span>
                  
                </a>
              </li>
              <li><a class="spf-link" href="{{url('dashboard_cooperative_list')}}">Koperasi</a></li>
              <li><a class="spf-link" href="{{url('dashboard_user_list')}}">Pengguna</a></li>
              <li><a class="spf-link" href="{{url('category')}}">Kategori</a></li>
            </ul>
          
          @elseif(Auth::User()->role == 3 || Auth::User()->role == 4)
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">Relationship Manager</li>
              <li><a class="spf-link" href="{{ url('dashboard_relationship_manager')}}">Dashboard</a></li>
              <li>
                <a class="spf-link uk-position-relative"
                   href="{{url('dashboard_report_list')}}">
                   <span>Laporan</span>
                </a>
              </li>
            </ul>
          @else
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">Layanan</li>
              <li><a class="spf-link" href="{{url('user_dashboard')}}">Dashboard</a></li>
              <li><a class="spf-link" href="{{url('help_page')}}">Bantuan Pengguna</a></li>
              <li class="uk-nav-header">Investor</li>
              <li><a class="spf-link" href="{{url('teaser_list')}}">Peluang Investasi</a></li>
              <li>
                <a class="spf-link uk-position-relative" href="{{url('investation_list')}}">
                  <span>Investasiku</span>
                </a>
              </li>
              <li class="uk-nav-header">Koperasi</li>
              <li><a class="spf-link" href="{{url('proposal/create')}}">Kirim Proposal</a></li>
              <li>
                <a class="spf-link uk-position-relative" href="{{url('app_plaza:proposal_list')}}">
                  <span>Proposalku</span>
                </a>
              </li>
              <!-- <li>
                <a class="spf-link uk-position-relative" href="{{url('report_list')}}">
                  <span>Laporanku</span>

                </a>
              </li> -->
            <li><a class="spf-link" href="{{url('cooperative')}}">Koperasiku</a></li>
            </ul>
            <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
              <li class="uk-nav-header">Akun</li>
              <li><a class="spf-link" href="{{url('profile')}}">Profile</a></li>
            </ul>
          @endif

          <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('user_profile_edit')}}">Profil</a></li>
            <li><a class="spf-link" href="{{url('account_email')}}">Email</a></li>
            <li><a class="spf-link" href="{{url('account_change_password')}}">Password</a></li>
            <li class="uk-nav-divider"></li>
            <li>

            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>


            </li>
          </ul>
        @endauth
        @guest
          <ul class="uk-nav uk-nav-default tm-nav uk-margin-top">
            <li class="uk-nav-header">Akun</li>
            <li><a class="spf-link" href="{{url('login')}}">Masuk</a></li>
            <li><a class="spf-link" href="{{url('register')}}">Daftar</a></li>
          </ul>
        @endguest
      </div>
    </div>
  </div>
</div>

        <div uk-height-viewport="offset-top: true; offset-bottom: true" class="uk-section uk-section-small uk-flex uk-flex-middle uk-text-center" style="box-sizing: border-box; min-height: calc(100vh - 183px);">
          <div class="uk-width-1-1">
            <div class="uk-container">
              <p>
                <canvas class="uk-visible@s" width="168" height="155" uk-svg src="{{asset('images/logos/svg/logo-white-100x100.svg')}}" hidden="true"></canvas>
                <canvas class="uk-hidden@s" width="120" uk-svg src="{{asset('images/logos/svg/logo-white-100x100.svg')}}" hidden="true"></canvas>
              </p>
              <p class="uk-margin-medium uk-text-lead">
                <strong>Plazadana.com</strong> the only and the first islamic retail equity <br />
                based crowdfunding market place in <strong>Indonesia</strong>!
              </p>
              <div uk-grid class="uk-child-width-auto uk-grid-medium uk-flex-inline uk-flex-center uk-grid">
                <div class="uk-first-column">
                  <a uk-scroll href="#info" class="uk-button uk-button-primary tm-button-primary uk-button-large tm-button-large uk-visible@s">Selengkapnya</a>
                  <a uk-scroll href="#info" class="uk-button uk-button-primary tm-button-primary uk-hidden@s">Selengkapnya</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="uk-section-small">
          <div class="uk-container uk-container-expand uk-text-center uk-position-relative">
            <ul uk-margin class="uk-subnav tm-subnav uk-flex-inline uk-flex-center uk-margin-remove-bottom">
              <li>
                <a class="uk-text-link">
                  <span uk-icon="icon: users" class="uk-margin-small-right uk-icon"></span>
                  <span uikit-stargazers></span> pengguna
                </a>
              </li>
              <li>
                <a href="https://facebook.com/plaza.dana.5" target="_blank" class="uk-text-lowercase">
                  <span uk-icon="icon: facebook" class="uk-margin-small-right uk-icon"></span> @plazadana
                </a>
              </li>
              <li>
                <a href="https://twitter.com/plazadana" target="_blank" class="uk-text-lowercase">
                  <span uk-icon="icon: twitter" class="uk-margin-small-right uk-icon"></span> @plazadana
                </a>
              </li>
              <li>
                <a href="https://instagram.com/plazadana" target="_blank" class="uk-text-lowercase">
                  <span uk-icon="icon: instagram" class="uk-margin-small-right uk-icon"></span> @plazadana
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

     
    <app-root uk-height-viewport style="box-sizing: border-box; min-height: 100vh; height: 100vh;">

      <app-info id="info">
        <div class="uk-container uk-container-small">
          <div uk-slideshow="animation: slide; autoplay: true; autoplay-interval: 4000; max-height: 350" class="uk-slideshow">
            <div class="uk-position-relative uk-visible-toggle uk-margin-large-top">
              <ul class="uk-slideshow-items" uk-height-viewport="offset-top: true; offset-bottom: 43">
                <li class>
                  <div class="uk-position-center uk-text-center">
                    <h5>DAPATKAN PELUANG INVESTASI DENGAN MUDAH, <br/>POTENSI IMBAL HASIL KOMPETITIF &amp; BERKAH KARENA SESUAI SYARIAH MULAI DARI Rp 100.000,-</h5>
                    <p class="uk-margin-remove uk-width-xxlarge@l uk-width-xlarge@s uk-width-large@s uk-text-center">Plazadana.com adalah retail equity based crowdfunding platform syariah yang mengkhususkan diri dalam mempertemukan peluang investasi antara anggota terdaftar di www.plazadana.com dengan Koperasi/UKM.</p>

                    <div class="uk-grid-margin uk-grid" uk-grid>
                      <div class="uk-width-expand@m uk-width-1-2@s uk-first-column">
                        <div class="uk-panel uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class>
                          <a class="uk-button uk-button-primary tm-button-primary uk-button-large uk-width-1-1@s uk-visible@s spf-link"
                            href="{{url('teaser_list')}}">Mulai Berinvestasi</a>
                          <a class="uk-button uk-button-primary tm-button-primary uk-width-1-1@s uk-hidden@s spf-link"
                            href="{{url('teaser_list')}}">Mulai Berinvestasi</a>
                        </div>
                      </div>
                      <div class="uk-width-expand@m uk-width-1-2@s">
                        <div class="uk-panel uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class>
                          <a class="uk-button uk-button-default tm-button-primary uk-button-large uk-width-1-1@s uk-visible@s spf-link"
                            href="{{url('fundraising_page')}}">Dapatkan Modal</a>
                          <a class="uk-button uk-button-default tm-button-primary uk-width-1-1@s uk-hidden@s spf-link"
                            href="{{url('fundraising_page')}}">Dapatkan Modal</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class>
                  <div class="uk-position-center uk-text-center">
                    <h5>DAPATKAN MODAL USAHA DENGAN MUDAH, ADIL &amp; BERKAH KARENA DENGAN SKIM SYARIAH</h5>
                    <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Plazadana.com adalah retail equity based crowdfunding platform syariah yang mengkhususkan diri dalam mempertemukan peluang investasi antara anggota terdaftar di www.plazadana.com dengan Koperasi/UKM.</p>

                    <div class="uk-grid-margin uk-grid" uk-grid>
                      <div class="uk-width-expand@m uk-width-1-2@s uk-first-column">
                        <div class="uk-panel uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class>
                          <a class="uk-button uk-button-default tm-button-primary uk-button-large uk-width-1-1@s uk-visible@s spf-link"
                            href="{{url('teaser_list')}}">Mulai Berinvestasi</a>
                          <a class="uk-button uk-button-default tm-button-primary uk-width-1-1@s uk-hidden@s spf-link"
                            href="{{url('teaser_list')}}">Mulai Berinvestasi</a>
                        </div>
                      </div>
                      <div class="uk-width-expand@m uk-width-1-2@s">
                        <div class="uk-panel uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class>
                          <a class="uk-button uk-button-primary tm-button-primary uk-button-large uk-width-1-1@s uk-visible@s spf-link"
                            href="{{url('fundraising_page')}}">Dapatkan Modal</a>
                          <a class="uk-button uk-button-primary tm-button-primary uk-width-1-1@s uk-hidden@s spf-link"
                            href="{{url('fundraising_page')}}">Dapatkan Modal</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
              <a class="uk-position-center-left uk-position-small uk-slidenav-previous uk-icon uk-slidenav uk-visible@s" uk-slidenav-previous uk-slideshow-item="previous"></a>
              <a class="uk-position-center-right uk-position-small uk-slidenav-next uk-icon uk-slidenav uk-visible@s" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>

            <ul class="uk-dotnav uk-flex-center uk-margin uk-hidden@s">
              <li uk-slideshow-item="0" class>
                <a class="icon-slide">Item 1</a>
              </li>
              <li uk-slideshow-item="1" class>
                <a class="icon-slide">Item 2</a>
              </li>
            </ul>
          </div>
        </div>
      </app-info>

      <app-teasers id="teasers">
        <div class="uk-container uk-position-relative uk-position-z-index">
          <div class="uk-grid-large uk-grid uk-grid-stack" uk-grid>
            <div class="uk-width-1-1@m uk-first-column">
              <h3 class="uk-heading-line uk-text-center" uk-scrollspy="cls: uk-animation-slide-top; delay: 100; repeat: true">
                <span>Penggalangan dana yang sedang berlangsung</span>
              </h3>
            </div>
          </div>
          <div class="uk-grid-margin uk-grid" uk-grid
            uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-left-medium; delay:150; repeat: true">
            <!-- teaser -->
            @if(count($proposals) != 0)
              @foreach($proposals as $teaser)
                <div class="uk-width-expand@m uk-width-1-3@s {% if forloop.first %}uk-first-column{% endif %}">
                  <div class="uk-card uk-card-hover uk-card-default" uk-scrollspy-class>
                    <div class="uk-card-media-top">
                      <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">
                        <img class="teaser-card" src="{{asset('file/'.$teaser->featured_image)}}" alt="{{$teaser->title}}">
                      </a>
                    </div>
                    <div class="uk-card-body uk-padding-20">
                      <a href="{{url('teaser_category_list')}}" class="uk-card-badge uk-label spf-link">{{$teaser->category->title ?? ''}}</a>
                      <h3 class="uk-card-title uk-margin-remove text-limit">
                        <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">{{$teaser->title ?? ''}}</a>
                      </h3>
                      <p></p>

                      <?php
                                $bills = App\Models\Bill::where('teaser_id', $teaser->id)->get();
                                // dd($bills);
                                $kosong = 0;
                                $terkumpul = 0;
                                foreach($bills as $bill){
                                  $terkumpul = $kosong + $bill->BILL;
                                }
                                $persen = $terkumpul / $teaser->amount * 100;
                              ?>
                      <progress class="uk-progress" value="{{$persen}}" max="100"></progress>

                      <table class="uk-table u uk-table-small uk-table-justify">
                        <tbody>
                          <tr>
                            <td>Nomor &amp; ID</td>
                            <td><strong>{{$teaser->cooperative->id}} :: {{$teaser->id}}</strong></td>
                          </tr>
                          <tr>
                            <td>Target</td>
                            <td><strong>Rp {{number_format($teaser->amount)}},-</strong></td>
                          </tr>
                          <tr>
                            <td>Terkumpul</td>
                            <td>
                              <strong>Rp {{number_format($terkumpul)}}</strong>
                              <span class="uk-label">{{$persen}}%</span>
                            </td>
                          </tr>
                          <tr>
                            <td>Proyeksi Imbal hasil pertahun </td>
                            <td><strong>{{$teaser->percentage_of_return}}%</strong></td>
                          </tr>
                          <tr>
                            <td>Pembayaran SHU</td>
                            <td><strong>
                              @if($teaser->payment_schedule == "monthly")1 bulan (bulanan)
                              @elseif($teaser->payment_schedule == "quarterly")3 bulan (triwulanan)
                              @elseif($teaser->payment_schedule == "semesters")6 bulan (semesteran)
                              @elseif($teaser->payment_schedule == "yearly")12 bulan (tahunan)
                              @endif
                              </strong></td>
                          </tr>
                          <tr>
                            <td>Kadaluarsa</td>
                            <td><strong>{{$teaser->expiration_date}}</strong></td>
                          </tr>
                          <tr>
                            <td>Sisa hari</td>
                            <td><strong>{{\Carbon\Carbon::parse($teaser->expiration_date)->diffForHumans()}}</strong></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>        
                            <a href="{{url('teaser_detail/'.$teaser->id)}}" class="spf-link">
                            <button class="uk-button uk-button-primary">
                                <!-- <i uk-icon="sign-in"></i>  -->
                                Investasi
                              </button>
                            </a>  
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                @endforeach
              @else
              <div class="uk-width-expand@m uk-width-1-3@s uk-first-column">
                <div class="uk-card" uk-scrollspy-class>
                  <div class="uk-card-body uk-padding uk-text-center">
                    <i uk-icon="info"></i> &nbsp; Mohon maaf, Belum ada Peluang Investasi yang tersedia
                  </div>
                </div>
              </div>
              @endif
          </div>

          <!-- {% if teasers.count == 3 %}
            <div class="uk-text-center uk-margin">
              <a class="uk-button uk-button-large uk-button-primary spf-link" href="{{url('teaser_list')}}">
                <div class="uk-visible@s"><span uk-icon="forward"></span> Lihat penggalangan dana lainnya...</div>
                <div class="uk-hidden@s"><span uk-icon="forward"></span> Lihat lainnya...</div>
              </a>
            </div>
          {% endif %} -->

          <br /><br />
          <app-scroll href="#why-pazadana">
            <div class="uk-position-relative" uk-scrollspy="cls: uk-animation-slide-top; repeat: true">
              <center>
                <a class="arrow" uk-scroll href="#why-pazadana">
                <i uk-icon="icon: arrow-down" aria-hidden="true" class="uk-button-primary uk-box-shadow-large uk-icon-button uk-icon"></i>
              </a>
              </center>
            </div>
          </app-scroll>
          <br /><br />
        </div>
      </app-teasers>

      <app-why-pazadana id="why-pazadana">
        <div class="uk-container uk-position-relative uk-position-z-index">
          <h3 class="uk-heading-line uk-text-center" uk-scrollspy="cls: uk-animation-slide-top; delay: 100; repeat: true">
            <span>Mengapa Plazadana?</span>
          </h3>

          <div class="uk-card uk-card-body">
            <img src="{{asset('images/contents/icon-transaksi-mudah.png')}}" alt="Transaksi Mudah Plazadana">
            <br />
            <img width="100%" src="{{asset('images/contents/product-vs.png')}}" alt="Produk PlazaDana">
            <br /><br /><br />

            <app-scroll href="#team">
              <div class="uk-position-relative" uk-scrollspy="cls: uk-animation-slide-top; repeat: true">
                <center>
                  <a class="arrow" uk-scroll href="#team">
                  <i uk-icon="icon: arrow-down" aria-hidden="true" class="uk-button-primary uk-box-shadow-large uk-icon-button uk-icon"></i>
                </a>
                </center>
              </div>
            </app-scroll>
          </div>
        </div>
      </app-why-plazadana>

      <app-team id="team">
        <div class="uk-section uk-section-default uk-section-small uk-section-overlap uk-position-relative uk-padding-remove">
          <div class="uk-background-norepeat uk-background-cover uk-section uk-section-small uk-background-fixed uk-background-center-center" style="background-image: url({{asset('images/photo2.jpg')}});">
            <div class="uk-position-cover" style="background-color: rgba(3, 9, 25, 0.9);"></div>
            <div class="uk-container uk-container-small uk-padding-small uk-position-relative uk-position-z-index">
              <div class="uk-grid-large uk-margin-small uk-grid uk-grid-stack" uk-grid>
                <div class="uk-width-1-1@m uk-first-column">
                  <h2 class="uk-text-center uk-text-primary" uk-scrollspy="cls: uk-animation-slide-top; repeat: true">Tim Kami</h2>
                </div>
              </div>
              <div class="uk-grid-small uk-grid uk-grid-stack" uk-grid>
                <div class="uk-width-1-1@m uk-first-column">
                  <div uk-slideshow="animation: slide; autoplay: true; autoplay-interval: 5000; max-height: 250" class="uk-slideshow">
                    <div class="uk-position-relative uk-visible-toggle uk-margin-top">
                      <ul class="uk-slideshow-items" style="height: 250px;">
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Ma'mun Zuberi" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/mamun-zuberi.png')}}" width="60"> Ma'mun Zuberi <br/>
                              <small>Co-Founder & Chief Executive Officer</small>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-width-xlarge@s uk-width-large@s uk-text-center">Bekerja di industry keuangan dan perbankan Syariah lebih dari 15 tahun dengan berbagai tugas dan jabatan diantaranya mulai dari Head of Corporate Planning & Strategy, Credit Analyst & Account Manager untuk Corporate Banking, Investment Service Head di Treasury dan International Banking di Bank Muamalat Indonesia, Business Manager di First Islamic Investment Bank Ltd Kuala Lumpur Malaysia, dan terahir sebagai Senior Vice President (SVP) in charge di Head of Treasury and Investment di Indonesia Infrastructure Guarantee Fund (IIGF) - BUMN.</p>
                          </div>
                        </li>
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Ramadlan Kurniawan" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/rkurni.jpeg')}}" width="60"> Ramadlan Kurniawan <br/>
                              <small>Co-Founder & Chief Executive Marketing</small>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Bekerja di industry keuangan dan perbankan syariah lebih dari 10 (sepuluh) tahun, baik di industry keuangan dan perbankan syariah di dalam negeri maupun di luar negeri. Selama bekerja di industry keuangan dan perbankan syariah beliau banyak terlibat dalam strategic marketing & distribution networks serta aliansi bisnis terutama untuk penjualan produk-produk pendanaan retail.</p>
                          </div>
                        </li>
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Unggal Bagus" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/unggal-bagus.png')}}" width="60"> Unggal Bagus <br/>
                              <small>Operation & Decision Support Manager</small>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Berpengalaman di industry perbankan lebih dari 6 (enam) tahun, baik di perbankan konvensional maupun perbankan syariah terutama di bidang decision support yang mencakup bidang pengolahan data, strategi dan business analisis, dan teknologi informasi. Unggal Bagus menyelesaikan pendidikan S1 jurusan Teknik Informatika tahun 2009 di Sekolah Tinggi Management dan Ilmu Komputer Nusa Mandiri.</p>
                          </div>
                        </li>
                        <!-- <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Bambang S" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/unggal-bagus.png')}}" width="60"> Bambang S <br/>
                              <small>Business Manager</small>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Berpengalaman di industry perbankan lebih dari 10 (sepuluh) tahun terutama di bidang Credit Analyst/pembiayaan komersial Syariah dan trade finance. Bambang S juga pernah menjabat sebagai Account Officer di Bank BUMN, dan terakhir menjabat sebagai Relationship Manager di Bank Syariah Terkemuka di Indonesia. Bambang menyelesaikan pendidikan terakhir di program pasca sarjana (S2) dalam bidang Managemen Keuangan di Universitas Indonesia.</p>
                          </div>
                        </li> -->
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Indra Jakanata" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/indra-jakanata.png')}}" width="60"> Indra Jakanata <br/>
                              <small>Senior Advisor</small>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Berpengalaman di industry pasar modal Indonesia lebih dari 20 (dua puluh) tahun. Beliau ahli di bidang corporate finance dan telah banyak menangani transaksi fund raising melalui pasar modal. Beliau memiliki izin sebagai broker – dealer dengan Licence, No: KEP 281 / PM / IP / PPE / 1997, Reg No : 1093/PPE, Otoritas Jasa Keuangan (OJK) d/h Badan Pengawas Pasar Modal (BAPEPAM), Kementrian Keuangan Republik Indonesia, 1997.</p>
                          </div>
                        </li>
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Husni Thamrin" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/husni-thamrin.png')}}" width="60"> Husni Thamrin <br/>
                              <small>Senior Advisor</small>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Husni Thamrin adalah dosen tetap di sebuah perguruan tinggi di Solo selama lebih dari 20 tahun. Bidang ilmu yang ditekuni selama studi dan mengajar adalah Teknik Elektro dan Informatika. Pengalaman di bidang software engineering bermula saat menempuh studi tingkat doktor pada bidang system modeling di University of Glasgow, di Skotlandia. Sedangkan program sarjana dan program master ditempuh di Program Studi Teknik Elektro, Universitas Gadjah Mada.</p>
                          </div>
                        </li>
                      </ul>
                      <a class="uk-position-center-left uk-position-small uk-slidenav-previous uk-icon uk-slidenav uk-visible@s" uk-slidenav-previous uk-slideshow-item="previous"></a>
                      <a class="uk-position-center-right uk-position-small uk-slidenav-next uk-icon uk-slidenav uk-visible@s" uk-slidenav-next uk-slideshow-item="next"></a>
                    </div>

                    <ul class="uk-dotnav uk-flex-center uk-margin">
                      <li uk-slideshow-item="0" class>
                        <a class="icon-slide">Item 1</a>
                      </li>
                      <li uk-slideshow-item="1" class>
                        <a class="icon-slide">Item 2</a>
                      </li>
                      <li uk-slideshow-item="2" class>
                        <a class="icon-slide">Item 3</a>
                      </li>
                      <li uk-slideshow-item="3" class="uk-active">
                        <a class="icon-slide">Item 4</a>
                      </li>
                      <li uk-slideshow-item="4" class>
                        <a class="icon-slide">Item 5</a>
                      </li>
                      <li uk-slideshow-item="5" class>
                        <a class="icon-slide">Item 6</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <app-scroll href="#partners">
                <div class="uk-position-relative" uk-scrollspy="cls: uk-animation-slide-top; repeat: true">
                  <center>
                    <a class="arrow" uk-scroll href="#partners">
                      <i uk-icon="icon: arrow-down" aria-hidden="true" class="uk-button-primary uk-box-shadow-large uk-icon-button uk-icon"></i>
                    </a>
                  </center>
                </div>
              </app-scroll>
            </div>
          </div>
        </div>
      </app-team>

      <app-partners id="partners">
        <div class="uk-section uk-section-default uk-section-small uk-section-overlap uk-position-relative uk-padding-remove uk-margin-large-top">
          <div class="uk-container uk-container-small uk-padding-small uk-position-relative uk-position-z-index">
            <div class="uk-grid-large uk-margin-small uk-grid uk-grid-stack uk-grid-divider uk-child-width-expand@s" uk-grid>
              <div class="uk-width-1-2@m uk-grid-margin uk-first-column">
                <h3 class="uk-width-medium uk-margin-auto uk-text-center uk-heading-line" uk-scrollspy="cls: uk-animation-slide-top; delay: 100; repeat: true">
                  <span class="uk-text-uppercase">Terdaftar</span>
                </h3>
                <div class="uk-margin uk-text-center uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-2@m uk-grid"
                  id="clients" uk-grid uk-scrollspy="cls: uk-animation-fade; target: > div; delay:100; repeat: true">
                  <div style="visibility: hidden;" class="">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="Air Adem" class="uk-transition-scale-up uk-transition-opaque" width="45%" src="{{asset('images/partners/Kemkominfo.png')}}">
                      <a class="uk-position-cover" href="" target="_blank"></a>
                    </div>
                  </div>
                  <div style="visibility: hidden;" class="">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="Air Adem" class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/partners/afsi.png')}}">
                      <a class="uk-position-cover" href="" target="_blank"></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="uk-width-1-2@m uk-grid-margin uk-first-column">
                <h3 class="uk-width-medium uk-margin-auto uk-text-center uk-heading-line" uk-scrollspy="cls: uk-animation-slide-top; delay: 100; repeat: true">
                  <span class="uk-text-uppercase">Partners</span>
                </h3>
                <div class="uk-margin uk-text-center uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-2@m uk-grid"
                  id="clients" uk-grid uk-scrollspy="cls: uk-animation-fade; target: > div; delay:100; repeat: true">
                  <div class="uk-first-column">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="Bank Muamalat" class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/partners/muamalat.png')}}">
                      <a class="uk-position-cover" href="http://www.bankmuamalat.co.id" target="_blank"></a>
                    </div>
                  </div>
                  <!--<div style="visibility: hidden;" class="">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="Bank BNI Syariah" class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/partners/bni-syariah.png')}}">
                      <a class="uk-position-cover" href="https://www.bnisyariah.co.id" target="_blank"></a>
                    </div>
                  </div>-->
                  <div style="visibility: hidden;" class="">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="Air Adem" class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/partners/air-adem.png')}}">
                      <a class="uk-position-cover" href="" target="_blank"></a>
                    </div>
                  </div>
                  <!-- <div style="visibility: hidden;" class="">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="USSI" class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/partners/ussi.png')}}">
                      <a class="uk-position-cover" href="" target="_blank"></a>
                    </div>
                  </div> -->
                  <div class="">
                    <div class="uk-inline-clip uk-transition-toggle">
                      <img alt="Bank Mandiri Syariah" class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/partners/asyki.png')}}">
                      <a class="uk-position-cover" href="http://www.asyki.com" target="_blank"></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br /><br />
            <app-scroll href="#testimonials">
              <div class="uk-position-relative uk-height-small" uk-scrollspy="cls: uk-animation-slide-top; repeat: true">
                <center>
                  <a class="arrow" uk-scroll="" href="#testimonials">
                    <i uk-icon="icon: arrow-down" aria-hidden="true" class="uk-button-primary uk-box-shadow-large uk-icon-button uk-icon"></i>
                  </a>
                </center>
              </div>
            </app-scroll>
          </div>
        </div>
      </app-partners>

      <app-testimonials id="testimonials">
        <div class="uk-section uk-section-default uk-section-small uk-section-overlap uk-position-relative uk-padding-remove">
          <div class="uk-background-norepeat uk-background-cover uk-section uk-section-small uk-background-fixed uk-background-center-center" style="background-image: url({{asset('images/contents/about-us.jpg')}});">
            <div class="uk-position-cover" style="background-color: rgba(3, 9, 25, 0.9);"></div>
            <div class="uk-container uk-container-small uk-padding-small uk-position-relative uk-position-z-index">
              <div class="uk-grid-large uk-margin-small uk-grid uk-grid-stack" uk-grid>
                <div class="uk-width-1-1@m uk-first-column">
                  <h2 class="uk-text-center uk-text-primary" uk-scrollspy="cls: uk-animation-slide-top; repeat: true">Testimonis</h2>
                </div>
              </div>
              <div class="uk-grid-small uk-grid uk-grid-stack" uk-grid>
                <div class="uk-width-1-1@m uk-first-column">
                  <div uk-slideshow="animation: slide; autoplay: true; autoplay-interval: 5000; max-height: 800" class="uk-slideshow">
                    <div class="uk-position-relative uk-visible-toggle uk-margin-top">
                      <ul class="uk-slideshow-items" style="height: 450px;">
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Jeff Zamora" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/sonny-zulhuda.png')}}" width="60"> Dr. Sonny Zulhuda
                              <span>Pakar Cyberlaw, Blogger at sonnyzulhuda.com</span>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-width-xlarge@s uk-width-large@s uk-text-center">Plazadana adalah solusi pemberdayaan ekonomi berbasis komunitas di era ekonomi digital, dimana akuntabilitas adalah kunci untuk mendapatkan kepercayaan masyarakat. Para arsitek dan konseptor Plazadana menyadari hal itu dan mencoba menawarkan sebuah solusi yang tepat.</p>
                          </div>
                        </li>
                        <!-- <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Patrice Williams" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/mamun-zuberi.png')}}" width="60"> Patrice Williams
                              <span>CEO - The 123 Target Marketing agency</span>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Steven is absolutely awesome. He was able to make my web changes within 24 hours, he asked me clarifying questions, he is pleasant to work with. He has a great sense of website design and color and also knows
                              zencart shopping cart application. I'm hiring him for a new job now.</p>
                          </div>
                        </li>
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Marianela Rojas" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/mamun-zuberi.png')}}" width="60"> Marianela Rojas
                              <span>VP of Web Technologies - Growth Acceleration Partners</span>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Steven es un trabajador motivado con muchas ganas de aprender nuevas tecnologías. Siempre esta dispuesto a ayudar en cualquier proyecto y es definitivamente una persona que le gusta trabajar en equipo y colaborar
                              con los demás.</p>
                          </div>
                        </li>
                        <li class="uk-active uk-transition-active">
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Michael Gutierrez" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/mamun-zuberi.png')}}" width="60"> Michael Gutierrez
                              <span>JavaScript Full Stack Consultant</span>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">I worked with Steven in 2 different companies member of my development teams. Great developer! The type of person that can be trusted to get the tasks done. He's a very smart person and he's always looking to learn from others new things and collaborate with the team.</p>
                          </div>
                        </li>
                        <li class>
                          <div class="uk-position-center uk-text-center uk-light">
                            <h4>
                              <img alt="Victor Martinez" class="uk-border-circle uk-margin-auto uk-display-block uk-visible@s" height="60" src="{{asset('images/contents/mamun-zuberi.png')}}" width="60"> Victor Martinez
                              <span>CEO - Laboratorio Electronico Digital</span>
                            </h4>
                            <p class="uk-margin-remove uk-width-xxlarge@l uk-text-center">Steven siempre se ha caracterizado por su responsabilidad y conocimiento en su área de trabajo. Siempre dispuesto a dar lo mejor de sí.</p>
                          </div>
                        </li> -->
                      </ul>
                      <a class="uk-position-center-left uk-position-small uk-slidenav-previous uk-icon uk-slidenav uk-visible@s" uk-slidenav-previous uk-slideshow-item="previous"></a>
                      <a class="uk-position-center-right uk-position-small uk-slidenav-next uk-icon uk-slidenav uk-visible@s" uk-slidenav-next uk-slideshow-item="next"></a>
                    </div>

                    <ul class="uk-dotnav uk-flex-center uk-margin">
                      <li uk-slideshow-item="0" class>
                        <a class="icon-slide">Item 1</a>
                      </li>
                      <li uk-slideshow-item="1" class>
                        <a class="icon-slide">Item 2</a>
                      </li>
                      <li uk-slideshow-item="2" class>
                        <a class="icon-slide">Item 3</a>
                      </li>
                      <li uk-slideshow-item="3" class="uk-active">
                        <a class="icon-slide">Item 4</a>
                      </li>
                      <li uk-slideshow-item="4" class>
                        <a class="icon-slide">Item 5</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <app-scroll href="#contact">
                <div class="uk-position-relative uk-height-small"
                  uk-scrollspy="cls: uk-animation-slide-top; repeat: true">
                  <center>
                    <a class="arrow" uk-scroll href="#contact">
                      <i uk-icon="icon: arrow-down" aria-hidden="true" class="uk-button-primary uk-box-shadow-large uk-icon-button uk-icon"></i>
                    </a>
                  </center>
                </div>
              </app-scroll>
            </div>
          </div>
        </div>
      </app-testimonials>

      <app-contact id="contact">
        <div class="uk-section uk-section-default uk-section-small uk-section-overlap uk-position-relative uk-padding-remove">
          <div class="uk-background-norepeat uk-background-cover uk-section uk-section-small uk-background-fixed uk-background-center-center" style="background-image: url({{asset('images/contents/snow-mountain.jpg')}});">
            <div class="uk-position-cover" style="background-color: rgba(255, 255, 255, 0.79);"></div>
            <div class="uk-container uk-container-small uk-padding-small uk-position-relative uk-position-z-index">
              <div class="uk-grid-large uk-margin-small uk-grid uk-grid-stack" uk-grid>
                <div class="uk-width-1-1@m uk-first-column">
                  <h2 class="uk-text-center" uk-scrollspy="cls: uk-animation-slide-top; delay: 100; repeat: true">I'd love to hear from you!</h2>
                  <p class="uk-margin-medium uk-margin-auto uk-text-center" uk-scrollspy="cls: uk-animation-slide-bottom; delay: 100; repeat: true">
                    I would love to be part of your team and help you to create a success business this 2018. Please feel free to reach me out!
                  </p>
                  <div class="uk-margin-medium uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom; delay: 100; repeat: true">
                    <a class="uk-button uk-button-primary uk-button-large uk-margin-small-bottom spf-link" href="{{url('contact_us_page')}}" aria-expanded="false">
                      <span uk-icon="mail"></span> Kirimkan Pesan
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </app-contact>
      </div>
      </div>

      @include('navbar/chat')
      @include('navbar/cookies')
      @include('navbar/footer')
      </div>

    <!-- begin spfjs -->
    <div id="spfjs-progress-bar"></div>
    <script src="{{asset('js/spfjs.min.js')}}"></script>
    <script src="{{asset('js/spfjs-main.js')}}"></script>
    <script>
function myVa() {
  var copyText = document.getElementById("myVa");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154899345-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154899345-1');
</script>


    @yield('js')
    <!-- end spfjs -->
    <!-- {% block js %}{% endblock %} -->
  </body>
</html>

