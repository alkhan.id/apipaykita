@extends('base')

<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>
<style>
#more1 {display: none;}
</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({{asset('images/contents/about-us.jpg')}});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Tim Kami</h1>
      <p class="uk-text-white short-description-teaser-detail">Dibangun oleh orang-orang yang telah profesional dibidangnya.</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <div class="uk-card-body uk-card-responsive">
      <hr class="uk-divider-icon">
      <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
        <div class="ceo-card">
          <div class="uk-card uk-card-hover uk-card-default uk-width-1-1@m">
            <a href="https://www.linkedin.com/in/ma-mun-zuberi-msc-cifa-cfp%C2%AE-39914924/"
              class="uk-card-badge uk-label uk-visible@m" target="_blank">LinkedIn</a>
            <div class="uk-card-header">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <img class="uk-border-circle" width="100" height="100" src="{{asset('images/contents/mamun-zuberi.png')}}">
                </div>
                <div class="uk-width-expand">
                  <h3 class="uk-card-title uk-margin-remove-bottom">Ma'mun Zuberi</h3>
                  <p class="uk-text-meta uk-margin-remove-top">
                    <span>Co-Founder & Chief Executive Officer</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="uk-card-body">
              <p>
                Bekerja di industry keuangan dan perbankan Syariah lebih dari 15 tahun dengan berbagai tugas dan jabatan diantaranya mulai dari Head of Corporate Planning & Strategy, Credit Analyst & Account Manager untuk Corporate Banking, Investment Service Head di Treasury dan International Banking di Bank Muamalat Indonesia, Business Manager di First Islamic Investment Bank Ltd Kuala Lumpur Malaysia, dan terahir sebagai Senior Vice President (SVP) in charge di Head of Treasury and Investment di Indonesia Infrastructure Guarantee Fund (IIGF) - BUMN.
              </p>
              <span id="dots1"></span><span id="more1">
              <p>
                Selama berkarir di industry keuangan dan perbankan Syariah, beliau banyak menangani transaksi keuangan Syariah mulai dari yang sederhana (plain vanilla) hingga transaksi yang rumit (exotic). Selain bekerja di industry keuangan Syariah, beliau juga mengajar di program pasca sarjana di beberapa Universitas negeri maupun swasta di Jakarta dengan subject Manajemen Investasi Syariah & Manajemen Perbankan Syariah. Beliau menerima piagam International Certified Islamic Financial Analyst dan Certified Financial Planner masing-masing tahun 2005 dan 2015. Beliau lulus dari University Putra Malaysia (UPM) dengan gelar Master of Science (MSc) in Financial Economics, Serdang Kuala Lumpur Malaysia.
              </p>
              </span>
              <a onclick="myFunction1()" id="myBtn1"> Read more </a>
            </div>
          </div>
        </div>
        <div class="cmo-card">
          <div class="uk-card uk-card-hover uk-card-default uk-width-1-1@m">
            <a href="" class="uk-card-badge uk-label uk-visible@m" target="_blank">LinkedIn</a>
            <div class="uk-card-header">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <img class="uk-border-circle" width="100" height="100" src="{{asset('images/contents/rkurni.jpeg')}}">
                </div>
                <div class="uk-width-expand">
                  <h3 class="uk-card-title uk-margin-remove-bottom">Ramadlan Kurniawan</h3>
                  <p class="uk-text-meta uk-margin-remove-top">
                    <span>Co-Founder & Chief Marketing Officer</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="uk-card-body">
              <p>
              Ramadlan Kurniawan bekerja di industry keuangan dan perbankan syariah lebih dari 10 (sepuluh) tahun, baik di industry keuangan dan perbankan syariah di dalam negeri maupun di luar negeri. Selama bekerja di industry keuangan dan perbankan syariah beliau banyak terlibat dalam strategic marketing & distribution networks serta aliansi bisnis terutama untuk penjualan produk-produk pendanaan retail. Beliau menyelesaikan program pasca sarjana di bidang ekonomi dan keuangan (MEc/Master of Economics) tahun 2007 di Faculty of Economics and Management, University of Malaya Kuala Lumpur Malaysia.
              </p>
            </div>
          </div>
        </div>
      </div><!-- end /.uk-child-width-expand@s -->
      <hr class="uk-divider-icon">
      
      <hr class="uk-divider-icon">
      <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
        <div class="worker-card">
          <div class="uk-card uk-card-hover uk-card-default uk-width-1-1@m">
            <a href="" class="uk-card-badge uk-label uk-visible@m" target="_blank">LinkedIn</a>
            <div class="uk-card-header">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <img class="uk-border-circle" width="100" height="100" src="{{asset('images/contents/indra-jakanata.png')}}">
                </div>
                <div class="uk-width-expand">
                  <h3 class="uk-card-title uk-margin-remove-bottom">Indra Jakanata</h3>
                  <p class="uk-text-meta uk-margin-remove-top">
                    <span>Senior Advisor</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="uk-card-body">
              <p>Berpengalaman di industry pasar modal Indonesia lebih dari 20 (dua puluh) tahun. Beliau ahli di bidang corporate finance dan telah banyak menangani transaksi fund raising melalui pasar modal. Beliau memiliki izin sebagai broker – dealer dengan Licence, No: KEP 281 / PM / IP / PPE / 1997, Reg No : 1093/PPE, Otoritas Jasa Keuangan (OJK) d/h Badan Pengawas Pasar Modal (BAPEPAM), Kementrian Keuangan Republik Indonesia, 1997. Jabatan yang pernah diamanahkan diantaranya adalah Direktur PT Muara Sekuritas, Direktur PT Biliton Nikel Indonesia, Direktur PT Bosowa Sekuritas, Direktor PT Telaga Petrogas Sejahtera, Corporate Finance Head di PT KBT Seafood Internasional, dan terahir menjabat sebagai Associate Director di PT Pavillion Capital - Wealth and Asset Management.</p>
            </div>
          </div>
        </div>
        <div class="worker-card">
          <div class="uk-card uk-card-hover uk-card-default uk-width-1-1@m">
            <a href="" class="uk-card-badge uk-label uk-visible@m" target="_blank">LinkedIn</a>
            <div class="uk-card-header">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <img class="uk-border-circle" width="100" height="100" src="{{asset('images/contents/husni-thamrin.png')}}">
                </div>
                <div class="uk-width-expand">
                  <h3 class="uk-card-title uk-margin-remove-bottom">Husni Thamrin</h3>
                  <p class="uk-text-meta uk-margin-remove-top">
                    <span>Senior Advisor</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="uk-card-body">
              <p>Husni Thamrin adalah dosen tetap di sebuah perguruan tinggi di Solo selama lebih dari 20 tahun. Bidang ilmu yang ditekuni selama studi dan mengajar adalah Teknik Elektro dan Informatika. Selama berkarir banyak terlibat dalam pengembangan program komputer dan sistem informasi baik sebagai programer maupun sebagai analis, dalam skop transactional processing maupun sistem informasi eksekutif. Sistem yang pernah dikembangkan termasuk sistem informasi akademik, sistem penerimaan mahasiswa baru, sistem kepegawaian dan karir, dan aplikasi pengembangan lembaga. Riset yang dilakukan secara intensif memanfaatkan aplikasi dan teknologi informasi, yaitu riset bidang pengolahan bahasa alami (natural language processing). Pengalaman di bidang software engineering bermula saat menempuh studi tingkat doktor pada bidang system modeling di University of Glasgow, di Skotlandia. Sedangkan program sarjana dan program master ditempuh di Program Studi Teknik Elektro, Universitas Gadjah Mada.</p>
            </div>
          </div>
        </div>
      </div> <!-- end /.uk-child-width-expand@s -->
      <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
        <div class="worker-card">
          <div class="uk-card uk-card-hover uk-card-default uk-width-1-1@m">
            <a href="https://www.linkedin.com/in/unggal-bagus-setyo-74054464"
              class="uk-card-badge uk-label uk-visible@m" target="_blank">LinkedIn</a>
            <div class="uk-card-header">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <img class="uk-border-circle" width="100" height="100" src="{{asset('images/contents/unggal-bagus.png')}}">
                </div>
                <div class="uk-width-expand">
                  <h3 class="uk-card-title uk-margin-remove-bottom">Unggal Bagus</h3>
                  <p class="uk-text-meta uk-margin-remove-top">
                    <span>Operation & Decision Support Manager</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="uk-card-body">
              <p>
                Berpengalaman di industry perbankan lebih dari 6 (enam) tahun, baik di perbankan konvensional maupun perbankan syariah terutama di bidang decision support yang mencakup bidang pengolahan data, strategi dan business analisis, dan teknologi informasi. Unggal Bagus menyelesaikan pendidikan S1 jurusan Teknik Informatika tahun 2009 di Sekolah Tinggi Management dan Ilmu Komputer Nusa Mandiri.
              </p>
            </div>
          </div>
        </div>
        <div class="worker-card">
          <div class="uk-card uk-card-hover uk-card-default uk-width-1-1@m">
            <!-- <a href="" class="uk-card-badge uk-label uk-visible@m" target="_blank">LinkedIn</a> -->
            <div class="uk-card-header">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                  <!-- <img class="uk-border-circle" width="100" height="100" src="{{asset('images/contents/mamun-zuberi.png')}}"> -->
                </div>
                <div class="uk-width-expand">
                  <!-- <h3 class="uk-card-title uk-margin-remove-bottom"></h3> -->
                  <p class="uk-text-meta uk-margin-remove-top">
                    <!-- <span>Business Manager</span> -->
                  </p>
                </div>
              </div>
            </div>
            <div class="uk-card-body">
              <!-- <p>
                Berpengalaman di industry perbankan lebih dari 10 (sepuluh) tahun terutama di bidang Credit Analyst/pembiayaan komersial Syariah dan trade finance. Bambang S juga pernah menjabat sebagai Account Officer di Bank BUMN, dan terakhir menjabat sebagai Relationship Manager di Bank Syariah Terkemuka di Indonesia. Bambang menyelesaikan pendidikan terakhir di program pasca sarjana (S2) dalam bidang Managemen Keuangan di Universitas Indonesia.
              </p> -->
            </div>
          </div>
        </div>
      </div><!-- end /.uk-child-width-expand@s -->
    </div>
  </div>

  <script>
function myFunction1() {
  var dots1 = document.getElementById("dots1");
  var moreText1 = document.getElementById("more1");
  var btnText1 = document.getElementById("myBtn1");

  if (dots1.style.display === "none") {
    dots1.style.display = "inline";
    btnText1.innerHTML = "Read more"; 
    moreText1.style.display = "none";
  } else {
    dots1.style.display = "none";
    btnText1.innerHTML = "Read less"; 
    moreText1.style.display = "inline";
  }
}
</script>
@endsection
