@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({{asset('images/contents/about-us.jpg')}});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Tentang Kami</h1>
      <p class="uk-text-white short-description-teaser-detail">Retail equity based crowdfunding platform Syariah pertama di Indonesia.</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <div class="uk-card-body">
      <hr class="uk-divider-icon">
      <p class="uk-dropcap">
        Plazadana.com adalah retail equity based crowdfunding platform syariah yang mengkhususkan diri dalam mempertemukan peluang investasi antara anggota terdaftar dengan Koperasi. plazadana.com didirikan untuk memberikan banyak kemudahan dan berbagai manfaat kepada koperasi dan anggotanya terutama dalam hal akuntabilitas, transparansi, dan optimalisasi pengelolaan unit usaha koperasi. Plazadana.com juga merupakan catalyst dan enabler yang memungkinkan para pemangku kepentingan khususnya para anggota koperasi untuk dapat bersama-sama memantau kinerja koperasi secara up to date dan dapat melakukan self-correction segera jika diperlukan agar kinerja keuangan koperasi meningkat secara berkelanjutan yang pada akhirnya trend peningkatan imbal hasil dapat dinikmati oleh para anggotanya.
      </p>
      <p>
        Komunitas/masyarakat yang akan/sedang/telah mendirikan koperasi dan jika telah resmi berbadan hukum koperasi kemudian berencana meningkatkan permodalan melalui penambahan serta memperluas jumlah keanggotaannya dapat melakukan penawaran umum kepada calon anggota melalui market place plazadana.com yang dapat diakses melalui website ataupun mobile apps kapanpun dan dari manapun.
      </p>
      <p>
        Untuk dapat memenuhi standar akuntabilitas yang dapat dipertanggung jawabkan kepada seluruh anggota dan para pemangku kepentingan, koperasi yang telah berhasil menggalang dana melalui market place plazadana.com - seiring dengan bertambahnya jumlah anggota beserta iuran-iuran yang dipersyaratkan sesuai dengan target - maka koperasi wajib melakukan publikasi laporan penggunaan dana/modal dan kinerja keuangan secara periodic (bulanan/triwulanan/semesteran/tahunan) dan melakukan distribusi imbal hasil secara berkala sesuai dengan kesepakatan melalui market place plazadana.com yang dapat diakses melalui website maupun mobile apps.
      </p>
      <p>
        Untuk memenuhi hal tersebut, Plazadana.com menyediakan standard system aplikasi akuntansi yang akan diinstal di setiap unit usaha koperasi yang melapak di market place agar secara real time dapat menyajikan laporan keuangan sesuai dengan permintaan, sehingga hal ini memungkinkan Koperasi untuk dapat memenuhi akuntabilitasnya dengan melakukan penyajian laporan keuangan sesuai dengan permintaan terutama setiap periode tutup buku baik secara harian, bulanan, triwulanan, semesteran dan tahunan melalui market place plazadana.com
      </p>
      <p>
        Plazadana.com juga menyediakan fasilitas Paykita (dompet elektronik) yang memungkinkan anggota koperasi dapat menikmati berbagai kemudahan dalam melakukan transaksi terutama terkait dengan setoran tunai keanggotaan, penerimaan pembayaran distribusi imbal hasil dari unit usaha koperasi, setoran tambahan (top up) berbagai produk simpanan Koperasi, serta tarik tunai melalui ATM seluruh bank yang telah memiliki kerjasama dengan anchor bank plazadana.com. Semua jenis transaksi yang dilakukan melalui Paykita dapat langsung dicek secara real time melalui mobile phone agar dapat dipastikan semua jenis transaksi berhasil (settled) dan dapat dilakukan rekonsiliasi dengan posisi saldo terkini yang ada di bank. Paykita juga memungkinkan untuk cek 10 transaksi terakhir.
      </p>
    </div>
  </div>
@endsection
