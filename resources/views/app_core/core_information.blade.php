@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({{asset('images/contents/chicago-city.jpg')}});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Informasi</h1>
      <p class="uk-text-white short-description-teaser-detail">Informasi Cara Kerja, Akad Musyarakah, Akad Wakalah & Penjelasan Bagi Hasil.</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Cara Kerja</a></li>
      <li><a>Akad-Akad Syariah</a></li>
      <!-- <li><a>Akad Wakalah</a></li> -->
      <li><a>Penjelasan Bagi Hasil</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item cara-kerja">
          <div class="content-1">
            <p>Dalam platform Plazadana ini skema kerjasama antar anggota yang tergabung dalam koperasi menggunakan akad musyarakah (partnership) sedangkan antara anggota koperasi, koperasi dengan Plazadana menggunakan akad wakalah bil ujroh (fee) dengan skema sebagai berikut:</p>
            <img alt="Cara Kerja Plazadana" src="{{asset('images/contents/cara-kerja.png')}}">
          </div>
        </li><!-- end /.tab-item.cara-kerja -->
        <li class="tab-item akad-musyarakah">
          <div class="content-2">
          <!-- <h4 class="uk-heading-bullet">AKAD SYARIAH</h4>
            <p>Semua transaksi yang dilakukan oleh para pihak (stake holders) dalam market place plazadana ini menggunakan akad-akad syariah sesuai dengan ketentuan standar fatwa akad yang dikeluarkan oleh Dewan Sayariah Nasional (DSN)..</p>
            <p>Berikut ini adalah akad-akad syariah yang banyak digunakan dalam transaksi di market place plazadana oleh para pemangku kepentingan (stake holder) dan disajikan secara ringkas, untuk mempelajari akad-akad syariah secara detail dan lengkap silahkan layari di link ini: <a href="https://dsnmui.or.id/kategori/fatwa/">Fatwa</a></p>
            <ol>
            <li>Ju'alah</li>
            <li>Wakalah</li>
            <li>Musyarakah</li>
            <li>Mudharabah</li>
            </ol>

            <h4 class="uk-heading-bullet">1. AKAD JU’ALAH</h4>
            <p>Ketentuan Umum</p>
            <ol>
              <li>Ju’alah adalah janji atau komitmen (iltizam) untuk memberikan imbalan (reward/’iwadh//ju’l) tertentu atas pencapaian hasil (natijah) yang ditentukan dari suatu pekerjaan.</li>
              <li>Ja’il adalah pihak yang berjanji akan memberikan imbalan tertentu atas pencapaian hasil pekerjaan (natijah) yang ditentukan.</li>
              <li>Maj’ul lah adalah pihak yang melaksanakan Ju’alah.</li>
              <li>Maj’ul alaih  adalah objek yang dijadikan sebagai underlying pekerjaan</li>
            </ol>
            <p>Ketentuan Akad</p>
            <p>Akad Ju’alah boleh dilakukan untuk memenuhi kebutuhan pelayanan jasa dengan ketentuan sebagai berikut:</p>
            <ol>
              <li>Pihak Ja’il harus memiliki kecakapan hukum dan kewenangan (muthlaq al-tasharruf) untuk melakukan akad;</li>
              <li>Objek Ju’alah (mahal al-‘aqd/maj’ul ‘alaih) harus berupa pekerjaan yang tidak dilarang oleh syariah, serta tidak menimbulkan akibat yang dilarang;</li>
              <li>Hasil pekerjaan (natijah) sebagaimana dimaksud harus jelas dan diketahui oleh para pihak pada saat penawaran;</li>
              <li>Imbalan Ju’alah (reward/’iwadh//ju’l) harus ditentukan besarannya oleh Ja’il dan diketahui oleh para pihak pada saat penawaran; dan</li>
              <li>Tidak boleh ada syarat imbalan diberikan di muka (sebelum pelaksanaan objek Ju’alah);</li>
            </ol>
            <p>Ketentuan umum</p>
            <ol>
              <li>Imbalan Ju’alah hanya berhak diterima oleh pihak maj’ul lahu apabila hasil dari pekerjaan tersebut terpenuhi;</li>
              <li>Pihak Ja’il harus memenuhi imbalan yang diperjanjikannya jika pihak maj’ullah menyelesaikan (memenuhi) prestasi (hasil pekerjaan/natijah) yang ditawarkan.</li>
            </ol>
            <p>Ketentuan Penutup</p>
            <ol>
              <li>Jika terjadi perselisihan (persengketaan) di antara para pihak, dan tidak tercapai kesepakatan di antara mereka maka penyelesaiannya dilakukan melalui Badan Arbitrase Syariah Nasional atau melalui Pengadilan Agama</li>
              <li>Fatwa ini berlaku sejak tanggal ditetapkan, dengan ketentuan jika di kemudian hari ternyata terdapat kekeliruan, akan diubah dan disempurnakan sebagaimana mestinya.</li>
            </ol>
            <h4 class="uk-heading-bullet">2. AKAD WAKALAH</h4>
            <p>Ketentuan Umum</p>
 -->

 <h4 class="uk-heading-bullet">AKAD SYARIAH</h4>
<p>Semua transaksi yang dilakukan oleh para pihak (stake holders) dalam market place plazadana ini menggunakan akad-akad syariah sesuai dengan ketentuan standar fatwa akad yang dikeluarkan oleh Dewan Sayariah Nasional (DSN).</p>
<p>Berikut ini adalah akad-akad syariah yang banyak digunakan dalam transaksi di market place plazadana oleh para pemangku kepentingan (stake holder) dan disajikan secara ringkas, untuk mempelajari akad-akad syariah secara detail dan lengkap silahkan layari di link ini: <a href="https://dsnmui.or.id/kategori/fatwa/">https://dsnmui.or.id/kategori/fatwa/</a></p>
<ol>
<li>Ju&rsquo;alah</li>
<li>Wakalah</li>
<li>Musyarakah</li>
<li>Mudharabah</li>
</ol>
<a data-toggle="collapse" data-target="#demo">Pelajari lebih lanjut&hellip;&hellip;</a>
<br><br>
<div id="demo" class="collapse">

  <ol>
  <h4 class="uk-heading-bullet">AKAD JU&rsquo;ALAH</h4>
  </ol>
  <p>Ketentuan Umum</p>
  <p>Dalam fatwa DSN yang dimaksud dengan:</p>
  <ol>
  <li><em>Ju&rsquo;alah</em> adalah janji atau komitmen (<em>iltizam</em> ) untuk memberikan imbalan (<em>reward/&rsquo;iwadh//ju&rsquo;l</em> ) tertentu atas pencapaian hasil (<em>natijah</em> ) yang ditentukan dari suatu pekerjaan.</li>
  <li><em>Ja&rsquo;il</em> adalah pihak yang berjanji akan memberikan imbalan tertentu atas pencapaian hasil pekerjaan (<em>natijah</em> ) yang ditentukan.</li>
  <li><em>Maj&rsquo;ul lah</em> adalah pihak yang melaksanakan Ju&rsquo;alah.</li>
  <li><em>Maj&rsquo;ul alaih </em> adalah objek yang dijadikan sebagai underlying pekerjaan</li>
  </ol>
  <p>Ketentuan Akad</p>
  <p>Akad&nbsp;<em>Ju&rsquo;alah </em> boleh dilakukan untuk memenuhi kebutuhan pelayanan jasa dengan ketentuan sebagai berikut:</p>
  <ol>
  <li>Pihak&nbsp;<em>Ja&rsquo;il</em> harus memiliki kecakapan hukum dan kewenangan (<em>muthlaq</em> &nbsp;<em>al</em> -<em>tasharruf</em> ) untuk melakukan akad;</li>
  <li>Objek&nbsp;<em>Ju&rsquo;alah (mahal al-&lsquo;aqd/maj&rsquo;ul &lsquo;alaih)</em> harus berupa pekerjaan yang tidak dilarang oleh syariah, serta tidak menimbulkan akibat yang dilarang;</li>
  <li>Hasil pekerjaan (<em>natijah</em> ) sebagaimana dimaksud harus jelas dan diketahui oleh para pihak pada saat penawaran;</li>
  <li>Imbalan&nbsp;<em>Ju&rsquo;alah</em> (<em>reward/&rsquo;iwadh//ju&rsquo;l</em> ) harus ditentukan besarannya oleh&nbsp;<em>Ja&rsquo;il</em> &nbsp;dan diketahui oleh para pihak pada saat penawaran; dan</li>
  <li>Tidak boleh ada syarat imbalan diberikan di muka (sebelum pelaksanaan objek&nbsp;<em>Ju&rsquo;alah</em> );</li>
  </ol>
  <p>Ketentuan Hukum</p>
  <ol>
  <li>Imbalan&nbsp;<em>Ju&rsquo;alah</em> hanya berhak diterima oleh pihak&nbsp;<em>maj&rsquo;ul lahu</em> &nbsp;apabila hasil dari pekerjaan tersebut terpenuhi;</li>
  <li>Pihak&nbsp;<em>Ja&rsquo;il</em> harus memenuhi imbalan yang diperjanjikannya jika pihak&nbsp;<em>maj&rsquo;ullah</em> &nbsp;menyelesaikan (memenuhi) prestasi (hasil pekerjaan/<em>natijah</em> ) yang ditawarkan.</li>
  </ol>
  <p>Ketentuan Penutup</p>
  <ol>
  <li>Jika terjadi perselisihan (persengketaan) di antara para pihak, dan tidak tercapai kesepakatan di antara mereka maka penyelesaiannya dilakukan melalui Badan Arbitrase Syariah Nasional atau melalui Pengadilan Agama</li>
  <li>Fatwa ini berlaku sejak tanggal ditetapkan, dengan ketentuan jika di kemudian hari ternyata terdapat kekeliruan, akan diubah dan disempurnakan sebagaimana mestinya.</li>
  </ol>
  <ol start="2">
  <h4 class="uk-heading-bullet">AKAD WAKALAH</h4>
  </ol>
  <p>Ketentuan Umum</p>
  <ol>
  <li>Akad <em>wakalah</em>  adalah akad pemberian kuasa dari <em>muwakkil</em>  kepada <em>wakil</em>  untuk melakukan perbuatan hukum tertentu.</li>
  <li>Akad <em>wakalah bi al-ujrah</em>  adalah akad wakalah yang disertai dengan imbalan berupa ujrah (fee).</li>
  <li><em>Muwakkil</em>  adalah pihak yang memberikan kuasa, baik berupa orang <em>(Syakhshiyah thabi'iyah/natuurlijke persoon)</em>  maupun yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum <em>(Syakhshiyah i'tibariah/syakhshiyah hulcrniyah/ rechtsperson).</em> </li>
  <li><em>Wakil</em>  adalah pihak yang menerima kuasa, baik berupa orang <em>(Syakhshiyah thabi'iyah/natuurlijke persoon')</em>  maupun yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum <em>(Syakhshiyah i'tibariah/syakhshiyah hukrniyah/ rechtsperson)</em> .</li>
  <li><em>Ujrah</em>  adalah imbalan yang wajib dibayar atas jasa yang dilakukan oleh <em>wakil</em> .</li>
  <li><em>Al-ta'addi</em>  adalah melakukan suatu perbuatan yang seharusnya tidak dilakukan. Al+aqshir adalah tidak melakukan suatu perbuatan yang seharusnya dilakukan.</li>
  <li><em>Mukhalafat al-syuruth</em>  adalah menyalahi isi dan/atau substansi atau syarat-syarat yang disepakati dalam akad.</li>
  </ol>
  <p>Ketentuan Hukum</p>
  <p>Akad wakalah bi al-ujrah boleh dilakukan dengan tunduk dan patuh pada ketentuan dan batasan yang terdapat dalam Fatwa DSN.</p>
  <p>Ketentuan terkait Shighat Akad Wakalah bi ul-Ujrah</p>
  <ol>
  <li>Akad wakalah bi al-ujrah harus dinyatakan secara tegas dan jelas serta dimengerti baik oleh wakil maupun muwakkil.</li>
  <li>Akad wakalah bi al-ujrah boleh dilakukan secara lisan, tertulis, isyarat, dan perbuatar &amp; tindakan, serta dapat dilakukan secara elektronik sesuai syariah dan peraturan perundang-undangan yang berlaku.</li>
  </ol>
  <p>Ketentuan terkait Wakil dan Muwakkil</p>
  <ol>
  <li>Muwakkil dan wakil boleh berupa orang <em>(Syakhshiyah thabi'iyah/natuurlijke persoon)</em>  atau yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum <em>(Syakhshiyah i'tibariah/syakhshiyah hukmiyah/rechtsperson)</em> , berdasarkan peraturan perundang-undangan yang berlaku.</li>
  <li>Muwakkil danwakil wajib cakap hukum sesuai dengan syariah dan peraturan perundang-undangan yang berlaku.</li>
  <li>Muwakkil wajib memiliki kewenangan untuk memberikan kuasa kepada pihak lain, baik kewenangan yang bersifat ashliyyah maupun niyabiyyah.</li>
  <li>Muwakkil wajib mempunyai kemampuan untuk membayar ujrah.</li>
  <li>Wakil wajib memiliki kemampuan untuk mewujudkan perbuatan hukum yang dikuasakan kepadanya.</li>
  </ol>
  <p>Ketentuan terkait Obyek Wakalah</p>
  <ol>
  <li><em>Wakalah bi al-ujrah</em>  hanya boieh dilakukan terhadap kegiatan atau perbuatan hukum yang boleh di wakalahkan.</li>
  <li>Obyek wakalah bi al-ujrah harus berupa pekerjaan atau perbuatan tertentu dan wajib diketahui secara jelas oleh wakil dan muwakkil.</li>
  <li>Obyek wakalah bi al-ujrah harus dapat dilaksanakan oleh wakil.</li>
  <li>Akad wakalah bi al-ujrah boleh dibatasi jangka waktunya.</li>
  <li>Wakil boleh mewakilkan ulang kepada pihak lain atas kuasa yang diterimanya, kecuali tidak drizinkan oleh muwakkil (pemberi kuasa).</li>
  <li>Wakil tidak wajib menanggung risiko atas kerugian yang timbul karena perbuatan yang dilakukannya, kecuali karena al-ta'addi, al - taq s hir, atau mukhal afat al-syuruth.</li>
  </ol>
  <p>Ketentuan terkait Ujrah</p>
  <ol>
  <li>Ujrah boleh berupa uang atau barang yang boleh dimanfaatkan menurut syariah (mutaqatuwam) dan peraturan perundangundangan yang berlaku.</li>
  <li>Kuantitas danlatau kualitas ujrah harus jelas, baik berupa angka nominal, prosentase tertentu, atau rumus yang disepakati dan diketahui oleh para pihak yang melakukan akad.</li>
  <li>Ujrah boleh dibayar secara tunai, angsur/bertahap, dan tangguh sesuai dengan syariah, kesepakatan, danlatau peraturan perundangundangan yang berlaku</li>
  <li>Ujrah yang telah disepakati boleh ditinjau-ulang atas manfaat yang belum diterima oleh muwakkil sesuai kesepakatan.</li>
  </ol>
  <ol start="3">
  <h4 class="uk-heading-bullet">AKAD MUSYARAKAH</h4>
  </ol>
  <p>Ketentuan Umum</p>
  <ol start="3">
  <li><em>Akad syirkah</em> adalah akad kerja sama antara dua pihak atau lebih untuk suatu usaha tertentu di mana setiap pihak memberikan kontribusi dana/modal usaha <em>(ra's al-mal)</em> dengan ketentuan bahwa keuntungan dibagi sesuai nisbah yang disepakati atau secara proporsional, sedangkan kerugian ditanggung oleh para pihak secara proporsional. Syirkah ini merupakan salah satu bentuk Syirkah amwal dan dikenal dengan nama syirkah inan. </li>
  <li><em>Syarik </em>adalah mitra atau pihak yang melakukan akad syirkah, baik berupa orang (sy akhs hiy ah thab i' iyah/natuurl ij ke p e r s oon) maupun yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum <em>(syakhshiyah i'tibariah/ syakhshiyah hulcrniy ah/r e cht s per s on)</em>. <em>Ra's al-mal (dlll cri-r)</em> adalah modal usaha berupa harta kekayaan (cJt-yi-rJu; yang disatukan yang berasal danparu syarik. </li>
  </ol>
  <p>Ketentuan Shighat Akad</p>
  <ol>
  <li>Akad syirkah harus dinyatakan secara tegas, jelas, mudah dipahami dan dimengerti, serta diterima oleh para mitra (syarik).</li>
  <li>Akad syirkah boleh dilakukan secara lisan, tertulis, isyarat, dan perbuatan/tindakan, serta dapat diiakukan secara elektronik sesuai syariah dan peraturan perundang-undangan yang berlaku.</li>
  </ol>
  <p>Ketentuan Para Pihak</p>
  <ol>
  <li>Syarik (mitra) boleh berupa orang (syakhshiyah thabi'iyaW natuurlijke persoon) atau yang disamakan dengan orang, baik berbadan hukum maupun tidak berbadan hukum (syakhshiyah i'tibariaWsyakhshiyah hul&lt;rniyah/rechtsperson), berdasarkan peraturan perundang-undangan yang berlaku</li>
  <li>Syarik (mitra) wajib cakap hukum sesuai dengan syariah dan peraturan perundang-undangan yang berlaku.</li>
  <li>Syarik (mitra) wajib memiliki harta yang disertakan sebagai modal usaha (ra's al-maf serta memiliki keahlian/keterampilan usaha.</li>
  </ol>
  <p>Ketentuan Ru's Al-Mal</p>
  <ol>
  <li>Modal usaha syirkah wajib diserahterimakan, baik secara tunai maupun bertahap, sesuai kesepakatan.</li>
  </ol>
  <p>Ketentuan Nisbah Bagi Hasil</p>
  <ol>
  <li>Sistem/metode pembagian keuntungan harus disepakati dan dinyatakan secara jelas dalam akad.</li>
  <li>Nisbah boleh disepakati dalam bentuk nisbah-proporsional atau dalam bentuk nisbah-kesepakatan.</li>
  <li>Nisbah sebagaimana angka 2 dinyatakan dalam bentuk angka persentase terhadap keuntungan dan tidak boleh dalam bentuk nominal atau angka persentase dari modal usaha.</li>
  <li>Nisbah-kesepakatan sebagaimana angka 2 tidak boleh menggunakan angka persentase yang mengakibatkan keuntungan hanya dapat diterima oleh salah satu mitra atau mitra tertentu.</li>
  <li>Nisbah-kesepakatan boleh dinyatakan dalam bentuk muitinisbah (berj enj ang/T i e r i n g).</li>
  <li>Nisbah-kesepakatan boleh diubah sesuai kesepakatan</li>
  </ol>
  <p>Ketentuan Kegiatan Usaha</p>
  <ol>
  <li>Usaha yang dilakukan syarik (mitra) harus usaha yang halal dan sesuai dengan prinsip-prinsip syariah dan/atau peraturan perundang-undangan yang berlaku.</li>
  <li>Syarik (mitra) dalam melakukan usaha syirkah harus atas nama entitas syirkah, tidak boleh atas nama diri sendiri.</li>
  <li>Para syarik (mitra) tidak boleh meminjam, meminjamkan, menyumbangkan, atau menghadiahkan <em>ra's al-mal</em>  dan keuntungan kepada pihak lain kecuali atas dasar kesepakatan mitra-mitra.</li>
  <li>Syarik (mitra) dalam melakukan usaha syirkah, tidak boleh melakukan perbuatan yang termasuk at-ta'addi, at-taqshir, danl atau mukhalafat asy - syuruth.</li>
  </ol>
  <p>Ketentuan Keuntungan (Al-Ribh), Kerugian (al-Khasaroh) dan Pembagiannya</p>
  <ol>
  <li>Keuntungan usaha syirkah harus dihitung dengan jelas untuk menghindarkan perbedaan dan/atau sengketa pada waktu alokasi keuntungan atau penghentian musyarakah.</li>
  <li>Seluruh keuntungan usaha syirkah harus dibagikan berdasarkan nisbah-proporsional atau nisbah-kesepakatan, dan tidak boleh ada sejumlah tertentu dari keuntungan ditentukan di awal yang ditetapkan hanya untuk syarik tertentu.</li>
  <li>Salah satu syariK boleh mengusulkan bahwa jika keuntungan melebihi jumlah tertentu, kelebihan atau persentase itu diberikan kepadanya.</li>
  <li>Keuntungan usaha (ar-ribh) boleh dibagikan sekaligus pada saat berakhirnya akad atau secara bertahap sesuai kesepakatan dalam akad.</li>
  <li>Kerugian usaha syirkah wajib ditanggung (menjadi beban) para syarik secara proporsinal sesuai dengan porsi modal usaha yang disertakannya.</li>
  </ol>
  <p>Ketentuan Penutup</p>
  <p>Jika salah satu pihak tidak menunaikan kewajibannya atau jika terjadi perselisihan di antara para pihak, maka penyelesaiannya dilakukan melalui lembaga penyelesaian sengketa berdasarkan syariah sesuai dengan peraturan perundang-undangan yang berlaku setelah tidak tercapai kesepakatan melalui musyawarah.</p>
  <ol start="4">
  <h4 class="uk-heading-bullet">AKAD MUDHARABAH</h4>
  </ol>
  <p>Ketentuan Umum</p>
  <ol>
  <li>Akad <em>mudharabah</em>  adalah akad kerja sama suatu usaha arrtara pemilik modal (shohibul al-ma[) yang menyediakan seluruh modal dengan pengelola (<em>mudharib</em> ) dan keuntungan usaha dibagi di antara mereka sesuai nisbah yang disepakati dalam akad.</li>
  <li><em>Shahib al-mal</em>  adalah pihak penyedia dana dalam usaha kerja sama usaha mudharabah, baik berupa orang maupun yang dipersamakan dengan orang, baik berbadan hukum maupun tidak berbadan hokum.</li>
  <li><em>Mudharib</em>  adalah pihak pengelola dana dalam usaha kerja sama usaha mudharabah, baik berupa orang maupun yang disamakan dengan orang, baik berbadan hukum maupun tidak berbadan hokum.</li>
  <li><em>Ra's mal al-mudharabah</em>  adalah modal usaha dalam usaha kerja sama mudharabah.</li>
  </ol>
  <p>Ketentuan Shighut Akad</p>
  <ol>
  <li>Akad mudharabaft harus dinyatakan secara tegas, jelas, mudah dipahami dan dimengerti serta diterima para pihak.</li>
  <li>Akad mudharabah boleh dilakukan secara lisan, tertulis, isyarat, dan perbuatan/tindakan, serta dapat dilakukan secara elektronik sesuai syariah dan peraturan perundang-undangan yang berlaku.</li>
  <li>Mudharib dalam akad mudharabah tsuna'tyyah tidak boleh melakukan mudharabah ulang (mudharib yudharib) kecuali mendapatkan iztn dari shahib almal.</li>
  </ol>
  <p>Ketentuan Para Pihak</p>
  <ol>
  <li><em>Shahib al-mal</em>  dan <em>mudharib</em>  boleh berupa orang maupun yang disamakan dengan orang, baik berbadan hukum maupun tidak berbadan hokum.</li>
  <li><em>Shahib al-mal</em>  dan <em>mudharib</em>  wajib cakap hukum sesuai dengan syariah dan peraturan perundang-undangan yang berlaku.</li>
  <li><em>Shahib al-mal</em>  wajib memiliki modal yang diserah terimakan kepada mudharib.</li>
  <li><em>Mudharib</em>  wajib memiliki keahlian/keterampilan melakukan usaha dalam rangka mendapatkan keuntungan.</li>
  </ol>
  <p>Ketentuan terkait <em>Ra's al-Mal</em> </p>
  <ol>
  <li>Modal usaha mudharabah harus diserah terimakan (al-taslim) secara bertahap atau tunai sesuai kesepakatan.</li>
  <li>Modal usaha mudharabah pada dasarnya wajib dalam bentuk uang, namun boleh juga dalam bentuk barang atau kombinasi antara uang dan barang.</li>
  <li>Jika modal usaha dalam bentuk barang, wajib dilakukan taqwim al'urudh pada saat akad.</li>
  <li>Modal usaha yang diserahkan oleh shohib al-mal wajib dijelaskan jumlah/nilai nominalnya.</li>
  <li>Jenis mata uang yang digunakan sebagai ra's al-mal wajib disepakati oleh para pihak (shahib al-mal dan mudharib).</li>
  <li>Jika shahib al-mal menyertakan ra's al-mal berupa mata uang yang berbeda, wajib dikonversi ke daiam mata uang yang disepakati sebagai ra's al-mal pada saat akad.</li>
  <li>Ra's al-mal tidak boleh dalam bentuk piutang.</li>
  </ol>
  <p>Ketentuan terkait Nisbah Bagi Hasil</p>
  <ol>
  <li>Sistem/metode pembagian keuntungan harus disepakati dan dinyatakan secara jelas dalam akad.</li>
  <li>Nisbah bagi hasil harus disepakati pada saat akad.</li>
  <li>Nisbah bagi hasil sebagaimana angka 2 tidak boleh dalam bentuk nominal atau angka persentase dari modal usaha.</li>
  <li>Nisbah bagi hasil sebagaimana angka 2 tidak boleh menggunakan angka persentase yang mengakibatkan keuntungan hanya dapat diterima oleh salah satu pihak; sementara pihak lainnya tidak berhak mendapatkan hasil usaha mudharabah.</li>
  <li>Nisbah bagi hasil boleh diubah sesuai kesepakatan.</li>
  <li>Nisbah bagi hasil boleh dinyatakan dalam bentuk multinisbah.</li>
  </ol>
  <p>Ketentuan Kegiatan Usaha</p>
  <ol>
  <li>Usaha yang dilakukan mudhqrib harus usaha yang halal dan sesuai dengan prinsip-prinsip syariah dan/atau peraturan perundangundangan yang berlaku.</li>
  <li>Mudharib dalam melakukan usaha mudharabah harus atas nama entitas mudharabah, tidakboleh atas nama dirinya sendiri.</li>
  <li>Biaya-biaya yang timbul karena kegiatan usaha atas nama entitas mudharabah, boleh dibebankan ke dalam entitas mudharabah.</li>
  <li>Muharib tidak boleh meminjam, meminjamkan, menlumbangkan, atau menghadiahkan ra's al-mal dan keuntungan kepada pihak lain, kecuali atas dasar izin dari shaltib al-mal.</li>
  <li>Mudhorib tidak boleh melakukan perbuatan yang termasuk a/t a' a dd| at - t aq s hir, danl atau mukh al afat a sy - syur ut h.</li>
  </ol>
  <p>Ketentuan terkait Pembagian Keuntungan dan Kerugian</p>
  <ol>
  <li>Keuntungan usaha mudharabah harus dihitung dengan jelas untuk menghindarkan perbedaan dan atau sengketa pada waktu alokasi keuntungan atau penghentian mudharabah.</li>
  <li>Seluruh keuntungan harus dibagikan sesuai nisbah bagi yang telah disepakati, dan tidak boleh ada sejumlah tertentu dari keuntungan, yang ditentukan di awal hanya untuk shahib al-mal ataumudharib.</li>
  <li>Mudharib boleh mengusulkan kelebihan atau persentase keuntungan untuk diberikan kepadanya jika keuntungan tersebut melebihi jumlah tertentu.</li>
  <li>Kerugian usaha mudharabah menjadi tanggung jawab shahib almal kecrali kerugian tersebut terjadi karena mudharib rnelakukan tindakan yang termasrk at-ta'addi, at-taqshir, dan/atau mukhalafat asy-syuruth, atau mudharib melakukan pelanggaran terhadap batasan dalam mudhar ab ah muqayyadah.</li>
  </ol>
  <p>Ketentuan Penutup</p>
  <p>Jika salah satu pihak tidak menunaikan kewajibannya atau jika terjadi perseiisihan di antara para pihak, maka penyelesaiannya dilakukan melalui lembaga penyelesaian sengketa berdasarkan syariah sesuai dengan peraturan perundang-undangan yang berlaku setelah tidak tercapai kesepakatan melalui musyawarah.</p>
</div>


          </div>
        </li><!-- end /.tab-item.akad-musyarakah -->
        <!-- <li class="tab-item akad-wakalah">
          <div class="content-3">
            <p>Wakalah adalah: Akad perwakilan antara dua pihak, dimana pihak pertama mewakilkan suatu urusan kepada pihak kedua untuk bertindak atas nama pihak pertama.</p>
            <ol>
              <li>Wakalah al-mutlaqah, adalah: mewakilkan secara mutlak, tanpa batasan waktu dan untuk segala urusan.</li>
              <li>Wakalah al-muqayyadah, adalah: penunjukan wakil untuk bertindak atas namanya dalam urusan-urusan tertentu.</li>
              <li>Wakalah al-ammah, adalah: perwakilan yang lebih luas dari pada al muqayyadah tetapi lebih sederhana dari pada al mutlaqah.</li>
            </ol>
          </div>
        </li> -->
        <!-- end /.tab-item.akad-wakalah -->
        <li class="tab-item bagi-hasil">
          <div class="content-4">
            <p>Imbal hasil usaha akan dibagi hasilkan secara periodik (bulanan, triwulanan, semesteran, tahunan) sesuai dengan kesepakatan dengan para nggota koperasi. Pendistribusian bagi hasil kepada anggota koperasi adalah imbal hasil bersih (telah dipotong pajak sebesar 15% sesuai dengan ketentuan pajak penghasilan)</p>
          </div>
        </li><!-- end /.tab-item.bagi-hasil -->
      </ul>
    </div>
  </div>
@endsection
