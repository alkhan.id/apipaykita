@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-card uk-card-default">
    <div class="uk-card-body">
      <div class="uk-grid-divider uk-child-width-expand@s" uk-grid>
        <div class="content-left">
          <h2>Hubungi Kami</h2>
          <p>Terima kasih atas minat Anda untuk bekerja sama dengan kami. Isikan form berikut untuk membantu kami memahami kebutuhan Anda. Atau anda juga dapat mengirimkan ke email plazadanasyariah@gmail.com</p>
        </div>
        <div class="content-right">


          <form class="uk-form uk-card uk-card-default uk-card-body uk-contact-form uk-card-responsive uk-no-border"
            method="post" action="{{url('/contact_us_page')}}">
            @csrf
            <div class="uk-margin">
              <label class="uk-form-label">Nama
              <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
              <input type="text" class="uk-input" name="name" id="">
              </div>
            </div>
            <div class="uk-margin">
              <label class="uk-form-label">Email
               <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
              <input type="email" class="uk-input" name="email" id="">
              </div>
            </div>
            <div class="uk-margin">
              <label class="uk-form-label">Pesan
               <i class="uk-text-danger">*</i>
              </label>
              <div class="uk-form-controls">
              <textarea name="pesan" class="uk-textarea" id="" cols="30" rows="10"></textarea>
              </div>
            </div>
            <div class="uk-margin">
              <div class="uk-grid" uk-grid>
                <div class="uk-width-expand@s" uk-grid>
                  <div class="uk-form-controls uk-width-expand@s">
                  </div>
                  <div class="uk-width-1-3@s">
                    <!-- <button class=""
                      type="submit">Kirimkan</button> -->
                    <input type="submit" class="uk-button uk-button-large uk-button-warning uk-text-right button-submit">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection


  <script>
    // $('form.uk-contact-form').submit(function(){
    //   $('.button-submit').attr('disabled', 'disabled');
    // });
  </script>
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
