@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-margin-large-top">
    <h2 class="uk-text-center uk-visible@s">Bantuan Pengguna</h2>
    <h4 class="uk-text-center uk-hidden@s">Bantuan Pengguna</h4>
    <p class="uk-text-center">Penjelasan bagaimana cara menggunakan layanan plazadana.com</p>
  </div>
  <div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Investor</a></li>
      <li><a>Koperasi / Pelapak</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item content-investor">
          <div class="uk-alert"><i uk-icon="info"></i> Alur transaksi singkat untuk Investor saat melakukan Investasi.</div>
          <p>1. Tampilan beranda plazadana.com, Masuk atau Mendaftar melalui tombol berikut.</p>
          <img src="{{asset('images/docs/investor/1.png')}}">
          <hr class="uk-divider-icon">
          <p>2. Masuk kedalam halaman login/register, untuk masuk kedalam otentikasi dasar sebelum melakukan aktivitas investasi.</p>
          <img src="{{asset('images/docs/investor/2.png')}}">
          <hr class="uk-divider-icon">
          <p>3. Menyorot menu dashboard yang telah disediakan pada website plazadana.com</p>
          <img src="{{asset('images/docs/investor/3.png')}}">
          <hr class="uk-divider-icon">
          <p>4. Melihat peluang Investasi yang tersedia sesuai dengan keinginan Investor.</p>
          <img src="{{asset('images/docs/investor/4.png')}}">
          <hr class="uk-divider-icon">
          <p>5. Memilih peluang Investasi yang diinginkan.</p>
          <img src="{{asset('images/docs/investor/5.png')}}">
          <hr class="uk-divider-icon">
          <p>{% trans '6. Mengklik tombol "Investasi", dan menyetujui "Lisensi Perjanjian" akad yang telah disediakan.')}}</p>
          <img src="{{asset('images/docs/investor/6.png')}}">
          <hr class="uk-divider-icon">
          <p>{% trans '7. Melengkapi data-data wajib, dan mengisikan "Jumlah Investasi", serta memilih biaya asuransi.')}}</p>
          <img src="{{asset('images/docs/investor/7.png')}}">
          <hr class="uk-divider-icon">
          <p>{% trans '8. Setelah selesai, selanjutnya anda diarahkan ke "Dashboard → Investasiku" untuk melihat detail Investasi. Selanjutnya mengklik tombol "Bayar Sekarang".')}}</p>
          <img src="{{asset('images/docs/investor/8.png')}}">
          <hr class="uk-divider-icon">
          <p>9. Pada tahapan ini pastikan anda sudah mendownload aplikasi Paykita & mentransfer dana melalui Paykita tersebut.</p>
          <p>{% trans '10. Masukkan "Nomor Rekening Investasi (pasif)" pada Paykita anda pada form berikut, dan klik "Cek Pembayaran" untuk mengecek pembayaran.')}}</p>
          <img src="{{asset('images/docs/investor/9.png')}}">
          <hr class="uk-divider-icon">
          <p>{% trans '11. Setelah pembayaran berhasil dilakukan, pada menu "Dashboard → Investasiku → Lihat Laporan Keuangan" anda dapat melihat ulang investasi anda dan melihat laporan keuangan dari proyek yang di investasikan.')}}</p>
          <img src="{{asset('images/docs/investor/10.png')}}">
        </li><!-- end /.content-investor -->

        <li class="tab-item content-cooperative">
          <div class="uk-alert"><i uk-icon="info"></i> Alur transaksi singkat untuk Koperasi/Pelapak saat melakukan Penggalangan Dana.</div>
          <p>1. Tampilan beranda plazadana.com, Masuk atau Mendaftar melalui tombol berikut.</p>
          <img src="{{asset('images/docs/cooperative/1.png')}}">
          <hr class="uk-divider-icon">
          <p>2. Masuk kedalam halaman login/register, untuk masuk kedalam otentikasi dasar sebelum melakukan aktivitas investasi.</p>
          <img src="{{asset('images/docs/cooperative/2.png')}}">
          <hr class="uk-divider-icon">
          <p>3. Menyorot menu dashboard yang telah disediakan pada website plazadana.com</p>
          <img src="{{asset('images/docs/cooperative/3.png')}}">
          <hr class="uk-divider-icon">
          <p>4. Mengisikan Data Proposal, Data Koperasi & Data Penanggung Jawab seperti form dibawah.</p>
          <img src="{{asset('images/docs/cooperative/4.png')}}">
          <hr class="uk-divider-icon">
          <p>4. Mengisikan Data Proposal, Data Koperasi & Data Penanggung Jawab seperti form dibawah.</p>
          <img src="{{asset('images/docs/cooperative/4.png')}}">
          <hr class="uk-divider-icon">
          <p>5. Menunggu proposal diproses oleh pihak plazadana.com. Pihak Koperasi/Pelapak juga bisa mendiskusikan sesuatu terkait dengan proposal yang diajukannya dengan pihak plazadana.com</p>
          <img src="{{asset('images/docs/cooperative/5.png')}}">
          <hr class="uk-divider-icon">
          <p>6. Apabila proposal sudah disetujui, selanjutnya pihak Koperasi/Pelapak tiap bulannya membuat laporan keuangan.</p>
          <img src="{{asset('images/docs/cooperative/6.png')}}">
          <hr class="uk-divider-icon">
          <p>{% trans '7. Melengkapi data laporan keuangan serta melampirkan buktinya untuk divalidasi oleh pihak plazadana.com. Jika tidak terdapat biaya pada field tertentu, maka diisikan dengan nilai "0" (nol).')}}</p>
          <img src="{{asset('images/docs/cooperative/7.png')}}">
          <hr class="uk-divider-icon">
          <p>{% trans '8. Setelah laporan dibuat dan disetujui oleh pihak plazadana.com, pada menu "Dashboard → Laporanku" anda dapat melihat laporan keuangannya seabgaimana berikut.')}}</p>
          <img src="{{asset('images/docs/cooperative/8.png')}}">
        </li><!-- end /.content-cooperative -->
      </ul>
    </div>
  </div>


@endsection
