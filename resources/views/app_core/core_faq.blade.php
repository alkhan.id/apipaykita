@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({% static 'images/road.jpg' %});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">FAQ</h1>
      <p class="uk-text-white short-description-teaser-detail">Frequently Asked Questions (FAQ) - Pertanyaan yang Sering Diajukan</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Investor</a></li>
      <li><a>Koperasi / Pelapak</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item investor">
          <ul uk-accordion>
            <li class="uk-open">
                <a class="uk-accordion-title">1. Apa yang dimaksud dengan retail equity based crowdfunding syariah?</a>
                <div class="uk-accordion-content">
                  <p>Retail equity based crowdfunding adalah penawaran online penyertaan investasi oleh unit  usaha koperasi secara retail kepada individu, sekelompok orang, jamaah, komunitas melalui market place <a href="www.plazadana.com"> www.plazadana.com</a>. Jenis Instrument penyertaan investasi yang dapat ditawarkan adalah equity based financing (seperti simpanan pokok, simpanan wajib, dan simpanan investasi), dan debt asset based financing (seperti Surat Utang Koperasi Syariah disingkat “SUKS”, SUKUK/Obligasi Syariah, Bilateral debt financing dan sejenisnya). Koperasi juga dapat menawarkan instrument penggalangan dana berupa waqaf produktif, CSR, Hibah, dan sejenisnya. Berikut di bawah ini adalah rangkuman instrument, jenis dan akad-akadnya:</p>
                  <p>
                    <table class="uk-table uk-table-striped">
                      <thead>
                        <tr>
                          <th><strong>No</strong></th>
                          <th><strong>Instrument</strong></th>
                          <th><strong>Jenis</strong></th>
                          <th><strong>Akad</strong></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Simpanan Pokok</td>
                          <td>Ekuitas</td>
                          <td>Musyarakah</td>
                        </tr>
                        <tr>
                            <td width="37">
                              <p>2</p>
                            </td>
                            <td width="165">
                              <p>Simpanan wajib</p>
                            </td>
                            <td width="104">
                              <p>Ekuitas</p>
                            </td>
                            <td width="188">
                              <p>Musyarakah</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>3</p>
                          </td>
                          <td width="165">
                            <p>Simpanan Investasi</p>
                          </td>
                          <td width="104">
                            <p>Ekuitas</p>
                          </td>
                          <td width="188">
                            <p>Musyarakah</p>
                          </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>4</p>
                            </td>
                            <td width="165">
                              <p>Bilateral financing</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>Semua akad syariah yang relevan</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>5</p>
                            </td>
                            <td width="165">
                              <p>Surat utang koperasi syariah (SUKS)</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>Semua akad syariah yang relevan</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>6</p>
                            </td>
                            <td width="165">
                              <p>Sukuk</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>idem</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>7</p>
                            </td>
                            <td width="165">
                              <p>Reksadana</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>idem</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>8</p>
                            </td>
                            <td width="165">
                              <p>Ziswaf</p>
                            </td>
                            <td width="104">
                              <p>Donasi</p>
                            </td>
                            <td width="188">
                              <p>Ziswaf</p>
                            </td>
                          </tr>
                      </tbody>
                    </table>
                  </p>
                </div>
            </li>
            <li>
              <a class="uk-accordion-title">2. Siapa yang menjadi Investor?</a>
              <div class="uk-accordion-content">
                <p>Siapa pun bisa menjadi investor terutama anggota komunitas yang mendirikan Koperasi.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">3. Siapa yang akan menggunakan pendanaan investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Yang akan menggunakan pendanaan investasi adalah unit usaha yang berbadan hukum koperasi terutama yang didirikan oleh komunitas.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">4. Apakah akad investasi saya sesuai dengan syariah?</a>
              <div class="uk-accordion-content">
                <p>Ya, akad (kontrak) para pihak yang terlibat dalam transaksi menggunakan akad syariah sesuai dengan Fatwa Dewan Syariah Nasional (DSN). Untuk penyertaan investasi dalam bentuk simpanan pokok, wajib dan simpanan investasi menggunakan akad musyarakah.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">5. Amankah uang yang saya investasikan di koperasi yang terdaftar di platform plazadana.com?</a>
              <div class="uk-accordion-content">
                <p>Uang yang anda investasikan di Koperasi yang terdaftar di plazadana tidak ada penjaminan, namun anda dapat mengawasi kinerja koperasi dimana anda berinvestasi melalui platform Plazadana, karena anda termasuk anggota koperasi dimana anda berinvestasi sehingga anda bisa memantau dan mengawasinya secara regular kapanpun dan dari manapun melalui platform online Plazadana.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">6. Bagaimana penyertaan investasi ini dapat menguntungkan saya?</a>
              <div class="uk-accordion-content">
                <p>Untung dan rugi penyertaan investasi anda tergantung kinerja pengelolaan koperasi anda dimana anda berinvestasi. Jika realisasinya menguntungkan maka anda akan memperoleh bagi hasil sesuai dengan proporsi penyertaan anda dalam unit usaha tersebut. Semakin tinggi keuntungan hasil usaha, maka semakin tinggi pula bagi hasil yang akan anda nikmati. Demikian pula sebaliknya.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">7. Bagaimana jika investasinya gagal?</a>
              <div class="uk-accordion-content">
                <p>Jika unit usaha yang menjadi underlying investasi anda mengalami kegagalan maka kemungkinan uang investasi anda tidak akan kembali utuh atau bahkan tidak kembali semuanya.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">8. Berapa jumlah dana minimal yang bisa saya investasikan?</a>
              <div class="uk-accordion-content">
                <p>Anda dapat melakukan investasi melalui plazadana minimal mulai dari Rp 100.000,-</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">9. Kapan saya bisa memperoleh pembayaran imbal hasil investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Anda dapat menikmati imbal hasil investasi secara periodik sesuai dengan kesepakatan para pihak yang tergabung dalam Koperasi di komunitas anda (bisa tiap bulan, triwulanan, semesteran, atau tahunan).</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">10. Bagaimana saya bisa mengetahui pembayaran imbal hasil investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Mudah saja, layanan investasi di Koperasi yang terdaftar platform Plazadana telah terintegrasi dengan layanan perbankan syariah secara online, anda dapat menikmati berbagai layanan kemudahan transaksi diantaranya cek saldo, transfer, tarik tunai via ATM, berbagai macam pembayaran billing seperti air minum, token listrik, iuran BPJS, tiket, pulsa berbagai macam provider, termasuk dapat digunakan untuk pembayaran belanja di koperasi dimana anda berinvestasi, dll.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">11. Bagaimana saya bisa memantau kinerja investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Mudah saja, anda dapat melakukan pemantauan kinerja unit usaha Koperasi anda dimana anda berinvestasi kapanpun dan dari manapun melalui dashboard yang tersedia di website plazadana.com. Unit usaha dimana anda berinvestasi wajib melakukan publikasi laporan kinerja keuangan secara periodik melalui plazadana, sehingga aspek transparansi dan akuntabilitasnya dapat dipenuhi sesuai dengan standard Good Corporate Governance (GCG).</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">12. Bagaimana aspek perpajakan terkait dengan imbal hasil saya?</a>
              <div class="uk-accordion-content">
                <p>Menyusul ............</p>
              </div>
            </li>
          </ul>
        </li>
        <li class="tab-item cooperative">
          <ul uk-accordion>
            <li class="uk-open">
              <a class="uk-accordion-title">1. Apa yang dimaksud dengan retail equity based crowdfunding syariah?</a>
              <div class="uk-accordion-content">
                <p>Retail equity based crowdfunding adalah penawaran online penyertaan investasi oleh unit  usaha koperasi secara retail kepada individu, sekelompok orang, jamaah, komunitas melalui market place <a href="www.plazadana.com"> www.plazadana.com</a>. Jenis Instrument penyertaan investasi yang dapat ditawarkan adalah equity based financing (seperti simpanan pokok, simpanan wajib, dan simpanan investasi), dan debt asset based financing (seperti Surat Utang Koperasi Syariah disingkat “SUKS”, SUKUK/Obligasi Syariah, Bilateral debt financing dan sejenisnya). Koperasi juga dapat menawarkan instrument penggalangan dana berupa waqaf produktif, CSR, Hibah, dan sejenisnya. Berikut di bawah ini adalah rangkuman instrument, jenis dan akad-akadnya:</p>
                <p>
                  <table class="uk-table uk-table-striped">
                    <thead>
                      <tr>
                        <th><strong>No</strong></th>
                        <th><strong>Instrument</strong></th>
                        <th><strong>Jenis</strong></th>
                        <th><strong>Akad</strong></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Simpanan Pokok</td>
                        <td>Ekuitas</td>
                        <td>Musyarakah</td>
                      </tr>
                      <tr>
                          <td width="37">
                            <p>2</p>
                          </td>
                          <td width="165">
                            <p>Simpanan wajib</p>
                          </td>
                          <td width="104">
                            <p>Ekuitas</p>
                          </td>
                          <td width="188">
                            <p>Musyarakah</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>3</p>
                        </td>
                        <td width="165">
                          <p>Simpanan Investasi</p>
                        </td>
                        <td width="104">
                          <p>Ekuitas</p>
                        </td>
                        <td width="188">
                          <p>Musyarakah</p>
                        </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>4</p>
                          </td>
                          <td width="165">
                            <p>Bilateral financing</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>Semua akad syariah yang relevan</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>5</p>
                          </td>
                          <td width="165">
                            <p>Surat utang koperasi syariah (SUKS)</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>Semua akad syariah yang relevan</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>6</p>
                          </td>
                          <td width="165">
                            <p>Sukuk</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>idem</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>7</p>
                          </td>
                          <td width="165">
                            <p>Reksadana</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>idem</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>8</p>
                          </td>
                          <td width="165">
                            <p>Ziswaf</p>
                          </td>
                          <td width="104">
                            <p>Donasi</p>
                          </td>
                          <td width="188">
                            <p>Ziswaf</p>
                          </td>
                        </tr>
                    </tbody>
                  </table>
                </p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">2. Apa persyaratan untuk menggalang dana melalui platform www.plazadana.com?</a>
              <div class="uk-accordion-content">
                <center>PERSYARATAN LISTED &amp; FUND RAISING VIA PLAZADANA</center>
                <table class="uk-table uk-table-small uk-table-striped">
                  <thead class="uk-text-bold" >
                    <tr>
                      <th class="uk-table-middle" rowspan="2">No</th>
                      <th class="uk-table-middle" rowspan="2">Persyaratan Koperasi</th>
                      <th class="uk-text-center" colspan="3"> Simpanan(equity financing)</th>
                      <th class="uk-text-center" colspan="2">Debt financing</th>
                      <th class="uk-table-middle uk-text-center" rowspan="2">Bilateral financing</th>
                    </tr>
                    <tr>
                      <th class="uk-text-center">Pokok</th>
                      <th class="uk-text-center">Wajib</th>
                      <th class="uk-text-center">investasi</th>
                      <th class="uk-text-center">SUKS</th>
                      <th class="uk-text-center">SUKUK</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Surat permohonan ke Plazadana</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Proposal kelayakan usaha</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Copy Akte pendirian</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Copy SK Kemenkop</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Copy Nomor Induk Berusaha</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Copy Izin usaha</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>7</td>
                      <td>Copy Izin operasional</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>8</td>
                      <td>Copy NPWP Koperasi</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>9</td>
                      <td>Copy AD/ART</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>10</td>
                      <td>Copy KTP Pengurus inti</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>11</td>
                      <td>Underlying asset/sebagai jaminan</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td></td>                    
                    </tr>
                    <tr>
                      <td>12</td>
                      <td>Rapat anggota > 2x</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td></td>                    
                    </tr>
                    <td>13</td>
                    <td>Laporan Keuangan audit</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="uk-text-center"><span uk-icon="check"></span></td>
                    <td class="uk-text-center"><span uk-icon="check"></span></td>
                    <td></td>                    
                  </tr>
                  </tbody>
                </table>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">3. Siapa yang akan memberikan penyertaan investasi kepada penggalang dana/modal?</a>
              <div class="uk-accordion-content">
                <p>Semua calon anggota koperasi yang berminat baik yang berasal dari komunitas yang mendirikan koperasi atau  di luar komunitas pendiri koperasi</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">4. Bagaimana cara menggalang dana melalui plazadana.com?</a>
              <div class="uk-accordion-content">
                <ol class="">
                  <li>Penuhi persyaratan legalitas usaha</li>
                  <li>Daftarkan Koperasi anda di platform <a href="www.plazadana.com">www.plazadana.com</a></li>
                  <li>Uji tuntas dan verifikasi (Pengumpulan informasi tentang profil kegiatan dan kelayakan usaha, verifikasi dokumen legal, proyeksi cashflow dan kelayakan finansial.</li>
                  <li>Dapatkan modal melalui penawaran umum peluang investasi kepada calon anggota Koperasi/investor melalui plazadana.com</li>
                  <li>Pencairan dana jika target penggalangan dana telah terpenuhi.</li>
                  <li>Pelaporan realisasi penggunaan modal &amp; laporan publikasi kinerja keuangan berkala melalui plazadana.com</li>
                </ol>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">5. Berapa imbal hasil yang harus saya berikan kepada investor?</a>
              <div class="uk-accordion-content">
                <p>Kontrak/akad yang digunakan dalam skema penyertaan investasi ke dalam unit usaha koperasi berjenis equity financing adalah musyarakah (Akad musyarakah adalah akad kerjasama antara dua pihak atau lebih untuk suatu usaha tertentu, dimana masing – masing pihak memberikan kontribusi dana dengan ketentuan dibagi berdasarkan kesepakatan sedangkan keuntungan/kerugian berdasarkan kontribusi dana atau sesuai dengan kesepakatan).</p>
                <p>Jadi besaran imbal hasil yang harus diberikan kepada investor tergatung realisasi keuntungan yang diperoleh Koperasi setelah dipotong biaya-biaya. Jika realisasi imbal hasil/keuntungan tinggi maka besaran imbal hasil yang akan dibagikan kepada anggota koperasi/investor juga tinggi, demikian sebaliknya. Sehingga aspek keadilan antara para pihak terpenuhi.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">6. Kapan saya harus membayarkan bagi hasil kepada investor?</a>
              <div class="uk-accordion-content">
                <p>Pembayaran bagi hasil dilakukan secara periodik, bisa bulanan, triwulanan, semesteran, atau tahunan sesuai kesepakatan para pihak yang diatur dalam Anggaran Rumah Tangga Koperasi. Namun demikian dianjurkan pembayaran bagi hasil dilakukan secara bulanan.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">7. Apa kewajiban penggalang dana/modal terhadap investor?</a>
              <div class="uk-accordion-content">
                <p>Penggalang dana/Koperasi wajib melakukan publikasi laporan kinerja keuangan tiap akhir bulan, akhir triwulan, akhir semesteran, dan akhir tahunan melalui dashboard yang telah disediakan di platform plazadana.com. Kemudian setiap akhir tahun Koperasi wajib melakukan publikasi laporan keuangan yang telah diaudit oleh independen auditor (bagi emiten koperasi yang menerbitkan instrument penggalanan dana seperti SUKS dan SUKUK)</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">8. Haruskah penggalang dana menyediakan jaminan?</a>
              <div class="uk-accordion-content">
                <p>Untuk penggalangan dana melalui instrument equity based financing (simpanan pokok, wajib, investasi) tidak memerlukan underlying asset sebagai jaminan. Sedangkan bagi Koperasi yang menggalang dana melalui instrument SUKS dan SUKUK wajib menyediakan underlying asset sebagai jaminan senilai jumlah penggalangan dana.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">9. Berapa biaya penggalangan dana yang harus dikeluarkan jika penggalangan dana melalui platform ini tercapai?</a>
              <div class="uk-accordion-content">
                <p>Penggalang dana yang berhasil mencapai target akan dikenakan biaya maksimal sebesar 3% dari total penggalangan dana dan akan dibayarkan pada saat pencairan dana yang telah terkumpul.</p>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
@endsection
