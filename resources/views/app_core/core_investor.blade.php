@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({{asset('images/photo2.jpg')}});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Investor</h1>
      <p class="uk-text-white short-description-teaser-detail">Memberikan pelayanan dan peluang investasi terbaik</p>
    </div>
  </div>
  <!-- <hr class="uk-divider-icon"> -->
  <div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Peluang Investasi</a></li>
      <li><a>Diversifikasi Investasi</a></li>
      <li><a>Paykita</a></li>
      <li><a>FAQ</a></li>
      <li><a>Dashboard</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item peluang-investasi">
          <div class="content-1">
            <h3 class="uk-heading-line-none">
              <span>1. Mengapa berinvestasi melalui PlazaDana?</span>
            </h3>
            <div class="uk-grid-divider uk-margin-top uk-child-width-expand@s" uk-grid>
              <div class="content-sub-1">
                <h4 class="uk-heading-bullet">Imbal hasil kompetitif</h4>
                <ul class="uk-list uk-list-bullet">
                  <li>Potensi imbal hasil kompetitif karena investasinya langsung ke sektor riil dan retail.</li>
                  <li>Berdasarkan bagi hasil, jika unit usaha dimana anda berinvestasi memiliki imbal hasil tinggi maka bagi hasil yang akan anda nikmati juga tinggi, demikian sebaliknya.</li>
                </ul>
              </div>
              <div class="content-sub-2">
                <h4 class="uk-heading-bullet">Transparan &amp; akuntable</h4>
                <ul class="uk-list uk-list-bullet">
                  <li>Anda dapat memantau kinerja keuangan terkini unit usaha dimana anda berinvestasi  dimanapun dan dari manapun melalui dashboard di plazadana.com</li>
                  <li>Unit usaha tempat penyaluran investasi anda akan melakukan publikasi laporan kinerja keuangan terkini secara berkala.</li>
                </ul>
              </div>
              <div class="content-sub-3">
                <h4 class="uk-heading-bullet">Mudah, murah, &amp; sesuai syariah</h4>
                <ul class="uk-list uk-list-bullet">
                  <li>Plazadana memberikan kemudahan dalam berinvestasi.</li>
                  <li>Dengan Rp 100,000,- Anda sudah bisa berinvestasi.</li>
                  <li>Semua transaksi investasi sesuai dengan syariah.</li>
                </ul>
              </div>
            </div>
            <p>
              Dapatkan peluang investasi di koperasi yang bergerak di berbagai bidang usaha dengan skema menarik, akuntable, transparan, dan imbal hasil kompetitif, serta sesuai dengan Syariah. Semua peluang investasi di masing-masing unit usaha yang diinisiasi oleh koperasi/UKM yang melapak di plazadana.com telah dilakukan penyaringan atau screening sesuai dengan risk acceptance criteria (RAC), namun demikian kewajiban untuk melakukan analisa kelayakan investasi/pembiayaan penyertaan modal atau uji tuntas sepenuhnya berada di pihak investor atau calon anggota koperasi, karena layanan kami tidak mencakup pemberian saran terkait hal tersebut. Setiap pembayaran bagi hasil akan dikenakan biaya 1% untuk pemeliharaan system. <a href="{{ url('teaser_list') }}">Pelajari penawaran peluang investasi...</a> 
            </p>
          </div><!-- end /.content-1 -->

          <hr class="uk-divider-icon">

          <div class="content-2">
            <h3 class="uk-margin-remove-top">
              <span>2. Mengapa PlazaDana?</span>
            </h3>
            <!--<ul class="uk-list uk-list-bullet">
              <li>Plazadana memberikan banyak kemudahan bagi investor dan investee (unit usaha yang melapak di plazadana.com)</li>
              <li>Bagi investor mudah untuk meng-akses semua peluang investasi di berbagai sektor, mudah untuk melakukan diversifikasi investasi, mudah untuk melakukan pemantauan kinerja investasi, dan mudah untuk melakukan berbagai macam transaksi keuangan seperti setoran awal atau topup investasi, penerimaan imbal hasil investasi, transfer antar bank, tarik tunai melalui ATM, cek saldo, cek 10 transaksi terakhir, billing payment dll, karena layanan plazadana.com telah terintegrasi dengan e-transaksi plazadana.</li>
              <li>Bagi investee mudah untuk meng-akses sumber pendanaan yang lebih fleksible dengan investor based yang lebih luas, mudah untuk melakukan publikasi kinerja laporan keuangan secara periodik (harian, mingguan, bulanan, dst) karena standard system aplikasi laporan keuangan telah disediakan, mudah untuk melakukan berbagai macam transaksi keuangan karena telah terintegrasi dengan layanan e-transaksi plazadana.com</li>
            </ul>-->
            <img src="{{asset('images/contents/icon-transaksi-mudah.png')}}" alt="Transaksi Mudah Plazadana">
          </div><!-- end /.content-2 -->

          <hr class="uk-divider-icon">

          <div class="content-3">
            <h3 class="uk-margin-remove-top">
              <span>3. Produk PlazaDana?</span>
            </h3>
            <div class="uk-alert">
              <!--  -->
              Produk yang tersedia di layanan plazadana.com saat ini adalah penyertaan investasi di koperasi/UMKM secara syariah (Islamic retail equity based crowd funding). Berikut ini adalah yang membedakan layanan plazadana.com dengan P2P lending lainnya:
              <!--  -->
            </div>
            <img width="100%" src="{{asset('images/contents/product-vs.png')}}" alt="Produk PlazaDana">
          </div><!-- end /.content-3 -->

          <hr class="uk-margin-large-top uk-divider-icon">

          <div class="content-4">
            <h3 class="uk-margin-remove-top">
              <span>4. Mulai berinvestasi dengan beberapa langkah mudah
            </h3>
            <div class="uk-child-width-1-4@m uk-grid-small uk-grid-match" uk-grid>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">1</div>
                  <h4>Daftar &amp; Login </h4>
                  <p>Daftarkan diri anda sebagai Investor.</p>
                  <a href="{{ route('register') }}" class="uk-button uk-button-primary spf-link">
                    <span uk-icon="sign-in"></span> Daftar Disini
                  </a>
                </div>
              </div>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">2</div>
                  <h4>Pilih Peluang Investasi</h4>
                  <p>
                    <!--  -->
                    Pilih peluang investasi, lakukan uji tuntas berdasarkan informasi yang tersedia, jika Ingin uji tuntas lebih lanjut hubungi PIC tujuan investasi yang tersedia.
                    <!--  -->
                  </p>
                  <a href="{{url('teaser_list')}}" class="uk-button uk-button-primary spf-link">
                    <span uk-icon="forward"></span> Lihat Peluang
                  </a>
                </div>
              </div>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">3</div>
                  <h4>Transfer</h4>
                  <p>Tentukan jumlah dana yang ingin Diinvestasikan, transfer dana Investasi ke rekening virtual Anda dan mulailah investasi.</p>
                </div>
              </div>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">4</div>
                  <h4>Pantau Kinerja Keuangan</h4>
                  <p>Pantau kinerja keuangan Unit usaha dimana anda berinvestasi melalui Dashboard di plazadana.com Nikmati imbal hasil kompetitif Bebas riba nan halal.</p>
                </div>
              </div>
            </div>

            <div class="uk-card uk-card-body uk-margin uk-card-responsive">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto@s uk-first-column">
                  <a class="uk-remove-underline" href="https://play.google.com/store/apps/details?id=com.app.plazadanamobilebanking" target="_blank" style="position:relative;margin-right: 1em;">
                    <img class="uk-border-circle" width="70" height="70" src="{{asset('images/wallet.png')}}">
                    <img width="20" height="20" src="{{asset('images/cursor.png')}}" style="position:absolute;right:-15px;bottom:-20px;">
                  </a>
                </div>
                <div class="uk-width-expand">
                  <p>Untuk menikmati berbagai kemudahan transaksi melalui mobilephone <b>(Android)</b> anda, pastikan anda mengunduh aplikasi Paykita plazadana melalui mobile phone anda di playstore atau google play secara gratis.</p>
                </div>
              </div>
            </div>
          </div><!-- end /.content-4 -->
        </li><!-- end /.tab-item.peluang-investasi -->

        <li class="tab-item diversifikasi-investasi">
          <div class="uk-alert uk-padding uk-alert-responsive">
            <p><span uk-icon="info"></span> Setelah anda terdaftar, anda bebas login, bebas memilih peluang investasi yang tersedia dan menentukan jumlah dana investasi serta mendiversifikasikannya ke beberapa peluang investasi yang sedang melakukan penggalangan dana berbeda. Jadi anda dapat melakukan penempatan investasi di berbagai peluang investasi dengan jumlah minimal Rp 100 ribu per penempatan di setiap unit usaha. Diversifikasi adalah salah satu cara yang efektif untuk menyebar potensi risiko kegagalan investasi. Kami menyarankan agar anggota Plazadana berinvestasi dari jumlah yang kecil dalam penggalangan pendanaan yang berbeda, daripada menempatkan semua investasi menjadi satu tempat investasi saja agar dapat mengurangi kemungkinan risiko kerugian. Hal ini dapat melindungi dana investasi dari efek yang berasal dari salah satu Koperasi yang mengalami kerugian. Kami juga menyarankan agar anggota Plazadana menempatkan dana penyertaannya di kelas aset lainnya, terutama di Koperasi lain yang menawarkan profil risiko yang berbeda. Dan terahir kami menyarankan kepada Anda agar mendiskusikan kapasitas Anda untuk berinvestasi melalui Plazadana dengan penasihat keuangan Anda.</p>
          </div>
        </li><!-- end /.tab-item.diversifikasi-investasi -->

        <li class="tab-item e-transaksi">
          <div class="uk-grid uk-child-width-1-2@s">
            <div class="left-content">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/gDOiEWiLMQQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="right-content">
              <ul class="uk-list uk-list-bullet">
                <li>Layanan investasi melalui Plazadana.com telah terintegrasi dengan fasilitas Paykita (dompet elektronik) sehingga investor dan koperasi/UKM dapat menikmati berbagai kemudahan dalam melakukan transaksi keuangan terutama terkait dengan setoran tunai keanggotaan, penerimaan pembayaran distribusi imbal hasil dari unit usaha koperasi, setoran tambahan (top up) berbagai produk simpanan Koperasi yang tersedia, transfer dana atau pindah buku, serta tarik tunai melalui ATM seluruh bank yang telah memiliki kerjasama dengan anchor bank plazadana.com. Semua jenis transaksi yang dilakukan melalui Paykita dapat langsung dicek secara real time melalui mobile phone agar dapat dipastikan semua jenis transaksi keuangan berhasil (settled) dan dapat dilakukan rekonsiliasi dengan posisi saldo terkini yang ada di rekening bank milik investor/anggota koperasi.</li>
                <li>Bagi investor/anggota Koperasi yang ingin memperoleh pendapatan tambahan juga dapat menjadi agen multi billing seperti penjualan token listrik PLN, pulsa prabayar electric berbagai macam provider (XL, Telkomsel, Indosat, dll), billing fixed line pascabayar, telp dan kabel data, pembayaran tiket pesawat dan keretapi, dll. Untuk menjadi agen multi billing, anggota koperasi dapat men-download aplikasi plazadana melalui playstore dan google play secara gratis.</li>
              </ul>
            </div>
          </div>
        </li><!-- end /.tab-item.Paykita -->

        <li class="tab-item faq">
          <ul uk-accordion>
            <li class="uk-open">
                <a class="uk-accordion-title">1. Apa yang dimaksud dengan retail equity based crowdfunding syariah?</a>
                <div class="uk-accordion-content">
                  <p>Retail equity based crowdfunding adalah penawaran online penyertaan investasi oleh unit  usaha koperasi secara retail kepada individu, sekelompok orang, jamaah, komunitas melalui market place <a href="www.plazadana.com"> www.plazadana.com</a>. Jenis Instrument penyertaan investasi yang dapat ditawarkan adalah equity based financing (seperti simpanan pokok, simpanan wajib, dan simpanan investasi), dan debt asset based financing (seperti Surat Utang Koperasi Syariah disingkat “SUKS”, SUKUK/Obligasi Syariah, Bilateral debt financing dan sejenisnya). Koperasi juga dapat menawarkan instrument penggalangan dana berupa waqaf produktif, CSR, Hibah, dan sejenisnya. Berikut di bawah ini adalah rangkuman instrument, jenis dan akad-akadnya:</p>
                  <p>
                    <table class="uk-table uk-table-striped">
                      <thead>
                        <tr>
                          <th><strong>No</strong></th>
                          <th><strong>Instrument</strong></th>
                          <th><strong>Jenis</strong></th>
                          <th><strong>Akad</strong></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Simpanan Pokok</td>
                          <td>Ekuitas</td>
                          <td>Musyarakah</td>
                        </tr>
                        <tr>
                            <td width="37">
                              <p>2</p>
                            </td>
                            <td width="165">
                              <p>Simpanan wajib</p>
                            </td>
                            <td width="104">
                              <p>Ekuitas</p>
                            </td>
                            <td width="188">
                              <p>Musyarakah</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>3</p>
                          </td>
                          <td width="165">
                            <p>Simpanan Investasi</p>
                          </td>
                          <td width="104">
                            <p>Ekuitas</p>
                          </td>
                          <td width="188">
                            <p>Musyarakah</p>
                          </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>4</p>
                            </td>
                            <td width="165">
                              <p>Bilateral financing</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>Semua akad syariah yang relevan</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>5</p>
                            </td>
                            <td width="165">
                              <p>Surat utang koperasi syariah (SUKS)</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>Semua akad syariah yang relevan</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>6</p>
                            </td>
                            <td width="165">
                              <p>Sukuk</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>idem</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>7</p>
                            </td>
                            <td width="165">
                              <p>Reksadana</p>
                            </td>
                            <td width="104">
                              <p>Pembiayaan</p>
                            </td>
                            <td width="188">
                              <p>idem</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="37">
                              <p>8</p>
                            </td>
                            <td width="165">
                              <p>Ziswaf</p>
                            </td>
                            <td width="104">
                              <p>Donasi</p>
                            </td>
                            <td width="188">
                              <p>Ziswaf</p>
                            </td>
                          </tr>
                      </tbody>
                    </table>
                  </p>
                </div>
            </li>
            <li>
              <a class="uk-accordion-title">2. Siapa yang menjadi Investor?</a>
              <div class="uk-accordion-content">
                <p>Siapa pun bisa menjadi investor terutama anggota komunitas yang mendirikan Koperasi.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">3. Siapa yang akan menggunakan pendanaan investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Yang akan menggunakan pendanaan investasi adalah unit usaha yang berbadan hukum koperasi terutama yang didirikan oleh komunitas.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">4. Apakah akad investasi saya sesuai dengan syariah?</a>
              <div class="uk-accordion-content">
                <p>Ya, akad (kontrak) para pihak yang terlibat dalam transaksi menggunakan akad syariah sesuai dengan Fatwa Dewan Syariah Nasional (DSN). Untuk penyertaan investasi dalam bentuk simpanan pokok, wajib dan simpanan investasi menggunakan akad musyarakah.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">5. Amankah uang yang saya investasikan di koperasi yang terdaftar di platform plazadana.com?</a>
              <div class="uk-accordion-content">
                <p>Uang yang anda investasikan di Koperasi yang terdaftar di plazadana tidak ada penjaminan, namun anda dapat mengawasi kinerja koperasi dimana anda berinvestasi melalui platform Plazadana, karena anda termasuk anggota koperasi dimana anda berinvestasi sehingga anda bisa memantau dan mengawasinya secara regular kapanpun dan dari manapun melalui platform online Plazadana.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">6. Bagaimana penyertaan investasi ini dapat menguntungkan saya?</a>
              <div class="uk-accordion-content">
                <p>Untung dan rugi penyertaan investasi anda tergantung kinerja pengelolaan koperasi anda dimana anda berinvestasi. Jika realisasinya menguntungkan maka anda akan memperoleh bagi hasil sesuai dengan proporsi penyertaan anda dalam unit usaha tersebut. Semakin tinggi keuntungan hasil usaha, maka semakin tinggi pula bagi hasil yang akan anda nikmati. Demikian pula sebaliknya.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">7. Bagaimana jika investasinya gagal?</a>
              <div class="uk-accordion-content">
                <p>Jika unit usaha yang menjadi underlying investasi anda mengalami kegagalan maka kemungkinan uang investasi anda tidak akan kembali utuh atau bahkan tidak kembali semuanya.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">8. Berapa jumlah dana minimal yang bisa saya investasikan?</a>
              <div class="uk-accordion-content">
                <p>Anda dapat melakukan investasi melalui plazadana minimal mulai dari Rp 100.000,-</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">9. Kapan saya bisa memperoleh pembayaran imbal hasil investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Anda dapat menikmati imbal hasil investasi secara periodik sesuai dengan kesepakatan para pihak yang tergabung dalam Koperasi di komunitas anda (bisa tiap bulan, triwulanan, semesteran, atau tahunan).</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">10. Bagaimana saya bisa mengetahui pembayaran imbal hasil investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Mudah saja, layanan investasi di Koperasi yang terdaftar platform Plazadana telah terintegrasi dengan layanan perbankan syariah secara online, anda dapat menikmati berbagai layanan kemudahan transaksi diantaranya cek saldo, transfer, tarik tunai via ATM, berbagai macam pembayaran billing seperti air minum, token listrik, iuran BPJS, tiket, pulsa berbagai macam provider, termasuk dapat digunakan untuk pembayaran belanja di koperasi dimana anda berinvestasi, dll.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">11. Bagaimana saya bisa memantau kinerja investasi saya?</a>
              <div class="uk-accordion-content">
                <p>Mudah saja, anda dapat melakukan pemantauan kinerja unit usaha Koperasi anda dimana anda berinvestasi kapanpun dan dari manapun melalui dashboard yang tersedia di website plazadana.com. Unit usaha dimana anda berinvestasi wajib melakukan publikasi laporan kinerja keuangan secara periodik melalui plazadana, sehingga aspek transparansi dan akuntabilitasnya dapat dipenuhi sesuai dengan standard Good Corporate Governance (GCG).</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">12. Bagaimana aspek perpajakan terkait dengan imbal hasil saya?</a>
              <div class="uk-accordion-content">
                <p>Menyusul ............</p>
              </div>
            </li>
          </ul>
        </li><!-- end /.tab-item.faq -->

        <li class="tab-item dashboard">
          Plazadana.com menyediakan dashboard portfolio kinerja setiap koperasi yang melapak di market place untuk memberikan kemudahan agar setiap anggota yang telah berpartisipasi dalam penyertaan modal di masing-masing koperasi dapat memantau kinerja keuangan setiap koperasi secara up to date. <a href="{{ 'url_dashboard' }}">Lihat dasboard investasi...</a>
        </li><!-- end /.tab-item.dashboard -->
      </ul>
    </div>
  </div>
  @endsection

