@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({{asset('images/dark-full.jpg')}}">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Penggalangan Dana</h1>
      <p class="uk-text-white short-description-teaser-detail">Nikmati kemudahan dalam penggalangan dana melalui plazadana.com</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Penggalangan Dana</a></li>
      <li><a>Bagi Hasil &amp; Biaya</a></li>
      <li><a>Paykita</a></li>
      <li><a>FAQ</a></li>
      <li><a>Laporan Publikasi</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item penggalangan-dana">
          <div class="content-1">
            <p>Salah satu misi plazadana.com adalah untuk meningkatkan partisipasi masyarakat dalam kegiatan ekonomi melalui koperasi. Untuk itu masyarakat yang ingin membangun kemitraan usaha bersama dalam bentuk koperasi atau sudah memiliki badan usaha koperasi dan ingin menambah serta memperluas jumlah keanggotaannya dapat melapak/listing di plazadana.com dalam rangka upaya menarik anggota baru sehingga seiring dengan bertambahnya anggota koperasi maka beban pendanaan/permodalan menjadi lebih ringan. Bagi koperasi yang sudah berhasil menggalang dana sesuai target, wajib melakukan publikasi laporan keuangan setiap bulan dan melakukan pembayaran imbal hasil kepada anggota secara periodic sesuai dengan kesepakatan melalui plazadana.com. <a href="{{ url ('/proposal/create') }}">Mulai penggalangan dana...</a></p>
          </div>

          <hr class="uk-divider-icon">

          <div class="content-2">
            <h3 class="uk-heading-line-none">
              <span>1. Mengapa penggalangan dana melalui PlazaDana?</span>
            </h3>
            <div class="uk-grid-divider uk-margin-top uk-child-width-expand@s" uk-grid>
              <div class="content-sub-1">
                <h4 class="uk-heading-bullet">Mudah</h4>
                <ul class="uk-list uk-list-bullet">
                  <li>Koperasi/UKM dapat meng-akses sumber pendanaan secara online dengan proses sederhana dan mudah.</li>
                </ul>
              </div>
              <div class="content-sub-2">
                <h4 class="uk-heading-bullet">Murah</h4>
                <ul class="uk-list uk-list-bullet">
                  <li>Biaya pendanaan relatif murah, jika koperasi merealisasikan keuntungan kecil maka beban bagi hasilnya rendah, demikian sebaliknya.</li>
                  <li>Berbeda dengan biaya bunga, berapapun realisasi keuntungan maka beban biaya bunga tidak berubah termasuk jika unit usaha merealisasikan kerugian tetap harus membayar beban bunga yang disepakati.</li>
                </ul>
              </div>
              <div class="content-sub-3">
                <h4 class="uk-heading-bullet">Sesuai Syariah</h4>
                <ul class="uk-list uk-list-bullet">
                  <li>Skema transaksi investasi berdasarkan kemitraan secara syariah.</li>
                  <li>Sharing risk &amp; return.</li>
                  <li>Trasnparan dan akuntable.</li>
                  <li>Gotong royong &amp; berkeadilan.</li>
                </ul>
              </div>
            </div>
          </div>

          <hr class="uk-divider-icon">

          <div class="content-3">
            <h3 class="uk-heading-line-none">
              <span>2. Ajukan penggalangan modal dengan beberapa langkah mudah</span>
            </h3>
            <div class="uk-child-width-1-4@m uk-grid-small uk-grid-match" uk-grid>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">1</div>
                  <h4>Daftar &amp; Login </h4>
                  <p>Daftarkan diri anda sebagai Koperasi/UKM.</p>
                  <a href="{{ route('register') }}" class="uk-button uk-button-primary spf-link">
                    <span uk-icon="sign-in"></span> Daftar Disini
                  </a>
                </div>
              </div>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">2</div>
                  <h4>Penuhi Persyaratan</h4>
                  <p>Legalitas Usaha (Akte Pendirian, Pengesahan menkumham, SIUP, Copy KTP Pengurus) Menyerahkan rencana bisnis (proposal kelayakan usaha), Proyeksi Imbal hasil pertahun > 15% Tentukan besarnya pendanaan.</p>
                </div>
              </div>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">3</div>
                  <h4>Uji Tuntas &amp; Verifikasi</h4>
                  <p>Pengumpulan informasi Tentang profil kegiatan dan kelayakan usaha, Verifikasi dokumen legal, Proyeksi cashflow dan Kelayakan finansial.</p>
                </div>
              </div>
              <div>
                <div class="uk-card uk-card-hover uk-card-default uk-card-body uk-card-responsive">
                  <div class="uk-card-badge uk-label">4</div>
                  <h4>Dapatkan Pendanaan</h4>
                  <p>Penawaran umum peluang Investasi Kepada investor Melalui plazadana.com, Pencairan dana, Pelaporan realisasi penggunaan Modal &amp; laporan publikasi Kinerja keuangan berkala Melalui plazadana.com</p>
                </div>
              </div>
            </div>
          </div>

          <div class="content-4">
            <div class="uk-card uk-card-body uk-margin uk-card-responsive">
              <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto@s uk-first-column">
                  <a class="uk-remove-underline" href="https://play.google.com/store/apps/details?id=com.app.plazadanamobilebanking" target="_blank" style="position:relative;margin-right: 1em;">
                    <img class="uk-border-circle" width="70" height="70" src="{{asset('images/wallet.png')}}">
                    <img width="20" height="20" src="{{asset('images/cursor.png')}}" style="position:absolute;right:-15px;bottom:-20px;">
                  </a>
                </div>
                <div class="uk-width-expand">
                  <p>Untuk menikmati berbagai kemudahan transaksi melalui mobilephone <b>(Android)</b> anda, pastikan anda mengunduh aplikasi Paykita plazadana melalui mobile phone anda di playstore atau google play secara gratis.</p>
                </div>
              </div>
            </div>
          </div>
        </li><!-- end /.tab-item.penggalangan-dana -->
        <li class="tab-item bagi-hasil">
          <div class="content-1">
            <p>Akad (kontrak) yang akan digunakan dalam penggalangan dana investasi adalah <b>Semua akad Syariah sesuai dengan ketentuan DSN </b>. <a href="http://dsnmui.or.id/kategori/fatwa">Pelajari lebih lanjut...</a></p>

            <div class="uk-alert">
              <strong>Biaya:</strong> Beban biaya penggalangan dana jika mencapai target maksimal adalah 3 % dari realisasi pencapaian target penggalangan dana yang akan didebet pada saat pencairan dana ke rekening virtual Koperasi/UKM.
            </div>
          </div>
        </li><!-- end /.tab-item.bagi-hasil -->
        <li class="tab-item e-transaksi">
          <div class="uk-grid uk-child-width-1-2@s">
            <div class="left-content">
              <!-- <iframe src="//www.youtube.com/embed/YE7VzlLtp-4?autoplay=0&amp;showinfo=0&amp;rel=0&amp;modestbranding=1&amp;playsinline=1" width="560" height="315" frameborder="0" allowfullscreen uk-responsive uk-video="automute: true"></iframe> -->
              <iframe width="560" height="315" src="https://www.youtube.com/embed/gDOiEWiLMQQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="right-content">
              <p>Plazadana.com menyediakan fasilitas Paykita (dompet elektronik) kepada investor dan koperasi/UKM sehingga investor/anggota koperasi dan Koperasi/UKM dapat menikmati berbagai kemudahan dalam melakukan transaksi terutama terkait dengan setoran tunai keanggotaan, penerimaan pembayaran distribusi imbal hasil dari unit usaha koperasi, setoran tambahan (top up) berbagai produk simpanan Koperasi, serta tarik tunai melalui ATM seluruh bank yang telah memiliki kerjasama dengan anchor bank plazadana.com. Semua jenis transaksi yang dilakukan melalui Paykita dapat langsung dicek secara real time melalui mobile phone agar dapat dipastikan semua jenis transaksi berhasil (settled) dan dapat dilakukan rekonsiliasi dengan posisi saldo terkini yang ada di bank. Paykita juga memungkinkan untuk cek 10 transaksi terakhir.</p>
            </div>
          </div>
        </li><!-- end /.tab-item.e-transaksi -->
        <li class="tab-item faq">
          <ul uk-accordion>
            <li class="uk-open">
              <a class="uk-accordion-title">1. Apa yang dimaksud dengan retail equity based crowdfunding syariah?</a>
              <div class="uk-accordion-content">
                <p>Retail equity based crowdfunding adalah penawaran online penyertaan investasi oleh unit  usaha koperasi secara retail kepada individu, sekelompok orang, jamaah, komunitas melalui market place <a href="www.plazadana.com"> www.plazadana.com</a>. Jenis Instrument penyertaan investasi yang dapat ditawarkan adalah equity based financing (seperti simpanan pokok, simpanan wajib, dan simpanan investasi), dan debt asset based financing (seperti Surat Utang Koperasi Syariah disingkat “SUKS”, SUKUK/Obligasi Syariah, Bilateral debt financing dan sejenisnya). Koperasi juga dapat menawarkan instrument penggalangan dana berupa waqaf produktif, CSR, Hibah, dan sejenisnya. Berikut di bawah ini adalah rangkuman instrument, jenis dan akad-akadnya:</p>
                <p>
                  <table class="uk-table uk-table-striped">
                    <thead>
                      <tr>
                        <th><strong>No</strong></th>
                        <th><strong>Instrument</strong></th>
                        <th><strong>Jenis</strong></th>
                        <th><strong>Akad</strong></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Simpanan Pokok</td>
                        <td>Ekuitas</td>
                        <td>Musyarakah</td>
                      </tr>
                      <tr>
                          <td width="37">
                            <p>2</p>
                          </td>
                          <td width="165">
                            <p>Simpanan wajib</p>
                          </td>
                          <td width="104">
                            <p>Ekuitas</p>
                          </td>
                          <td width="188">
                            <p>Musyarakah</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>3</p>
                        </td>
                        <td width="165">
                          <p>Simpanan Investasi</p>
                        </td>
                        <td width="104">
                          <p>Ekuitas</p>
                        </td>
                        <td width="188">
                          <p>Musyarakah</p>
                        </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>4</p>
                          </td>
                          <td width="165">
                            <p>Bilateral financing</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>Semua akad syariah yang relevan</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>5</p>
                          </td>
                          <td width="165">
                            <p>Surat utang koperasi syariah (SUKS)</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>Semua akad syariah yang relevan</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>6</p>
                          </td>
                          <td width="165">
                            <p>Sukuk</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>idem</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>7</p>
                          </td>
                          <td width="165">
                            <p>Reksadana</p>
                          </td>
                          <td width="104">
                            <p>Pembiayaan</p>
                          </td>
                          <td width="188">
                            <p>idem</p>
                          </td>
                        </tr>
                        <tr>
                          <td width="37">
                            <p>8</p>
                          </td>
                          <td width="165">
                            <p>Ziswaf</p>
                          </td>
                          <td width="104">
                            <p>Donasi</p>
                          </td>
                          <td width="188">
                            <p>Ziswaf</p>
                          </td>
                        </tr>
                    </tbody>
                  </table>
                </p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">2. Apa persyaratan untuk menggalang dana melalui platform www.plazadana.com?</a>
              <div class="uk-accordion-content">
                <center>PERSYARATAN LISTED &amp; FUND RAISING VIA PLAZADANA</center>
                <table class="uk-table uk-table-small uk-table-striped">
                  <thead class="uk-text-bold" >
                    <tr>
                      <th class="uk-table-middle" rowspan="2">No</th>
                      <th class="uk-table-middle" rowspan="2">Persyaratan Koperasi</th>
                      <th class="uk-text-center" colspan="3"> Simpanan(equity financing)</th>
                      <th class="uk-text-center" colspan="2">Debt financing</th>
                      <th class="uk-table-middle uk-text-center" rowspan="2">Bilateral financing</th>
                    </tr>
                    <tr>
                      <th class="uk-text-center">Pokok</th>
                      <th class="uk-text-center">Wajib</th>
                      <th class="uk-text-center">investasi</th>
                      <th class="uk-text-center">SUKS</th>
                      <th class="uk-text-center">SUKUK</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Surat permohonan ke Plazadana</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Proposal kelayakan usaha</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Copy Akte pendirian</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Copy SK Kemenkop</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Copy Nomor Induk Berusaha</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Copy Izin usaha</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>7</td>
                      <td>Copy Izin operasional</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>8</td>
                      <td>Copy NPWP Koperasi</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>9</td>
                      <td>Copy AD/ART</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>10</td>
                      <td>Copy KTP Pengurus inti</td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>                    
                    </tr>
                    <tr>
                      <td>11</td>
                      <td>Underlying asset/sebagai jaminan</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td></td>                    
                    </tr>
                    <tr>
                      <td>12</td>
                      <td>Rapat anggota > 2x</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td class="uk-text-center"><span uk-icon="check"></span></td>
                      <td></td>                    
                    </tr>
                    <td>13</td>
                    <td>Laporan Keuangan audit</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="uk-text-center"><span uk-icon="check"></span></td>
                    <td class="uk-text-center"><span uk-icon="check"></span></td>
                    <td></td>                    
                  </tr>
                  </tbody>
                </table>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">3. Siapa yang akan memberikan penyertaan investasi kepada penggalang dana/modal?</a>
              <div class="uk-accordion-content">
                <p>Semua calon anggota koperasi yang berminat baik yang berasal dari komunitas yang mendirikan koperasi atau  di luar komunitas pendiri koperasi</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">4. Bagaimana cara menggalang dana melalui plazadana.com?</a>
              <div class="uk-accordion-content">
                <ol class="">
                  <li>Penuhi persyaratan legalitas usaha</li>
                  <li>Daftarkan Koperasi anda di platform <a href="www.plazadana.com">www.plazadana.com</a></li>
                  <li>Uji tuntas dan verifikasi (Pengumpulan informasi tentang profil kegiatan dan kelayakan usaha, verifikasi dokumen legal, proyeksi cashflow dan kelayakan finansial.</li>
                  <li>Dapatkan modal melalui penawaran umum peluang investasi kepada calon anggota Koperasi/investor melalui plazadana.com</li>
                  <li>Pencairan dana jika target penggalangan dana telah terpenuhi.</li>
                  <li>Pelaporan realisasi penggunaan modal &amp; laporan publikasi kinerja keuangan berkala melalui plazadana.com</li>
                </ol>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">5. Berapa imbal hasil yang harus saya berikan kepada investor?</a>
              <div class="uk-accordion-content">
                <p>Kontrak/akad yang digunakan dalam skema penyertaan investasi ke dalam unit usaha koperasi berjenis equity financing adalah musyarakah (Akad musyarakah adalah akad kerjasama antara dua pihak atau lebih untuk suatu usaha tertentu, dimana masing – masing pihak memberikan kontribusi dana dengan ketentuan dibagi berdasarkan kesepakatan sedangkan keuntungan/kerugian berdasarkan kontribusi dana atau sesuai dengan kesepakatan).</p>
                <p>Jadi besaran imbal hasil yang harus diberikan kepada investor tergatung realisasi keuntungan yang diperoleh Koperasi setelah dipotong biaya-biaya. Jika realisasi imbal hasil/keuntungan tinggi maka besaran imbal hasil yang akan dibagikan kepada anggota koperasi/investor juga tinggi, demikian sebaliknya. Sehingga aspek keadilan antara para pihak terpenuhi.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">6. Kapan saya harus membayarkan bagi hasil kepada investor?</a>
              <div class="uk-accordion-content">
                <p>Pembayaran bagi hasil dilakukan secara periodik, bisa bulanan, triwulanan, semesteran, atau tahunan sesuai kesepakatan para pihak yang diatur dalam Anggaran Rumah Tangga Koperasi. Namun demikian dianjurkan pembayaran bagi hasil dilakukan secara bulanan.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">7. Apa kewajiban penggalang dana/modal terhadap investor?</a>
              <div class="uk-accordion-content">
                <p>Penggalang dana/Koperasi wajib melakukan publikasi laporan kinerja keuangan tiap akhir bulan, akhir triwulan, akhir semesteran, dan akhir tahunan melalui dashboard yang telah disediakan di platform plazadana.com. Kemudian setiap akhir tahun Koperasi wajib melakukan publikasi laporan keuangan yang telah diaudit oleh independen auditor (bagi emiten koperasi yang menerbitkan instrument penggalanan dana seperti SUKS dan SUKUK)</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">8. Haruskah penggalang dana menyediakan jaminan?</a>
              <div class="uk-accordion-content">
                <p>Untuk penggalangan dana melalui instrument equity based financing (simpanan pokok, wajib, investasi) tidak memerlukan underlying asset sebagai jaminan. Sedangkan bagi Koperasi yang menggalang dana melalui instrument SUKS dan SUKUK wajib menyediakan underlying asset sebagai jaminan senilai jumlah penggalangan dana.</p>
              </div>
            </li>
            <li>
              <a class="uk-accordion-title">9. Berapa biaya penggalangan dana yang harus dikeluarkan jika penggalangan dana melalui platform ini tercapai?</a>
              <div class="uk-accordion-content">
                <p>Penggalang dana yang berhasil mencapai target akan dikenakan biaya maksimal sebesar 3% dari total penggalangan dana dan akan dibayarkan pada saat pencairan dana yang telah terkumpul.</p>
              </div>
            </li>
          </ul>
        </li><!-- end /.tab-item.faq -->
        <li class="tab-item laporan-publikasi">
          <p>Untuk dapat memenuhi standar akuntabilitas yang dapat dipertanggung jawabkan kepada seluruh anggota dan para pemangku kepentingan, koperasi yang telah berhasil menggalang dana melalui market place plazadana.com - seiring dengan bertambahnya jumlah anggota beserta iuran-iuran yang dipersyaratkan sesuai dengan target - maka koperasi wajib melakukan publikasi laporan penggunaan dana/modal dan kinerja keuangan secara periodic (bulanan/triwulanan/semesteran/tahunan) dan melakukan distribusi imbal hasil secara berkala sesuai dengan kesepakatan melalui market place plazadana.com yang dapat diakses melalui website maupun mobile apps.</p>
          <p>Untuk memenuhi hal tersebut, Plazadana.com menyediakan standard system aplikasi akuntansi yang akan diinstal di setiap unit usaha koperasi yang melapak di market place agar secara real time dapat menyajikan laporan keuangan sesuai dengan permintaan, sehingga hal ini memungkinkan Koperasi untuk dapat memenuhi akuntabilitasnya dengan melakukan penyajian laporan keuangan sesuai dengan permintaan terutama setiap periode tutup buku baik secara harian, bulanan, triwulanan, semesteran dan tahunan melalui market place plazadana.com. <a href="{{ 'url_read_more' }}">Penjelasan lebih lanjut...</a></p>
        </li><!-- end /.tab-item.laporan-publikasi -->
      </ul>
    </div>
  </div>
@endsection
