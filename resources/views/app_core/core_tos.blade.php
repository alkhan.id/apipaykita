@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({{asset('images/road.jpg')}});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Ketentuan Penggunaan</h1>
      <p class="uk-text-white short-description-teaser-detail">Aturan yang berlaku mengacu pada Ketentuan Penggunaan dan Kebijakan Privasi.</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <ul uk-switcher="connect: #ul-content-info; animation: uk-animation-fade"
      class="uk-tab no-padding uk-child-width-expand@s uk-background-default uk-margin-remove" uk-tab>
      <li class="active uk-padding-remove-left"><a>Ketentuan Penggunaan</a></li>
      <li><a>Kebijakan Privasi</a></li>
    </ul>
    <div class="uk-card-body">
      <ul id="ul-content-info" class="uk-switcher uk-margin">
        <li class="tab-item ketentuan-penggunaan">
          <div class="content-1">
            <p>Keterangan dalam website ini hanya untuk tujuan informasi dan bukan merupakan (1) penawaran untuk membeli atau menjual atau meminta untuk membeli atau menjual instrumen keuangan apa pun yang disebutkan dalam website ini dan (2) saran profesional apa pun. Informasi, bahan dan isi yang disediakan dalam website ini, termasuk namun tidak terbatas pada produk, data, teks, gambar, grafik, pendapat, analisis, perkiraan, proyeksi dan / atau harapan (selanjutnya secara kolektif disebut sebagai "informasi") disediakan oleh plazadana.com untuk tujuan informasi saja. Informasi ini dapat berubah tanpa pemberitahuan.</p>
            <p>Kemudian bersama ini kami sampaikan bahwa dalam mempersiapkan dan mendapatkan informasi dalam dokumen ini dengan menggunakan pertimbangan-pertimbangan yang masuk akal. Ini adalah merupakan upaya agar dapat dipastikan keandalan dan keakuratan informasi. Plazadana dan afiliasinya, direktur, karyawan, dan mitranya tidak memberikan jaminan terkait dengan konten dan kelengkapannya dan tidak menerima tanggung jawab atas kerugian yang mungkin timbul dari penggunaan informasi ini.</p>
            <p>Pandangan atau pendapat yang dikemukakan dalam dokumen ini adalah dari Plazadana, semua angka tidak diaudit. Penerima secara khusus, direkomendasikan untuk memeriksa bahwa informasi yang diberikan sesuai dengan keadaannya sendiri berkenaan dengan segala peraturan hukum, peraturan, pajak atau konsekuensi lainnya, jika perlu dengan bantuan penasihat profesional.</p>
            <p>PT Bina Amwaluna Suarga adalah pengelola platform plazadana yang merupakan penyelenggara layanan retail equity based crowdfunding platform Syariah yang berbadan hukum dan berdasarkan Hukum Republik Indonesia. Perusahaan ini dikategorikan sebagai penyelenggara jasa keuangan dimana industri ini diatur dan diawasi oleh Otoritas Jasa Keuangan (OJK) sesuai Peraturan OJK Nomor 77/POJK.01/2016 tentang Layanan Pinjam Meminjam Uang Berbasis Teknologi Informasi, pada saat ini PT Bina Amwaluna Suarga sedang mempersiapkan untuk pendaftaran ke Otoritas Jasa Keuangan (OJK) dan approval diharapkan dapat diterima paling lama akhir tahun 2018 ini.</p>
            <p>PT Bina Amwaluna Suarga mengkhususkan diri dalam mempertemukan peluang investasi antara anggota terdaftar dengan usaha kecil menengah terutama Koperasi. Berkaitan dengan peluang investasi tersebut, kewajiban untuk melakukan analisa kelayakan investasi/pembiayaan atau uji tuntas sepenuhnya berada di pihak anggota, karena layanan kami tidak mencakup pemberian saran terkait hal tersebut. Seluruh transaksi terjadi secara langsung antara anggota dan Koperasi sebagai pemilik proyek/unit usaha, dan agen yang telah mendapat persetujuan (yang disebut sebagai "anggota" kami).</p>
          </div>
        </li><!-- end /.tab-item.ketentuan-penggunaan -->
        <li class="tab-item kebijakan-privasi">
          <div class="content-2">
            <p>Plazadana menganggap sangat penting privasi Anda. Kami meminta Anda membaca kebijakan ini dengan hati-hati karena terkait informasi penting tentang apa yang diharapkan ketika Plazadana mengumpulkan informasi pribadi tentang Anda dan bagaimana Plazadana akan menggunakan data pribadi Anda. Kebijakan ini berlaku untuk informasi yang kami kumpulkan melalui www.plazadana.com terkait dengan:</p>
            <ol>
              <li>Pengunjung www.plazadana.com</li>
              <li>Orang yang mendaftarkan email dengan kami</li>
            </ol>

            <h4 class="uk-heading-bullet">1. Informasi dari semua pengunjung web kami</h4>
            <p>Kami akan memperoleh data pribadi tentang Anda ketika Anda mengunjungi kami, dan memantaunya melalui penggunaan cookie serta perangkat pelacakan serupa pada saat anda mengunjungi situs web kami. Contoh: kami dapat memantau berapa kali Anda mengunjungi Situs kami atau ke halaman mana Anda pergi. Informasi ini membantu kami untuk membuat profil pengguna kami. Beberapa data ini akan diolah untuk keperluan statistik, yang berarti bahwa kami tidak dapat mengidentifikasi Anda secara individual. Silakan lihat lebih lanjut bagian tentang Penggunaan Cookies Kami di bawah ini.</p>

            <h4 class="uk-heading-bullet">2. Informasi terkumpul dari pengguna yang mendaftarkan emailnya kepada kami</h4>
            <p>Kami mengumpulkan, menggunakan, dan / atau memproses data pribadi Anda untuk tujuan berikut:</p>
            <ol>
              <li>Mengirim materi pemasaran, produk dan layanan yang mungkin menarik bagi Anda;</li>
              <li>Melakukan uji tuntas dan kegiatan penyaringan lainnya, atau untuk mencegah atau menyelidiki penipuan atau kegiatan yang melanggar hukum;</li>
              <li>Menyimpan, hosting, mencadangkan data pribadi.</li>
            </ol>
            <p>secara bersama-sama disebut "Tujuan".</p>
            <div class="uk-alert uk-alert-warning">
              <i uk-icon="info"></i> Jika Anda tidak ingin terganggu dengan email yang berisi tentang materi pemasaran, silakan kirim pesan email ke support@plazadana.com dengan subjek UNSUBSCRIBE.
            </div>
            <p>Menarik persetujuan untuk penggunaan data pribadi.</p>
            <div class="uk-alert uk-alert-warning">
              <i uk-icon="info"></i> Jika Anda ingin menarik persetujuan Anda untuk menggunakan data pribadi Anda ke Plazadana dan menghapus / menonaktifkan akun Anda, silakan kirim pesan email ke support@plazadana.com dengan subjek DEACTIVATE.
            </div>

            <h4 class="uk-heading-bullet">3. Penggunaan cookie kami</h4>
            <p>Cookie adalah file teks yang ditempatkan di komputer Anda untuk mengumpulkan informasi log Internet standar dan informasi perilaku pengunjung. Informasi ini digunakan untuk melacak penggunaan pengunjung Situs dan untuk mengkompilasi laporan statistik pada aktivitas Situs. Untuk informasi lebih lanjut tentang cookie, kunjungi www.allaboutcookies.org. Anda dapat mengatur browser Anda untuk tidak menerima cookie dan situs web di atas memberi tahu Anda cara menghapus cookie dari browser Anda. Namun, dalam beberapa kasus, beberapa fitur Situs kami mungkin tidak berfungsi jika Anda menghapus cookie dari browser Anda.</p>

            <h4 class="uk-heading-bullet">4. Bagaimana kami melindungi informasi Anda</h4>
            <p>Kami berkomitmen untuk menjaga keamanan informasi pribadi. Kami telah menempatkan prosedur keamanan yang sesuai dan tindakan teknis untuk menjaga informasi pribadi Anda seperti mengamankan situs web kami dengan SSL.</p>

            <h4 class="uk-heading-bullet">5. Akses dan update informasi Anda</h4>
            <p>Jika Anda ingin menyalin beberapa atau semua informasi pribadi Anda, silakan kirim email ke support@plazadana.com. Kami dapat mengenakan biaya murah untuk layanan ini.</p>
            <p>Kami ingin memastikan bahwa informasi pribadi Anda akurat dan terbaru. Jika ada informasi yang Anda berikan untuk perubahan Plazadana, misalnya jika Anda mengubah alamat email, nama atau detail pembayaran, atau jika Anda ingin membatalkan pendaftaran, beri tahu kami rincian yang benar dengan mengirim email ke support@plazadana.com. Anda dapat bertanya kepada kami, atau kami mungkin meminta Anda, untuk mengoreksi informasi yang Anda atau kami anggap tidak akurat, dan Anda juga dapat meminta kami untuk menghapus informasi yang tidak akurat.</p>

            <h4 class="uk-heading-bullet">6. Tidak mengungkapkan informasi</h4>
            <p>Kami tidak menjual, memperdagangkan, atau mentransfer ke pihak ketiga informasi pribadi Anda. Kami juga dapat melepaskan informasi Anda ketika kami percaya bahwa hal ini sesuai dengan kebijakan dan untuk mematuhi hukum, menegakkan kebijakan situs kami, atau melindungi milik kami dan hak, properti, atau keamanan orang lain. Namun, informasi pengunjung yang tidak dapat diidentifikasi secara pribadi dapat diberikan kepada pihak lain untuk pemasaran, iklan, atau penggunaan lainnya.</p>

            <h4 class="uk-heading-bullet">7. Tautan ke situs web lain</h4>
            <p>Situs kami mungkin berisi tautan ke situs web lain. Kebijakan privasi ini hanya berlaku untuk situs web ini jadi ketika Anda menautkan ke situs web lain, Anda harus membaca kebijakan privasi mereka sendiri.</p>

            <h4 class="uk-heading-bullet">8. Perubahan kepemilikan</h4>
            <p>Jika bisnis Plazadana dijual atau terintegrasi dengan bisnis lain, detail Anda dapat diungkapkan kepada penasihat kami, setiap calon pembeli dan penasihat mereka dan akan diteruskan pihak-pihak yang berkepentingan.</p>

            <h4 class="uk-heading-bullet">9. Perubahan kebijakan privasi</h4>
            <p>Kami akan mengelola kebijakan privasi kami agar selalu updated dengan cara melakukan peninjauan ulang secara. Jika kami mengubah dan memperbaharui kebijakan privasi kami, kami akan memposting perubahan pada halaman ini, dan menempatkan pemberitahuan pada halaman lain dari Situs, sehingga Anda mungkin menyadari informasi yang kami kumpulkan dan bagaimana kami menggunakannya setiap saat.</p>

            <h4 class="uk-heading-bullet">10. Bagaimana cara menghubungi kami</h4>
            <p>Kami menyambut pandangan Anda tentang Situs kami dan kebijakan privasi kami. Jika Anda ingin menghubungi kami dengan pertanyaan atau komentar apa pun, silakan kirim email ke support@plazadana.com</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
@endsection
