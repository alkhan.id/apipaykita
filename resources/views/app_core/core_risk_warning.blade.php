@extends('base')
<style>.uk-base-section{padding-top: 0}.uk-tab::before{left: 0}</style>

@section('content')
  <div class="uk-background-cover uk-background-fixed uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
    style="background-image: url({% static 'images/road.jpg' %});">
    <div class="uk-position-cover uk-overlay uk-overlay-default" style="background-image: linear-gradient(to bottom right, #5c9bc1, #1b1b1b);opacity: .6"></div>
    <div class="uk-overlay uk-position-center uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">
      <h1 class="uk-text-white title-teaser-detail">Peringatan Risiko</h1>
      <p class="uk-text-white short-description-teaser-detail">Setiap bisnis yang dikelola, pasti memiliki resiko.</p>
    </div>
  </div>
  <div class="uk-card uk-card-default">
    <div class="uk-card-body">
      <h4 class="uk-heading-bullet">Pendahuluan</h4>
      <p>Plazadana hadir untuk melayani Anda agar dapat memberikan kemudahan berinvestasi langsung di Koperasi dengan skim syariah bersama dengan investor lain. Anda dapat menentukan pilihan berbagai macam instrument investasi yang ditawarkan Koperasi melalui market place Plazadana sesuai dengan “selera risiko” Anda. Jenis instrument investasi yang ditawarkan sesuai dengan Peraturan Menteri Koperasi No.09 tahun 2018 tentang Penyelenggaraan dan Pembinaan Koperasi. </p>
      <p>Berikut adalah berbagai macam dan jenis instrument investasi yang dapat ditawarkan oleh Koperasi melalui Plazadana:</p>
<center>
  <div class="uk-width-xxlarge uk-margin">
<table class="uk-table uk-table-small uk-table-middle uk-table-divider">
<tbody>
  <thead>
    <th><strong>No</strong></th>
    <th><strong>Instrument</strong></th>
    <th><strong>Jenis</strong></th>
    <th><strong>Akad</strong></th>
  </thead>
  <tbody>

</tr>
<tr>
  <td width="37">
    <p>1</p>
</td>
<td width="165">
  <p>Simpanan pokok</p>
</td>
<td width="104">
  <p>Ekuitas</p>
</td>
<td width="188">
  <p>Musyarakah</p>
</td>
</tr>
<tr>
  <td width="37">
    <p>2</p>
  </td>
  <td width="165">
    <p>Simpanan wajib</p>
  </td>
  <td width="104">
    <p>Ekuitas</p>
  </td>
  <td width="188">
    <p>Musyarakah</p>
  </td>
</tr>
<tr>
  <td width="37">
    <p>3</p>
</td>
<td width="165">
  <p>Simpanan Investasi</p>
</td>
<td width="104">
  <p>Ekuitas</p>
</td>
<td width="188">
  <p>Musyarakah</p>
</td>
</tr>
<tr>
  <td width="37">
    <p>4</p>
  </td>
  <td width="165">
    <p>Bilateral financing</p>
  </td>
  <td width="104">
    <p>Pembiayaan</p>
  </td>
  <td width="188">
    <p>Semua akad syariah yang relevan</p>
  </td>
</tr>
<tr>
  <td width="37">
    <p>5</p>
  </td>
  <td width="165">
    <p>Surat utang koperasi syariah (SUKS)</p>
  </td>
  <td width="104">
    <p>Pembiayaan</p>
  </td>
  <td width="188">
    <p>Semua akad syariah yang relevan</p>
  </td>
</tr>
<tr>
  <td width="37">
    <p>6</p>
  </td>
  <td width="165">
    <p>Sukuk</p>
  </td>
  <td width="104">
    <p>Pembiayaan</p>
  </td>
  <td width="188">
    <p>idem</p>
  </td>
</tr>
<tr>
  <td width="37">
    <p>7</p>
  </td>
  <td width="165">
    <p>Reksadana</p>
  </td>
  <td width="104">
    <p>Pembiayaan</p>
  </td>
  <td width="188">
    <p>idem</p>
  </td>
</tr>
<tr>
  <td width="37">
    <p>8</p>
  </td>
  <td width="165">
    <p>Ziswaf</p>
  </td>
  <td width="104">
    <p>Donasi</p>
  </td>
  <td width="188">
    <p>Ziswaf</p>
  </td>
</tr>
</tbody>
</table>
</div>
</center>
<p>Dari Tabel diatas dapat dijelaskan bahwa Koperasi dapat menawarkan kerjasama kemitraan peluang investasi dengan investor (anggota/non anggota) melalui 2 (dua) jenis instrument, yaitu berupa penyertaan ekuitas (simpanan pokok/wajib), dan pembiayaan dengan berbagai varian akad (debt asset based financing). Semua standar akad syariah (DSN) yang relevan dapat digunakan untuk pengikatan masing-masing instrument investasi antara pihak koperasi dengan investor baik anggota maupun non anggota. Sedangkan Plazadana adalah bukan merupakan pihak dalam akad-akad syariah pada instrument investasi yang ditawarkan oleh Koperasi melalui Plazadana. Namun Plazadana setuju untuk menfasilitasi dan mengaturnya berdasarkan persyaratan dan ketentuan yang disepakati.</p>
<p>Penting untuk dipertimbangkan bahwa penyertaan investasi Anda di unit usaha koperasi berpotensi menghadapi berbagai risiko termasuk diantaranya adalah risiko fluktuasi pembayaran tingkat imbal hasil usaha, risiko tidak dapat dicairkannya dana penyertaan investasi (tidak likuid karena belum adanya pasar sekunder), risiko default dalam hal ini ketika Koperasi, karena kondisi bisnis yang berubah termasuk di dalamnya akibat persaingan usaha yang ketat dan juga krisis keuangan, tidak dapat memenuhi kewajiban keuangan mereka kepada investor. Ringkasnya ketika Anda berinvestasi, modal Anda berpotensi menghadapi risiko kerugian sebagian atau bahkan semuanya. Untuk itu sebelum melakukan penyertaan kemitraan dengan koperasi pastikan Anda telah membaca dengan cermat AD/ART, akad-akadnya serta telah melakukan uji tuntas atas peluang investasi yang ditawarkan oleh Koperasi.</p>
<p>Namun demikian, sebelum risiko terjadi Anda dapat memonitor secara periodic kinerja keuangan Koperasi dimana Anda berinvestasi melalui dashboard yang tersedia khusus untuk Anda di platform Plazadana.</p>
<h4 class="uk-heading-bullet">Uji kelayakan</h4>
<p>Sebelum melakukan penawaran kepada calon anggota koperasi atau investor, Plazadana melakukan standar penyaringan yang tinggi dengan menerapkan Risk Acceptance Criteria (RAC) pada setiap Koperasi yang akan memperluas penyertaan investasinya kepada calon anggota baru atau non anggota melalui Plazadana. Untuk penyaringan awal, Koperasi yang melakukan penggalangan dana harus memenuhi persyaratan minimum sebagai berikut: 1) telah berbadan hukum dan memiliki izin usaha sesuai ketentuan yang relevan, 2) memenuhi standar kelayakan usaha dan 3) memiliki arus kas positif di 12 bulan terakhir (bagi Koperasi yang telah beroperasi lebih dari satu tahun). Jika Anda menginginkan untuk melakukan verifikasi kelayakan usaha yang ditawarkan oleh Koperasi dimana Anda berminat, Anda dapat menghubungi PIC pengurus Koperasi baik secara online atau offline seperti site visit secara langsung ke tempat usaha Koperasi.</p>
<h4 class="uk-heading-bullet">Diversifikasi</h4>
<p>Diversifikasi adalah salah satu cara yang efektif untuk menyebar potensi risiko kegagalan investasi. Kami menyarankan agar Anda berinvestasi dari jumlah yang kecil dalam penggalangan pendanaan yang berbeda, daripada menempatkan semua investasi mereka menjadi satu untuk mengurangi kemungkinan risiko kerugian. Hal ini dapat melindungi dana investor dari efek yang berasal dari salah satu Koperasi yang mengalami kerugian. Kami juga menyarankan agar anggota Plazadana menempatkan dana penyertaannya di kelas aset lainnya, terutama di Koperasi lain yang menawarkan profil risiko yang berbeda. Dan terahir kami menyarankan kepada Anda agar mendiskusikan kapasitas Anda untuk berinvestasi melalui Plazadana dengan penasihat keuangan Anda.</p>
<h4 class="uk-heading-bullet">Pembayaran bagi hasil</h4>
<p>Semua Koperasi yang terdaftar di Plazadana diharuskan untuk melakukan pembayaran bagi hasil kepada anggotanya secara periodik sesuai dengan kesepakatan. Namun demikian realisasi pembayaran imbal hasil kepada anggota koperasi sangat bergantung kepada naik turunya keuntungan/kerugian unit usaha Koperasi dimana investor menempatkan dana penyertaannya. Jika tingkat keuntungan Koperasi tinggi maka pembayaran bagi hasil kepada anggota juga tinggi, demikian sebaliknya. Untuk itu dianjurkan agar semua anggota Koperasi dapat memanfaatkan jasa/berbelanja kebutuhannya yang tersedia di Koperasi dimana anggota koperasi (investor) menempatkan dana penyertaannya, karena pada akhirnya yang akan menikmati imbal hasil usaha koperasi adalah anggotanya.</p>
<h4 class="uk-heading-bullet">Default</h4>
<p>Unit usaha Koperasi dapat mengalami kerugian sebagaimana unit usaha lainnya. Kerugian dapat disebabkan oleh salah urus termasuk fraud (penipuan, penggelapan, kecurangan, dll), persaingan usaha yang ketat atau bahkan diakibatkan oleh siklus/krisis ekonomi dan keuangan.</p>
<p>Jika risiko (default misalnya) terjadi, masing-masing instrument memiliki mekanisme penyelesaian yang berbeda-beda. Jenis instrument pertama (ekuitas) penyelesaiannya akan mengikuti mekanisme yang diatur dan ditetapkan di dalam Anggaran Dasar dan Anggaran Rumah Tangga Koperasi dimana Anda berinvestasi.</p>
<p>Sedangkan untuk jenis instrument kedua penyelesaaiannya bergantung pada jenis instrument pembiayaan yang ditawarkan. Jika jenis instrument pembiayaan dilakukan secara bilateral antara Investor (anggota/non anggota Koperasi) dengan Koperasi maka penyelesaiannya akan menggunakan standar penyelesaian pembiayaan bermasalah</p>
<p>&nbsp;Sedangkan untuk jenis instrument kedua (debt asset based financing) penyelesaiannya akan mengikuti prosedur penyelesaian pembiayaan bermasalah jika jenis instrumentnnya pembiayaan, dan melalui mekanisme pasar modal jika jenis instrumentnya berupa Sukuk atau Reksadana.</p>
<h4 class="uk-heading-bullet">Kegagalan platform</h4>
<p>Dalam skenario di mana www.plazadana.com berhenti beroperasi sebagai platform crowdfunding, Koperasi masih bertanggung jawab untuk melayani kewajiban keuangannya kepada investor. Semua kesepakatan antara para pihak mengikat secara hukum dan akan terus diberlakukan jika Plazadana keluar dari bisnis.</p>
<p>Plazadana tidak menyimpan uang investor kecuali bahwa kami adalah agen mentransfer uang tunai dari investor ke Koperasi, dan sebaliknya. Oleh karena itu Plazadana tidak mengklaim uang yang Anda investasikan.</p>
<p>&nbsp;</p>
    </div>
  </div>
@endsection
