@extends('auth.base')
@section('content')
  <div uk-grid>
    <div class="uk-width-1-1">
      <div class="uk-margin uk-text-center uk-flex uk-flex-center uk-flex-middle">
        <div class="uk-margin-auto uk-width-1-4@s uk-card uk-card-body">
          <form class="uk-form password_reset" method="post" action="{{ route('password.email') }}">@csrf
            <div class="uk-flex uk-flex-center uk-flex-middle">
              <a href="/" class="spf-link">
                <canvas width="100" uk-svg="" src="{{asset('images/logos/svg/logo-150x150.svg')}}" style="color: #1e87f0" hidden="true"></canvas>
              </a>
            </div>


            <div class="uk-alert uk-text-small">
              Silahkan masukkan alamat email, dan kami akan mengirimkan email untuk mereset password."
            </div>
            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input" name="email" type="email" size="30" placeholder="Email" required>
            </div>
            <div class="uk-margin">
              <button type="submit" class="uk-width-1-1 uk-button uk-button-primary">Reset Password</button>
            </div>
            <div class="uk-margin uk-text-small uk-text-center">
              Silakan hubungi kami jika memiliki masalah mengatur ulang kata sandi.
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
