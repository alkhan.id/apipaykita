@extends('auth.base')

@section('content')
  <div uk-grid>
    <div class="uk-width-1-1">
      <div class="uk-margin uk-text-center uk-flex uk-flex-center uk-flex-middle">
        <div class="uk-margin-auto uk-width-1-4@s uk-card uk-card-body">
          <form class="uk-form" method="post" action="{{ route('login') }}">@csrf
            <div class="uk-flex uk-flex-center uk-flex-middle">
              <a href="/" class="spf-link">
                <canvas width="100" uk-svg="" src="{{asset('images/logos/svg/logo-150x150.svg')}}" style="color: #1e87f0" hidden="true"></canvas>
              </a>
            </div>

            <!-- <a title="Google" class="socialaccount_provider google"
              href="{{url('google_login')}}">Masuk dengan Google+</a> -->

            <!-- <h5 class="uk-margin-small uk-heading-line uk-text-center"><span>ATAU</span></h5> -->

            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="text" placeholder="email" required>
              @if ($errors->has('email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input" name="password" type="password" placeholder="Password" required>
              @if ($errors->has('password'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="uk-margin">
            </div>
            <div class="uk-margin">
              <button type="submit" class="uk-width-1-1 uk-button uk-button-primary">Masuk</button>
            </div>
            <div class="uk-margin uk-text-small">
              <!-- <label class="uk-float-left">
                <input class="uk-checkbox" name="remember" type="checkbox">
              </label> -->
              <a class="uk-float-center uk-link uk-link-muted" href="{{url('password/reset')}}">Lupa Password?</a>
            </div>
            <!-- {% if redirect_field_value %} -->
              <input type="hidden" name="{{'redirect_field_name'}}" value="{{'redirect_field_value'}}" />
            <!-- {% endif %} -->
          </form>
          <br />
          <div class="uk-alert">
            Jika anda belum memiliki akun, <br />
            silahkan <a href="{{url('register')}}">mendaftar disini</a>.
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('extra_body ')
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection
