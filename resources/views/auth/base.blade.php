<!DOCTYPE html>{
<html>
  <head>
    <title>Plazadana | login</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('icons/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('css/theme.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />

    <script src="{{asset('js/uikit.min.js')}}"></script>
    <script src="{{asset('js/uikit-icons.min.js')}}"></script>
  </head>
  <body>
      @yield('content')

    @section('extra_body')
    @endsection
  </body>
</html>
