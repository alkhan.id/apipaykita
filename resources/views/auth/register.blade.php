@extends('auth.base')
@section('content')
  <div uk-grid>
    <div class="uk-width-1-1">
      <div class="uk-margin uk-text-center uk-flex uk-flex-center uk-flex-middle">
        <div class="uk-margin-auto uk-width-1-4@s uk-card uk-card-body">
          <form class="uk-form" method="post" action="{{ route('register') }}">@csrf
            <div class="uk-flex uk-flex-center uk-flex-middle">
              <a href="/" class="spf-link">
                <canvas width="100" uk-svg="" src="{{asset('images/logos/svg/logo-150x150.svg')}}" style="color: #1e87f0" hidden="true"></canvas>
              </a>
            </div>

      
            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input" name="name" type="text" placeholder="Username" required>
              @if ($errors->has('name'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input" name="email" type="email" placeholder="Email" required>
              @if ($errors->has('email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input" name="password" type="password" placeholder="Password Baru" required autocomplete="new-password">
              @if ($errors->has('password'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="uk-margin">
              <input class="uk-width-1-1 uk-input" name="password_confirmation" type="password" placeholder="Password Baru (lagi)" required autocomplete="new-password">
            </div>
            <div class="uk-margin">
            </div>
            <div class="uk-margin">
              <button type="submit" class="uk-width-1-1 uk-button uk-button-danger">Daftar</button>
            </div>
            <div class="uk-margin uk-text-small uk-text-center">
              <a class="uk-link uk-link-muted" href="{{url('login') }}">Saya sudah punya akun?</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('extra_body')
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection
