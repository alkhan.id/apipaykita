<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invest extends Model
{
    //
    public function teaser(){ 
        return $this->belongsTo('App\Models\Teaser'); 
  }

  public function user(){ 
    return $this->belongsTo('App\User'); 
}

}
