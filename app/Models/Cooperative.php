<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cooperative extends Model
{
    protected $fillable = [
        'user_id','name', 'address', 'proposal_business_unit', 'deed_of_incorporation', 'business_license', 'domicile_permit', 'others'
    ];

    public function user(){ 
        return $this->belongsTo('App\User'); 
  }
}
