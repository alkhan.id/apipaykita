<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teaser extends Model
{
    protected $fillable = [
        'user_id','category_id', 'cooperative_id', 'title', 'description', 'amount', 'minimal_investation', 'expiration_date', 'percentage_of_return', 'payment_schedule', 'featured_image', 'attachment', 'status', 'add_member', 'invest', 'sukuk', 'suks'
    ];

  public function user(){ 
      return $this->belongsTo('App\User'); 
}

  public function cooperative(){ 
        return $this->belongsTo('App\Models\Cooperative'); 
  }

  public function category(){ 
    return $this->belongsTo('App\Models\Category'); 
  }
}

