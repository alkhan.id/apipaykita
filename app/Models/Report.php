<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //
    public function teaser(){ 
        return $this->belongsTo('App\Models\Teaser'); 
  }
}
