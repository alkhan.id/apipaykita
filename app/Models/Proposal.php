<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $fillable = [
        'user_id','name', 'address', 'proposal_business_unit', 'deed_of_incorporation', 'business_license', 'domicile_permit', 'others'
    ];
}
