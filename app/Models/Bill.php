<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    //

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function teaser(){ 
        return $this->belongsTo('App\Models\Teaser'); 
    }

    public function invest(){ 
        return $this->belongsTo('App\Models\Invest'); 
      }
}

