<?php

namespace App\Http\Controllers\RelationshipManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invest;
use App\Models\Teaser;

class DashboardController extends Controller
{
    public function index(){
        $teasers = Teaser::where('status', 1)->get(); // status 1 adalah approved
        $invests = Invest::where('status', 1)->get(); // status 1 adalah approved
        // $Cooperations = Cooperative::paginate(5);
        // dd($payments);
        $filtered_by_month = array(
            'teasers' => array(),
            'invests' => array()
        );
        foreach ([ 'teasers' => $teasers, 'invests' => $invests] as $key => $objek) {
            foreach ($objek as $data) {
                for ($bulan=1; $bulan < 13; $bulan++) { 
                    if (!array_key_exists(strval($bulan), $filtered_by_month[$key])) {
                        $filtered_by_month[$key][strval($bulan)] = 0;
                    }
                    if (date('n', strtotime($data->created_at)) == $bulan) {
                        $filtered_by_month[$key][strval($bulan)] += 1;
                    }
                }
            }
        }

        // dd($filtered_by_month['teasers']);
        // dd(array_key_exists('1', $filtered_by_month['teasers']) ? $filtered_by_month["teasers"]["1"] : 0 );
        


        $results = [
            'teasers' => $teasers,
            'invests' => $invests,
            'filtered_by_month' => $filtered_by_month
        ];

        return view('app_management.rm_dashboard')->with($results);
    }}
