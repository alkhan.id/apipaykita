<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Models\Teaser;
use App\Models\Message;
use App\Models\Payment;
use App\Models\Category;
use Illuminate\Http\Request;

class HomepageController extends Controller
{


    public function home()
    {
        $proposals = Teaser::where('status', 'approved')->get();
        session_start();
        if(!isset($_SESSION["persetujuan"])){
            $session = $_SESSION["persetujuan"] = false;
        }else{
            $session = true;
        }
        return view('app_core.core_homepage')->with(['proposals' => $proposals, 'session' => $session]);
    }

    public function list()
    {
        $teasers = Teaser::where('status','approved')->get();
        $categories = Category::all();
        return view('app_plaza.plaza_teaser_list')->with(['teasers' => $teasers, 'categories' => $categories, 'id' => Null]);
    }

    public function search($id=Null)
    {
        if($id == 0){
            $teasers = Teaser::where('status','approved')->get();
        }else{
            $teasers = Teaser::where('status','approved')->where('category_id',$id)->get();
        }
        $categories = Category::all();
        return view('app_plaza.plaza_teaser_list')->with(['teasers' => $teasers, 'categories' => $categories, 'id'=> $id]);
    }

    public function searching(Request $request)
    {
        // dd($request);
        $cari = $request->q;
        $teasers = Teaser::where('status','approved')->where('title','like',"%".$cari."%")->get();
        $categories = Category::all();
        return view('app_plaza.plaza_teaser_list')->with(['teasers' => $teasers, 'categories' => $categories]);
    }

    public function cookie()
    {
        session(['persetujuan' => true]);
        $session = Session::get('persetujuan');
        return response()->json($session);
    }


    public function userDashboard()
    {
        $teasers = Teaser::where('status', 'approved')->get();
        $payments = Payment::where('user_id', Auth::user()->id)->get();
        $total = 0;
        foreach($payments as $payment){
            $total = $total + $payment->PAYMENT;
        }
        $myTeaser = Teaser::where('user_id', Auth::user()->id)->where('status', 'approved')->count();
        $myInvest = count($payments);
  

        return view('app_user.user_dashboard')->with(['teasers' => $teasers,
                                                      'total'=> $total,
                                                      'myTeaser'=>$myTeaser,
                                                      'myInvest'=>$myInvest]);
    }

    public function core_investor()
    {
        return view('app_core.core_investor');
    }
    public function core_fundraising()
    {
        return view('app_core.core_fundraising');
    }
    public function core_information()
    {
        return view('app_core.core_information');
    }
    public function core_our_team()
    {
        return view('app_core.core_our_team');
    }
    public function docs_investor_and_cooperative()
    {
        return view('app_core.docs.docs_investor_and_cooperative');
    }
    public function core_tos()
    {
        return view('app_core.core_tos');
    }
    public function core_risk_warning()
    {
        return view('app_core.core_risk_warning');
    }
    public function core_contact_us()
    {
        return view('app_core.core_contact_us');
    }
    public function contact_us(Request $request)
    {
        $message = new Message;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->pesan = $request->pesan;
        $message->save();

        return view('app_core.core_contact_us');
    }
    public function core_faq()
    {
        return view('app_core.core_faq');
    }
    public function core_about_us()
    {
        return view('app_core.core_about_us');
    }
    public function career()
    {
        return view('app_core.core_career');
    }


}
