<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Teaser;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('app_plaza.plaza_report_submit')->with(['id'=>$id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Report;
        if($request->attachment != null){
            $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
            $request->attachment->move(public_path('file'), $attachment);
        }else{
            $attachment ="";
        }
        $data->attachment = $attachment;
        $data->teaser_id = $request->proposal_id;
        $data->actual = $request->actual;
        $data->target = $request->target;
        $data->revenue = $request->revenue;
        $data->costs_sales_person = $request->costs_sales_person;
        $data->costs_sales_commission = $request->costs_sales_commission;
        $data->costs_promotion_and_advertising = $request->costs_promotion_and_advertising;
        $data->costs_warehouse = $request->costs_warehouse;
        $data->costs_billing_charge = $request->costs_billing_charge;
        $data->costs_phone_and_electricity = $request->costs_phone_and_electricity;
        $data->costs_transport = $request->costs_transport;
        $data->costs_employee_salary = $request->costs_employee_salary;
        $data->costs_office_equipment = $request->costs_office_equipment;
        $data->costs_raw_material = $request->costs_raw_material;
        $data->costs_it = $request->costs_it;
        $data->costs_other = $request->costs_other;
        $data->description = $request->description;
        $data->save();

        return redirect('report/'.$request->proposal_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reports = Report::where('teaser_id',$id)->get();
        return view('app_plaza.plaza_report_list')->with(['id'=>$id, 'reports'=>$reports]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::where('id', $id)->first();
        return view('app_plaza.plaza_report_edit')->with('report',$report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $data = Report::where('id',$id)->first();
        if($request->attachment != null){
            $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
            $request->attachment->move(public_path('file'), $attachment);
            $data->attachment = $attachment;
        }   
        $data->teaser_id = $request->proposal_id;
        $data->actual = $request->actual;
        $data->target = $request->target;
        $data->revenue = $request->revenue;
        $data->costs_sales_person = $request->costs_sales_person;
        $data->costs_sales_commission = $request->costs_sales_commission;
        $data->costs_promotion_and_advertising = $request->costs_promotion_and_advertising;
        $data->costs_warehouse = $request->costs_warehouse;
        $data->costs_billing_charge = $request->costs_billing_charge;
        $data->costs_phone_and_electricity = $request->costs_phone_and_electricity;
        $data->costs_transport = $request->costs_transport;
        $data->costs_employee_salary = $request->costs_employee_salary;
        $data->costs_office_equipment = $request->costs_office_equipment;
        $data->costs_raw_material = $request->costs_raw_material;
        $data->costs_it = $request->costs_it;
        $data->costs_other = $request->costs_other;
        $data->description = $request->description;
        $data->save();

        return redirect('report/'.$request->proposal_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
