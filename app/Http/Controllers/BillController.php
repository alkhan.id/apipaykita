<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bill;

class BillController extends Controller
{
    public function index(){
        $bills = Bill::paginate(10);
        return view('app_management.it_officer_bill_list')->with(['bills' => $bills]);
    }
}
