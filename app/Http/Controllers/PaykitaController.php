<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bill;
use App\Models\Invest;
use App\User;
use Auth;


class PaykitaController extends Controller
{
    public function invest($id)
    {
       $tagihan = Bill::where('id', $id)->first();
       
    //    dump($tagihan);
       $user = User::where('id', Auth::user()->id)->first();
    //    dump($user);   
       if($tagihan->BILL > $user->saldo){
        
        $tagihan->STATUS = "KURANG";
        $tagihan->PAYMENT = $user->Saldo;
        $tagihan->save();

        $bayar = $tagihan->BILL - $user->saldo;
        $tagihanBaru = new Bill;
        $tagihanBaru->user_id = $tagihan->user_id;
        $tagihanBaru->cooperative_id = $tagihan->cooperative_id;
        $tagihanBaru->teaser_id = $tagihan->teaser_id;
        $tagihanBaru->invest_id = $tagihan->invest_id;
        $tagihanBaru->VANO = $tagihan->VANO;
        $tagihanBaru->ERR = null;
        $tagihanBaru->CCY = null;
        $tagihanBaru->TRXDATE = null;
        $tagihanBaru->METHOD = null;
        $tagihanBaru->PAYMENT = $user->saldo;
        $tagihanBaru->USERNAME = null;
        $tagihanBaru->PASSWORD = null;
        $tagihanBaru->CHANNELID = null;
        $tagihanBaru->REFNO = null;
        $tagihanBaru->BILL = $bayar;
        $tagihanBaru->DESCRIPTION = "Kekurangan Pembayaran ".$tagihan->DESCRIPTION;
        $tagihanBaru->DESCRIPTION2 = null;
        $tagihanBaru->CUSTNAME = null;
        $tagihanBaru->PYMTDATE = null;
        $tagihanBaru->STATUS = null;
        $tagihanBaru->save();

        $user->saldo = 0;
        $user->save();

        // dd($tagihanBaru);
       }else{
         
        $sisaSaldo = $user->saldo - $tagihan->BILL ;
        
        $tagihan->PAYMENT = $tagihan->BILL;
        $tagihan->BILL = 0;
        $tagihan->STATUS = 'Lunas';
        $tagihan->save();

      //   $koperasi = $tagihan->teaser->user_id;
        $userkoperasi = User::find($tagihan->teaser->user_id);
        $userkoperasi->saldo = $koperasi->teaser->suks;
        $userkoperasi->save();
      //   dd($koperasi);

        $user->saldo = $sisaSaldo;
        $user->save();
        
        $invest = Invest::where('id',$tagihan->invest_id)->first();
        $invest->status = 1;
        $invest->save();

        // dump($sisaSaldo);
       }

       return redirect('investation_list');
       
    }
}
