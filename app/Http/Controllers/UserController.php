<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = User::where('id', Auth::user()->id)->first();
        // dd($user);
        return view('app_user.user_profile_edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dump($request);
        $user = User::where('id', Auth::user()->id)->first();

        if($request->photo != null){
            $photo = time().'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('file'), $photo);
            $user->photo = $photo;   
        }else{
            $photo ="";
        }


        $user->tglLahir = $request->tglLahir;
        $user->tempatLahir = $request->tempatLahir;     
        $user->fullName = $request->full_name;
        $user->motherName = $request->mother_name;
        $user->ktp = $request->ktp_number;
        $user->npwp = $request->npwp_number;
        $user->hp = $request->phone_number;
        $user->Save();
        // dd($user);

        // return redirect('profile');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
