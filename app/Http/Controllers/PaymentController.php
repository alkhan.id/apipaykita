<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function list(){
        $payments = Payment::paginate(10);
        return view('app_management.it_officer_payment_list')->with(['payments' => $payments]);
    }

    // public function listUser(){
    //     $payments = Payment::paginate(10);
    //     return view('app_management.it_officer_payment_list')->with(['payments' => $payments]);
    // }

    public function listPaymentUser(){
        $payments = Payment::where('user_id',Auth::user()->id)->paginate(10);
        return view('app_management.it_officer_payment_list')->with(['payments' => $payments]);
    }
}
