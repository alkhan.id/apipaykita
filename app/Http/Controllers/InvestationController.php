<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Investation;

class InvestationController extends Controller
{
    public function list(){
        $Investations = Investation::paginate(5);
        // dd($payments);
        return view('app_management.it_officer_investation_list')->with(['investations' => $Investations]);
    }

}
