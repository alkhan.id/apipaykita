<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = 'all';

        if ($request->status == "waiting") {
            $messages = Message::where("status",0)->paginate(10);
        }
        else if ($request->status == "replied") {
            $messages = Message::where("status",1)->paginate(10);
        }
        else if ($request->status == "rejected") {
            $messages = Message::where("status",2)->paginate(10);
        }
        else{
            $messages = Message::paginate(10);
        }
        return view('app_management.it_officer_message_list')->with(['messages' => $messages, 'status' => $status]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::where('id', $id)->first();
        return view('app_management.it_officer_message_edit')->with(['message'=>$message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = Message::find($id);
        $message->user_id = Auth::user()->id;
        $message->response = $request->response;
        $message->save();
        return redirect('message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
