<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\User;
use App\Models\Bill;
use App\Models\Invest;
use App\Models\Teaser;
use Illuminate\Http\Request;
use App\Mail\InvestationSuccessMail;
use App\Mail\AkadMail;
use App\Mail\AkadWakalahMail;

class InvestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->simpanan_pokok);
        $simpanan_pokok = (isset($request->simpanan_pokok)) ? $request->simpanan_pokok : 0;
        $simpanan_wajib = (isset($request->simpanan_wajib)) ? $request->simpanan_wajib : 0;
        $invest = (isset($request->invest)) ? $request->invest : 0;
        $SUKS = (isset($request->SUKS)) ? $request->SUKS : 0;
        $SUKUK = (isset($request->SUKUK)) ? $request->SUKUK : 0;
        $bill = $simpanan_pokok + $simpanan_wajib + $invest + $SUKS + $SUKUK;
        // dump($invest);
        // dd($bill);
        $user = User::where('id', $request->user_id)->first();
        $teaser = Teaser::where('id', $request->teaser_id)->first();
        $dataInvest = new Invest;
        $dataInvest->user_id = Auth::user()->id;
        $dataInvest->teaser_id = $request->teaser_id;
        $dataInvest->simpanan_pokok = $simpanan_pokok;
        $dataInvest->simpanan_wajib = $simpanan_wajib;
        $dataInvest->invest = $invest;
        $dataInvest->SUKS = $SUKS;
        $dataInvest->SUKUK = $SUKUK;
        $dataInvest->save();
        $dataInvest = Invest::orderBy('id','desc')->first();
        $dataBill = new Bill;
        $dataBill->user_id = $request->user_id;
        $dataBill->teaser_id = $request->teaser_id;
        $dataBill->cooperative_id = $request->cooperative_id;
        $dataBill->invest_id = $dataInvest->id;
        $dataBill->VANO = $user->va;
        $dataBill->Bill = $bill;
        $dataBill->DESCRIPTION = 'Investasi di '.$teaser->title;
        $dataBill->PAYMENT = 0;
        $dataBill->save();
        // dump($dataInvest);
        // dd($dataBill);
        $investations = Bill::all();
        // $user = User::where('user_id', $teaser->user_id)->get();
        // Mail::to($user->email)->send(new InvestationSuccessMail($user));
        Mail::to($user->email)->send(new AkadMail($teaser));
        // Mail::to($user->email)->send(new AkadWakalahMail($user));

        return redirect('investation_list');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invest = Invest::where('id', $id)->first();
        $invest->delete();

        return redirect('investation_list');
    }

    public function list()
    {

        $investations = Invest::where('user_id', Auth::user()->id)->paginate(3);
        // dd($investations);
        $bills = Bill::where('user_id', Auth::user()->id)
                    ->whereNull('STATUS')
                    ->paginate(5);
                    // dd($bills);
        return view('app_plaza.plaza_investation_list')->with(['bills' => $bills,
                                                                'investations' => $investations]);
    }
}
