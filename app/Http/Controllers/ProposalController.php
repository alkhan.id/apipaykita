<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Models\Invest;
use App\Models\Teaser;
use App\Models\Category;
use App\Models\Cooperative;
use Illuminate\Http\Request;

class ProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proposals = Teaser::where('user_id',Auth::user()->id)->get();
        return view('app_plaza.plaza_proposal_list')->with('proposals',$proposals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorys = Category::all();
        if(Auth::user()->ktp == null){
            $user = User::where('id', Auth::user()->id)->first();
            return view('app_user.user_profile_edit')->with(['user'=> $user, 'warning' => 'Silahkan melengkapi Profile terlebih dahulu']);
        }
        $cooperative = Cooperative::where('user_id', Auth::user()->id)->first();
        if($cooperative == null){
            $invests = Invest::where('user_id', Auth::user()->id)->get();
            return view('app_plaza.plaza_cooperative_submit')->with(['warning' => 'Silahkan membuat Koperasi terlebih dahulu',
                                                                    'invests'=>$invests]);;
        }
        $user = User::where('id', Auth::user()->id)->first();
        return view('app_plaza.plaza_proposal_submit')->with(['cooperative' => $cooperative, 'user' => $user, 'categorys' => $categorys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        if($request->featured_image != null){
            $featured_image = time().'.'.$request->featured_image->getClientOriginalExtension();
            $request->featured_image->move(public_path('file'), $featured_image);
        }else{
            $featured_image ="";
        }
        if($request->attachment != null){
            $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
            $request->attachment->move(public_path('file'), $attachment);
        }else{
            $attachment ="";
        }
// dd($request->title);
        $teaser = new Teaser;
        $teaser->user_id = Auth::user()->id;
        $teaser->category_id = 1;
        $teaser->cooperative_id = Cooperative::where('user_id', Auth::user()->id)->first()->id;
        $teaser->title = $request->title;
        $teaser->description = $request->description;
        $teaser->amount = $request->amount;
        $teaser->minimal_investation = 100000;
        $teaser->add_member = $request->add_member;
        $teaser->invest = $request->invest;
        $teaser->SUKUK = $request->SUKUK;
        $teaser->SUKS = $request->SUKS;
        $teaser->expiration_date = $request->expiration_date;
        $teaser->percentage_of_return = $request->percentage_of_return;
        $teaser->payment_schedule = $request->payment_schedule;
        $teaser->featured_image = $featured_image;
        $teaser->attachment = $attachment;
        $teaser->save();

        return redirect('proposal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $cooperative = Cooperative::where('user_id', Auth::user()->id)->first();
        $categorys = Category::all();
        $teaser = Teaser::where('id',$id)->first();
        return view('app_plaza.plaza_teaser_edit')->with(['teaser'=>$teaser,
                                                        'categorys'=>$categorys,
                                                        'cooperative'=>$cooperative,
                                                        'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teaser = Teaser::where('id', $id)->first();
        if($request->featured_image != null){
            $featured_image = time().'.'.$request->featured_image->getClientOriginalExtension();
            $request->featured_image->move(public_path('file'), $featured_image);
            $teaser->featured_image = $featured_image;
        }else{
            $featured_image ="";
        }
        if($request->attachment != null){
            $attachment = time().'.'.$request->attachment->getClientOriginalExtension();
            $request->attachment->move(public_path('file'), $attachment);
            $teaser->attachment = $attachment;
        }else{
            $attachment ="";
        }
        $teaser->user_id = Auth::user()->id;
        $teaser->category_id = 1;
        $teaser->cooperative_id = Cooperative::where('user_id', Auth::user()->id)->first()->id;
        $teaser->title = $request->title;
        $teaser->description = $request->description;
        $teaser->amount = $request->amount;
        $teaser->minimal_investation = 100000;
        $teaser->expiration_date = $request->expiration_date;
        $teaser->percentage_of_return = $request->percentage_of_return;
        $teaser->payment_schedule = $request->payment_schedule;
        $teaser->save();
        // dd($teaser2);
        return redirect('proposal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
