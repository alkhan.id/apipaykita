<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;
    /** 
         * login api 
         * 
         * @return \Illuminate\Http\Response 
         */ 
        public function login(){ 
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                return response()->json([
                    'status' => 'success',
                    'saldo' => $user->saldo,
                    'id' => $user->id,
                    'va' => $user->va,
                    'name' => $user->name,
                    'email' => $user->email,
                    'success' => $success
                ], $this-> successStatus); 
            } 
            else{ 
                return response()->json(['error'=>'Unauthorised', 'status' => 'error'], 401); 
            } 
        }
    /** 
         * Register api 
         * 
         * @return \Illuminate\Http\Response 
         */ 
        public function register(Request $request) 
        { 
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|email', 
                'password' => 'required', 
                'c_password' => 'required|same:password', 
            ]);
    if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(), 'status'=>'error'], 401);            
            }
            $last = User::orderBy('id','Desc')->first();
            $id = sprintf("%'.03d", $last->id);
            $month = date('m');
            $year = date('y');
            $va = '7772'.'01'.$id.$month.$year.$id;
            // $va2 = '7772'.'02'.$id.$month.$year.$id;
            $input = $request->all(); 
            $input['password'] = bcrypt($input['password']); 
            $input['va'] = $va;
            // $input['va2'] = $va2;
            $user = User::create($input); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['name'] =  $user->name;
    return response()->json([
            'name' => $user->name,
            'saldo' => $user->saldo,
            'va' => $user->va,
            'id' => $user->id,
            'email' => $user->email,
            'status' => 'success',
            'success'=>$success
            ], $this-> successStatus); 
        }
    /** 
         * details api 
         * 
         * @return \Illuminate\Http\Response 
         */ 
        public function details() 
        { 
            $user = Auth::user(); 
            return response()->json(['success' => $user], $this-> successStatus); 
        } 

}
