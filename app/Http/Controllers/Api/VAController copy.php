<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use \Firebase\JWT\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Bill;
use App\Models\Log;

class VAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(REQUEST $request)
    {   
        $key = "vECChcFcfzjDpctQCCdqrvleB1veOvSJ";
        $jwt = $request->getContent();
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $request = json_encode($decoded);
        $request = json_decode($request);
        
        if(isset($request->VANO)){
            $dataDB = Bill::where('VANO', $request->VANO)->first();
        }
        // dump($dataDB);
        // dump($data);

         if($request->METHOD == 'INQUIRY'){
            //INQUIRY
            
            // $data['TRXDATE'] = $request->TRXDATE;
            // $data['REFNO'] = $request->REFNO;
            // // $data['PAYMENT'] = $request->PAYMENT;
            // $data['CHANNELID'] = $request->CHANNELID;
            // $data['VANO'] = $request->VANO;
            // $data['USERNAME'] = $request->USERNAME;
            // $data['PASSWORD'] = $request->PASSWORD;

            $data['CCY'] = $request->CCY; 
            if($dataDB != null ){
                $data['ERR']  = "00"; 
                $data['BILL']  = $dataDB->BILL; 
                $data['DESCRIPTION']  = $dataDB->DESCRIPTION;
            }else{
                $data['BILL']  = ""; 
                $data['DESCRIPTION']  = "";
                $data['ERR']  = "15"; 
            }
            $data['METHOD']  = $request->METHOD; 
            $data['DESCRIPTION2']  = 'INVESTASI SAHAM PERTAMA '; 
            $data['CUSTNAME']  = 'BUDI UTAMA'; 
         
         }elseif($request->METHOD == 'PAYMENT'){


            // $data['TRXDATE'] = $request->TRXDATE;
            // $data['REFNO'] = $request->REFNO;
            // $data['PAYMENT'] = $request->PAYMENT;
            // $data['CHANNELID'] = $request->CHANNELID;
            // $data['VANO'] = $request->VANO;
            // $data['USERNAME'] = $request->USERNAME;
            // $data['PASSWORD'] = $request->PASSWORD;


            //PAYMENT
            $data['CCY']  = $request->CCY; 
            $data['BILL']  = $request->BILL;
            $data['CUSTNAME']  = $request->CUSTNAME; 
            if($dataDB != null ){
                $data['ERR']  = "00"; 
                $data['DESCRIPTION2']  = $dataDB->DESCRIPTION2;
                $data['DESCRIPTION']  = $dataDB->DESCRIPTION;
            }else{
                $data['ERR']  = "15"; 
                $data['DESCRIPTION2']  = "";
                $data['DESCRIPTION']  = "";
            }
            $data['METHOD']  = $request->METHOD; 
            
         }elseif($request->METHOD == 'REVERSAL'){

            // $data['CCY'] = $request->CCY;
            // $data['TRXDATE'] = $request->TRXDATE;
            // $data['REFNO'] = $request->REFNO;
            // $data['BILL'] = $request->BILL;
            // $data['CHANNELID'] = $request->CHANNELID;
            // $data['PYMTDATE'] = $request->PYMTDATE;
            // $data['PAYMENT'] = $request->PAYMENT;
            // $data['VANO'] = $request->VANO;
            // $data['USERNAME'] = $request->USERNAME;
            // $data['PASSWORD'] = $request->PASSWORD;

            //REVERSAL
            if($dataDB != null ){
                $data['ERR']  = "00"; 
            }else{
                $data['ERR']  = "15"; 
            }
            $data['METHOD'] = $request->METHOD;

        }elseif($request->METHOD == 'SIGNON'){

            
            // $data['SIGNONINFO'] = $request->SIGNONINFO;
            // $data['USERNAME'] = $request->USERNAME;
            // $data['PASSWORD'] = $request->PASSWORD;

            //SIGN ON
            $data['ERR'] = $request->SIGNONINFO;
            $data['METHOD'] = $request->METHOD;
            $data['ERR'] = substr_replace( $data['ERR'], '00;', 15, 0 );
        }elseif($request->METHOD == 'SIGNOFF'){

            // $data['SIGNOFFINFO'] = $request->SIGNOFFINFO;
            // $data['USERNAME'] = $request->USERNAME;
            // $data['PASSWORD'] = $request->PASSWORD;

            //SIGN OFF
            $data['ERR'] = $request->SIGNOFFINFO;
            $data['METHOD'] = $request->METHOD;
            $data['ERR'] = substr_replace( $data['ERR'], '00;', 15, 0 );
        }elseif($request->METHOD == 'ECHO'){

            // $data['SIGNOFFINFO'] = $request->SIGNOFFINFO;
            // $data['USERNAME'] = $request->USERNAME;
            // $data['PASSWORD'] = $request->PASSWORD;
            //ECHO
            $data['METHOD'] = $request->METHOD;
            $data['ERR'] = '00';
        }

        // $data->save();


        $dataLog = new log;
         if($request->METHOD == 'INQUIRY'){
            //INQUIRY
            
            $dataLog['TRXDATE'] = $request->TRXDATE;
            $dataLog['REFNO'] = $request->REFNO;
            // $dataLog['PAYMENT'] = $request->PAYMENT;
            $dataLog['CHANNELID'] = $request->CHANNELID;
            $dataLog['VANO'] = $request->VANO;
            $dataLog['USERNAME'] = $request->USERNAME;
            $dataLog['PASSWORD'] = $request->PASSWORD;

            $dataLog['CCY'] = $request->CCY;
            $dataLog['BILL']  = 27650000; 
            $dataLog['DESCRIPTION']  = 'INVESTASI SAHAM'; 
            $dataLog['ERR']  = '00'; 
            $dataLog['METHOD']  = $request->METHOD; 
            $dataLog['DESCRIPTION2']  = 'INVESTASI SAHAM PERTAMA '; 
            $dataLog['CUSTNAME']  = 'BUDI UTAMA'; 
         
         }elseif($request->METHOD == 'PAYMENT'){


            $dataLog['TRXDATE'] = $request->TRXDATE;
            $dataLog['REFNO'] = $request->REFNO;
            $dataLog['PAYMENT'] = $request->PAYMENT;
            $dataLog['CHANNELID'] = $request->CHANNELID;
            $dataLog['VANO'] = $request->VANO;
            $dataLog['USERNAME'] = $request->USERNAME;
            $dataLog['PASSWORD'] = $request->PASSWORD;


            //PAYMENT
            $dataLog['CCY']  = $request->CCY; 
            $dataLog['BILL']  = $request->BILL;
            $dataLog['CUSTNAME']  = $request->CUSTNAME; 
            $dataLog['DESCRIPTION2']  = "";
            $dataLog['DESCRIPTION']  = ""; 
            $dataLog['ERR']  = "00"; 
            $dataLog['METHOD']  = $request->METHOD; 
            
         }elseif($request->METHOD == 'REVERSAL'){

            $dataLog['CCY'] = $request->CCY;
            $dataLog['TRXDATE'] = $request->TRXDATE;
            $dataLog['REFNO'] = $request->REFNO;
            $dataLog['BILL'] = $request->BILL;
            $dataLog['CHANNELID'] = $request->CHANNELID;
            $dataLog['PYMTDATE'] = $request->PYMTDATE;
            $dataLog['PAYMENT'] = $request->PAYMENT;
            $dataLog['VANO'] = $request->VANO;
            $dataLog['USERNAME'] = $request->USERNAME;
            $dataLog['PASSWORD'] = $request->PASSWORD;

            //REVERSAL
            $dataLog['ERR'] = '00';
            $dataLog['METHOD'] = $request->METHOD;

        }elseif($request->METHOD == 'SIGNON'){

            
            $dataLog['SIGNONINFO'] = $request->SIGNONINFO;
            $dataLog['USERNAME'] = $request->USERNAME;
            $dataLog['PASSWORD'] = $request->PASSWORD;

            //SIGN ON
            $dataLog['ERR'] = $request->SIGNONINFO;
            $dataLog['METHOD'] = $request->METHOD;
            $dataLog['ERR'] = substr_replace( $dataLog['ERR'], '00;', 15, 0 );
        }elseif($request->METHOD == 'SIGNOFF'){

            $dataLog['SIGNOFFINFO'] = $request->SIGNOFFINFO;
            $dataLog['USERNAME'] = $request->USERNAME;
            $dataLog['PASSWORD'] = $request->PASSWORD;

            //SIGN OFF
            $dataLog['ERR'] = $request->SIGNOFFINFO;
            $dataLog['METHOD'] = $request->METHOD;
            $dataLog['ERR'] = substr_replace( $dataLog['ERR'], '00;', 15, 0 );
        }elseif($request->METHOD == 'ECHO'){
            $dataLog = $request;
            $dataLog = json_decode(json_encode($dataLog), true);
            
            // dd(gettype($dataLog));
            // $dataLog = json_encode($dataLog, true);
            // $dataLog['SIGNOFFINFO'] = $request->SIGNOFFINFO;
            // $dataLog['USERNAME'] = $request->USERNAME;
            // $dataLog['PASSWORD'] = $request->PASSWORD;
            // //ECHO
            $dataLog['METHOD'] = $request->METHOD;
            $dataLog['ERR'] = '00';
            $serializedArr = serialize($dataLog);
            // $dataLog = json_encode($dataLog, true);
        }
        if($request->METHOD != "ECHO"){
            $dataLog->save();
        }
        // $data = json_decode($data, true);
        $data = array_map('strval', $data);
        $dataEncode = JWT::encode($data, $key);
        return $dataEncode;
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $key = "vECChcFcfzjDpctQCCdqrvleB1veOvSJ";
        $jwt = $request->getContent();
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $request = json_encode($decoded);
        $request = json_decode($request);

        $data = new Bill;

        if($request->METHOD == 'INQUIRY'){
           //INQUIRY
           
           $data['TRXDATE'] = $request->TRXDATE;
           $data['REFNO'] = $request->REFNO;
           // $data['PAYMENT'] = $request->PAYMENT;
           $data['CHANNELID'] = $request->CHANNELID;
           $data['VANO'] = $request->VANO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;

           $data['CCY'] = $request->CCY;
           $data['BILL']  = 27650000; 
           $data['DESCRIPTION']  = 'INVESTASI SAHAM'; 
           $data['ERR']  = '00'; 
           $data['METHOD']  = $request->METHOD; 
           $data['DESCRIPTION2']  = 'INVESTASI SAHAM PERTAMA '; 
           $data['CUSTNAME']  = 'BUDI UTAMA'; 
        
        }elseif($request->METHOD == 'PAYMENT'){


           $data['TRXDATE'] = $request->TRXDATE;
           $data['REFNO'] = $request->REFNO;
           $data['PAYMENT'] = $request->PAYMENT;
           $data['CHANNELID'] = $request->CHANNELID;
           $data['VANO'] = $request->VANO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;


           //PAYMENT
           $data['CCY']  = $request->CCY; 
           $data['BILL']  = $request->BILL;
           $data['CUSTNAME']  = $request->CUSTNAME; 
           $data['DESCRIPTION2']  = "";
           $data['DESCRIPTION']  = ""; 
           $data['ERR']  = "00"; 
           $data['METHOD']  = $request->METHOD; 
           
        }elseif($request->METHOD == 'REVERSAL'){

           $data['CCY'] = $request->CCY;
           $data['TRXDATE'] = $request->TRXDATE;
           $data['REFNO'] = $request->REFNO;
           $data['BILL'] = $request->BILL;
           $data['CHANNELID'] = $request->CHANNELID;
           $data['PYMTDATE'] = $request->PYMTDATE;
           $data['PAYMENT'] = $request->PAYMENT;
           $data['VANO'] = $request->VANO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;

           //REVERSAL
           $data['ERR'] = '00';
           $data['METHOD'] = $request->METHOD;

       }elseif($request->METHOD == 'SIGNON'){

           
           $data['SIGNONINFO'] = $request->SIGNONINFO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;

           //SIGN ON
           $data['ERR'] = $request->SIGNONINFO;
           $data['METHOD'] = $request->METHOD;
           $data['ERR'] = substr_replace( $data['ERR'], '00;', 15, 0 );
       }elseif($request->METHOD == 'SIGNOFF'){

           $data['SIGNOFFINFO'] = $request->SIGNOFFINFO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;

           //SIGN OFF
           $data['ERR'] = $request->SIGNOFFINFO;
           $data['METHOD'] = $request->METHOD;
           $data['ERR'] = substr_replace( $data['ERR'], '00;', 15, 0 );
       }elseif($request->METHOD == 'ECHO'){

           $data['SIGNOFFINFO'] = $request->SIGNOFFINFO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;
           //ECHO
           $data['METHOD'] = $request->METHOD;
           $data['ERR'] = '00';
       }

       $data->save();

       $dataLog = new log;
       if($request->METHOD == 'INQUIRY'){
          //INQUIRY
          
          $dataLog['TRXDATE'] = $request->TRXDATE;
          $dataLog['REFNO'] = $request->REFNO;
          // $dataLog['PAYMENT'] = $request->PAYMENT;
          $dataLog['CHANNELID'] = $request->CHANNELID;
          $dataLog['VANO'] = $request->VANO;
          $dataLog['USERNAME'] = $request->USERNAME;
          $dataLog['PASSWORD'] = $request->PASSWORD;

          $dataLog['CCY'] = $request->CCY;
          $dataLog['BILL']  = 27650000; 
          $dataLog['DESCRIPTION']  = 'INVESTASI SAHAM'; 
          $dataLog['ERR']  = '00'; 
          $dataLog['METHOD']  = $request->METHOD; 
          $dataLog['DESCRIPTION2']  = 'INVESTASI SAHAM PERTAMA '; 
          $dataLog['CUSTNAME']  = 'BUDI UTAMA'; 
       
       }elseif($request->METHOD == 'PAYMENT'){


          $dataLog['TRXDATE'] = $request->TRXDATE;
          $dataLog['REFNO'] = $request->REFNO;
          $dataLog['PAYMENT'] = $request->PAYMENT;
          $dataLog['CHANNELID'] = $request->CHANNELID;
          $dataLog['VANO'] = $request->VANO;
          $dataLog['USERNAME'] = $request->USERNAME;
          $dataLog['PASSWORD'] = $request->PASSWORD;


          //PAYMENT
          $dataLog['CCY']  = $request->CCY; 
          $dataLog['BILL']  = $request->BILL;
          $dataLog['CUSTNAME']  = $request->CUSTNAME; 
          $dataLog['DESCRIPTION2']  = "";
          $dataLog['DESCRIPTION']  = ""; 
          $dataLog['ERR']  = "00"; 
          $dataLog['METHOD']  = $request->METHOD; 
          
       }elseif($request->METHOD == 'REVERSAL'){

          $dataLog['CCY'] = $request->CCY;
          $dataLog['TRXDATE'] = $request->TRXDATE;
          $dataLog['REFNO'] = $request->REFNO;
          $dataLog['BILL'] = $request->BILL;
          $dataLog['CHANNELID'] = $request->CHANNELID;
          $dataLog['PYMTDATE'] = $request->PYMTDATE;
          $dataLog['PAYMENT'] = $request->PAYMENT;
          $dataLog['VANO'] = $request->VANO;
          $dataLog['USERNAME'] = $request->USERNAME;
          $dataLog['PASSWORD'] = $request->PASSWORD;

          //REVERSAL
          $dataLog['ERR'] = '00';
          $dataLog['METHOD'] = $request->METHOD;

      }elseif($request->METHOD == 'SIGNON'){

          
          $dataLog['SIGNONINFO'] = $request->SIGNONINFO;
          $dataLog['USERNAME'] = $request->USERNAME;
          $dataLog['PASSWORD'] = $request->PASSWORD;

          //SIGN ON
          $dataLog['ERR'] = $request->SIGNONINFO;
          $dataLog['METHOD'] = $request->METHOD;
          $dataLog['ERR'] = substr_replace( $dataLog['ERR'], '00;', 15, 0 );
      }elseif($request->METHOD == 'SIGNOFF'){

          $dataLog['SIGNOFFINFO'] = $request->SIGNOFFINFO;
          $dataLog['USERNAME'] = $request->USERNAME;
          $dataLog['PASSWORD'] = $request->PASSWORD;

          //SIGN OFF
          $dataLog['ERR'] = $request->SIGNOFFINFO;
          $dataLog['METHOD'] = $request->METHOD;
          $dataLog['ERR'] = substr_replace( $dataLog['ERR'], '00;', 15, 0 );
      }elseif($request->METHOD == 'ECHO'){

          $dataLog['SIGNOFFINFO'] = $request->SIGNOFFINFO;
          $dataLog['USERNAME'] = $request->USERNAME;
          $dataLog['PASSWORD'] = $request->PASSWORD;
          //ECHO
          $dataLog['METHOD'] = $request->METHOD;
          $dataLog['ERR'] = '00';
      }

      $dataLog->save();

      dump($data);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
