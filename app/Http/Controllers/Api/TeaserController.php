<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Teaser;
use App\Http\Controllers\Controller;

class TeaserController extends Controller
{
    public $successStatus = 200;

    public function teaser(){ 
        $teasers = Teaser::get();
        if( $teasers != null ){ 
            return response()->json([
                'status' => 'ok',
                'query' => [
                    "format" => "json",
                    "surat" => "semua"
                ],
                'hasil' => $teasers
            ], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'failed', 'status' => 'error'], 401); 
        } 
    }

}
