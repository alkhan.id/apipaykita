<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Bill;
use App\Models\Teaser;
use App\Http\Controllers\Controller;

class MutasiController extends Controller
{
    public $successStatus = 200;

    public function mutasi($id){ 
        $payments = Payment::where('user_id',$id)->get();
        if( $payments != null ){ 
            return response()->json([
                'status' => 'ok',
                'hasil' => $payments
            ], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'failed', 'status' => 'error'], 401); 
        } 
    }

    public function invoice($id){ 
        $invoices = Bill::where('user_id',$id)->get();
        if( $invoices != null ){ 
            return response()->json([
                'status' => 'ok',
                'query' => [
                    "format" => "json",
                    "surat" => "semua"
                ],
                'hasil' => $invoices
            ], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'failed', 'status' => 'error'], 401); 
        } 
    }
}

