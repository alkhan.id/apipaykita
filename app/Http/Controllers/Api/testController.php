<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use \Firebase\JWT\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Bill;
use App\Models\Payment;


class testController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(REQUEST $request)
    {
        $key = "vECChcFcfzjDpctQCCdqrvleB1veOvSJ";
        $jwt = $request->getContent();
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $request = json_encode($decoded);
        $request = json_decode($request);
        
        if(isset($request->VANO)){
            $user = User::where('va', $request->VANO)->first();
            $bill = Bill::where('VANO', $request->VANO)->where('PAYMENT', '0')->orderBy('id', 'DESC')->first();
        }

            if($request->METHOD == 'INQUIRY'){
            $data['CCY'] = $request->CCY; 
            // dump($user);
            // dump($bill);
            // dd('stop');
            if($bill != null && $user != null ){
                $data['ERR']  = "00"; 
                $data['BILL']  = $bill->BILL; 
                $data['DESCRIPTION']  = $bill->DESCRIPTION;
                $data['DESCRIPTION2']  = $bill->DESCRIPTION; 
                $data['CUSTNAME']  = $user->name;
            }
            elseif($user != null ){
                $data['ERR']  = "00"; 
                $data['BILL']  = "0"; 
                $data['DESCRIPTION']  = "TOPUP";
                $data['DESCRIPTION2']  = "TOPUP / Isi Saldo"; 
                $data['CUSTNAME']  = $user->name; 
            }else{
                $data['BILL']  = ""; 
                $data['DESCRIPTION']  = "";
                $data['ERR']  = "15"; 
                $data['DESCRIPTION2']  = "";
            }
            $data['METHOD']  = $request->METHOD; 
         }elseif($request->METHOD == 'PAYMENT'){
            $request->PAYMENT = $request->PAYMENT/100;
            //PAYMENT
            // dump($request);
            // dd('me');
            $data['CCY']  = $request->CCY; 
            $data['BILL']  = $request->BILL;
            $data['CUSTNAME']  = $user->name; 
            if($user != null ){
                $data['ERR']  = "00"; 
                if($bill != null){
                    $data['DESCRIPTION2']  = $bill->DESCRIPTION2;
                    $data['DESCRIPTION']  = $bill->DESCRIPTION;    
                    //bayar tagihan
                    $payment = new Payment;
                    $payment['user_id']  = $user->id; 
                    $payment['bill_id']  = $bill->id; 
                    $payment['ERR']  = "00"; 
                    $payment['CCY']  = $request->CCY; 
                    $payment['BILL']  = $bill->BILL;
                    $payment['CUSTNAME']  = $user->name;     
                    $payment['DESCRIPTION2']  = $bill->DESCRIPTION2;
                    $payment['DESCRIPTION']  = $bill->DESCRIPTION;
                    $payment['TRXDATE'] = $request->TRXDATE;
                    $payment['REFNO'] = $request->REFNO;
                    $payment['PAYMENT'] = $request->PAYMENT;
                    $payment['CHANNELID'] = $request->CHANNELID;
                    $payment['VANO'] = $user->va;
                    $payment['USERNAME'] = $request->USERNAME;
                    $payment['PASSWORD'] = $request->PASSWORD;
                    $payment['STATUS'] = 'PAYMENT';
                    $payment->save();
                    $updateBill = Bill::find($bill->id);
                    $updateBill->PAYMENT = $request->PAYMENT;
                    $updateBill['STATUS'] = 'PAYMENT';
                    $updateBill->save();
                }else{
                    //TOPUP
                    $data['DESCRIPTION2']  = "TOPUP / Isi Saldo Paykita";
                    $data['DESCRIPTION']  = "TOPUP";
                    $updateUser = User::where('id',$user->id)->first();
                    $updateUser->saldo = $updateUser->saldo+$request->PAYMENT;
                    $updateUser->save();
                    $payment = new Payment;
                    $payment['user_id']  = $user->id; 
                    $payment['bill_id']  = 1; 
                    $payment['ERR']  = "00"; 
                    $payment['CCY']  = $request->CCY; 
                    $payment['BILL']  = 0;
                    $payment['CUSTNAME']  = $user->name;     
                    $payment['DESCRIPTION2']  = "TOPUP / Isi Saldo Paykita";
                    $payment['DESCRIPTION']  = "TOPUP";
                    $payment['TRXDATE'] = $request->TRXDATE;
                    $payment['REFNO'] = $request->REFNO;
                    $payment['PAYMENT'] = $request->PAYMENT;
                    $payment['CHANNELID'] = $request->CHANNELID;
                    $payment['VANO'] = $user->va;
                    $payment['USERNAME'] = $request->USERNAME;
                    $payment['PASSWORD'] = $request->PASSWORD;
                    $payment['STATUS'] = 'PAYMENT';
                    $payment->save();
                }
            }else{
                $data['ERR']  = "15"; 
                $data['DESCRIPTION2']  = "";
                $data['DESCRIPTION']  = "";
            }
            $data['METHOD']  = $request->METHOD; 
         }elseif($request->METHOD == 'REVERSAL'){
            $billReversal = Bill::where('VANO', $request->VANO)->where('PAYMENT', '!=', 0)->where('TRXDATE', $request->TRXDATE)->orderBy('id', 'DESC')->first();
            $paymentReversal = Payment::where('VANO', $request->VANO)->where('TRXDATE', $request->TRXDATE)->where('STATUS', 'PAYMENT')->orderBy('id', 'DESC')->first();
            if($user != null && $billReversal != null && $paymentReversal != null ){
                $data['ERR']  = "00";
                $billReversalUpdate = Bill::find($billReversal->id);
                $billReversalUpdate->PAYMENT = 0;
                $billReversalUpdate->STATUS = 'REVERSAL';
                $billReversalUpdate->save();
                $paymentReversalupdate = Payment::find($paymentReversal->id);
                $paymentReversalupdate->STATUS = 'REVERSAL';
                $paymentReversalupdate->save();
            }else{
                $data['ERR']  = "15"; 
            }
            $data['METHOD'] = $request->METHOD;
        }elseif($request->METHOD == 'SIGNON'){
            //SIGN ON
            $data['ERR'] = $request->SIGNONINFO;
            $data['METHOD'] = $request->METHOD;
            $data['ERR'] = substr_replace( $data['ERR'], '00;', 15, 0 );
        }elseif($request->METHOD == 'SIGNOFF'){
            //SIGN OFF
            $data['ERR'] = $request->SIGNOFFINFO;
            $data['METHOD'] = $request->METHOD;
            $data['ERR'] = substr_replace( $data['ERR'], '00;', 15, 0 );
        }elseif($request->METHOD == 'ECHO'){
            //ECHO
            $data['METHOD'] = $request->METHOD;
            $data['ERR'] = '00'; 
        }
        $data = array_map('strval', $data);
        $dataEncode = JWT::encode($data, $key);
        return $dataEncode;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $data = new Bill;
           $data['user_id'] = 2;
           $data['TRXDATE'] = $request->TRXDATE;
           $data['REFNO'] = $request->REFNO;
           $data['CHANNELID'] = $request->CHANNELID;
           $data['VANO'] = $request->VANO;
           $data['USERNAME'] = $request->USERNAME;
           $data['PASSWORD'] = $request->PASSWORD;
           //PAYMENT
           $data['CCY']  = $request->CCY; 
           $data['BILL']  = $request->BILL;
           $data['CUSTNAME']  = $request->CUSTNAME; 
           $data['DESCRIPTION2']  = $request->DESCRIPTION2;
           $data['DESCRIPTION']  = $request->DESCRIPTION; 
       $data->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mutasis()
    {
        $mutasis = Payment::get();
        return $mutasis;
    }

    public function mutasi($id)
    {
        $mutasis = Payment::where('id',$id)->get();
        return $mutasis;
    }
}
