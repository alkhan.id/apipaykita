<?php

namespace App\Http\Controllers;

use App\Models\Teaser;
use App\Models\Category;
use App\Models\Cooperative;
use Illuminate\Http\Request;
use App\User;


class ProposalAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = 'all';

        if ($request->status == "pending") {
            $teasers = Teaser::where("status",0)->paginate(10);
        }
        else if ($request->status == "approved") {
            $teasers = Teaser::where("status",1)->paginate(10);
        }
        else if ($request->status == "rejected") {
            $teasers = Teaser::where("status",2)->paginate(10);
        }
        else if ($request->status == "finished") {
            $teasers = Teaser::where("status",3)->paginate(10);
        }
        else{
            $teasers = Teaser::paginate(10);
        }
        return view('app_management.it_officer_proposal_list')->with(['teasers' => $teasers, 'status' => $status]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ping');
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teaser = Teaser::where('id', $id)->first();
        $categorys = Category::all();
        $cooperative = Cooperative::where('id', $teaser->cooperative_id)->first();
        $user = User::where('id', $teaser->user_id)->first();
        return view('app_management.it_officer_proposal_edit')->with(['teaser'=>$teaser, 
            'categorys'=>$categorys, 
            'cooperative'=>$cooperative,
            'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $teaser = Teaser::where('id', $id)->first();
        $teaser->status = $request->status;
        $teaser->expiration_date = $request->expiration_date;
        $teaser->save();
        return redirect('dashboard_proposal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function status($status)
    {
        $teasers = Teaser::where('status',$status)->get();
        return view('app_management.it_officer_proposal_list')->with(['teasers' => $teasers, 'status' => $status]);
    }
}
