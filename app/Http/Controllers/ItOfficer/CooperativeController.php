<?php

namespace App\Http\Controllers\ItOfficer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cooperative;

class CooperativeController extends Controller
{
    public function list(){
        $Cooperatives = Cooperative::paginate(10);
        // dd($Cooperatives);
        return view('app_management.it_officer_cooperative_list')->with(['cooperatives' => $Cooperatives]);
    }
}
