<?php

namespace App\Http\Controllers\ItOfficer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class UserController extends Controller
{
    public function list(){
        $users = User::paginate(10);
        // dd($payments);
        return view('app_management.it_officer_user_list')->with(['users' => $users]);
    }
}
