<?php

namespace App\Http\Controllers\ItOfficer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invest;

class InvestationController extends Controller
{
    public function list(Request $request){
        if ($request->status == "pending") {
            $Investations = Invest::where("status",0)->paginate(10);
        }
        else if ($request->status == "approved") {
            $Investations = Invest::where("status",1)->paginate(10);
        }
        else if ($request->status == "rejected") {
            $Investations = Invest::where("status",2)->paginate(10);
        }
        else {
            $Investations = Invest::paginate(10);
        }
        // dd($payments);
        return view('app_management.it_officer_investation_list')->with(['investations' => $Investations]);
    }

    public function update(Request $request, $id)
    {
        $data = Invest::find($id);
        $data->status = $request->investation_status;
        $data->save();

        return redirect('dashboard_investation_list');
    }

    public function checkdate(){
        $Investations = Investation::get();
        // dd($payments);
        return view('app_management.it_officer_investation_list')->with(['investations' => $Investations]);
    }

}
