<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Models\Invest;
use App\Models\Teaser;
use App\Models\Category;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detail($id)
    {
        if(Auth::user()->ktp == null){
            $user = User::where('id', Auth::user()->id)->first();
            return view('app_user.user_profile_edit')->with(['user'=> $user, 'warning' => 'Silahkan melengkapi Profile terlebih dahulu']);
        }
        $user = User::where('id', Auth::user()->id)->first();
        $invest = Invest::where('user_id', Auth::user()->id)->where('teaser_id', $id)->first();
        // dd($invest);
        $teaser = Teaser::where('id',$id)->first();
        return view('app_plaza.plaza_teaser_detail')->with(['teaser'=>$teaser, 'user'=>$user, 'invest'=>$invest]);
    }

}
