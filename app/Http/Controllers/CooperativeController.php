<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Invest;
use App\Models\Cooperative;
use Illuminate\Http\Request;

class CooperativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invests = Invest::where('user_id', Auth::user()->id)->get();
        $cooperative = Cooperative::where('user_id',Auth::user()->id)->first();
        if($cooperative != null){
            return view('app_plaza.plaza_cooperative_edit')->with(['cooperative' => $cooperative,
                                                                    'invests'=>$invests]);
        }else{
            return view('app_plaza.plaza_cooperative_submit')->with(['invests'=>$invests]);
        }
        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request);
        if($request->proposal_business_unit != null){
            $proposal_business_unit = time().'.'.$request->proposal_business_unit->getClientOriginalExtension();
            $request->proposal_business_unit->move(public_path('file'), $proposal_business_unit);
            }else{
              $proposal_business_unit ="";
        }

        if($request->deed_of_incorporation != null){
            $deed_of_incorporation = time().'.'.$request->deed_of_incorporation->getClientOriginalExtension();
            $request->deed_of_incorporation->move(public_path('file'), $deed_of_incorporation);
            }else{
              $deed_of_incorporation ="";
        }

        if($request->business_license != null){
            $business_license = time().'.'.$request->business_license->getClientOriginalExtension();
            $request->business_license->move(public_path('file'), $business_license);
            }else{
              $business_license ="";
        }

        if($request->domicile_permit != null){
            $domicile_permit = time().'.'.$request->domicile_permit->getClientOriginalExtension();
            $request->domicile_permit->move(public_path('file'), $domicile_permit);
            }else{
              $domicile_permit ="";
        }

        $cooperative = new Cooperative;
        $cooperative->user_id = Auth::user()->id;
        $cooperative->name = $request->name;
        $cooperative->address = $request->address;
        $cooperative->proposal_business_unit = $proposal_business_unit;
        $cooperative->deed_of_incorporation = $deed_of_incorporation;
        $cooperative->business_license = $business_license;
        $cooperative->domicile_permit = $domicile_permit;
        $cooperative->others = $request->others;
        $cooperative->save();

        return redirect('cooperative');
        // return view('app_plaza.plaza_cooperative_edit')->with('cooperative',$cooperative);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cooperative = Cooperative::where('id',$id)->first();
        if($request->proposal_business_unit != null){
            $proposal_business_unit = time().'.'.$request->proposal_business_unit->getClientOriginalExtension();
            $request->proposal_business_unit->move(public_path('file'), $proposal_business_unit);
            $cooperative->business_license = $business_license;
        }else{
              $proposal_business_unit ="";
        }

        if($request->deed_of_incorporation != null){
            $deed_of_incorporation = time().'.'.$request->deed_of_incorporation->getClientOriginalExtension();
            $request->deed_of_incorporation->move(public_path('file'), $deed_of_incorporation);
            $cooperative->deed_of_incorporation = $deed_of_incorporation;    
        }else{
              $deed_of_incorporation ="";
        }

        if($request->business_license != null){
            $business_license = time().'.'.$request->business_license->getClientOriginalExtension();
            $request->business_license->move(public_path('file'), $business_license);
            $cooperative->business_license = $business_license;
        }else{
              $business_license ="";
        }

        if($request->domicile_permit != null){
            $domicile_permit = time().'.'.$request->domicile_permit->getClientOriginalExtension();
            $request->domicile_permit->move(public_path('file'), $domicile_permit);
            $cooperative->domicile_permit = $domicile_permit;
        }else{
              $domicile_permit ="";
        }

        
        $cooperative->user_id = Auth::user()->id;
        $cooperative->name = $request->name;
        $cooperative->address = $request->address;
        $cooperative->others = $request->others;
        $cooperative->save();
        
        // $cooperative = Cooperative::where('id', $id)->first();
        // return view('app_plaza.plaza_cooperative_edit')->with('cooperative',$cooperative);
        return redirect('cooperative');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
