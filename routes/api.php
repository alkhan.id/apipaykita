<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
});


// Route::post('register', 'APIRegisterController@register');
// Route::post('login', 'APILoginController@login');
Route::post('va', 'Api\testController@index');
Route::get('mutasi/{id}', 'Api\MutasiController@mutasi');
Route::get('invoice/{id}', 'Api\MutasiController@invoice');
Route::get('teaser', 'Api\TeaserController@teaser');
Route::get('mutasis', 'Api\testController@mutasis');
// Route::post('/', 'Api\VAController@index');
Route::post('store', 'Api\testController@store');

Route::middleware('jwt.auth')->get('users', function(Request $request) {
    return auth()->user();
});

