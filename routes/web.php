<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomepageController@home');
Route::get('/home', 'HomepageController@home');
Route::get('/teaser_detail/{id}', 'DetailController@detail');
Route::get('/teaser_list', 'HomepageController@list');
Route::post('/teaser_list/searching', 'HomepageController@searching');
Route::get('/teaser_list/{id}', 'HomepageController@search');
Route::get('/user_dashboard', 'HomepageController@userDashboard');
Route::get('/investation_list', 'InvestController@list');
Route::delete('/investation_delete/{id}', 'InvestController@destroy');
Route::get('/set_cookies_page', 'HomepageController@cookie');
Route::get('/investor_page', 'HomepageController@core_investor');
Route::get('/fundraising_page', 'HomepageController@core_fundraising');
Route::get('/information_page', 'HomepageController@core_information');
Route::get('/our_team_page', 'HomepageController@core_our_team');
Route::get('/help_page', 'HomepageController@docs_investor_and_cooperative');
Route::get('/tos_page', 'HomepageController@core_tos');
Route::get('/risk_warning_page', 'HomepageController@core_risk_warning');
Route::get('/contact_us_page', 'HomepageController@core_contact_us');
Route::post('/contact_us_page', 'HomepageController@contact_us');
Route::get('/faq_page', 'HomepageController@core_faq');
Route::get('/about_us_page', 'HomepageController@core_about_us');
Route::get('/career', 'HomepageController@career');


//harus login
Route::group(['middleware' => ['auth']], function () {
    Route::get('dashboard_it_officer', 'ItOfficer\DashboardController@index');
    Route::get('dashboard_investation_list', 'ItOfficer\InvestationController@list');
    Route::patch('investation/{id}', 'ItOfficer\InvestationController@update');
    Route::get('dashboard_cooperative_list', 'ItOfficer\CooperativeController@list');
    Route::get('dashboard_user_list', 'ItOfficer\UserController@list');

    Route::get('dashboard_relationship_manager', 'RelationshipManager\DashboardController@index');
    Route::get('dashboard_report_list', 'RelationshipManager\ReportController@list');
    Route::get('dashboard_report_list/edit/edit', 'RelationshipManager\ReportController@list');



    Route::resource('proposal', 'ProposalController');
    Route::resource('report', 'ReportController');
    Route::resource('category', 'CategoryController');
    Route::get('dashboard_payment_list', 'PaymentController@list');
    Route::get('dashboard_bill_list', 'BillController@index');
    Route::get('history', 'PaymentController@listPaymentUser');
    Route::resource('message', 'MessageController');
    Route::get('report/{id}/create', 'ReportController@create');
    Route::resource('cooperative', 'CooperativeController');
    Route::resource('invest', 'InvestController');
    Route::resource('dashboard_proposal', 'ProposalAdminController');
    Route::get('Proposal/{Status}', 'ProposalAdminController@status');
    // Route::get('report/{id}', 'ProposalController@report');
    Route::get('profile', 'UserController@edit');
    Route::get('paykita/{id}', 'PaykitaController@invest');
    
    Route::post('profile/update', 'UserController@update');
    Route::post('profile', 'UserController@post');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
