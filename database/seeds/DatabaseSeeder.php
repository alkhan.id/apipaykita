<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@plazadana.com',
            'va' => '7772010010819001',
            'password' => bcrypt('password'),
        ]);

        DB::table('categories')->insert([
            'title' => 'Otomotif',
            'description' => 'Otomotif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
