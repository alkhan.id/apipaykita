<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('teaser_id')->unsigned();
            $table->string('actual')->nullable();
            $table->string('target')->nullable();
            $table->string('revenue')->nullable();
            $table->string('costs_sales_person')->nullable();
            $table->string('costs_sales_commission')->nullable();
            $table->string('costs_promotion_and_advertising')->nullable();
            $table->string('costs_warehouse')->nullable();
            $table->string('costs_billing_charge')->nullable();
            $table->string('costs_phone_and_electricity')->nullable();
            $table->string('costs_transport')->nullable();
            $table->string('costs_employee_salary')->nullable();
            $table->string('costs_office_equipment')->nullable();
            $table->string('costs_raw_material')->nullable();
            $table->string('costs_it')->nullable();
            $table->string('costs_other')->nullable();
            $table->string('description')->nullable();
            $table->string('attachment')->nullable();
            $table->string('status')->default('waiting');
            $table->timestamps();
            $table->foreign('teaser_id')->references('id')->on('teasers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
