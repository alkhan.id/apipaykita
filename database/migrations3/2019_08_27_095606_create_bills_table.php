<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('cooperative_id')->unsigned();
            $table->integer('teaser_id')->unsigned();
            $table->integer('invest_id')->unsigned();
            $table->string('VANO')->nullable();
            $table->string('ERR')->nullable();
            $table->string('CCY')->nullable();
            $table->string('TRXDATE')->nullable();
            $table->string('METHOD')->nullable();
            $table->string('PAYMENT')->nullable();
            $table->string('USERNAME')->nullable();
            $table->string('PASSWORD')->nullable();
            $table->string('CHANNELID')->nullable();
            $table->string('REFNO')->nullable();
            $table->string('BILL')->nullable();
            $table->string('DESCRIPTION')->nullable();
            $table->string('DESCRIPTION2')->nullable();
            $table->string('CUSTNAME')->nullable();
            $table->string('PYMTDATE')->nullable();
            $table->string('STATUS')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('cooperative_id')->references('id')->on('cooperatives');
            $table->foreign('teaser_id')->references('id')->on('teasers');
            $table->foreign('invest_id')->references('id')->on('invests')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
