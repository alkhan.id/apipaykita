<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('VANO')->nullable();
            $table->string('ERR')->nullable();
            $table->string('CCY')->nullable();
            $table->string('TRXDATE')->nullable();
            $table->string('METHOD')->nullable();
            $table->string('PAYMENT')->nullable();
            $table->string('USERNAME')->nullable();
            $table->string('PASSWORD')->nullable();
            $table->string('CHANNELID')->nullable();
            $table->string('REFNO')->nullable();
            $table->string('BILL')->nullable();
            $table->string('DESCRIPTION')->nullable();
            $table->string('DESCRIPTION2')->nullable();
            $table->string('CUSTNAME')->nullable();
            $table->string('PYMTDATE')->nullable();
            $table->string('SIGNONINFO')->nullable();
            $table->string('SIGNOFFINFO')->nullable();
            $table->string('ECHODATE')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
