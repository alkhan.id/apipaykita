<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeasersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teasers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('cooperative_id')->unsigned();
            $table->integer('add_member')->default(1);
            $table->integer('invest')->default(1);
            $table->integer('SUKUK')->default(1);
            $table->integer('suks')->default(1);
            $table->string('title');
            $table->string('description');
            $table->string('amount');
            $table->string('minimal_investation');
            $table->datetime('expiration_date')->nullable();
            $table->integer('percentage_of_return');
            $table->string('payment_schedule');
            $table->string('featured_image');
            $table->string('attachment');
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('cooperative_id')->references('id')->on('cooperatives');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teasers');
    }
}
